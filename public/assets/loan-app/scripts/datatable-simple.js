var TableDatatablesSimple = function () {

    var initTable = function () {
        var table = $('#data_table_simple');

        var oTable = table.dataTable();

    }

    return {

        //main function to initiate the module
        init: function () {

            if (!jQuery().dataTable) {
                return;
            }

            initTable();
        }

    };

}();

jQuery(document).ready(function () {
    TableDatatablesSimple.init();
});