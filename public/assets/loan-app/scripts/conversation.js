function addReply(action, csrf, user_id, message) {
    $.ajax({
        url: action,
        type: 'POST',
        data: {_token: csrf, user_id: user_id, message: message},
        dataType: 'json',
        async: false,
        success: function (data) {
            if (data.level == "success") {
                onReplySuccess(data.object.id);
            } else {
                onReplyFail(data.level, data.message);
            }
        },
        error: function () {
            onReplyFail('error', 'Failed');
        },
    });
}

function renderReply(reply_id) {
    $.get(app_url + '/replies/' + reply_id + '/render', {dataType: 'json'}, function (data) {
        onRenderReplySuccess(data);
    });
}

function canFollowUp(bid_id) {
    $.ajax({
        url: app_url + '/bids/' + bid_id + '/can_follow_up',
        type: 'GET',
        data: {},
        dataType: 'json',
        success: function (data) {
            if (data.level == "success") {
                yesCanFollowUp();
            } else {
                canNotFollowUp();
            }
        },
        error: function () {
            canNotFollowUp();
        },
    });
}

function handleFollowUpMessage(bid_id) {
    $.get(app_url + '/bids/' + bid_id + '/render_followup_message', {dataType: 'json'}, function (data) {
        renderFollowUpMessage(data)
    });
}

var getLastPostPos = function () {
    var height = 0;
    $('#chats').find("li.out, li.in").each(function () {
        height = height + $(this).outerHeight();
    });

    return height;
}
