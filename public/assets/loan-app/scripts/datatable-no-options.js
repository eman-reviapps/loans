var TableDatatablesSimple = function () {

    var initTable = function () {
        var table = $('#data_table_no_actions');

        var oTable = table.dataTable({
            "ordering": false,
            "paging": false,
            "bInfo": false,
        });

    }

    return {

        //main function to initiate the module
        init: function () {

            if (!jQuery().dataTable) {
                return;
            }

            initTable();
        }

    };

}();

jQuery(document).ready(function () {
    TableDatatablesSimple.init();
});