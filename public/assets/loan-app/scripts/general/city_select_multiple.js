$(document).ready(function () {
    initializeCitesDD();
});


function initializeCitesDD() {
    $(".cities").select2({
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 2,
        width: null,
        tokenSeparators: [','],
        placeholder: 'Select',
        allowClear: true,
        ajax: {
            url: app_url + '/dropdowns/cities',
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (term) {
                return {
                    term: term,
                    state_code: $(".states").val()
                };
            },
            processResults: function (data) {
                var results = [];
                $.each(data, function (index, item) {
                    results.push({
                        id: item.city,
                        text: item.city + ", " + item.state_code
                    });
                });
                return {
                    results: results,
                };
            },
        }
    });
}