var Address = function () {

    var getDefaultSelectOption = function () {
        return '<option selected value="" >Select</option>';
    }

    var initAddress = function () {
        $(".zip_mask").inputmask({
            "mask": "9",
            "repeat": 5,
            "greedy": false
        });

        $(document).on('change', '#state', function () {

            var state_code = $(this).val();
            var form = $(this).parents('form:first');

            $("#" + form.attr("id") + " select[name=city]").val(getDefaultSelectOption());

            $.getJSON(app_url + '/dropdowns/cities', {state_code: state_code}, function (data) {

                $("#" + form.attr("id") + " select[name=city]").html(
                    getDefaultSelectOption() + " " +
                    $.map(data, function (item, index) {
                        return '<option data-content="' + item.city + '" value="' + item.city + '">' + item.city + '</option>'
                    }));
            });

        });

        $(document).on('change', '#city', function () {

            var city_name = $(this).val();
            var form = $(this).parents('form:first');
            var state_code = $("#" + form.attr("id") + " select[name=state]").val();
            $("#" + form.attr("id") + " select[name=zip_code]").val(getDefaultSelectOption());
            $.getJSON(app_url + '/dropdowns/zip_codes_for_city', {state_code:state_code, city_name: city_name}, function (data) {

                $("#" + form.attr("id") + " select[name=zip_code]").html(

                    $.map(data, function (item, index) {
                        return '<option data-content="' + item.zip_code + '" value="' + item.zip_code + '">' + item.zip_code + '</option>'
                    }));
            });

        });

    }
    return {

        //main function to initiate the module
        init: function () {
            initAddress();
        }

    };

}();

// jQuery(document).ready(function () {
Address.init();
// });