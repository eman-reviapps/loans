$(document).ready(function () {
    $(".states").select2({
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 2,
        width: null,
        tokenSeparators: [','],
        placeholder: 'Select',
        allowClear: true,
        ajax: {
            url: app_url + '/dropdowns/states',
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (term) {
                return {
                    term: term
                };
            },
            processResults: function (data) {
                var results = [];
                $.each(data, function (index, item) {
                    results.push({
                        id: item.state_code,
                        text: item.state
                    });
                });
                return {
                    results: results,
                };
            },
        }
    });
});
