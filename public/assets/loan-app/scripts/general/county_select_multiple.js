$(document).ready(function () {
    initializeCountiesDD();
});


function initializeCountiesDD() {
    $(".counties").select2({
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 2,
        width: null,
        tokenSeparators: [','],
        placeholder: 'Select',
        allowClear: true,
        ajax: {
            url: app_url + '/dropdowns/counties',
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (term) {
                return {
                    term: term,
                    states: $(".states").val(),
                    cities: $(".cities").val(),
                };
            },
            processResults: function (data) {
                var results = [];
                $.each(data, function (index, item) {
                    results.push({
                        id: item.county,
                        text: item.county
                    });
                });
                return {
                    results: results,
                };
            },
        }
    });
}
// + ", " + item.city