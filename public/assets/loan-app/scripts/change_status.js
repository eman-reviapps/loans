$(document).on('change', '[id^=status]', function (e) {
    var id = $(this).attr('id');
    var status = $("#" + id + " option:selected").val();

    var form = $(this).parents('form:first');

    $("#" + form.attr("id") + " input[name=status]").val(status);

    changeStateWithAjax(form, data_div);
});