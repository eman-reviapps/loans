$('.archive').on('click', function (e) {
    var data = $(this);
    swal({
            title: "Are you sure you want to archive this loan ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes",
        },
        function () {
            var archived = archive($(data).data('action'),
                $(data).data('csrf'));
        });
    e.preventDefault();
});

$('.restore').on('click', function (e) {
    var data = $(this);
    swal({
            title: "Are you sure you want to restore this loan ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes",
        },
        function () {
            restore($(data).data('action'),
                $(data).data('csrf'));
        });
    e.preventDefault();
});

$('.place_bid').on('click', function (e) {
    var data = $(this);
    id = $(data).data('id');
    href = $(data).data('href');
    e.preventDefault();

    $.ajax({
        url: $(data).data('action'),
        type: 'GET',
        data: {},
        dataType: 'json',
        success: function (data) {

            if (data.level == "success") {
                swal({
                        title: data.object,
                        type: "info",
                        showCancelButton: false,
                    },
                    function () {
                        window.location = href;
                    });
            } else {
                window.location = href;
            }

        },
        error: function () {
        },
    });
});

$('.attachments_show').on('click', function (e) {
    $('.js-grid-juicy-projects').cubeportfolio('destroy');
    init_cubeportfolio();
});

function onArchiveSuccess() {
    location.reload();
}

function onArchiveFail() {
    swal({
        title: "Failed to archive",
        type: "error",
        confirmButtonText: "Close"
    });
}

function onRestoreSuccess() {
    location.reload();
}

function onRestoreFail() {
    swal({
        title: "Failed to restore",
        type: "error",
        confirmButtonText: "Close"
    });
}

$('.not_interested').on('click', function (e) {
    var href = $(this).attr('href');
    e.preventDefault();
    swal({
            title: "Are sure you are not interested in this bid because this will close the chat with this lender",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes",
        },
        function () {
            // alert('ok')
            console.log($(this))
            window.location = href;

        });
    return false; //<---- Add this line
});

$('.reply').on('click', function (e) {
    e.preventDefault();
    handleClick();
});

$("#message").keypress(function (e) {
    if (e.which == 13) {
        handleClick();
        return false; //<---- Add this line
    }
});

canFollowUp($('.chats').data('id'));

function handleClick() {
    //add my code here eman

    var data = $('#btn-follow-up');
    id = $(data).data('id');
    is_display_alert = $(data).data('content');

    if (is_display_alert == 1) {
        $.ajax({
            url: $(data).data('action'),
            type: 'GET',
            data: {},
            dataType: 'json',
            success: function (data) {
                if (data.level == "success") {
                    count = data.object;

                    if (count == 1 || count == 2) {

                        if (count == 1) {
                            msg = "This is your 1st follow up, you only are allowed to follow up twice";
                        } else if (count == 2) {
                            msg = "This is your last follow up";
                        }

                        swal({
                                title: msg,
                                type: "info",
                                showCancelButton: true,
                                confirmButtonText: "Ok",
                            },
                            function () {
                                var data = $('.reply');
                                var message = $("#message").val();

                                addReply($(data).data('action'), $(data).data('csrf'), $(data).data('user_id'), message);
                            });
                    } else {
                        var data = $('.reply');
                        var message = $("#message").val();

                        addReply($(data).data('action'), $(data).data('csrf'), $(data).data('user_id'), message);
                    }
                }

            },
            error: function () {
            },
        });
    } else {
        var data = $('.reply');
        var message = $("#message").val();

        addReply($(data).data('action'), $(data).data('csrf'), $(data).data('user_id'), message);
    }

}


function onReplySuccess(reply_id) {
    renderReply(reply_id);
}

function onReplyFail(level, message) {
    showToast(level, message, level)
}

function onRenderReplySuccess(html) {
    $("ul.chats").append(html);
    $("#message").val("");

    $('#chats').find('.scroller').slimScroll({
        scrollTo: getLastPostPos()
    });

    canFollowUp($('.chats').data('id'));
}

function yesCanFollowUp() {
    $(".chat-form").show();
    handleFollowUpMessage($('.chats').data('id'));
}

function canNotFollowUp() {
    $(".chat-form").hide();
    handleFollowUpMessage($('.chats').data('id'));
}

function renderFollowUpMessage(html) {
    $('#CanFollowUpMessage').html(html)
}