var id = 0;

var table = $('#archive-table').DataTable({
    processing: true,
    serverSide: true,
    bInfo: false,
    bSort: false,
    bLengthChange: false,
    ajax: getBaseURL(),
    columns: [
        {data: 'created_at', name: 'created_at'},
        {data: 'name', name: 'name'},
        {data: 'location', name: 'location'},
        {data: 'profile_type', name: 'profile_type'},
        {data: 'loan_type', name: 'loan_type'},
        {data: 'requested_amount', name: 'requested_amount'},
        {data: 'action', name: 'action', searchable: false, orderable: false, width: '75px'},
    ],
    "fnDrawCallback": function () {
        $('.restore').on('click', function (e) {
            var data = $(this);
            id = $(data).data('id');

            swal({
                    title: "Are you sure you want to restore this loan ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes",
                },
                function () {
                    restore($(data).data('action'),
                        $(data).data('csrf'));
                });
            e.preventDefault();
        });
    }
});

function getBaseURL() {
    return '?';
}

function onRestoreSuccess() {
    if (typeof table === 'undefined') {
        $('tr#' + id).remove();
    } else {
        table.row('#' + id).remove().draw(false); // this line when use data table only
    }
}

function onRestoreFail() {
    swal({
        title: "Failed to restore",
        type: "error",
        confirmButtonText: "Close"
    });
}