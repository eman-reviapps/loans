var table = $('#my_work-table').DataTable({
    processing: true,
    serverSide: true,
    bInfo: false,
    bFilter: false,
    bSort: false,
    bLengthChange: false,
    ajax: getBaseURL(),
    columns: [
        {data: 'created_at', name: 'created_at'},
        {data: 'name', name: 'name'},
        {data: 'location', name: 'location'},
        {data: 'loan_type', name: 'loan_type'},
        {data: 'requested_amount', name: 'requested_amount'},
        {data: 'bid_status', name: 'bid_status'},
        {data: 'action', name: 'action', searchable: false, orderable: false, width: '75px'},
    ],
    "fnDrawCallback": function () {
        $('.archive').on('click', function (e) {
            var data = $(this);
            id = $(data).data('id');

            swal({
                    title: "Are you sure you want to archive this loan ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes",
                },
                function () {
                    archive($(data).data('action'),
                        $(data).data('csrf'));
                });
            e.preventDefault();
        });
        $('.btn-follow-up').on('click', function (e) {
            var data = $(this);
            id = $(data).data('id');
            href = $(data).data('href');
            e.preventDefault();

            $.ajax({
                url: $(data).data('action'),
                type: 'GET',
                data: {},
                dataType: 'json',
                success: function (data) {
                    if (data.level == "success") {
                        count = data.object;

                        if (count == 1) {
                            msg = "This is your 1st follow up, you only are allowed to follow up twice";
                        } else if (count == 2) {
                            msg = "This is your last follow up";
                        }
                        swal({
                                title: msg,
                                type: "info",
                                showCancelButton: true,
                                confirmButtonText: "Ok",
                            },
                            function () {
                                window.location = href;
                            });
                    }

                },
                error: function () {
                },
            });
        });
    }
});

function getBaseURL() {
    return '?';
}

function onArchiveSuccess() {
    if (typeof table === 'undefined') {
        $('tr#' + id).remove();
    } else {
        table.row('#' + id).remove().draw(false); // this line when use data table only
    }
}

function onArchiveFail() {
    swal({
        title: "Failed to archive",
        type: "error",
        confirmButtonText: "Close"
    });
}