var filter_form = $("#filterLoansLender");
$(document).ready(function () {
    $(".mask_number").inputmask({
        "mask": "9",
        "repeat": 10,
        "greedy": false
    });

    // $('.mask_money').mask("00,000,000", {reverse: true, placeholder: "$00,000,000"});
    $('.mask_money').mask('#,##0', {placeholder: "$000,000,000",reverse: true});

    $('input[type=text]').keyup(function () {
        filter();
    });

    $('input').on('ifChanged', function (event) {
        $(event.target).trigger('change');
    });
    $('input[type="checkbox"]').change(function () {
        filter();
    });

    $('select').change(function () {
        filter();
    });

    $('.btn-close-filter').click(function () {
        // $('#collapse').slideUp()
    });
});

var id = 0;

var table = $('#loans-table').DataTable({
    processing: true,
    serverSide: true,
    bInfo: false,
    bFilter: false,
    bSort: false,
    bLengthChange: false,
    ajax: getDataURL(),
    columns: [
        {data: 'created_at', name: 'created_at'},
        {data: 'name', name: 'name'},
        {data: 'location', name: 'location'},
        {data: 'profile_type', name: 'profile_type'},
        {data: 'loan_type', name: 'loan_type'},
        {data: 'requested_amount', name: 'requested_amount'},
        {data: 'action', name: 'action', searchable: false, orderable: false, width: '75px'},
    ],
    "fnDrawCallback": function () {
        $('.archive').on('click', function (e) {
            var data = $(this);
            id = $(data).data('id');

            swal({
                    title: "Are you sure you want to archive this loan ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes",
                },
                function () {
                    archive($(data).data('action'),
                        $(data).data('csrf'));
                });
            e.preventDefault();
        });
    }
});
function getBaseURL() {
    return '?';
}
function getDataURL() {
    return getBaseURL() + filter_form.serialize();
}

function filter() {
    table.ajax.url(getDataURL()).load();
}

// function saveToProfile() {
//     $.ajax({
//         type: "GET",
//         url: './save_to_profile',
//         data: "",
//         success: function() {
//             console.log("Geodata sent");
//         }
//     })
// }
$(document).on('click', '.btn-save-to_profile', function (e) {
    var form = $(this).parents('form:first');
    console.log(form)
    console.log(form.serialize())

    $.ajax({
        type: "GET",
        url: './save_to_profile',
        data: form.serialize(),
        success: function (data) {
            console.log('save to profile success')
            swal({
                title: "Preferences saved to profile",
                type: "success",
                confirmButtonText: "Close"
            });
        },
        error: function (data) {
            console.log('save to profile error')
            swal({
                title: "Failed to save preferences to profile",
                type: "error",
                confirmButtonText: "Close"
            });
        }
    });
});

function onArchiveSuccess() {
    if (typeof table === 'undefined') {
        $('tr#' + id).remove();
    } else {
        table.row('#' + id).remove().draw(false); // this line when use data table only
    }
}

function onArchiveFail() {
    swal({
        title: "Failed to archive",
        type: "error",
        confirmButtonText: "Close"
    });
}