var PlaceBid = function () {
    return {
        init: function () {
            $(".mask_number").inputmask({
                "mask": "9",
                "repeat": 10,
                "greedy": false,
            });
            $(".numeric").numeric();
            var form = $('#place_bid_form');

            form.validate({
                errorClass: validation_error_class,
                ignore: [],
                rules: {
                    rate: {
                        required: true,
                        number: true
                    },
                    term: {
                        required: true,
                        digits: true
                    },
                    term_unit: {
                        required: true,
                    },
                    amortization: {
                        required: true,
                        digits: true
                    },
                    amortization_unit: {
                        required: true,
                    },
                    description: {
                        required: true,
                        minlength: 6
                    }
                },
                messages: {
                    rate: {
                        required: messages_validations_array.rate.required,
                        number: messages_validations_array.rate.required
                    },
                    term: {
                        required: messages_validations_array.term.required,
                        digits: messages_validations_array.term.required
                    },
                    term_unit: {
                        required: messages_validations_array.term_unit.required,
                    },
                    amortization: {
                        required: messages_validations_array.amortization.required,
                        digits: messages_validations_array.amortization.required
                    },
                    amortization_unit: {
                        required: messages_validations_array.amortization_unit.required,
                    },
                    description: {
                        required: messages_validations_array.description.required,
                        min: messages_validations_array.description.min,
                    }
                }

            });
        }
    };

}();

window.onload=function() {
    document.getElementById("term_unit").onchange=function() {
        document.getElementById("amortization_unit").value=this.options[this.selectedIndex].getAttribute("value");
    }
    document.getElementById("term_unit").onchange(); // trigger when loading
}

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        PlaceBid.init();
    });
}