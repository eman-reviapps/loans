edit_validation_array = {
    errorClass: validation_error_class,
    ignore: []
};
$(document).ready(function () {
    bindTree();
    $("#editPermissionsForm").validate(edit_validation_array);
});

function bindTree() {
    $('#jstree1').jstree({
        'plugins': ["checkbox", "types", "themes"],
        'core': {
            'check_callback': true,
            "themes": {
                "responsive": false
            },
            'data': privilages
        },
        "types": {
            "default": {
                "icon": "fa fa-folder icon-state-warning icon-lg"
            },
            "file": {
                "icon": "fa fa-file icon-state-warning icon-lg"
            }
        }
    }).bind("loaded.jstree", function (e, data) {
        setPrivilages($('#jstree1').jstree('get_checked', 1, true));
    }).bind("select_node.jstree", function (e, data) {
        setPrivilages($('#jstree1').jstree('get_checked', 1, true));
    }).bind("deselect_node.jstree", function (e, data) {
        setPrivilages($('#jstree1').jstree('get_checked', 1, true));
    });
}

function getTreeData() {
    return [
        {
            "id": "dashboard",
            "text": "Dashboard",
            "state": {
                "selected": (privilages["dashboard"] == 1 && user_operation == 'edit' ? true : false)
            }
        },
        {
            "id": "settings",
            "text": "Settings",
            "state": {
                "selected": (privilages["settings"] == 1 && user_operation == 'edit' ? true : false)
            }
        },
        {
            "id": "institution_management",
            "text": "Institutions Managment",
            "state": {
                "selected": (privilages["institution_management"] == 1 && user_operation == 'edit' ? true : false),
                'opened': true
            },
            "children": [
                {
                    "id": "institution",
                    "text": "Institutions",
                    "icon": "fa fa-folder icon-state-default",
                    "state": {
                        "opened": true,
                        "selected": (privilages["institution"] == 1 && user_operation == 'edit' ? true : false)
                    },
                    "children": [
                        {
                            "id": "institution.view",
                            "text": "View",
                            "icon": "fa fa-eye icon-state-success",
                            "state": {
                                "selected": (privilages["institution.view"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "institution.create",
                            "text": "Create",
                            "icon": "fa fa-plus icon-state-success",
                            "state": {
                                "selected": (privilages["institution.create"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "institution.update",
                            "text": "Update",
                            "icon": "fa fa-edit icon-state-success",
                            "state": {
                                "selected": (privilages["institution.update"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "institution.delete",
                            "text": "Delete",
                            "icon": "fa fa-close icon-state-danger",
                            "state": {
                                "selected": (privilages["institution.delete"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        }
                    ]
                }, {
                    "id": "institution_types",
                    "text": "Institution Types",
                    "icon": "fa fa-folder icon-state-default",
                    "state": {
                        "opened": true,
                        "selected": (privilages["institution_types"] == 1 && user_operation == 'edit' ? true : false)
                    },
                    "children": [
                        {
                            "id": "institution_types.view",
                            "text": "View",
                            "icon": "fa fa-eye icon-state-success",
                            "state": {
                                "selected": (privilages["institution_types.view"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "institution_types.create",
                            "text": "Create",
                            "icon": "fa fa-plus icon-state-success",
                            "state": {
                                "selected": (privilages["institution_types.create"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "institution_types.update",
                            "text": "Update",
                            "icon": "fa fa-edit icon-state-success",
                            "state": {
                                "selected": (privilages["institution_types.update"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "institution_types.delete",
                            "text": "Delete",
                            "icon": "fa fa-close icon-state-danger",
                            "state": {
                                "selected": (privilages["institution_types.delete"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        }
                    ]
                }, {
                    "id": "institution_sizes",
                    "text": "Institution Sizes",
                    "icon": "fa fa-folder icon-state-default",
                    "state": {
                        "opened": true,
                        "selected": (privilages["institution_sizes"] == 1 && user_operation == 'edit' ? true : false)
                    },
                    "children": [
                        {
                            "id": "institution_sizes.view",
                            "text": "View",
                            "icon": "fa fa-eye icon-state-success",
                            "state": {
                                "selected": (privilages["institution_sizes.view"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "institution_sizes.create",
                            "text": "Create",
                            "icon": "fa fa-plus icon-state-success",
                            "state": {
                                "selected": (privilages["institution_sizes.create"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "institution_sizes.update",
                            "text": "Update",
                            "icon": "fa fa-edit icon-state-success",
                            "state": {
                                "selected": (privilages["institution_sizes.update"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "institution_sizes.delete",
                            "text": "Delete",
                            "icon": "fa fa-close icon-state-danger",
                            "state": {
                                "selected": (privilages["institution_sizes.delete"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        }
                    ]
                }
            ]
        },
        {
            "id": "users_management",
            "text": "Users Management",
            'state': {
                'opened': true,
                "selected": (privilages["users_management"] == 1 && user_operation == 'edit' ? true : false)
            },
            "children": [{
                "id": "users",
                "text": "Users",
                "icon": "fa fa-folder icon-state-default",
                "state": {
                    "opened": true,
                    "selected": (privilages["users"] == 1 && user_operation == 'edit' ? true : false)
                },
                "children": [
                    {
                        "id": "users.view",
                        "text": "View",
                        "icon": "fa fa-eye icon-state-success",
                        "state": {
                            "selected": (privilages["users.view"] == 1 && user_operation == 'edit' ? true : false)
                        }
                    },
                    {
                        "id": "users.create",
                        "text": "Create",
                        "icon": "fa fa-plus icon-state-success",
                        "state": {
                            "selected": (privilages["users.create"] == 1 && user_operation == 'edit' ? true : false)
                        }
                    },
                    {
                        "id": "users.update",
                        "text": "Update",
                        "icon": "fa fa-edit icon-state-success",
                        "state": {
                            "selected": (privilages["users.update"] == 1 && user_operation == 'edit' ? true : false)
                        }
                    },
                    {
                        "id": "users.delete",
                        "text": "Delete",
                        "icon": "fa fa-close icon-state-danger",
                        "state": {
                            "selected": (privilages["users.delete"] == 1 && user_operation == 'edit' ? true : false)
                        }
                    }
                ]
            }, {
                "id": "roles",
                "text": "Roles",
                "icon": "fa fa-folder icon-state-default",
                "state": {
                    "opened": true,
                    "selected": (privilages["roles"] == 1 && user_operation == 'edit' ? true : false)
                },
                "children": [
                    {
                        "id": "roles.view",
                        "text": "View",
                        "icon": "fa fa-eye icon-state-success",
                        "state": {
                            "selected": (privilages["roles.view"] == 1 && user_operation == 'edit' ? true : false)
                        }
                    },
                    {
                        "id": "roles.create",
                        "text": "Create",
                        "icon": "fa fa-plus icon-state-success",
                        "state": {
                            "selected": (privilages["roles.create"] == 1 && user_operation == 'edit' ? true : false)
                        }
                    },
                    {
                        "id": "roles.update",
                        "text": "Update",
                        "icon": "fa fa-edit icon-state-success",
                        "state": {
                            "selected": (privilages["roles.update"] == 1 && user_operation == 'edit' ? true : false)
                        }
                    },
                    {
                        "id": "roles.delete",
                        "text": "Delete",
                        "icon": "fa fa-close icon-state-danger",
                        "state": {
                            "selected": (privilages["roles.delete"] == 1 && user_operation == 'edit' ? true : false)
                        }
                    }
                ]
            }
            ]

        },
        {
            "id": "posts_management",
            "text": "Posts Management",
            'state': {
                'opened': true,
                "selected": (privilages["posts_management"] == 1 && user_operation == 'edit' ? true : false)
            },
            "children": [{
                "id": "posts",
                "text": "Posts",
                "icon": "fa fa-folder icon-state-default",
                "state": {
                    "opened": true,
                    "selected": (privilages["posts"] == 1 && user_operation == 'edit' ? true : false)
                },
                "children": [
                    {
                        "id": "posts.view",
                        "text": "View",
                        "icon": "fa fa-eye icon-state-success",
                        "state": {
                            "selected": (privilages["posts.view"] == 1 && user_operation == 'edit' ? true : false)
                        }
                    },
                    {
                        "id": "posts.create",
                        "text": "Create",
                        "icon": "fa fa-plus icon-state-success",
                        "state": {
                            "selected": (privilages["posts.create"] == 1 && user_operation == 'edit' ? true : false)
                        }
                    },
                    {
                        "id": "posts.update",
                        "text": "Update",
                        "icon": "fa fa-edit icon-state-success",
                        "state": {
                            "selected": (privilages["posts.update"] == 1 && user_operation == 'edit' ? true : false)
                        }
                    },
                    {
                        "id": "posts.delete",
                        "text": "Delete",
                        "icon": "fa fa-close icon-state-danger",
                        "state": {
                            "selected": (privilages["posts.delete"] == 1 && user_operation == 'edit' ? true : false)
                        }
                    }
                ]
            },
                {
                    "id": "real_estate_types",
                    "text": "Real Estate Types",
                    "icon": "fa fa-folder icon-state-default",
                    "state": {
                        "opened": true,
                        "selected": (privilages["real_estate_types"] == 1 && user_operation == 'edit' ? true : false)
                    },
                    "children": [
                        {
                            "id": "real_estate_types.view",
                            "text": "View",
                            "icon": "fa fa-eye icon-state-success",
                            "state": {
                                "selected": (privilages["real_estate_types.view"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "real_estate_types.create",
                            "text": "Create",
                            "icon": "fa fa-plus icon-state-success",
                            "state": {
                                "selected": (privilages["real_estate_types.create"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "real_estate_types.update",
                            "text": "Update",
                            "icon": "fa fa-edit icon-state-success",
                            "state": {
                                "selected": (privilages["real_estate_types.update"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "real_estate_types.delete",
                            "text": "Delete",
                            "icon": "fa fa-close icon-state-danger",
                            "state": {
                                "selected": (privilages["real_estate_types.delete"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        }
                    ]
                },
                {
                    "id": "property_types",
                    "text": "Property Types",
                    "icon": "fa fa-folder icon-state-default",
                    "state": {
                        "opened": true,
                        "selected": (privilages["property_types"] == 1 && user_operation == 'edit' ? true : false)
                    },
                    "children": [
                        {
                            "id": "property_types.view",
                            "text": "View",
                            "icon": "fa fa-eye icon-state-success",
                            "state": {
                                "selected": (privilages["property_types.view"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "property_types.create",
                            "text": "Create",
                            "icon": "fa fa-plus icon-state-success",
                            "state": {
                                "selected": (privilages["property_types.create"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "property_types.update",
                            "text": "Update",
                            "icon": "fa fa-edit icon-state-success",
                            "state": {
                                "selected": (privilages["property_types.update"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "property_types.delete",
                            "text": "Delete",
                            "icon": "fa fa-close icon-state-danger",
                            "state": {
                                "selected": (privilages["property_types.delete"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        }
                    ]
                },
                {
                    "id": "purpose_types",
                    "text": "Purpose Types",
                    "icon": "fa fa-folder icon-state-default",
                    "state": {
                        "opened": true,
                        "selected": (privilages["purpose_types"] == 1 && user_operation == 'edit' ? true : false)
                    },
                    "children": [
                        {
                            "id": "purpose_types.view",
                            "text": "View",
                            "icon": "fa fa-eye icon-state-success",
                            "state": {
                                "selected": (privilages["purpose_types.view"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "purpose_types.create",
                            "text": "Create",
                            "icon": "fa fa-plus icon-state-success",
                            "state": {
                                "selected": (privilages["purpose_types.create"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "purpose_types.update",
                            "text": "Update",
                            "icon": "fa fa-edit icon-state-success",
                            "state": {
                                "selected": (privilages["purpose_types.update"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "purpose_types.delete",
                            "text": "Delete",
                            "icon": "fa fa-close icon-state-danger",
                            "state": {
                                "selected": (privilages["purpose_types.delete"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        }
                    ]
                },
                {
                    "id": "lease_types",
                    "text": "Lease Types",
                    "icon": "fa fa-folder icon-state-default",
                    "state": {
                        "opened": true,
                        "selected": (privilages["lease_types"] == 1 && user_operation == 'edit' ? true : false)
                    },
                    "children": [
                        {
                            "id": "lease_types.view",
                            "text": "View",
                            "icon": "fa fa-eye icon-state-success",
                            "state": {
                                "selected": (privilages["lease_types.view"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "lease_types.create",
                            "text": "Create",
                            "icon": "fa fa-plus icon-state-success",
                            "state": {
                                "selected": (privilages["lease_types.create"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "lease_types.update",
                            "text": "Update",
                            "icon": "fa fa-edit icon-state-success",
                            "state": {
                                "selected": (privilages["lease_types.update"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        },
                        {
                            "id": "lease_types.delete",
                            "text": "Delete",
                            "icon": "fa fa-close icon-state-danger",
                            "state": {
                                "selected": (privilages["lease_types.delete"] == 1 && user_operation == 'edit' ? true : false)
                            }
                        }
                    ]
                }
            ]

        },
    ];
}


function setPrivilages(data) {
    var array_privilages = [];
    var un_checked_parent = [];
    data.forEach(function (privilage) {
        if (array_privilages.indexOf(privilage.id) === -1) {
            var key = privilage.id;
            array_privilages.push(privilage.id);
        }
        if (privilage.parents) {
            privilage.parents.forEach(function (parent) {
                if (un_checked_parent.indexOf(parent) === -1 && parent !== '#') {
                    is_parent_checked = $('#jstree1').jstree('is_checked', parent);
                    if (is_parent_checked == true) {
                        var index = un_checked_parent.indexOf(parent);
                        if (index != -1) {
                            un_checked_parent.splice(index, 1);
                        }
                    } else {
                        un_checked_parent.push(parent);
                    }

                }
            });
        }
    });
    $("#privilages").val(array_privilages)
    $("#un_checked_parent").val(un_checked_parent)
}