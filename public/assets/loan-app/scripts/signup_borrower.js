var RegisterBorrower = function () {
    var handleRegisterBorrower = function () {

        function format(state) {
            if (!state.id) {
                return state.text;
            }
            var $state = $(
                '<span><img src="../assets/global/img/flags/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
            );

            return $state;
        }

        $('.register-form-borrower').validate({
            errorClass: "font-red-sunglo",
            rules: {
                first_name: {
                    required: true,
                    minlength: 3
                },
                last_name: {
                    required: true,
                    minlength: 3
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    equalTo: "#password"
                },
            },

            messages: { // custom messages for radio buttons and checkboxes
                first_name: {
                    required: messages_validations_array.first_name.required,
                    minlength: messages_validations_array.first_name.min
                },
                last_name: {
                    required: messages_validations_array.last_name.required,
                    minlength: messages_validations_array.last_name.min
                },
                email: {
                    required: messages_validations_array.email.required,
                    email: messages_validations_array.email.email
                },
                password: {
                    required: messages_validations_array.password.required,
                    minlength: messages_validations_array.password.min,
                },
                password_confirmation: {
                    equalTo: messages_validations_array.password_confirmation.confirmed
                },
            }
        });

        $('.register-form-borrower input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.register-form-borrower').validate().form()) {
                    $('.register-form-borrower').submit();
                }
                return false;
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {

            handleRegisterBorrower();

        }

    };

}();

jQuery(document).ready(function () {
    RegisterBorrower.init();
});