var ForgotPassword = function () {

    var handleForgotPassword = function () {

        $('.forget-form').validate({
            errorClass: "font-red-sunglo",
            rules: {
                email: {
                    required: true,
                    email: true
                },
            },

            messages: {
                email: {
                    required: messages_validations_array.email.required,
                    email: messages_validations_array.email.email
                },
            }
        });

        $('.forget-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    $('.forget-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    }
    return {
        //main function to initiate the module
        init: function () {

            handleForgotPassword();

        }

    };

}();

jQuery(document).ready(function () {
    ForgotPassword.init();
});