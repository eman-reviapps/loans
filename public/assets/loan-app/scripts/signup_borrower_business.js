function calcEBITDA() {
    var company_EBITDA_year1 = 0;
    var company_EBITDA_year2 = 0;
    var company_EBITDA_year3 = 0;

    $('.calc_EBITDA_year1').each(function () {
        if ($(this).val()) {
            var value = parseFloat($(this).val().replace(/[^0-9\.-]+/g, ""));
            company_EBITDA_year1 += value;
        }
    });
    $('.calc_EBITDA_year2').each(function () {
        if ($(this).val()) {
            var value = parseFloat($(this).val().replace(/[^0-9\.-]+/g, ""));
            company_EBITDA_year2 += value;
        }
    });
    $('.calc_EBITDA_year3').each(function () {
        if ($(this).val()) {
            var value = parseFloat($(this).val().replace(/[^0-9\.-]+/g, ""));
            company_EBITDA_year3 += value;
        }
    });
    $("#company_EBITDA_year_1").val(company_EBITDA_year1)
    $("#company_EBITDA_year_2").val(company_EBITDA_year2)
    $("#company_EBITDA_year_3").val(company_EBITDA_year3)
    $("#company_EBITDA").val(company_EBITDA_year1 + company_EBITDA_year2 + company_EBITDA_year3)
}

var FormWizard = function () {

    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            function format(state) {
                if (!state.id) return state.text; // optgroup
                return "<img class='flag' src='../../assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
            }

            $(".select2").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                width: 'auto',
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });
            $(".establish_year").inputmask({
                "mask": "9",
                "repeat": 4,
                "greedy": false
            });
            $(".fisical_year_end").inputmask("m/d", {
                "placeholder": "mm/dd",
                autoUnmask: true
            });

            $(".mask_phone").inputmask("mask", {
                "mask": "(999) 999-9999"
            });

            // $('.mask_money').mask("00,000,000", {reverse: true, placeholder: "$00,000,000"});
            $('.mask_money').mask('#,##0', {placeholder: "$000,000,000",reverse: true});

            $('.calc_EBITDA_year1').on('input', function () {
                calcEBITDA();
            });
            $('.calc_EBITDA_year2').on('input', function () {
                calcEBITDA();
            });
            $('.calc_EBITDA_year3').on('input', function () {
                calcEBITDA();
            });
            var form = $('#signup_borrower_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: app_url + '/users/check_email_exists',
                            type: "get",
                            data: {
                                email: function () {
                                    return $("#" + form.attr("id") + " input[name=email]").val();
                                }
                            }
                        }
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    password_confirmation: {
                        equalTo: "#password"
                    },
                    first_name: {
                        required: true,
                        minlength: 3
                    },
                    last_name: {
                        required: true,
                        minlength: 3
                    },
                    profile_creator_role: {
                        required: true,
                        minlength: 3
                    },
                    phone: {
                        required: true,
                        phoneUS: true
                    },
                    business_name: {
                        required: true,
                        minlength: 3
                    },
                    state: {
                        required: true,
                    },
                    city: {
                        required: true,
                    },
                    zip_code: {
                        required: true,
                        remote: {
                            url: app_url + '/states/check_valid_zip_code',
                            type: "get",
                            data: {
                                zip_code: function () {
                                    return $("#" + form.attr("id") + " #zip_code").find(":selected").text();
                                    //return $("#" + form.attr("id") + " input[name=zip_code]").val();
                                },
                                city: function () {
                                    return $("#" + form.attr("id") + " #city").find(":selected").text();
                                },
                                state: function () {
                                    return $("#" + form.attr("id") + " #state").find(":selected").text();
                                    ;
                                }
                            }
                        }
                    },
                    address: {
                        required: true,
                        minlength: 3
                    },
                    apt_suite_no: {
                        required: false,
                        alphanumeric: true
                    },
                    description: {
                        required: true,
                        minlength: 3
                    },
                    establish_year: {
                        required: true,
                        minlength: 4,
                        number: true,
                        valid_year: true
                    },
                    fisical_year_end: {
                        required: true,
                        dayMonthDate: true,
                    },
                    annual_revenue_year_1: {
                        required: true,
                        number: true
                    },
                    annual_revenue_year_2: {
                        required: true,
                        number: true
                    },
                    annual_revenue_year_3: {
                        required: true,
                        number: true
                    },

                    net_income_before_tax_year_1: {
                        required: true,
                        number: true
                    },
                    net_income_before_tax_year_2: {
                        required: true,
                        number: true
                    },
                    net_income_before_tax_year_3: {
                        required: true,
                        number: true
                    },

                    interest_expense_year_1: {
                        required: true,
                        number: true
                    },
                    interest_expense_year_2: {
                        required: true,
                        number: true
                    },
                    interest_expense_year_3: {
                        required: true,
                        number: true
                    },

                    depreciation_amortization_year_1: {
                        required: true,
                        number: true
                    },
                    depreciation_amortization_year_2: {
                        required: true,
                        number: true
                    },
                    depreciation_amortization_year_3: {
                        required: true,
                        number: true
                    },

                    company_EBITDA: {
                        required: true,
                        number: true
                    },
                },
                messages: {
                    email: {
                        required: messages_validations_array.email.required,
                        email: messages_validations_array.email.email,
                        remote: messages_validations_array.email.unique
                    },
                    password: {
                        required: messages_validations_array.password.required,
                        minlength: messages_validations_array.password.min,
                    },
                    password_confirmation: {
                        equalTo: messages_validations_array.password_confirmation.confirmed
                    },
                    first_name: {
                        required: messages_validations_array.first_name.required,
                        minlength: messages_validations_array.first_name.min
                    },
                    last_name: {
                        required: messages_validations_array.last_name.required,
                        minlength: messages_validations_array.last_name.min
                    },
                    profile_creator_role: {
                        required: messages_validations_array.profile_creator_role.required,
                        minlength: messages_validations_array.profile_creator_role.min
                    },
                    phone: {
                        required: messages_validations_array.phone.required,
                        phoneUS: messages_validations_array.phone.regex
                    },
                    business_name: {
                        required: messages_validations_array.business_name.required,
                        minlength: messages_validations_array.business_name.min,
                    },
                    zip_code: {
                        required: messages_validations_array.zip_code.required,
                        remote: messages_validations_array.zip_code.not_served_yet
                    },
                    state: {
                        required: messages_validations_array.state.required,
                    },
                    city: {
                        required: messages_validations_array.city.required,
                    },
                    address: {
                        required: messages_validations_array.address.required,
                        minlength: messages_validations_array.address.min,
                    },
                    apt_suite_no: {
                        required: messages_validations_array.apt_suite_no.required,
                        number: messages_validations_array.apt_suite_no.numeric,
                    },
                    description: {
                        required: messages_validations_array.description.required,
                        minlength: messages_validations_array.description.min,
                    },
                    establish_year: {
                        required: messages_validations_array.establish_year.required,
                        minlength: messages_validations_array.establish_year.min,
                        number: messages_validations_array.establish_year.numeric,
                        valid_year: messages_validations_array.establish_year.numeric,
                    },
                    fisical_year_end: {
                        required: messages_validations_array.fisical_year_end.required,
                        dayMonthDate: messages_validations_array.fisical_year_end.valid_day_month,
                    },

                    annual_revenue_year_1: {
                        required: messages_validations_array.annual_revenue_year_1.required,
                        number: messages_validations_array.annual_revenue_year_1.numeric,
                    },
                    annual_revenue_year_2: {
                        required: messages_validations_array.annual_revenue_year_2.required,
                        number: messages_validations_array.annual_revenue_year_2.numeric,
                    },
                    annual_revenue_year_3: {
                        required: messages_validations_array.annual_revenue_year_3.required,
                        number: messages_validations_array.annual_revenue_year_3.numeric,
                    },

                    net_income_before_tax_year_1: {
                        required: messages_validations_array.net_income_before_tax_year_1.required,
                        number: messages_validations_array.net_income_before_tax_year_1.numeric,
                    },
                    net_income_before_tax_year_2: {
                        required: messages_validations_array.net_income_before_tax_year_2.required,
                        number: messages_validations_array.net_income_before_tax_year_2.numeric,
                    },
                    net_income_before_tax_year_3: {
                        required: messages_validations_array.net_income_before_tax_year_3.required,
                        number: messages_validations_array.net_income_before_tax_year_3.numeric,
                    },

                    interest_expense_year_1: {
                        required: messages_validations_array.net_income_before_tax_year_1.required,
                        number: messages_validations_array.net_income_before_tax_year_1.numeric,
                    },
                    interest_expense_year_2: {
                        required: messages_validations_array.net_income_before_tax_year_2.required,
                        number: messages_validations_array.net_income_before_tax_year_2.numeric,
                    },
                    interest_expense_year_3: {
                        required: messages_validations_array.net_income_before_tax_year_3.required,
                        number: messages_validations_array.net_income_before_tax_year_3.numeric,
                    },

                    depreciation_amortization_year_1: {
                        required: messages_validations_array.net_income_before_tax_year_1.required,
                        number: messages_validations_array.net_income_before_tax_year_1.numeric,
                    },
                    depreciation_amortization_year_2: {
                        required: messages_validations_array.net_income_before_tax_year_2.required,
                        number: messages_validations_array.net_income_before_tax_year_2.numeric,
                    },
                    depreciation_amortization_year_3: {
                        required: messages_validations_array.net_income_before_tax_year_3.required,
                        number: messages_validations_array.net_income_before_tax_year_3.numeric,
                    },

                    company_EBITDA: {
                        required: messages_validations_array.company_EBITDA.required,
                        number: messages_validations_array.company_EBITDA.numeric,
                    },

                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                            .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

            });

            var displayConfirm = function () {
                $('#tab_confirm .form-control-static', form).each(function () {
                    var input = $('[name="' + $(this).attr("data-display") + '"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="' + $(this).attr("data-display") + '"]:checked', form);
                    }

                    if (input.is(":hidden") || input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment[]') {
                        var payment = [];
                        $('[name="payment[]"]:checked', form).each(function () {
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }

            var handleTitle = function (tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#signup_borrower_form_wizard')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#signup_borrower_form_wizard')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#signup_borrower_form_wizard').find('.button-previous').hide();
                } else {
                    $('#signup_borrower_form_wizard').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#signup_borrower_form_wizard').find('.button-next').hide();
                    $('#signup_borrower_form_wizard').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#signup_borrower_form_wizard').find('.button-next').show();
                    $('#signup_borrower_form_wizard').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title'));
            }

            // default form wizard
            $('#signup_borrower_form_wizard').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    return false;

                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, clickedIndex);
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#signup_borrower_form_wizard').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#signup_borrower_form_wizard').find('.button-previous').hide();
            $('#signup_borrower_form_wizard .button-submit').click(function () {

                $('#signup_borrower_form_wizard  input.mask_money').each(function () {
                    this.value = this.value.replace(/[^0-9\.-]+/g, "");
                });

                form.submit();
            }).hide();

            //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('#country_list', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
        }

    };

}();

jQuery(document).ready(function () {
    Address.init();
    FormWizard.init();
});