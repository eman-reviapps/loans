$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var validation_error_class = 'font-red-sunglo';
var data_div = $("#FilteredData");

$(document).on('input', '#name', function (e) {
    slug = slugify($(this).val())
    var form = $(this).parents('form:first');
    $("#" + form.attr("id") + " input[name=slug]").val(slug);
});

$(document).on('click', '.btn_action_back', function (e) {
    parent.history.back();
    return false;
});
$(document).on('click', '.btn_action_reload', function (e) {
    location.reload();
    return false;
});
$(document).on('click', '.btn_action_save', function (e) {
    $("#save_continue").val('no');
    $("#loan_status").val('LIVE');
    var form = $(this).parents('form:first');
    submitForm(form);
});
$(document).on('click', '.btn_action_save_continue', function (e) {
    $("#save_continue").val('yes');
    var form = $(this).parents('form:first');
    submitForm(form);
});
$(document).on('click', '.btn_action_save_continue_tab', function (e) {
    var form = $(this).parents('form:first');
    var active_tab = $(this).parents("div[id*='tab']").attr("id");

    $("#" + form.attr("id") + " input[name=save_continue_tab]").val(active_tab);

    // $("#save_continue_tab").val(active_tab);
    submitForm(form);
});

$(document).on('click', '.delete_record_btn', function (e) {
    var delete_form = $(this).parents('form:first');
    deleteWithAjax(delete_form, $(".delete_modal"), data_div);
});

$(document).on('switchChange.bootstrapSwitch', '.make-switch', function (event, state) {

    var state_form = $(this).parents('form:first');

    var is_enabled = state ? '1' : '0';

    $("#" + state_form.attr("id") + " input[name=is_enabled]").val(is_enabled);

    changeStateWithAjax(state_form, data_div)
});

$(document).on('change', 'input[type=radio][id*="active"]', function (event, state) {
    var form = $(this).parents('form:first');
    changeStateWithAjax(form, data_div)
});

$('.delete_modal').on('hidden.bs.modal', function () {
    resetModalForm($(this), null, false);
});

function submitForm(form) {
    if (form.valid()) {
        form.submit();
    }
}


function resetModalForm(modal, validator, clear) {
    if (validator != null) {
        validator.resetForm();
        validator.prepareForm();
    }
    if (clear == true) {
        modal.find("input,textarea,select").val('').end()
    }
    modal.find("input,textarea,select").removeClass(validation_error_class);

    $(".alert-danger").hide();
    $(".alert-success").hide();

}

function addWithAjax(form, modal, validator, div) {
    if (form.valid()) {
        $.ajax({
            type: form.attr("method"),
            url: form.attr("action"),
            data: form.serialize(),
            success: function (data) {
                // var message = data.message[0];
                // onSuccess(modal, validator, message, div, true);
                onSuccess(modal, null, data, div);
            },
            error: function (data) {
                onError(data);
            }
        });
    }
}

function editWithAjax(form, modal, validator, div) {
    if (form.valid()) {

        var method = $("#" + form.attr("id") + " input[name=_method]").val();

        $.ajax({
            type: method,
            url: form.attr("action"),
            data: form.serialize(),
            success: function (data) {
                // showToast(data.level, data.message, data.level);
                onSuccess(modal, null, data, div);
            },
            error: function (data) {
                onError(data);
            }
        });
    }
}

function changeStateWithAjax(form, div, state) {
    var method = $("#" + form.attr("id") + " input[name=_method]").val();
    // form.submit();
    // return;
    $.ajax({
        type: method,
        url: form.attr("action"),
        data: form.serialize(),
        success: function (data) {
            showToast(data.level, data.message, data.level);
            getWithAjax(div);
        },
        error: function (data) {
            onError(data);
        }
    });
}
function deleteWithAjax(delete_form, modal, div) {
    var method = $("#" + delete_form.attr("id") + " input[name=_method]").val();

    $.ajax({
        type: method,
        url: delete_form.attr("action"),
        data: "",
        dataType: 'json',
        success: function (data) {
            onSuccess(modal, null, data, div);
        },
        error: function (data) {
            console.log(data)
            onError(data);
        }
    });
}

function resetPlugins() {
    TableDatatablesButtons.init();
    App.initAjax(); // handle bootstrap switch plugin
    ComponentsBootstrapSelect.init();
}

function getWithAjax(div) {
    $.get("", {dataType: 'json'}, function (data) {
        div.children().remove();
        div.html(data);
        resetPlugins();
    });
}

function makeGetAjaxCall(url) {
    var result;
    $.ajax({
        type: 'GET',
        url: url,
        data: "",
        async: false,
        success: function (data) {
            result = data;
        },
        error: function (data) {
            result = "";
        }
    });
    return result;
}

function onSuccess(modal, validator, data, div, clear) {
    $(".msg_success").html(message);
    $(".alert-danger").hide();
    $(".alert-success").hide();

    showToast(data.level, data.message, data.level);

    resetModalForm(modal, validator, clear);
    modal.modal('hide');
    getWithAjax(div);
}

function onError(data) {
    var errorsHtml = '';
    if (data.message) {
        errorsHtml = data.message;
    } else {
        var errors = data.Error;
        if (errors) {
            errorsHtml = errors.message + '</br/>';
        } else {
            $.each(errors, function (key, value) {
                errorsHtml += value[0] + '</br/>';
            });
        }
    }

    $(".msg_error").html(errorsHtml);
    $(".alert-success").hide();
    $(".alert-danger").fadeIn();
}

function slugify(text) {
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
}