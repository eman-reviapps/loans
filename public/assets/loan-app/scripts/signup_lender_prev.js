var FormWizard = function () {
    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }
            function format(state) {
                if (!state.id) return state.text; // optgroup
                return "<img class='flag' src='../../assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
            }

            $('#state').change(function () {
                form.validate().element($(this));
            });
            $('#city').change(function () {
                form.validate().element($(this));
            });

            $(document).on('change', '#institution', function (e) {
                var domain = $(this).children(":selected").attr("data-content");
                $("#institution_domain").text('@' + domain);
            });

            $(document).on('input', '#domain', function (e) {
                $("#institution_domain").text('@' + $(this).val());
                //check if exists or not
                form.validate().element($(this));
            });
            $(document).on('input', '#user_email', function (e) {
                $("#email").val($(this).val() + $("#institution_domain").text());
                form.validate().element($("#email"));
            });

            $(".select2").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                width: 'auto',
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });

            $(".mask_phone").inputmask("mask", {
                "mask": "(999) 999-9999"
            });

            $('.existing-institution-div').show();
            $('.confirm-existing-institution-div').show();

            $('.new-institution-div').hide();
            $('.confirm-new-institution-div').hide();

            $('input[type=radio][name="institution_exists"]').change(function () {
                if ($(this).val() == 1) {
                    $('.existing-institution-div').show();
                    $('.confirm-existing-institution-div').show();

                    $('.new-institution-div').hide();
                    $('.new-institution-div').hide();
                } else {
                    $('.existing-institution-div').hide();
                    $('.confirm-existing-institution-div').hide();

                    $('.new-institution-div').show();
                    $('.confirm-new-institution-div').show();
                }
            });

            function matchCustom(params, data) {


                if ($.trim(params.term) === '') {
                    return data;
                }

                if (typeof data.text === 'undefined') {
                    return null;
                }

                var customParams = params.term ;
                var tempString = "";

                for (var i=0 ; i < customParams.length ; i++){
                    if((customParams.charCodeAt(i) >= 65 && customParams.charCodeAt(i)  <= 90) || (customParams.charCodeAt(i) >= 97 && customParams.charCodeAt(i) <= 122) || customParams.charCodeAt(i)== 32){
                        tempString += customParams[i] ;
                    }
                }

                customParams = data.text ;
                var tempString2 = "";

                for (var i=0 ; i < customParams.length ; i++){
                    if((customParams.charCodeAt(i) >= 65 && customParams.charCodeAt(i)  <= 90) || (customParams.charCodeAt(i) >= 97 && customParams.charCodeAt(i) <= 122) || customParams.charCodeAt(i)== 32){
                        tempString2 += customParams[i] ;
                    }
                }

                tempString = tempString.toLowerCase();
                tempString2 = tempString2.toLowerCase();
                if (tempString2.indexOf(tempString) > -1) {
                    var modifiedData = $.extend({}, data, true);
                    return modifiedData;
                }
                return null;
            }

            $('#institution').select2({
                placeholder:'Search For Institution',
                language: {
                    noResults: function() {
                        return "<a onclick=\'addNewInstitution()\'>Not listed? Add here</a>";
                    }
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                allowClear: true,
                formatResult: format,
                width: 'auto',
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                },
                matcher: matchCustom
            });

            $('.institution-div').hide();
            $("#role").change(function () {

                if ($(this).val() == role_lender) {
                    $('.institution-div').show();
                } else {
                    $('.institution-div').hide();
                }
            });

            var form = $('#signup_lender_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    state: {
                        required: true,
                        remote: {
                            url: app_url + '/states/check_enabled',
                            type: "get",
                            data: {
                                state_code: function () {
                                    return $('#signup_lender_form :input[name="state"]').val();
                                }
                            }
                        }
                    },
                    city: {
                        required: true,
                        remote: {
                            url: app_url + '/states/check_city_enabled',
                            type: "get",
                            data: {
                                state_code: function () {
                                    return $('#signup_lender_form :input[name="state"]').val();
                                },
                                city: function () {
                                    return $('#signup_lender_form :input[name="city"]').val();
                                }
                            }
                        }
                    },
                    institution: {
                        required: true,
                    },
                    title: {
                        required: true,
                        minlength: 3
                    },
                    domain: {
                        required: true,
                        complete_url: true,
                        remote: {
                            url: app_url + '/institutions/check_domain_taken',
                            type: "get",
                            data: {
                                domain: function () {
                                    return $('#signup_lender_form :input[name="domain"]').val();
                                }
                            }
                        }
                    },
                    first_name: {
                        required: true,
                        minlength: 3
                    },
                    last_name: {
                        required: true,
                        minlength: 3
                    },
                    phone: {
                        required: true,
                        phoneUS: true
                    },
                    user_email: {
                        required: true,
                        without_domain: true,
                        remote: {
                            url: app_url + '/users/check_email_exists',
                            type: "get",
                            data: {
                                email: function () {
                                    return $('#signup_lender_form :input[name="email"]').val();
                                }
                            }
                        }
                    },
                    email: {
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    password_confirmation: {
                        equalTo: "#password"
                    },
                },

                messages: {
                    state: {
                        required: messages_validations_array.state.required,
                        remote: messages_validations_array.state.not_served_yet
                    },
                    city: {
                        required: messages_validations_array.city.required,
                        remote: messages_validations_array.city.not_served_yet
                    },
                    institution: {
                        required: messages_validations_array.institution.required,
                    },
                    title: {
                        required: messages_validations_array.title.required,
                        minlength: messages_validations_array.title.min
                    },
                    domain: {
                        required: messages_validations_array.domain.required,
                        complete_url: messages_validations_array.domain.regex,
                        remote: messages_validations_array.domain.unique,
                    },
                    first_name: {
                        required: messages_validations_array.first_name.required,
                        minlength: messages_validations_array.first_name.min
                    },
                    last_name: {
                        required: messages_validations_array.last_name.required,
                        minlength: messages_validations_array.last_name.min
                    },
                    phone: {
                        required: messages_validations_array.phone.required,
                        phoneUS: messages_validations_array.phone.regex
                    },
                    user_email: {
                        required: messages_validations_array.email.required,
                        without_domain: messages_validations_array.email.without_domain,
                        remote: messages_validations_array.email.unique
                    },
                    email: {
                        required: messages_validations_array.email.required,
                        email: messages_validations_array.email.email,
                    },
                    password: {
                        required: messages_validations_array.password.required,
                        minlength: messages_validations_array.password.min,
                    },
                    password_confirmation: {
                        equalTo: messages_validations_array.password_confirmation.confirmed
                    },
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                            .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

            });

            var displayConfirm = function () {
                $('#tab_confirm .form-control-static', form).each(function () {
                    var input = $('[name="' + $(this).attr("data-display") + '"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="' + $(this).attr("data-display") + '"]:checked', form);
                    }

                    if (input.is(":hidden") || input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment[]') {
                        var payment = [];
                        $('[name="payment[]"]:checked', form).each(function () {
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }

            var handleTitle = function (tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#signup_lender_form_wizard')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#signup_lender_form_wizard')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#signup_lender_form_wizard').find('.button-previous').hide();
                } else {
                    $('#signup_lender_form_wizard').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#signup_lender_form_wizard').find('.button-next').hide();
                    $('#signup_lender_form_wizard').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#signup_lender_form_wizard').find('.button-next').show();
                    $('#signup_lender_form_wizard').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title'));
            }

            // default form wizard
            $('#signup_lender_form_wizard').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    return false;

                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, clickedIndex);
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#signup_lender_form_wizard').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#signup_lender_form_wizard').find('.button-previous').hide();
            $('#signup_lender_form_wizard .button-submit').click(function () {
                form.submit();
            }).hide();

            //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('#country_list', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
        }

    };

}();

jQuery(document).ready(function () {
    FormWizard.init();
});