var Login = function () {

    var handleLogin = function () {

        $('.login-form').validate({
            errorClass: "font-red-sunglo",
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6
                }
            },

            messages: {
                email: {
                    required: messages_validations_array.email.required,
                    email: messages_validations_array.email.email
                },
                password: {
                    required: messages_validations_array.password.required,
                    minlength: messages_validations_array.password.min,
                }
            }
        });

        $('.login-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    }
    return {
        //main function to initiate the module
        init: function () {

            handleLogin();

        }

    };

}();

jQuery(document).ready(function () {
    Login.init();
});