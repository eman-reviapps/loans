validation_array = {
    errorClass: validation_error_class,
    ignore: [],
    rules: {
        title: {
            minlength: 3
        },
        zip_code: {
            zipcodeUS: true
        },
        phone: {
            phoneUS: true
        }
    },
    messages: {
        title: {
            minlength: messages_validations_array.title.min
        },
        zip_code: {
            required: messages_validations_array.zip_code.regex
        },
        phone: {
            phoneUS: messages_validations_array.phone.regex
        }
    }
};
create_validation_array = {
    errorClass: validation_error_class,
    ignore: [],
    rules: {
        title: {
            minlength: 3
        },
        domain: {
            required: true,
            complete_url: true
        },
        status: {
            required: true,
        },
        zip_code: {
            zipcodeUS: true
        },
        phone: {
            phoneUS: true
        }
    },
    messages: {
        title: {
            minlength: messages_validations_array.title.min
        },
        domain: {
            required: messages_validations_array.domain.required,
            complete_url: messages_validations_array.domain.regex,
        },
        status: {
            required: messages_validations_array.status.required,
        },
        zip_code: {
            required: messages_validations_array.zip_code.regex
        },
        phone: {
            phoneUS: messages_validations_array.phone.regex
        }
    }
};

$(document).ready(function () {
    $("#createInstitutionBranchForm").validate(validation_array);
    $("#editInstitutionBranchForm").validate(validation_array);
    $(".select2").select2();
});
