var filter_form = $("#filterForm");

var InstitutionsClass = function () {

    var handleInstitutions = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });

        $('select').change(function () {
            filter();
        });

    }

    function filter()
    {
        var url = '?' + filter_form.serialize();

        $.get(url, function (data) {
            $("#FilteredData").children().remove();
            $("#FilteredData").html(data);
            TableDatatablesButtons.init();
        });
    }

    function getDataURL() {
        return '?' + filter_form.serialize();
    }

    return {
        //main function to initiate the module
        init: function () {
            handleInstitutions();
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        InstitutionsClass.init();
    });
}