var FormWizard = function () {
    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }
            function format(state) {
                if (!state.id) return state.text; // optgroup
                return "<img class='flag' src='../../assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
            }

            $('.mt-repeater').each(function () {
                $(this).repeater({
                    show: function () {
                        $(this).slideDown();
                        $(".select2").select2({
                            placeholder: "Select",
                            allowClear: true,
                            formatResult: format,
                            width: 'auto',
                            formatSelection: format,
                            escapeMarkup: function (m) {
                                return m;
                            }
                        });
                        $(".mask_number").inputmask({
                            "mask": "9",
                            "repeat": 5,
                            "greedy": false
                        });

                    },

                    hide: function (deleteElement) {
                        if (confirm('Are you sure you want to delete this element?')) {
                            $(this).slideUp(deleteElement);
                        }
                    },

                    ready: function (setIndexes) {

                    }

                });
            });
            $(".select2").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                width: 'auto',
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });
            $(".mask_number").inputmask({
                "mask": "9",
                "repeat": 5,
                "greedy": false
            });

            $(".mask_phone").inputmask("mask", {
                "mask": "(999) 999-9999"
            });

            $('.existing-institution-div').show();
            $('.confirm-existing-institution-div').show();

            $('.new-institution-div').hide();
            $('.confirm-new-institution-div').hide();

            $('input[type=radio][name="institution_exists"]').change(function () {
                if ($(this).val() == 1) {
                    $('.existing-institution-div').show();
                    $('.confirm-existing-institution-div').show();

                    $('.new-institution-div').hide();
                    $('.new-institution-div').hide();
                } else {
                    $('.existing-institution-div').hide();
                    $('.confirm-existing-institution-div').hide();

                    $('.new-institution-div').show();
                    $('.confirm-new-institution-div').show();
                }
            });

            $('.institution-div').hide();
            $("#role").change(function () {

                if ($(this).val() == role_lender) {
                    $('.institution-div').show();
                } else {
                    $('.institution-div').hide();
                }
            });

            var form = $('#signup_borrower_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: app_url + '/users/check_email_exists',
                            type: "get",
                            data: {
                                email: function () {
                                    return $('#signup_borrower_form :input[name="email"]').val();
                                }
                            }
                        }
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    password_confirmation: {
                        equalTo: "#password"
                    },
                    first_name: {
                        required: true,
                        minlength: 3
                    },
                    last_name: {
                        required: true,
                        minlength: 3
                    },
                    state: {
                        required: true,
                    },
                    city: {
                        required: true,
                    },
                    zip_code: {
                        required: true,
                        remote: {
                            url: app_url + '/states/check_valid_zip_code',
                            type: "get",
                            data: {
                                zip_code: function () {
                                    return $("#" + form.attr("id") + " #zip_code").find(":selected").text();
                                    //return $("#" + form.attr("id") + " input[name=zip_code]").val();
                                },
                                city: function () {
                                    return $("#" + form.attr("id") + " #city").find(":selected").text();
                                },
                                state: function () {
                                    return $("#" + form.attr("id") + " #state").find(":selected").text();;
                                }
                            }
                        }
                    },
                    address: {
                        required: true,
                        minlength: 3
                    },
                    apt_suite_no: {
                        required: false,
                        alphanumeric: true,
                    },
                    phone: {
                        required: true,
                        phoneUS: true
                    },
                    properties_owned_no: {
                        required: true,
                        number: true
                    },
                },

                messages: {
                    email: {
                        required: messages_validations_array.email.required,
                        email: messages_validations_array.email.email,
                        remote: messages_validations_array.email.unique
                    },
                    password: {
                        required: messages_validations_array.password.required,
                        minlength: messages_validations_array.password.min,
                    },
                    password_confirmation: {
                        equalTo: messages_validations_array.password_confirmation.confirmed
                    },
                    first_name: {
                        required: messages_validations_array.first_name.required,
                        minlength: messages_validations_array.first_name.min
                    },
                    last_name: {
                        required: messages_validations_array.last_name.required,
                        minlength: messages_validations_array.last_name.min
                    },
                    state: {
                        required: messages_validations_array.state.required,
                    },
                    city: {
                        required: messages_validations_array.city.required,
                    },
                    zip_code: {
                        required: messages_validations_array.zip_code.required,
                        remote: messages_validations_array.zip_code.not_served_yet
                    },
                    address: {
                        required: messages_validations_array.address.required,
                        minlength: messages_validations_array.address.min,
                    },
                    apt_suite_no: {
                        required: messages_validations_array.apt_suite_no.required,
                        number: messages_validations_array.apt_suite_no.numeric,
                    },
                    phone: {
                        required: messages_validations_array.phone.required,
                        phoneUS: messages_validations_array.phone.regex
                    },
                    properties_owned_no: {
                        required: messages_validations_array.properties_owned_no.required,
                        number: messages_validations_array.properties_owned_no.numeric
                    },
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                            .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

            });

            var displayConfirm = function () {
                $('#tab_confirm .form-control-static', form).each(function () {
                    var input = $('[name="' + $(this).attr("data-display") + '"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="' + $(this).attr("data-display") + '"]:checked', form);
                    }

                    if (input.is(":hidden") || input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment[]') {
                        var payment = [];
                        $('[name="payment[]"]:checked', form).each(function () {
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }

            var handleTitle = function (tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#signup_borrower_form_wizard')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#signup_borrower_form_wizard')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#signup_borrower_form_wizard').find('.button-previous').hide();
                } else {
                    $('#signup_borrower_form_wizard').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#signup_borrower_form_wizard').find('.button-next').hide();
                    $('#signup_borrower_form_wizard').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#signup_borrower_form_wizard').find('.button-next').show();
                    $('#signup_borrower_form_wizard').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title'));
            }

            // default form wizard
            $('#signup_borrower_form_wizard').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    return false;

                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, clickedIndex);
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#signup_borrower_form_wizard').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#signup_borrower_form_wizard').find('.button-previous').hide();
            $('#signup_borrower_form_wizard .button-submit').click(function () {
                form.submit();
            }).hide();

            //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('#country_list', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
        }

    };

}();

jQuery(document).ready(function () {
    Address.init();
    FormWizard.init();
});