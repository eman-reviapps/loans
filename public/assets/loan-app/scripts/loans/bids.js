var delete_modal = $('#deleteModal');
var delete_form = $('#deleteForm');

var table = $('#bids-table').DataTable({
    processing: true,
    serverSide: true,
    bInfo: false,
    bLengthChange: false,
    ajax: getFilterQuery(),
    columns: [
        {data: 'borrower_name', name: 'borrower_name'},
        {data: 'loan_date', name: 'loan_date'},
        {data: 'location', name: 'location'},
        {data: 'loan_type', name: 'loan_type'},
        {data: 'requested_amount', name: 'requested_amount'},
        {data: 'bid_status', name: 'bid_status'},
        {data: 'created_at', name: 'created_at'},
        {data: 'lender_name', name: 'lender_name'},
        {data: 'action', name: 'action', searchable: false, orderable: false, width: '75px'},
    ],
    "fnDrawCallback": function () {

    }
});


function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
function getFilterQuery() {
    var status = getQueryStringValue('status');
    var ajax_url = '?';
    if (status) {
        ajax_url += "&status=" + status;
    }
    return ajax_url;
}