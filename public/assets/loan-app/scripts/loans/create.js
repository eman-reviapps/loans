var CreateLoan = function () {
    var bindControls = function(){
        $(".select2").select2();


        $('.mt-repeater').each(function () {
            $(this).repeater({
                show: function () {
                    $(this).slideDown();

                    $(".select2").select2();

                    $(".mask_number").inputmask({
                        "mask": "9",
                        "repeat": 5,
                        "greedy": false
                    });

                },

                hide: function (deleteElement) {
                    if (confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                    }
                },

                ready: function (setIndexes) {

                }

            });
        });
    }

    var loadLoanTemplate = function () {

        $("#loan_type_slug").val($("#loan_type").val())
        var form = $("#loan_type").parents('form:first');

        $.ajax({
            type: form.attr("method"),
            url: form.attr("action"),
            data: form.serialize(),
            success: function (data) {
                $("#template").html(data);
                bindControls();
            },
            error: function (data) {
                console.log(data)
                // onError(data);
            }
        });

    }

    var handleCreate = function () {

        bindControls();

        loadLoanTemplate();

        $(".loan_type").on('change', function () {
            loadLoanTemplate()
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleCreate();
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        CreateLoan.init();
    });
}
