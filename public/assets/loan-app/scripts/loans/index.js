var delete_modal = $('#deleteModal');
var delete_form = $('#deleteForm');

$(document).ready(function () {
    $('.form-horizontal .modal-body .feedback_description').hide();

    delete_form.validate({
        errorElement: 'span',
        errorClass: "font-red-sunglo",
        rules: {
            id: {
                required: true,
            },
            reason: {
                required: true,
            },
            description: {
                required: function (element) {
                    return $('input[name=reason]:checked', '#deleteForm').val() == 'OTHER';
                },
                minlength: 3
            }
        },

        messages: {
            id: {
                required: messages_validations_array.email.required,
            },
            reason: {
                required: messages_validations_array.email.required,
            },
            description: {
                required: messages_validations_array.email.required,
                minlength: messages_validations_array.password.min,
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.parents('.mt-radio-list') || element.parents('.mt-checkbox-list')) {
                if (element.parents('.mt-radio-list')[0]) {
                    error.appendTo(element.parents('.mt-radio-list')[0]);
                }
                if (element.parents('.mt-checkbox-list')[0]) {
                    error.appendTo(element.parents('.mt-checkbox-list')[0]);
                }
            } else if (element.parents('.mt-radio-inline') || element.parents('.mt-checkbox-inline')) {
                if (element.parents('.mt-radio-inline')[0]) {
                    error.appendTo(element.parents('.mt-radio-inline')[0]);
                }
                if (element.parents('.mt-checkbox-inline')[0]) {
                    error.appendTo(element.parents('.mt-checkbox-inline')[0]);
                }
            } else if (element.parent(".input-group").size() > 0) {
                error.insertAfter(element.parent(".input-group"));
            } else if (element.attr("data-error-container")) {
                error.appendTo(element.attr("data-error-container"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

    });
});

$(document).on('change', '.reason', function (e) {
    var reason = $(this).val();
    var form = $(this).parents('form:first');

    if (reason == 'OTHER') {
        $('.form-horizontal .modal-body .feedback_description').show();
    } else {
        $('.form-horizontal .modal-body .feedback_description').hide();
    }

});

var table = $('#loans-table').DataTable({
    processing: true,
    serverSide: true,
    bInfo: false,
    bLengthChange: false,
    ajax: getFilterQuery(),
    order: [
        [0, 'desc']
    ],
    columns: [
        {data: 'created_at', name: 'created_at'},
        {data: 'name', name: 'name'},
        {data: 'requested_amount', name: 'requested_amount'},
        {data: 'status', name: 'status'},
        {data: 'action', name: 'action', searchable: false, orderable: false, width: '10%'},
    ],
    "fnDrawCallback": function () {
        $('.delete').on('click', function (e) {

            delete_modal.modal('show');

            delete_form.attr("action", $(this).data('action'));

            $("#" + delete_form.attr("id") + " input[name=id]").val($(this).data('id'));

            e.preventDefault();
        });
    }
});

$(document).on('click', '.delete_loan_btn', function (e) {

    if (delete_form.valid()) {
        deleteDataAjax();
    }
});

function deleteDataAjax() {
    var method = $("#" + delete_form.attr("id") + " input[name=_method]").val();
    var id = $("#" + delete_form.attr("id") + " input[name=id]").val();

    $.ajax({
        type: method,
        url: delete_form.attr("action"),
        data: delete_form.serialize(),
        dataType: 'json',
        success: function (data) {
            if (data.level == "success") {

                if (typeof table === 'undefined') {
                    $('tr#' + id).remove();
                } else {
                    table.row('#' + id).remove().draw(false); // this line when use data table only
                }
                swal({
                    title: "Deleted",
                    type: "success",
                    confirmButtonText: "Close"
                });

                $('.form-horizontal .modal-body .feedback_description').hide();
                $('#description').val('');
                $('input[name="reason"]').prop('checked', false);

                delete_modal.modal('hide');

            } else {
                swal({
                    title: "Failed to delete",
                    type: "error",
                    confirmButtonText: "Close"
                });
            }
        },
        error: function () {
            swal({
                title: "Failed to delete",
                type: "error",
                confirmButtonText: "Close"
            });
        },
    });
}

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
function getFilterQuery() {
    var status = getQueryStringValue('status');
    var ajax_url = '?';
    if (status) {
        ajax_url += "&status=" + status;
    }
    return ajax_url;
}