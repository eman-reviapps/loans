function archive(action, csrf) {
    $.ajax({
        url: action,
        type: 'PUT',
        data: {_token: csrf},
        dataType: 'json',
        async: false,
        success: function (data) {
            if (data.level == "success") {
                onArchiveSuccess();
            } else {
                onArchiveFail();
            }
        },
        error: function () {
            onArchiveFail();
        },
    });
}

function restore(action, csrf) {
    $.ajax({
        url: action,
        type: 'PUT',
        data: {_token: csrf},
        dataType: 'json',
        async: false,
        success: function (data) {
            if (data.level == "success") {
                onRestoreSuccess();

            } else {
                onRestoreFail();
            }
        },
        error: function () {
            onRestoreFail();
        },
    });
}