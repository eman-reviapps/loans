var validator;
var current_form;
var myDropZone;

$.fn.digits = function () {
    return this.each(function () {
        $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    })
}

function handleCurrencyInputs()
{
    $("#" + current_form + " input.mask_money").each(function () {
        this.value = this.value.replace(/[^0-9\.-]+/g, "");
    });
}

var CreateLoan = function () {

    var prepareValidationArray = function () {

        var validationObject = getLoanValidationObject(current_form, $("#loan_type").val());

        if (validator) {
            validator.resetForm();
            $("#" + current_form).find(".error").removeClass("error");
            validator.destroy();
        }

        validator = $("#" + current_form).validate(validationObject)
    }

    var loadPreferences = function (load_default) {

        elem = $("#" + current_form).find(" #manual-ajax");

        if (load_default)
            loan_id = null;
        else
            loan_id = elem.data('loan_id');

        $.ajax({
            type: 'GET',
            url: elem.data('action'),
            data: {
                _token: elem.data('csrf'),
                key_by: elem.data('key_by'),
                profile_id: elem.data('profile_id'),
                loan_id: loan_id,
                user_id: elem.data('user_id')
            },
            success: function (data) {
                $("#" + current_form).find(".modal-body").find('#data').html(data);

                $(".select2-multiple").select2({
                    placeholder: 'Select',
                    width: null
                });
            },
            error: function (data) {
                alert('failed')
            }
        });
    };

    var showHide = function (elem, value) {
        if (value == 1) {
            $("#" + current_form + " input[name=" + elem + "]").show();
        }
        else if (value == 0) {
            $("#" + current_form + " input[name=" + elem + "]").hide();
        }
    };

    var resetDropzone = function () {

        Dropzone.autoDiscover = false;

        myDropZone = $("#my-dropzone").dropzone({
            dictDefaultMessage: "",
            url: app_url + '/borrower/loans/upload_file',
            // autoProcessQueue: false,
            maxFiles: 10,
            init: function () {
                var wrapperThis = this;

                this.on("addedfile", function (file) {
                    // Create the remove button
                    var removeButton = Dropzone.createElement("<a href='javascript:;'' class='btn red btn-sm btn-block'>Remove</a>");

                    // Capture the Dropzone instance as closure.
                    var _this = this;

                    // Listen to the click event
                    removeButton.addEventListener("click", function (e) {
                        // Make sure the button click doesn't submit the form:
                        e.preventDefault();
                        e.stopPropagation();

                        // Remove the file preview.
                        _this.removeFile(file);
                        // If you want to the delete the file on the server as well,
                        // you can do the AJAX request here.
                    });

                    // Add the button to the file preview element.
                    file.previewElement.appendChild(removeButton);
                });
                this.on("success", function (file, reposnse) {
                    
                    alert("File "+ file.name+" uploaded successfully")
                    // swal({
                    //     title: "File uploaded successfully",
                    //     type: "info",
                    //     confirmButtonText: "Close"
                    // });
                });
                this.on("maxfilesexceeded", function (file) {
                    alert('Max files exceeded (10)')
                    // swal({
                    //     title: "Max files exceeded (10)",
                    //     type: "error",
                    //     confirmButtonText: "Close"
                    // });
                });
                this.on("error", function (file, errorMessage) {

                    alert('Failed to upload file '+file.name)
                    // swal({
                    //     title: "Failed to upload file",
                    //     type: "error",
                    //     confirmButtonText: "Close"
                    // });

                    this.removeFile(file);
                });
            }
        });
    };

    var bindControls = function () {

        $(document).on('click', '.reset_default_preferneces', function (e) {
            loadPreferences(true);
        });

        loadPreferences(false);

        $(document).on('input', '#purchase_price', function (e) {
            var requested_percent = $("#" + current_form + " input[name=requested_percent]").val();
            var less_trade =  $("#" + current_form + " input[name=less_trade]").val().replace(/[^0-9\.-]+/g, "");
            var less_down_payment = $("#" + current_form + " input[name=less_down_payment]").val().replace(/[^0-9\.-]+/g, "");

            var purchase_price = $(this).val().replace(/[^0-9\.-]+/g, "");

            if (requested_percent) {
                var requested_amount = (purchase_price * (requested_percent / 100)).toFixed(0);
                $("#" + current_form + " input[name=requested_amount]").unmask().val(requested_amount)
            }
            else
            {
                var requested_amount = purchase_price - less_trade - less_down_payment;
                $("#" + current_form + " input[name=requested_amount]").unmask().val(requested_amount)
            }
        });

        $(document).on('input', '#requested_amount', function (e) {
            var requested_amount = $(this).val().replace(/[^0-9\.-]+/g, "");
            var purchase_price = $("#" + current_form + " input[name=purchase_price]").val().replace(/[^0-9\.-]+/g, "");

            if ( purchase_price ) {
                var requested_percent = ((requested_amount / purchase_price) * 100).toFixed(2);

                $("#" + current_form + " input[name=requested_percent]").val(requested_percent)
            }
        });

        $(document).on('input', '#requested_percent', function (e) {
            var requested_percent = $(this).val();
            var purchase_price = $("#" + current_form + " input[name=purchase_price]").val().replace(/[^0-9\.-]+/g, "");

            if (purchase_price) {
                var requested_amount = (purchase_price * (requested_percent / 100)).toFixed(0);

                $("#" + current_form + " input[name=requested_amount]").unmask().val(requested_amount)
            }
        });

        $(document).on('input', '#less_trade', function (e) {
            var purchase_price = $("#" + current_form + " input[name=purchase_price]").val().replace(/[^0-9\.-]+/g, "");
            var less_trade = $(this).val().replace(/[^0-9\.-]+/g, "");
            var less_down_payment = $("#" + current_form + " input[name=less_down_payment]").val().replace(/[^0-9\.-]+/g, "");

            var requested_amount = purchase_price - less_trade - less_down_payment;
            $("#" + current_form + " input[name=requested_amount]").unmask().val(requested_amount)

        });
        $(document).on('input', '#less_down_payment', function (e) {
            var purchase_price = $("#" + current_form + " input[name=purchase_price]").val().replace(/[^0-9\.-]+/g, "");
            var less_trade = $("#" + current_form + " input[name=less_trade]").val().replace(/[^0-9\.-]+/g, "");
            var less_down_payment = $(this).val().replace(/[^0-9\.-]+/g, "");

            var requested_amount = purchase_price - less_trade - less_down_payment;
            $("#" + current_form + " input[name=requested_amount]").unmask().val(requested_amount)

        });

        $(document).on('input', '#gross_annual_revenue', function (e) {
            var gross_annual_revenue = $(this).val().replace(/[^0-9\.-]+/g, "");
            var estimated_annual_expenses = $("#" + current_form + " input[name=estimated_annual_expenses]").val().replace(/[^0-9\.-]+/g, "");

            var estimated_net_operating_income = gross_annual_revenue - estimated_annual_expenses;
            $("#" + current_form + " input[name=estimated_net_operating_income]").unmask().val(estimated_net_operating_income)
            $("#" + current_form + " input[name=net_operating_income]").unmask().val(estimated_net_operating_income)

        });
        $(document).on('input', '#estimated_annual_expenses', function (e) {
            var gross_annual_revenue = $("#" + current_form + " input[name=gross_annual_revenue]").val().replace(/[^0-9\.-]+/g, "");
            var estimated_annual_expenses = $(this).val().replace(/[^0-9\.-]+/g, "");

            var estimated_net_operating_income = gross_annual_revenue - estimated_annual_expenses;
            $("#" + current_form + " input[name=estimated_net_operating_income]").unmask().val(estimated_net_operating_income)
            $("#" + current_form + " input[name=net_operating_income]").unmask().val(estimated_net_operating_income)

        });

        $(document).on('click', '.btn_loan_save', function (e) {
                var form = $(this).parents('form:first');

                $("#" + current_form + " input[name=save_continue]").val('no')
                $("#" + current_form + " input[name=loan_status]").val('LIVE');

                $("#" + current_form + " input[name^='files']").remove()


                if ($("#" + current_form).valid()) {

                    if (myDropZone) {
                        Dropzone.forElement("#my-dropzone").files.forEach(function (file) {
                            if (file.status) {

                                $("#" + current_form).append('<input type="hidden" name="files[]" value="' + file.xhr.response + '" />');
                            }
                        });
                    }
                }

                handleCurrencyInputs();

                submitForm(form);
            }
        );
        $(document).on('click', '.btn_loan_save_draft', function (e) {
            var form = $(this).parents('form:first');

            $("#" + current_form + " input[name=save_continue]").val('no')
            $("#" + current_form + " input[name=loan_status]").val('DRAFT')

            $("#" + current_form + " input[name^='files']").remove()


            if ($("#" + current_form).valid()) {

                if (myDropZone) {
                    Dropzone.forElement("#my-dropzone").files.forEach(function (file) {
                        if (file.status) {

                            $("#" + current_form).append('<input type="text" name="files[]" value="' + file.xhr.response + '" />');
                        }
                    });
                }
            }

            handleCurrencyInputs();

            submitForm(form);
        });

        $(document).on('click', '.btn_loan_save_continue', function (e) {
            var form = $(this).parents('form:first');

            $("#" + current_form + " input[name=save_continue]").val('yes')

            handleCurrencyInputs();

            submitForm(form);
        });

        $(document).on('click', '.btn_loan_publish', function (e) {
            var form = $(this).parents('form:first');

            $("#" + form.attr("id") + " input[name=status]").val('LIVE');

            handleCurrencyInputs();
            
            submitForm(form);
        });

        $("#" + current_form + " input[name=loan_type_slug]").val($("#loan_type").val())

        $(".select2").select2();

        $(".mask_number").inputmask({
            "mask": "9",
            "repeat": 5,
            "greedy": false
        });

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true
            });
        }

        $("#" + current_form + ' .mt-repeater').each(function () {
            $(this).repeater({
                show: function () {
                    $(this).slideDown();

                    if (jQuery().datepicker) {
                        $('.date-picker').datepicker({
                            rtl: App.isRTL(),
                            orientation: "left",
                            autoclose: true
                        });
                    }

                },

                hide: function (deleteElement) {
                    if (confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                    }
                },

                ready: function (setIndexes) {

                }

            });
        });

        showHide("current_lease_expense", is_moving_from_leased);
        showHide("outstanding_value", is_loan_outstanding);

        $("#" + current_form + ' input[type=radio][name=is_moving_from_leased]').change(function () {
            showHide("current_lease_expense", this.value);
        });
        $("#" + current_form + ' input[type=radio][name=is_loan_outstanding]').change(function () {
            showHide("outstanding_value", this.value);
        });

        if ($(".loan_type").val() == 'LINE_OF_CREDIT') {

            $("#" + current_form + ' input[name=purpose]').hide();
        }

        $("#" + current_form + ' select[name=purpose_type_id]').on('change', function () {

            if ($("#" + current_form + ' select[name=purpose_type_id] option:selected').text() == 'Other') {
                $("#" + current_form + ' input[name=purpose]').show();
            }
            else {
                $("#" + current_form + ' input[name=purpose]').hide();
            }
        });
        if (myDropZone)
            Dropzone.forElement("#my-dropzone").removeAllFiles(true);

        // $('.mask_money').maskMoney({placeholder: "$000,000,000"});
        // $('.mask_money').mask('000,000,000', {placeholder: "$000,000,000",reverse: true,greedy: true});

        $('.mask_money').mask('#,##0', {placeholder: "$000,000,000",reverse: true});
        //
        // $(".building_sq_ft").inputmask({
        //     "mask": "9",
        //     "repeat": 10,
        //     "greedy": false
        // });

        // $(".mask_money").inputmask('999,999,999', {
        //     numericInput: true,
        //     greedy: false
        // });

        Address.init();
    }

    var loadLoanTemplate = function () {

        $('[id*="template_"]').hide()
        $('#template_' + $(".loan_type").val()).show();

        current_form = 'form_' + $(".loan_type").val();

        prepareValidationArray();

        bindControls();
    }

    var handleCreate = function () {

        if (loan_type) {
            $(".loan_type").val(loan_type);
        }
        loadLoanTemplate();

        $(".loan_type").on('change', function () {

            loadLoanTemplate();
        });
    }

    return {
        //main function to initiate the module
        init: function () {

            handleCreate();

            resetDropzone();
        }
    };

}
();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        CreateLoan.init();
    });
}
