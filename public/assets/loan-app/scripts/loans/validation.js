var the_form;

var getRules = function (attributes, required) {
    var rules = {};
    var messages = {};

    attributes.forEach(function (element) {
        switch (element) {
            case 'requested_amount':
                if ($('#purchase_price').is(":visible")) {
                    rules[element] = required_numeric_less_than_validation(element, '#purchase_price').rules;
                    messages[element] = required_numeric_less_than_validation(element, '#purchase_price').messages;
                } else {
                    rules[element] = required_numeric_validation(element, '#purchase_price').rules;
                    messages[element] = required_numeric_validation(element, '#purchase_price').messages;
                }
                break;
            case 'requested_percent':
                rules[element] = required_numeric_percent_validation(element, 'requested_amount').rules;
                messages[element] = required_numeric_percent_validation(element, 'requested_amount').messages;
                break;
            case 'purpose_type_id':
                rules[element] = required_validation(element).rules;
                messages[element] = required_validation(element).messages;
                break;
            case 'purchase_price':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'purpose':
                rules[element] = purpose_required_validation(element).rules;
                messages[element] = required_validation(element).messages;
                break;
            case 'receivable_range_from':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'receivable_range_to':
                rules[element] = required_numeric_greater_than_validation(element, '#receivable_range_from').rules;
                messages[element] = required_numeric_greater_than_validation(element, '#receivable_range_from').messages;
                break;
            case 'inventory_range_from':
                rules[element] = numeric_validation(element).rules;
                messages[element] = numeric_validation(element).messages;
                break;
            case 'inventory_range_to':
                rules[element] = numeric_greater_than_validation(element, '#inventory_range_from').rules;
                messages[element] = numeric_greater_than_validation(element, '#inventory_range_from').messages;
                break;
            case 'current_ratio':
                rules[element] = numeric_validation(element).rules;
                messages[element] = numeric_validation(element).messages;
                break;
            case 'state':
                rules[element] = required_validation(element).rules;
                messages[element] = required_validation(element).messages;
                break;
            case 'city':
                rules[element] = required_validation(element).rules;
                messages[element] = required_validation(element).messages;
                break;
            case 'zip_code':
                rules[element] = zip_code_validation(element).rules;
                messages[element] = zip_code_validation(element).messages;
                break;
            case 'address':
                rules[element] = required_validation(element).rules;
                messages[element] = required_validation(element).messages;
                break;
            case 'apt_suite_no':
                rules[element] = required_digits_validation(element).rules;
                messages[element] = required_digits_validation(element).messages;
                break;
            case 'real_estate_type_id':
                rules[element] = required_validation(element).rules;
                messages[element] = required_validation(element).messages;
                break;
            case 'annual_income_from_tenants':
                rules[element] = numeric_validation(element).rules;
                messages[element] = numeric_validation(element).messages;
                break;
            case 'estimated_property_value':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'building_sq_ft':
                rules[element] = building_sq_ft_validation(element).rules;
                messages[element] = building_sq_ft_validation(element).messages;
                break;
            case 'buildings_no':
                rules[element] = buildings_no_validation(element).rules;
                messages[element] = buildings_no_validation(element).messages;
                break;
            case 'units_no':
                rules[element] = units_no_validation(element).rules;
                messages[element] = units_no_validation(element).messages;
                break;
            case 'owner_occupied_percent':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'owner_not_occupied_percent':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'gross_annual_revenue':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'estimated_market_price':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'estimated_annual_expenses':
                rules[element] = numeric_validation(element).rules;
                messages[element] = numeric_validation(element).messages;
                break;
            case 'estimated_net_operating_income':
                rules[element] = numeric_validation(element).rules;
                messages[element] = numeric_validation(element).messages;
                break;
            case 'net_operating_income':
                rules[element] = numeric_validation(element).rules;
                messages[element] = numeric_validation(element).messages;
                break;
            case 'current_lease_expense':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'company_EBITDA':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'total_liabilities':
                if (required && required[element]) {
                    rules[element] = required_numeric_validation(element).rules;
                    messages[element] = required_numeric_validation(element).messages;

                } else {
                    rules[element] = numeric_validation(element).rules;
                    messages[element] = numeric_validation(element).messages;
                }
                break;
            case 'funded_debt':
                if (required && required[element]) {
                    rules[element] = required_numeric_validation(element).rules;
                    messages[element] = required_numeric_validation(element).messages;

                } else {
                    rules[element] = numeric_validation(element).rules;
                    messages[element] = numeric_validation(element).messages;
                }
                break;
            case 'tangible_net_worth':
                if (required && required[element]) {
                    rules[element] = required_numeric_validation(element).rules;
                    messages[element] = required_numeric_validation(element).messages;

                } else {
                    rules[element] = numeric_validation(element).rules;
                    messages[element] = numeric_validation(element).messages;
                }
                break;
            case 'outstanding_value':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'cash_liquid_investments':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'rental_annual_revenue':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'rental_revenue_by_tenants':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'equipment_type':
                rules[element] = required_validation(element).rules;
                messages[element] = required_validation(element).messages;
                break;
            case 'vendor_name':
                rules[element] = required_validation(element).rules;
                messages[element] = required_validation(element).messages;
                break;
            case 'equipment_purpose':
                rules[element] = required_validation(element).rules;
                messages[element] = required_validation(element).messages;
                break;
            case 'equipment_description':
                rules[element] = required_validation(element).rules;
                messages[element] = required_validation(element).messages;
                break;
            case 'equipment_finance_type':
                rules[element] = required_validation(element).rules;
                messages[element] = required_validation(element).messages;
                break;
            case 'less_trade':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            case 'less_down_payment':
                rules[element] = required_numeric_validation(element).rules;
                messages[element] = required_numeric_validation(element).messages;
                break;
            default:
                break;
        }
    });

    return {
        errorElement: 'span', //default input error message container
        errorClass: validation_error_class,
        ignore: ":not(:visible)",
        rules: rules,
        messages: messages,

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            if (cont) {
                cont.after(error);
            } else {
                element.after(error);
            }
        },

        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
    };
};

var getLoanValidationObject = function (form_name, loan_type) {

    var validation_array;

    the_form = form_name;

    switch (loan_type) {
        case 'LINE_OF_CREDIT':
            validation_array = getRules([
                'requested_amount',
                'purpose_type_id',
                'receivable_range_from',
                'receivable_range_to',
                'inventory_range_from',
                'inventory_range_to',
                'current_ratio',
            ]);

            break;
        case 'RE_PURCHASE_INVESTOR_0_50':

            validation_array = getRules([
                'state',
                'city',
                'zip_code',
                'address',
                'apt_suite_no',
                'real_estate_type_id',
                'purchase_price',
                'requested_amount',
                'requested_percent',
                'building_sq_ft',
                'buildings_no',
                'units_no',
                'owner_occupied_percent',
                'gross_annual_revenue',
                'estimated_annual_expenses',
                'estimated_net_operating_income',
            ]);

            break;

        case 'RE_PURCHASE_OWNER_51_100':
            validation_array = getRules([
                'state',
                'city',
                'zip_code',
                'address',
                'apt_suite_no',
                'real_estate_type_id',
                'purchase_price',
                'requested_amount',
                'requested_percent',
                'building_sq_ft',
                'buildings_no',
                'units_no',
                'annual_income_from_tenants',
                'owner_occupied_percent',
                'current_lease_expense',
            ]);
            break;

        case 'RE_CASH_OUT_INVESTOR_0_50':
            validation_array = getRules([
                'state',
                'city',
                'zip_code',
                'address',
                'apt_suite_no',
                'real_estate_type_id',
                'estimated_property_value',
                'requested_amount',
                'purpose',
                'building_sq_ft',
                'buildings_no',
                'units_no',
                'outstanding_value',
                'owner_occupied_percent',
                'gross_annual_revenue',
                'estimated_annual_expenses',
                'net_operating_income',
            ]);
            break;

        case 'RE_CASH_OUT_OWNER_51_100':
            validation_array = getRules([
                'state',
                'city',
                'zip_code',
                'address',
                'apt_suite_no',
                'real_estate_type_id',
                'estimated_property_value',
                'requested_amount',
                'purpose',
                'building_sq_ft',
                'buildings_no',
                'units_no',
                'outstanding_value',
                'annual_income_from_tenants',
            ]);
            break;
        case 'RE_REFINANCE_INVESTOR_0_50':
            validation_array = getRules([
                'state',
                'city',
                'zip_code',
                'address',
                'apt_suite_no',
                'real_estate_type_id',
                'requested_amount',
                'estimated_market_price',
                'outstanding_value',
                'purpose',
                'building_sq_ft',
                'buildings_no',
                'units_no',
                'owner_occupied_percent',
                'gross_annual_revenue',
                'estimated_annual_expenses',
                'net_operating_income',
            ]);
            break;
        case 'RE_REFINANCE_OWNER_51_100':
            validation_array = getRules([
                'state',
                'city',
                'zip_code',
                'address',
                'apt_suite_no',
                'real_estate_type_id',
                'requested_amount',
                'estimated_market_price',
                'outstanding_value',
                'purpose',
                'building_sq_ft',
                'buildings_no',
                'units_no',
                'owner_occupied_percent',
                'annual_income_from_tenants',
            ]);
            break;
        case 'EQUIPMENT_FINANCE':
            validation_array = getRules([
                'vendor_name',
                'equipment_purpose',
                'equipment_description',
                'purchase_price',
                'less_trade',
                'less_down_payment',
                'requested_amount',
                'equipment_finance_type',
            ]);
            break;
        case 'TERM_LOAN_OTHER':
            validation_array = getRules([
                'requested_amount',
            ]);
            break;
        default:
            break;
    }
    // console.log(validation_array)
    return validation_array;
}

var required_validation = function (control) {
    return {
        rules: {required: true},
        messages: {
            required: messages_validations_array[control].required
        }
    };
};
var required_digits_validation = function (control) {
    return {
        rules: {digits: true},
        messages: {
            digits: messages_validations_array[control].integer
        }
    };
};

var numeric_validation = function (control) {
    return {
        rules: {number: true},
        messages: {
            required: messages_validations_array[control].numeric
        }
    };
};

var required_numeric_validation = function (control) {
    return {
        rules: {required: true, number: true},
        messages: {
            required: messages_validations_array[control].required,
            number: messages_validations_array[control].numeric
        }
    };
}
var required_numeric_percent_validation = function (control) {
    return {
        rules: {required: true, number: true, min: 0, max: 100,},
        messages: {
            required: messages_validations_array[control].required,
            number: messages_validations_array[control].numeric,
            min: messages_validations_array[control].min,
            max: messages_validations_array[control].max,
        }
    };
}

var required_numeric_greater_than_validation = function (control, control2) {
    return {
        rules: {required: true, number: true, greaterThan: control2},
        messages: {
            required: messages_validations_array[control].required,
            number: messages_validations_array[control].numeric,
            greaterThan: messages_validations_array[control].max
        }
    };
}
var numeric_greater_than_validation = function (control, control2) {
    return {
        rules: {number: true, greaterThan: control2},
        messages: {
            number: messages_validations_array[control].numeric,
            greaterThan: messages_validations_array[control].max
        }
    };
}
var required_numeric_less_than_validation = function (control, control2) {
    return {
        rules: {required: true, number: true, lessThanEqual: control2},
        messages: {
            required: messages_validations_array[control].required,
            number: messages_validations_array[control].numeric,
            lessThanEqual: messages_validations_array[control].max
        }
    };
}

var zip_code_validation = function (control) {
    return {
        rules: {
            required: true,
            number: true,
            zipcodeUS: true,
            // remote: {
            //     url: app_url + '/states/check_valid_zip_code',
            //     type: "get",
            //     data: {
            //         state: function () {
            //             return $('#' + the_form + " #state").find(":selected").text();
            //         }, city: function () {
            //             return $('#' + the_form + " #city").find(":selected").text();
            //         }, zip_code: function () {
            //             return $('#' + the_form + ' :input[name="zip_code"]').val();
            //         }
            //     }
            // }
        },
        messages: {
            required: messages_validations_array[control].required,
            number: messages_validations_array[control].numeric,
            zipcodeUS: messages_validations_array.zip_code.regex,
            // remote: messages_validations_array.zip_code.belongs_to_city,
        }
    };
}

var required_numeric_or_validation = function (control, control2) {

    return {
        rules: {
            required: function (element) {
                if (
                    $("#" + the_form + " input[name=" + control2 + "]").is(":visible") &&
                    $("#" + the_form + " input[name=" + control2 + "]").val()== "") {
                    return false;
                }
                else {
                    return true;
                }
            }, number: true
        },
        messages: {
            required: messages_validations_array[control].required,
            number: messages_validations_array[control].numeric,
        }
    };
}

var purpose_required_validation = function (control) {
    return {
        rules: {
            required: function (element) {
                return $("#" + the_form + ' select[name=purpose_type_id] option:selected').text() == 'Other';
            }
        },
        messages: {
            required: messages_validations_array[control].required
        }
    };
};

var building_sq_ft_validation = function (control) {
    return {
        rules: {
            required: function (element) {
                if (
                    $("#" + the_form + " input[name=buildings_no]").val().length != 0 &&
                    $("#" + the_form + " input[name=units_no]").val().length != 0
                ) {
                    return false;
                }
                else {
                    return true;
                }
            },
        number: true
        },
        messages: {
            required: messages_validations_array[control].required,
            number: messages_validations_array[control].numeric
        }
    };
};

var buildings_no_validation = function (control) {
    return {
        rules: {
            required: function (element) {
                if (
                    $("#" + the_form + " input[name=building_sq_ft]").val().length == 0
                ) {
                    return false;
                }
                else {
                    return true;
                }
            },
            digits: true
        },
        messages: {
            required: messages_validations_array[control].required,
            digits: messages_validations_array[control].integer
        }
    };
};

var units_no_validation = function (control) {
    return {
        rules: {
            required: function (element) {
                if (
                    $("#" + the_form + " input[name=building_sq_ft]").is(":visible") &&
                    $("#" + the_form + " input[name=building_sq_ft]").val().length == ""
                ) {
                    return false;
                }
                else {
                    return true;
                }
            },
            digits: true
        },
        messages: {
            required: messages_validations_array[control].required,
            digits: messages_validations_array[control].integer
        }
    };
};
