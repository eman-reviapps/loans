$('#state').change(function () {
    getCities();
});

$('#city').change(function () {
    $("#zip_code").val($(this).val());
});

function getCities() {
    var state_code = $('#state').val();

    $('#city').html(getDefaultSelectOption());

    $.getJSON(app_url + '/dropdowns/zip_codes', {state_code: state_code}, function (data) {

        $('#city').html(
            getDefaultSelectOption() + " " +
            $.map(data, function (item, index) {
                return '<option data-content="' + item.city + '" value=' + item.zip_code + '>' + item.city + " - " + item.zip_code + '</option>'
            }));
    });
}

function getDefaultSelectOption() {
    return '<option selected value="" >Select</option>';
}