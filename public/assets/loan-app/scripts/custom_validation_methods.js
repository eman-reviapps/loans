jQuery.validator.addMethod("complete_url", function (val, elem) {
    // if no url, don't do anything
    if (val.length == 0) {
        return true;
    }

    // if user has not entered http:// https:// or ftp:// assume they mean http://
    if (!/^(https?|ftp):\/\//i.test(val)) {
        val = 'http://' + val; // set both the value
        // $(elem).val(val); // also update the form element
    }
    // now check if valid url
    // http://docs.jquery.com/Plugins/Validation/Methods/url
    // contributed by Scott Gonzalez: http://projects.scottsplayground.com/iri/
    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(val);
});

jQuery.validator.addMethod("without_domain", function (val, elem) {

    var is_email = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(val);
    return !is_email;
});

jQuery.validator.addMethod("valid_year", function (val, elem) {

    var year = val;
    var currYear = new Date().toString().match(/(\d{4})/)[1];
    if (year >= 1900 && year <= currYear) {
        return true;
    } else {
        return false;
    }
});

jQuery.validator.addMethod("greaterThan", function (value, element, param) {
    var $min = $(param);

    var min_val = $min.val().replace(/[^0-9\.-]+/g, "");

    var value = value.replace(/[^0-9\.-]+/g, "");

    if (this.settings.onfocusout) {
        $min.off(".validate-greaterThan").on("blur.validate-greaterThan", function () {
            $(element).valid();
        });
    }
    return value && $min.val() ? parseInt(value) > parseInt(min_val) : true;
}, "Max must be greater than min");

jQuery.validator.addMethod('lessThanEqual', function (value, element, param) {
    var param_val = $(param).val().replace(/[^0-9\.-]+/g, "");
    var value = value.replace(/[^0-9\.-]+/g, "");

    return this.optional(element) || parseInt(value) <= parseInt(param_val);
}, "The value {0} must be less than or equal {1}");

jQuery.validator.addMethod('lessThan', function (value, element, param) {
    return this.optional(element) || parseInt(value) < parseInt($(param).val()) || parseInt($(param).val()) == 0;
}, "The value {1} must be less than {0}");

$.validator.addMethod("currency", function (value, element) {
    return this.optional(element) || /^\$(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
}, "Please specify a valid amount");

$.validator.addMethod("dayMonthDate", function (value, element) {
    value = [value.slice(0, 2), '/', value.slice(2)].join('');

    if (value.length == 0) {
        return true;
    }

    var dateformat = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/;

    if (dateformat.test(value)) {
        var opera1 = value.split('/');
        lopera1 = opera1.length;
        // Extract the string into month, date and year
        if (lopera1 > 1) {
            var pdate = value.split('/');
        }
        var mm  = parseInt(pdate[0]);
        var dd = parseInt(pdate[1]);
        var yy = 2017;
        // Create list of days of a month [assume there is no leap year by default]
        var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        if (mm == 1 || mm > 2) {
            if (dd > ListofDays[mm - 1]) {
                return false;
            }
        }
        if (mm == 2) {
            var lyear = false;
            if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
                lyear = true;
            }
            if ((lyear == false) && (dd >= 29)) {
                return false;
            }
            if ((lyear == true) && (dd > 29)) {
                return false;
            }
        }
        return true;
    }
    else {
        return false;
    }
}, "Please specify a valid day/month");





