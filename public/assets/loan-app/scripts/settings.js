validation_array = {
    errorClass: validation_error_class,
    ignore: [],
    rules: {
        post_expire_period: {
            required: true,
            number: true,
            maxlength: 3
        },
        post_expire_period_unit: {
            required: true,
        },
        lender_max_post_bids: {
            required: true,
            number: true,
            maxlength: 3
        },

        free_trial_period: {
            required: true,
            number: true,
        },
        free_trial_period_unit: {
            required: true,
        },

        free_lifetime_subscriptions_count: {
            required: true,
            number: true,
        },

        subscribe_reminder_email_first: {
            required: true,
            number: true,
        },
        subscribe_reminder_email_first_unit: {
            required: true,
        },

        subscribe_reminder_email_second: {
            required: true,
            number: true,
            lessThan: '#subscribe_reminder_email_first'
        },
        subscribe_reminder_email_second_unit: {
            required: true,
        },

        subscribe_reminder_email_third: {
            required: true,
            number: true,
            lessThan: '#subscribe_reminder_email_second'
        },
        subscribe_reminder_email_third_unit: {
            required: true,
        },

        subscribe_renew_reminder_email: {
            required: true,
            number: true
        },
        subscribe_renew_reminder_email_unit: {
            required: true,
        }
    },
    messages: {
        post_expire_period: {
            required: messages_validations_array.post_expire_period.required,
            number: messages_validations_array.post_expire_period.numeric,
            maxlength: messages_validations_array.post_expire_period.max
        },
        post_expire_period_unit: {
            required: messages_validations_array.post_expire_period_unit.required
        },
        lender_max_post_bids: {
            required: messages_validations_array.lender_max_post_bids.required,
            number: messages_validations_array.lender_max_post_bids.numeric,
            maxlength: messages_validations_array.lender_max_post_bids.max
        },

        free_trial_period: {
            required: messages_validations_array.free_trial_period.required,
            number: messages_validations_array.free_trial_period.numeric,
        },
        free_trial_period_unit: {
            required: messages_validations_array.free_trial_period_unit.required
        },

        free_lifetime_subscriptions_count: {
            required: messages_validations_array.free_lifetime_subscriptions_count.required,
            number: messages_validations_array.free_lifetime_subscriptions_count.numeric,
        },

        subscribe_reminder_email_first: {
            required: messages_validations_array.subscribe_reminder_email_first.required,
            number: messages_validations_array.subscribe_reminder_email_first.numeric,
        },
        subscribe_reminder_email_first_unit: {
            required: messages_validations_array.subscribe_reminder_email_first_unit.required
        },

        subscribe_reminder_email_second: {
            required: messages_validations_array.subscribe_reminder_email_second.required,
            number: messages_validations_array.subscribe_reminder_email_second.numeric,
            lessThan: messages_validations_array.subscribe_reminder_email_second.max,
        },
        subscribe_reminder_email_second_unit: {
            required: messages_validations_array.subscribe_reminder_email_second_unit.required
        },

        subscribe_reminder_email_third: {
            required: messages_validations_array.subscribe_reminder_email_third.required,
            number: messages_validations_array.subscribe_reminder_email_third.numeric,
            lessThan: messages_validations_array.subscribe_reminder_email_third.max,
        },
        subscribe_reminder_email_third_unit: {
            required: messages_validations_array.subscribe_reminder_email_third_unit.required
        },

        subscribe_renew_reminder_email: {
            required: messages_validations_array.subscribe_renew_reminder_email.required,
            number: messages_validations_array.subscribe_renew_reminder_email.numeric,
        },
        subscribe_renew_reminder_email_unit: {
            required: messages_validations_array.subscribe_renew_reminder_email_unit.required
        }
    }
};

$(document).ready(function () {
    $("#settingForm").validate(validation_array);
    $(".mask_number").inputmask({
        "mask": "9",
        "repeat": 3,
        "greedy": false
    });
    $(".select2").select2();
});