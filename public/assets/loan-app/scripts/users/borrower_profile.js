var default_tab = "tab_account_profile_info";
var default_parent_tab = "tab_overview";
var myDropZone;

function calcEBITDA() {
    var company_EBITDA_year1 = 0;
    var company_EBITDA_year2 = 0;
    var company_EBITDA_year3 = 0;

    $('.calc_EBITDA_year1').each(function () {
        if ($(this).val()) {
            var value = parseFloat($(this).val().replace(/[^0-9\.-]+/g, ""));
            company_EBITDA_year1 += value;
        }
    });
    $('.calc_EBITDA_year2').each(function () {
        if ($(this).val()) {
            var value = parseFloat($(this).val().replace(/[^0-9\.-]+/g, ""));
            company_EBITDA_year2 += value;
        }
    });
    $('.calc_EBITDA_year3').each(function () {
        if ($(this).val()) {
            var value = parseFloat($(this).val().replace(/[^0-9\.-]+/g, ""));
            company_EBITDA_year3 += value;
        }
    });
    $("#company_EBITDA_year_1").val(company_EBITDA_year1)
    $("#company_EBITDA_year_2").val(company_EBITDA_year2)
    $("#company_EBITDA_year_3").val(company_EBITDA_year3)
    $("#company_EBITDA").val(company_EBITDA_year1 + company_EBITDA_year2 + company_EBITDA_year3)
}

var resetDropzone = function () {

    Dropzone.autoDiscover = false;

    myDropZone = $("#my-dropzone").dropzone({
        dictDefaultMessage: "",
        url: app_url + '/upload_file',
        // autoProcessQueue: false,
        maxFiles: 10,
        init: function () {
            var wrapperThis = this;

            this.on("addedfile", function (file) {
                // Create the remove button
                var removeButton = Dropzone.createElement("<a href='javascript:;'' class='btn red btn-sm btn-block'>Remove</a>");

                // Capture the Dropzone instance as closure.
                var _this = this;

                // Listen to the click event
                removeButton.addEventListener("click", function (e) {
                    // Make sure the button click doesn't submit the form:
                    e.preventDefault();
                    e.stopPropagation();

                    // Remove the file preview.
                    _this.removeFile(file);
                    // If you want to the delete the file on the server as well,
                    // you can do the AJAX request here.
                });

                // Add the button to the file preview element.
                file.previewElement.appendChild(removeButton);
            });
            this.on("success", function (file, reposnse) {

                alert("File "+ file.name+" uploaded successfully")
            });
            this.on("maxfilesexceeded", function (file) {
                alert('Max files exceeded (10)')
            });
            this.on("error", function (file, errorMessage) {

                alert('Failed to upload file '+file.name)

                this.removeFile(file);
            });
        }
    });
};

var BorrowerProfile = function () {
    return {
        //main function to initiate the module
        init: function () {

            if (active_tab) {
                $("#" + active_tab).addClass("active");
                $('li > a[href="#' + active_tab + '"]').parent().addClass('active');

                $("#tab_account").addClass("active");
                $('li > a[href="#tab_account"]').parent().addClass('active');
            } else {
                $("#" + default_tab).addClass("active");
                $('li > a[href="#' + default_tab + '"]').parent().addClass('active');

                $("#" + default_parent_tab).addClass("active");
                $('li > a[href="#' + default_parent_tab + '"]').parent().addClass('active');
            }

            $('.mt-repeater').each(function () {
                $(this).repeater({
                    show: function () {
                        $(this).slideDown();
                        $(".select2").select2({
                            placeholder: "Select",
                            allowClear: true,
                            width: 'auto',
                            escapeMarkup: function (m) {
                                return m;
                            }
                        });
                        $(".mask_number").inputmask({
                            "mask": "9",
                            "repeat": 5,
                            "greedy": false
                        });


                        $('.date-picker').datepicker({
                            rtl: App.isRTL(),
                            orientation: "left",
                            autoclose: true
                        });

                    },

                    hide: function (deleteElement) {
                        if (confirm('Are you sure you want to delete this element?')) {
                            $(this).slideUp(deleteElement);
                        }
                    },

                    ready: function (setIndexes) {

                    }

                });
            });

            $(".mask_number").inputmask({
                "mask": "9",
                "repeat": 5,
                "greedy": false
            });

            $(".mask_phone").inputmask("mask", {
                "mask": "(999) 999-9999"
            });

            // $('.mask_money').mask("00,000,000", {reverse: true, placeholder: "$00,000,000"});
            $('.mask_money').mask('#,##0', {placeholder: "$000,000,000",reverse: true});

            $(".establish_year").inputmask({
                "mask": "9",
                "repeat": 4,
                "greedy": false
            });
            $(".fisical_year_end").inputmask("m/d", {
                "placeholder": "mm/dd",
                autoUnmask: true
            });
            $.fn.datepicker.defaults.format = "yyyy-mm-dd";
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true
            });

            $(".select2, .select2-multiple").select2({
                placeholder: 'Select',
                width: null
            });

            $('.calc_EBITDA_year1').on('input', function () {
                calcEBITDA();
            });
            $('.calc_EBITDA_year2').on('input', function () {
                calcEBITDA();
            });
            $('.calc_EBITDA_year3').on('input', function () {
                calcEBITDA();
            });




            var real_estate_form = $('#borrower_realEstate_profile');

            real_estate_form.submit(function(e) {
                $('#borrower_realEstate_profile  input.mask_money').each(function () {
                    this.value = this.value.replace(/[^0-9\.-]+/g, "");
                });

                $("#borrower_realEstate_profile input[name^='files']").remove()

                if ($("#borrower_realEstate_profile").valid()) {

                    if (myDropZone) {
                        Dropzone.forElement("#my-dropzone").files.forEach(function (file) {
                            if (file.status) {

                                $("#borrower_realEstate_profile").append('<input type="hidden" name="files[]" value="' + file.xhr.response + '" />');
                            }
                        });
                    }
                }
            });

            real_estate_form.validate({
                errorClass: validation_error_class,
                rules: {
                    state: {
                        required: true,
                    },
                    city: {
                        required: true,
                    },
                    zip_code: {
                        required: true,
                        remote: {
                            url: app_url + '/states/check_valid_zip_code',
                            type: "get",
                            data: {
                                zip_code: function () {
                                    return $("#borrower_realEstate_profile #zip_code").find(":selected").text();
                                },
                                city: function () {
                                    return $("#borrower_realEstate_profile #city").find(":selected").text();
                                },
                                state: function () {
                                    return $("#borrower_realEstate_profile #state").find(":selected").text();;
                                }
                            }
                        }
                    },
                    address: {
                        required: true,
                        minlength: 3
                    },
                    apt_suite_no: {
                        required: false,
                        alphanumeric: true
                    },
                    phone: {
                        required: true,
                        phoneUS: true
                    },
                    properties_owned_no: {
                        required: true,
                        number: true
                    },
                },

                messages: {
                    state: {
                        required: messages_validations_array.state.required,
                    },
                    city: {
                        required: messages_validations_array.city.required,
                    },
                    zip_code: {
                        required: messages_validations_array.zip_code.required,
                        remote: messages_validations_array.zip_code.not_served_yet
                    },
                    address: {
                        required: messages_validations_array.address.required,
                        minlength: messages_validations_array.address.min,
                    },
                    apt_suite_no: {
                        required: messages_validations_array.apt_suite_no.required,
                        number: messages_validations_array.apt_suite_no.numeric,
                    },
                    phone: {
                        required: messages_validations_array.phone.required,
                        phoneUS: messages_validations_array.phone.regex
                    },
                    properties_owned_no: {
                        required: messages_validations_array.properties_owned_no.required,
                        number: messages_validations_array.properties_owned_no.numeric
                    },
                }
            });



            var business_form = $('#borrower_business_profile');

            business_form.submit(function(e) {
                $('#borrower_business_profile  input.mask_money').each(function () {
                    this.value = this.value.replace(/[^0-9\.-]+/g, "");
                });
            });

            business_form.validate({
                errorClass: validation_error_class,
                rules: {
                    state: {
                        required: true,
                    },
                    city: {
                        required: true,
                    },
                    zip_code: {
                        required: true,
                        remote: {
                            url: app_url + '/states/check_valid_zip_code',
                            type: "get",
                            data: {
                                zip_code: function () {
                                    return $("#borrower_business_profile #zip_code").val();
                                },
                                city: function () {
                                    return $("#borrower_business_profile #city").find(":selected").text();
                                },
                                state: function () {
                                    return $("#borrower_business_profile #state").find(":selected").text();;
                                }
                            }
                        }
                    },
                    street: {
                        required: true,
                        minlength: 3
                    },
                    apt_suite_no: {
                        required: false,
                        alphanumeric: true
                    },
                    phone: {
                        required: true,
                        phoneUS: true
                    },
                    business_name: {
                        required: true,
                        minlength: 3
                    },
                    profile_creator_role: {
                        required: true,
                        minlength: 3
                    },
                    description: {
                        required: true,
                        minlength: 3
                    },
                    establish_year: {
                        required: true,
                        minlength: 4,
                        number: true,
                        valid_year: true
                    },
                    fisical_year_end: {
                        required: true,
                        dayMonthDate: true,
                    },
                    annual_revenue_year_1: {
                        required: true,
                        number: true
                    },
                    annual_revenue_year_2: {
                        required: true,
                        number: true
                    },
                    annual_revenue_year_3: {
                        required: true,
                        number: true
                    },

                    net_income_before_tax_year_1: {
                        required: true,
                        number: true
                    },
                    net_income_before_tax_year_2: {
                        required: true,
                        number: true
                    },
                    net_income_before_tax_year_3: {
                        required: true,
                        number: true
                    },

                    interest_expense_year_1: {
                        required: true,
                        number: true
                    },
                    interest_expense_year_2: {
                        required: true,
                        number: true
                    },
                    interest_expense_year_3: {
                        required: true,
                        number: true
                    },

                    depreciation_amortization_year_1: {
                        required: true,
                        number: true
                    },
                    depreciation_amortization_year_2: {
                        required: true,
                        number: true
                    },
                    depreciation_amortization_year_3: {
                        required: true,
                        number: true
                    },

                    company_EBITDA: {
                        required: true,
                        number: true
                    },

                },
                messages: {
                    state: {
                        required: messages_validations_array.state.required,
                    },
                    city: {
                        required: messages_validations_array.city.required,
                    },
                    zip_code: {
                        required: messages_validations_array.zip_code.required,
                        remote: messages_validations_array.zip_code.not_served_yet
                    },
                    street: {
                        required: messages_validations_array.street.required,
                        minlength: messages_validations_array.street.min,
                    },
                    apt_suite_no: {
                        required: messages_validations_array.apt_suite_no.required,
                        number: messages_validations_array.apt_suite_no.numeric,
                    },
                    phone: {
                        required: messages_validations_array.phone.required,
                        phoneUS: messages_validations_array.phone.regex
                    },
                    business_name: {
                        required: messages_validations_array.business_name.required,
                        minlength: messages_validations_array.business_name.min,
                    },
                    profile_creator_role: {
                        required: messages_validations_array.profile_creator_role.required,
                        minlength: messages_validations_array.profile_creator_role.min,
                    },
                    description: {
                        required: messages_validations_array.description.required,
                        minlength: messages_validations_array.description.min,
                    },
                    establish_year: {
                        required: messages_validations_array.establish_year.required,
                        minlength: messages_validations_array.establish_year.min,
                        number: messages_validations_array.establish_year.numeric,
                        valid_year: messages_validations_array.establish_year.numeric,
                    },
                    fisical_year_end: {
                        required: messages_validations_array.fisical_year_end.required,
                        dayMonthDate: messages_validations_array.fisical_year_end.valid_day_month,
                    },

                    annual_revenue_year_1: {
                        required: messages_validations_array.annual_revenue_year_1.required,
                        number: messages_validations_array.annual_revenue_year_1.numeric,
                    },
                    annual_revenue_year_2: {
                        required: messages_validations_array.annual_revenue_year_2.required,
                        number: messages_validations_array.annual_revenue_year_2.numeric,
                    },
                    annual_revenue_year_3: {
                        required: messages_validations_array.annual_revenue_year_3.required,
                        number: messages_validations_array.annual_revenue_year_3.numeric,
                    },

                    net_income_before_tax_year_1: {
                        required: messages_validations_array.net_income_before_tax_year_1.required,
                        number: messages_validations_array.net_income_before_tax_year_1.numeric,
                    },
                    net_income_before_tax_year_2: {
                        required: messages_validations_array.net_income_before_tax_year_2.required,
                        number: messages_validations_array.net_income_before_tax_year_2.numeric,
                    },
                    net_income_before_tax_year_3: {
                        required: messages_validations_array.net_income_before_tax_year_3.required,
                        number: messages_validations_array.net_income_before_tax_year_3.numeric,
                    },

                    interest_expense_year_1: {
                        required: messages_validations_array.net_income_before_tax_year_1.required,
                        number: messages_validations_array.net_income_before_tax_year_1.numeric,
                    },
                    interest_expense_year_2: {
                        required: messages_validations_array.net_income_before_tax_year_2.required,
                        number: messages_validations_array.net_income_before_tax_year_2.numeric,
                    },
                    interest_expense_year_3: {
                        required: messages_validations_array.net_income_before_tax_year_3.required,
                        number: messages_validations_array.net_income_before_tax_year_3.numeric,
                    },

                    depreciation_amortization_year_1: {
                        required: messages_validations_array.net_income_before_tax_year_1.required,
                        number: messages_validations_array.net_income_before_tax_year_1.numeric,
                    },
                    depreciation_amortization_year_2: {
                        required: messages_validations_array.net_income_before_tax_year_2.required,
                        number: messages_validations_array.net_income_before_tax_year_2.numeric,
                    },
                    depreciation_amortization_year_3: {
                        required: messages_validations_array.net_income_before_tax_year_3.required,
                        number: messages_validations_array.net_income_before_tax_year_3.numeric,
                    },

                    company_EBITDA: {
                        required: messages_validations_array.company_EBITDA.required,
                        number: messages_validations_array.company_EBITDA.numeric,
                    },

                }
            });
        }
    };

}();

jQuery(document).ready(function () {
    Address.init();
    BorrowerProfile.init();
    resetDropzone();
});