var personnel_info_validation_array = {
    errorClass: validation_error_class,
    ignore: [],
    rules: {
        first_name: {
            required: true,
            minlength: 3
        },
        last_name: {
            required: true,
            minlength: 3
        },
        email: {
            required: true,
            email: true
        },
        phone: {
            required: true,
            phoneUS: true
        },
        status: {
            required: true,
        },
        role: {
            required: true,
        },
    },
    messages: {
        first_name: {
            required: messages_validations_array.first_name.required,
            minlength: messages_validations_array.first_name.min
        },
        last_name: {
            required: messages_validations_array.last_name.required,
            minlength: messages_validations_array.last_name.min
        },
        email: {
            required: messages_validations_array.email.required,
            email: messages_validations_array.email.email
        },
        phone: {
            required: messages_validations_array.phone.required,
            phoneUS: messages_validations_array.phone.regex
        },
        status: {
            required: messages_validations_array.status.required,
        },
        role: {
            required: messages_validations_array.role.required,
        },
    }
};

var change_password_validation_array = {
    errorClass: validation_error_class,
    rules: {
        current_password: {
            required: true,
            minlength: 6
        },
        new_password: {
            required: true,
            minlength: 6
        },
        new_password_confirmation: {
            equalTo: "#new_password"
        },
    },

    messages: {
        current_password: {
            required: messages_validations_array.current_password.required,
            minlength: messages_validations_array.current_password.min
        },
        new_password: {
            required: messages_validations_array.password.required,
            minlength: messages_validations_array.password.min,
        },
        new_password_confirmation: {
            equalTo: messages_validations_array.password_confirmation.confirmed
        },
    }
};

var lender_preferences_validation_array = {
    errorClass: validation_error_class,
    rules: {
        FAVORITE_LOAN_SIZE_MIN: {
            number: true,
        },
        FAVORITE_LOAN_SIZE_MAX: {
            number: true,
            greaterThan: '#FAVORITE_LOAN_SIZE_MIN'
        },
    },

    messages: {
        FAVORITE_LOAN_SIZE_MIN: {
            number: messages_validations_array.FAVORITE_LOAN_SIZE_MIN.numeric,
        },
        FAVORITE_LOAN_SIZE_MAX: {
            number: messages_validations_array.FAVORITE_LOAN_SIZE_MAX.numeric,
            greaterThan: messages_validations_array.FAVORITE_LOAN_SIZE_MAX.max,
        },
    }
};

var default_tab = "tab_account_personnel_info";
var default_parent_tab = "tab_overview";

$(document).ready(function () {
    $("#personnelInfo").validate(personnel_info_validation_array);
    $("#changePassword").validate(change_password_validation_array);
    $("#LenderPreferences").validate(lender_preferences_validation_array);

    if (active_tab) {
        $("#" + active_tab).addClass("active");
        $('li > a[href="#' + active_tab + '"]').parent().addClass('active');

        $("#tab_account").addClass("active");
        $('li > a[href="#tab_account"]').parent().addClass('active');
    } else {
        $("#" + default_tab).addClass("active");
        $('li > a[href="#' + default_tab + '"]').parent().addClass('active');

        $("#" + default_parent_tab).addClass("active");
        $('li > a[href="#' + default_parent_tab + '"]').parent().addClass('active');
    }

    $(".mask_number").inputmask({
        "mask": "9",
        "repeat": 10,
        "greedy": false
    });

    $(".mask_phone").inputmask("mask", {
        "mask": "(999) 999-9999"
    });

    // $('.mask_money').mask("00,000,000", {reverse: true, placeholder: "$00,000,000"});
    $('.mask_money').mask('#,##0', {placeholder: "$000,000,000",reverse: true});

    $('input:radio[name="profile_types_select"]').change(function () {
        if (this.checked && this.value == 1) {
            $('input:checkbox[name="FAVORITE_PROFILE_TYPES[]"]').prop("checked", true);
        } else if (this.checked && this.value == 0) {
            $('input:checkbox[name="FAVORITE_PROFILE_TYPES[]"]').prop("checked", false);
        }
    });
    $('input:radio[name="loan_types_select"]').change(function () {
        if (this.checked && this.value == 1) {
            $('input:checkbox[name="FAVORITE_LOAN_TYPES[]"]').prop("checked", true);
        } else if (this.checked && this.value == 0) {
            $('input:checkbox[name="FAVORITE_LOAN_TYPES[]"]').prop("checked", false);
        }
    });

});
