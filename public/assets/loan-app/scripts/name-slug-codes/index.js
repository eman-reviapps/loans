add_validation_array = {
    errorClass: validation_error_class,
    rules: {
        name: {
            required: true,
            minlength: 3,
            maxlength: 50
        }
    },
    messages: {
        name: {
            required: messages_validations_array.name.required,
            minlength: messages_validations_array.name.min,
            maxlength: messages_validations_array.name.max
        }
    }
};

var add_form = $("#createForm");

var add_validator = add_form.validate(add_validation_array);

$('.add_modal').on('hidden.bs.modal', function () {
    resetModalForm($(this), add_validator, true);
});
$('.delete_modal').on('hidden.bs.modal', function () {
    resetModalForm($(this), null, false);
});
$('.edit_modal').on('hidden.bs.modal', function () {
    resetModalForm($(this), null, false);
});

$(document).on('click', '.add_record_btn', function (e) {
    addWithAjax(add_form, $('#createModal'), add_validator, data_div);
});

$(document).on('click', '.edit_record_btn', function (e) {
    var edit_form = $(this).parents('form:first');
    var edit_validator = edit_form.validate(add_validation_array);
    editWithAjax(edit_form, $(".edit_modal"), edit_validator, data_div);
});

function resetPlugins() {
    TableDatatablesButtons.init();
    App.initAjax(); // handle bootstrap switch plugin
}