var ResetPassword = function () {
    var handleResetPassword = function () {

        $('.reset-form').validate({
            errorClass: "font-red-sunglo",
            rules: {
                new_password: {
                    required: true,
                    minlength: 6
                },
                new_password_confirmation: {
                    equalTo: "#new_password"
                },
            },

            messages: {
                new_password: {
                    required: messages_validations_array.password.required,
                    minlength: messages_validations_array.password.min,
                },
                new_password_confirmation: {
                    equalTo: messages_validations_array.password_confirmation.confirmed
                },
            }
        });

        $('.reset-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.reset-form').validate().form()) {
                    $('.reset-form').submit();
                }
                return false;
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleResetPassword();

        }

    };

}();

jQuery(document).ready(function () {
    ResetPassword.init();
});