$('.open_notification').on("click", function (e) {
    var url = this.href;

    e.preventDefault();

    $.ajax({
        url: $(this).data('action'),
        type: 'PUT',
        data: {_token: $(this).data('csrf')},
        dataType: 'json',
        async: false,
        success: function (data) {
            if (data.level == "success") {
                window.location = url;

            } else {

            }
        },
        error: function () {

        },
    });


});


$('.mark_all_notifications_read').on("click", function (e) {

    e.preventDefault();

    $.ajax({
        url: $(this).data('action'),
        type: 'PUT',
        data: {_token: $(this).data('csrf')},
        dataType: 'json',
        async: false,
        success: function (data) {
            if (data.level == "success") {
                location.reload();

            } else {

            }
        },
        error: function () {

        },
    });


});