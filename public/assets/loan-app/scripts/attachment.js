$('.remove_attachement').on("click", function (e) {

    e.preventDefault();

    $.ajax({
        url: $(this).data('action'),
        type: 'DELETE',
        data: {_token: $(this).data('csrf')},
        dataType: 'json',
        async: false,
        success: function (data) {
            if (data.level == "success") {
                location.reload();
            } else {

            }
        },
        error: function () {

        },
    });


});