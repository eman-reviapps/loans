var ChartsAmcharts = function() {

    var initChartSample1 = function() {
        var chart = AmCharts.makeChart("chart_1", {
            "type": "serial",
            "theme": "light",
            "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,

            "fontFamily": 'Open Sans',
            "color":    '#888',
            "startDuration": 1,
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "dashLengthField": "dashLengthColumn",
                "fillAlphas": 1,
                "title": "Loans",
                "type": "column",
                "valueField": "count"
            }, {
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "bullet": "round",
                "dashLengthField": "dashLengthLine",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Expenses",
                "valueField": "expenses"
            }],
            "categoryField": "institution",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            },
            "dataLoader": {
                "url": app_url + "/admin/institutions/institution_loan_handling_chart",
                "format": "json"
            }
        });

        $('#chart_1').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    };

    var initChartSample11 = function() {
        var chart = AmCharts.makeChart("chart_11", {
            "type": "serial",
            "theme": "light",
            "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,

            "fontFamily": 'Open Sans',
            "color":    '#888',
            "startDuration": 1,
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "dashLengthField": "dashLengthColumn",
                "fillAlphas": 1,
                "title": "state",
                "type": "column",
                "valueField": "count"
            }, {
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "bullet": "round",
                "dashLengthField": "dashLengthLine",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Expenses",
                "valueField": "expenses"
            }],
            "categoryField": "state",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            },
            "dataLoader": {
                "url": app_url + "/admin/lenders_per_state",
                "format": "json"
            }
        });
        $('#chart_11').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    };

    var initChartSample7 = function() {
        var chart = AmCharts.makeChart("chart_7", {
            "type": "pie",
            "theme": "light",
            "labelsEnabled": false,
            "legend": {
                "markerType": "circle",
                "position": "right",
                "marginRight": 40,
                "autoMargins": false,
                "truncateLabels": 25 // custom parameter
            },
            "fontFamily": 'Open Sans',
            "autoMargins": false,
            "color":    '#888',
            "valueField": "count_value",
            "titleField": "count_type",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            },
            "dataLoader": {
                "url": app_url + "/admin/loans_count",
                "format": "json"
            }
        });

        jQuery('.chart_7_chart_input').off().on('input change', function() {
            var property = jQuery(this).data('property');
            var target = chart;
            var value = Number(this.value);
            chart.startDuration = 0;

            if (property == 'innerRadius') {
                value += "%";
            }

            target[property] = value;
            chart.validateNow();
        });

        $('#chart_7').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    };

    var initChartSample77 = function() {
        var chart = AmCharts.makeChart("chart_77", {
            "type": "pie",
            "theme": "light",
            "labelsEnabled": false,
            "legend": {
                "markerType": "circle",
                "position": "right",
                "marginRight": 40,
                "autoMargins": false,
                "truncateLabels": 25 // custom parameter
            },
            "fontFamily": 'Open Sans',
            "autoMargins": false,
            "color":    '#888',
            "valueField": "count",
            "titleField": "status",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            },
            "dataLoader": {
                "url": app_url + "/admin/loans_status_statistics",
                "format": "json"
            }
        });

        jQuery('.chart_77_chart_input').off().on('input change', function() {
            var property = jQuery(this).data('property');
            var target = chart;
            var value = Number(this.value);
            chart.startDuration = 0;

            if (property == 'innerRadius') {
                value += "%";
            }

            target[property] = value;
            chart.validateNow();
        });

        $('#chart_77').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    };

    var initChartSample777 = function() {
        var chart = AmCharts.makeChart("chart_777", {
            "type": "pie",
            "theme": "light",
            "labelsEnabled": false,
            "legend": {
                "markerType": "circle",
                "position": "right",
                "marginRight": 40,
                "autoMargins": false,
                "truncateLabels": 25 // custom parameter
            },
            "fontFamily": 'Open Sans',
            "autoMargins": false,
            "color":    '#888',
            "valueField": "count",
            "titleField": "reason",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            },
            "dataLoader": {
                "url": app_url + "/admin/loans_feedback_statistics",
                "format": "json"
            }
        });

        jQuery('.chart_777_chart_input').off().on('input change', function() {
            var property = jQuery(this).data('property');
            var target = chart;
            var value = Number(this.value);
            chart.startDuration = 0;

            if (property == 'innerRadius') {
                value += "%";
            }

            target[property] = value;
            chart.validateNow();
        });

        $('#chart_777').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    };

    var initChartSample7777 = function() {
        var chart = AmCharts.makeChart("chart_7777", {
            "type": "pie",
            "theme": "light",
            "labelsEnabled": false,
            "legend": {
                "markerType": "circle",
                "position": "right",
                "marginRight": 40,
                "autoMargins": false,
                "truncateLabels": 25 // custom parameter
            },
            "fontFamily": 'Open Sans',
            "autoMargins": false,
            "color":    '#888',
            "valueField": "count",
            "titleField": "institution",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            },
            "dataLoader": {
                "url": app_url + "/admin/number_of_lenders_per_institution",
                "format": "json"
            }
        });

        jQuery('.chart_7777_chart_input').off().on('input change', function() {
            var property = jQuery(this).data('property');
            var target = chart;
            var value = Number(this.value);
            chart.startDuration = 0;

            if (property == 'innerRadius') {
                value += "%";
            }

            target[property] = value;
            chart.validateNow();
        });

        $('#chart_7777').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    };

    return {
        //main function to initiate the module

        init: function() {

            initChartSample1();
            initChartSample11();
            initChartSample7();
            initChartSample77();
            initChartSample777();
            initChartSample7777();
        }

    };

}();

jQuery(document).ready(function() {
   ChartsAmcharts.init();
});