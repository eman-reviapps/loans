<?php

namespace Tests\Browser;

use App\Models\User;
use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * test login
     *
     * @return void
     */
    public function testLogin()
    {

        $this->browse(function ($browser){
            $browser->visit('/login')
                ->type('email', 'eman.mohamed@seamlabs.com')
                ->type('password', '123456')
                ->press('login_btn')
                ->assertPathIs('/home');
        });
    }
}
