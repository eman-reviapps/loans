<?php

return [
    "loan_expire_period" => 3,
    "loan_expire_period_unit" => \App\Enums\PeriodUnits::DEFAULT_VALUE,
    "lender_max_loan_bids" => 3,

    "page_size" => 10,

    "free_trial_period" => 3,
    "free_trial_period_unit" => \App\Enums\PeriodUnits::MONTHS,

    "free_lifetime_subscriptions_count" => 50,

    "subscribe_reminder_email_first" => 20,
    "subscribe_reminder_email_first_unit" => \App\Enums\PeriodUnits::DAYS,
    "subscribe_reminder_email_second" => 10,
    "subscribe_reminder_email_second_unit" => \App\Enums\PeriodUnits::DAYS,
    "subscribe_reminder_email_third" => 1,
    "subscribe_reminder_email_third_unit" => \App\Enums\PeriodUnits::DAYS,


];