<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute must be a valid email address.',
    'exists' => 'The selected :attribute is invalid.',
    'file' => 'The :attribute must be a file.',
    'filled' => 'The :attribute field is required.',
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values is present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'state' => [
            'required' => 'State Is Required.',
            'not_served_yet' => 'We are not serving this state yet.',
        ],
        'city' => [
            'required' => 'City Is Required.',
            'not_served_yet' => 'We are not serving this city yet.',
        ],
        'zip_code' => [
            'required' => 'Zip code Is Required.',
            'regex' => 'The specified US ZIP Code is invalid.',
            'not_served_yet' => 'We are not serving this area yet.',
            'belongs_to_city' => 'Zip code not belongs to specified city.',
        ],
        'street' => [
            'required' => 'Street Address Is Required.',
            'min' => 'Street Address must be at least 3 characters.',
        ],
        'apt_suite_no' => [
            'required' => 'Apt/Suite/Other Is Required.',
            'not_served_yet' => 'Apt/Suite/Other must be a number!.',
        ],
        'institution' => [
            'required' => 'Institution Is Required.',
        ],
        'first_name' => [
            'required' => 'First Name Is Required.',
            'min' => 'First Name must be at least 3 characters. No special characters allowed.',
        ],
        'last_name' => [
            'required' => 'Last Name Is Required.',
            'min' => 'Last Name must be at least 3 characters. No special characters allowed.',
        ],
        'profile_creator_role' => [
            'required' => 'Job title Is Required.',
            'min' => 'Job title must be at least 3 characters. No special characters allowed.',
        ],
        'institution_name' => [
            'required' => 'Institution Name Is Required.',
            'min' => 'Institution Name must be at least 3 characters. No special characters allowed.',
        ],
        'email' => [
            'required' => 'E-Mail address Is Required.',
            'email' => 'E-Mail is not valid.',
            'min' => 'E-Mail must be at least 6 characters.',
            'exists' => 'No such user with that email.',
            'without_domain' => 'Please enter email without domain.',
            'unique' => 'This email has already registered before.',
        ],
        'current_password' => [
            'required' => 'Current password Is Required.',
            'min' => 'Current password must be at least 6 characters.',
        ],
        'password' => [
            'required' => 'Password is Required.',
            'min' => 'Password must be at least 6 characters.',
            'confirmed' => 'Password Confirmation does not match the password',
        ],
        'password_confirmation' => [
            'required' => 'Password confirmation Is Required.',
            'min' => 'Password must be at least 6 characters.',
            'confirmed' => 'Password Confirmation does not match the password',
        ],
        'title' => [
            'required' => 'Title Is Required.',
            'min' => 'Title must be at least 3 characters.',
            'max' => 'The title may not be greater than 50 characters.',
        ],
        'domain' => [
            'required' => 'Domain Is Required.',
            'regex' => 'Please enter a valid url.',
            'unique' => 'The domain has already been taken.',
        ],
        'phone' => [
            'required' => 'Phone Is Required.',
            'regex' => 'Please specify a valid phone number.',
        ],
        'status' => [
            'required' => 'Status Is Required.',
        ],
        'name' => [
            'required' => 'Name Is Required.',
            'min' => 'Name must be at least 3 characters.',
            'max' => 'The name may not be greater than 50 characters.',
        ],
        'role' => [
            'required' => 'Role Is Required.',
        ],

        'post_expire_period' => [
            'required' => 'Post expire period Is Required.',
            'numeric' => 'Post expire period must be a number!.',
            'max' => 'Max allowed digits is 3.',
        ],
        'post_expire_period_unit' => [
            'required' => 'Period unit Is Required.',
        ],
        'lender_max_post_bids' => [
            'required' => 'Lender max post bids Is Required.',
            'numeric' => 'Lender max post bids must be a number!.',
            'max' => 'Max allowed digits is 3.',
        ],


        'free_trial_period' => [
            'required' => 'Free trial period Is Required.',
            'numeric' => 'Free trial period must be a number!.',
        ],
        'free_trial_period_unit' => [
            'required' => 'Free trial period unit Is Required.',
        ],

        'free_lifetime_subscriptions_count' => [
            'required' => 'Lifetime subscriptions limit Is Required.',
            'numeric' => 'Lifetime subscriptions limit must be a number!.',
        ],

        'subscribe_reminder_email_first' => [
            'required' => 'First Email period Is Required.',
            'numeric' => 'First Email period must be a number!.',
        ],
        'subscribe_reminder_email_first_unit' => [
            'required' => 'First Email period unit Is Required.',
        ],

        'subscribe_reminder_email_second' => [
            'required' => 'Second Email period Is Required.',
            'numeric' => 'Second Email period must be a number!.',
            'max' => 'Second Email period must be less than First Email period',
        ],
        'subscribe_reminder_email_second_unit' => [
            'required' => 'Second Email period unit Is Required.',
        ],

        'subscribe_reminder_email_third' => [
            'required' => 'Third Email period Is Required.',
            'numeric' => 'Third Email period must be a number!.',
            'max' => 'Third Email period must be less than Second Email period',
        ],
        'subscribe_reminder_email_third_unit' => [
            'required' => 'Third Email period unit Is Required.',
        ],

        'subscribe_renew_reminder_email' => [
            'required' => 'Email period Is Required.',
            'numeric' => 'Email period must be a number!.',
        ],
        'subscribe_renew_reminder_email_unit' => [
            'required' => 'Email period unit Is Required.',
        ],


        'properties_owned_no' => [
            'required' => 'Properties own number Is Required.',
            'numeric' => 'Properties own number must be a number!.',
        ],

        'business_name' => [
            'required' => 'Business name Is Required.',
            'min' => 'Business name must be at least 3 characters.',
        ],
        'business_name' => [
            'required' => 'Business name Is Required.',
            'min' => 'Business name must be at least 3 characters.',
        ],
        'address' => [
            'required' => 'Address Is Required.',
            'min' => 'Address must be at least 3 characters.',
        ],
        'description' => [
            'required' => 'Description Is Required.',
            'min' => 'Description must be at least 3 characters.',
        ],
        'establish_year' => [
            'required' => 'Establish year Is Required.',
            'min' => 'Please enter valid year.',
            'numeric' => 'Establish year must be a number!.',
        ],
        'fisical_year_end' => [
            'required' => 'Fisical year end Is Required.',
            'dayMonthDate' => 'Please enter a avalid month/day.',
        ],

        'annual_revenue_year_1' => [
            'required' => 'Year 1 annual Revenue Is Required.',
            'numeric' => 'Year 1 annual Revenue must be a number!.',
        ],
        'annual_revenue_year_2' => [
            'required' => 'Year 2 annual Revenue Is Required.',
            'numeric' => 'Year 2 annual Revenue must be a number!.',
        ],
        'annual_revenue_year_3' => [
            'required' => 'Year 3 annual Revenue Is Required.',
            'numeric' => 'Year 3 annual Revenue must be a number!.',
        ],

        'net_income_before_tax_year_1' => [
            'required' => 'Year 1 Net Income Before Tax Is Required.',
            'numeric' => 'Year 1 Net Income Before Tax must be a number!.',
        ],
        'net_income_before_tax_year_2' => [
            'required' => 'Year 2 Net Income Before Tax Is Required.',
            'numeric' => 'Year 2 Net Income Before Tax must be a number!.',
        ],
        'net_income_before_tax_year_3' => [
            'required' => 'Year 3 Net Income Before Tax Is Required.',
            'numeric' => 'Year 3 Net Income Before Tax must be a number!.',
        ],

        'interest_expense_year_1' => [
            'required' => 'Year 1 Interest Expense Is Required.',
            'numeric' => 'Year 1 Interest Expense must be a number!.',
        ],
        'interest_expense_year_2' => [
            'required' => 'Year 2 Interest Expense Is Required.',
            'numeric' => 'Year 2 Interest Expense must be a number!.',
        ],
        'interest_expense_year_3' => [
            'required' => 'Year 3 Interest Expense Is Required.',
            'numeric' => 'Year 3 Interest Expense must be a number!.',
        ],

        'depreciation_amortization_year_1' => [
            'required' => 'Year 1 Depreciation/Amortization Is Required.',
            'numeric' => 'Year 1 Depreciation/Amortization must be a number!.',
        ],
        'depreciation_amortization_year_2' => [
            'required' => 'Year 2 Depreciation/Amortization Is Required.',
            'numeric' => 'Year 2 Depreciation/Amortization must be a number!.',
        ],
        'depreciation_amortization_year_3' => [
            'required' => 'Year 3 Depreciation/Amortizatione Is Required.',
            'numeric' => 'Year 3 Depreciation/Amortization must be a number!.',
        ],

        'FAVORITE_LOAN_SIZE_MIN' => [
            'numeric' => 'Favorite loan size must be a number!.',
        ],
        'FAVORITE_LOAN_SIZE_MAX' => [
            'numeric' => 'Max loan size must be a number!.',
            'max' => 'Max loan size must be greate than minimum value!.',
        ],
        'requested_amount' => [
            'required' => 'Requested amount Is Required.',
            'numeric' => 'Requested amount must be a number!.',
            'max' => 'Requested amount must be less than or equal purchase price!.',
        ],
        'requested_percent' => [
            'required' => 'Requested percent Is Required.',
            'numeric' => 'Requested percent must be a number!.',
            'min' => 'Please enter a value less than or equal to 100.',
            'max' => 'Please enter a value less than or equal to 100.',
        ],
        'purpose_type_id' => [
            'required' => 'Purpose Is Required.',
        ],
        'purpose' => [
            'required' => 'Purpose Is Required.',
        ],
        'receivable_range_from' => [
            'required' => 'Receivable range low value Is Required.',
            'numeric' => 'Receivable range low value must be a number!.',
        ],
        'receivable_range_to' => [
            'required' => 'Receivable range high value Is Required.',
            'numeric' => 'Receivable range high value must be a number!.',
            'max' => 'High value must be greate than low value!.',
        ],
        'inventory_range_from' => [
            'required' => 'Inventory range low value Is Required.',
            'numeric' => 'Inventory range low value must be a number!.',
        ],
        'inventory_range_to' => [
            'required' => 'Inventory range high value Is Required.',
            'numeric' => 'Inventory range high value must be a number!.',
            'max' => 'High value must be greater than low value!.',
        ],
        'inventory_type' => [
            'required' => 'Inventory type Is Required.',
        ],
        'current_ratio' => [
            'numeric' => 'Current ratio must be a number!.',
        ],
        'real_estate_type_id' => [
            'required' => 'RealEstate type Is Required.',
        ],
        'purchase_price' => [
            'required' => 'Purchase Price Is Required.',
            'numeric' => 'Purchase Price must be a number!.',
        ],
        'cash_before_down_payment' => [
            'required' => 'Cash on hand before down payment Is Required.',
            'numeric' => 'Cash on hand before down payment must be a number!.',
        ],
        'building_sq_ft' => [
            'required' => 'Building sq.ft Is Required.',
            'numeric' => 'Building sq.ft must be a number!.',
        ],
        'buildings_no' => [
            'required' => 'No of Buildings Is Required.',
            'integer' => 'No of Buildings Is must be a number!.',
        ],
        'units_no' => [
            'required' => 'No of Units Is Required.',
            'integer' => 'No of Units Is must be a number!.',
        ],
        'owner_occupied_percent' => [
            'required' => 'Owner occupied percent Is Required.',
            'numeric' => 'Owner occupied percent must be a number!.',
        ],
        'owner_occupied_no' => [
            'required' => 'Owner occupied # Is Required.',
            'numeric' => 'Owner occupied # must be a number!.',
        ],
        'owner_not_occupied_percent' => [
            'required' => 'Owner not occupied percent Is Required.',
            'numeric' => 'Owner not occupied percent must be a number!.',
        ],
        'gross_annual_revenue' => [
            'required' => 'Gross annual revenue Is Required.',
            'numeric' => 'Gross annual revenue must be a number!.',
        ],
        'estimated_annual_expenses' => [
            'required' => 'Estimated annual expenses Is Required.',
            'numeric' => 'Estimated annual expenses must be a number!.',
        ],
        'estimated_net_operating_income' => [
            'required' => 'Estimated Net Operating Income Is Required.',
            'numeric' => 'Estimated Net Operating Income be a number!.',
        ],
        'current_lease_expense' => [
            'required' => 'Current lease expense Is Required.',
            'numeric' => 'Current lease expense must be a number!.',
        ],
        'company_EBITDA' => [
            'required' => 'Company EBITDA Is Required.',
            'numeric' => 'Company EBITDA must be a number!.',
        ],
        'total_liabilities' => [
            'required' => 'Total liabilities Is Required.',
            'numeric' => 'Total liabilities must be a number!.',
        ],
        'funded_debt' => [
            'required' => 'Funded debt Is Required.',
            'numeric' => 'Funded debt must be a number!.',
        ],
        'tangible_net_worth' => [
            'required' => 'Tangible net worth Is Required.',
            'numeric' => 'Tangible net worth must be a number!.',
        ],
        'outstanding_value' => [
            'required' => 'Outstanding value Is Required.',
            'numeric' => 'Outstanding value must be a number!.',
        ],
        'rental_annual_revenue' => [
            'required' => 'Rental annual revenue Is Required.',
            'numeric' => 'Rental annual revenue must be a number!.',
        ],
        'annual_income_from_tenants' => [
            'required' => 'Annual Income From Tenants Is Required.',
            'numeric' => 'Annual Income From Tenants must be a number!.',
        ],
        'estimated_property_value' => [
            'required' => 'Estimated Property Value Is Required.',
            'numeric' => 'Estimated Property Value must be a number!.',
        ],
        'net_operating_income' => [
            'required' => 'Net Operating Income Is Required.',
            'numeric' => 'Net Operating Income must be a number!.',
        ],
        'estimated_market_price' => [
            'required' => 'Estimated Market Price Is Required.',
            'numeric' => 'Estimated Market Price must be a number!.',
        ],
        'cash_liquid_investments' => [
            'required' => 'Cash liquid investments Is Required.',
            'numeric' => 'Cash liquid investments must be a number!.',
        ],
        'rental_revenue_by_tenants' => [
            'required' => 'Rental revenue by tenants Is Required.',
            'numeric' => 'Rental revenue by tenants must be a number!.',
        ],
        'equipment_type' => [
            'required' => 'Equipment type Is Required.',
        ],
        'vendor_name' => [
            'required' => 'Vendor name Is Required.',
        ],
        'equipment_purpose' => [
            'required' => 'Equipment purpose Is Required.',
        ],
        'equipment_description' => [
            'required' => 'Equipment description Is Required.',
        ],
        'equipment_finance_type' => [
            'required' => 'Equipment finance type Is Required.',
        ],

        'less_trade' => [
            'required' => 'Less trade Is Required.',
            'numeric' => 'Less trade must be a number!.',
        ],

        'less_down_payment' => [
            'required' => 'Less down payment Is Required.',
            'numeric' => 'Less down payment must be a number!.',
        ],

        'term_other_loan_type' => [
            'required' => 'Loan type Is Required.',
        ],
        'equipment_name' => [
            'required' => 'Equipment name Is Required.',
        ],
        'equipment_state' => [
            'required' => 'Equipment state Is Required.',
        ],
        'equipment_age' => [
            'required' => 'Equipment age Is Required.',
            'integer' => 'Numbers only allowed!.',
        ],


        'rate' => [
            'required' => 'Rate Is Required.',
            'number' => 'Rate must be a number!.',
        ],
        'term' => [
            'required' => 'Term Is Required.',
            'integer' => 'Term must be a number!.',
        ],
        'term_unit' => [
            'required' => 'Term unit Is Required.',
        ],
        'amortization' => [
            'required' => 'Amortization Is Required.',
            'integer' => 'Amortization must be a number!.',
        ],
        'amortization_unit' => [
            'required' => 'Amortization unit Is Required.',
        ],
        'requires_recourse' => [
            'required' => 'Requires Recourse Is Required.',
        ],
        'description' => [
            'required' => 'Description Is Required.',
            'min' => 'Description must be at least 6 characters.',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
