<?php
return
    [
        'main_page' => 'Return Home',
        'http_status_codes' => [
            "400" => [
                "title" => "400 Bad Request",
                "oops" => "Oops! Something went wrong.",
                "description" => "Bad Request",
            ],
            "401" => [
                "title" => "401 Unauthorized",
                "oops" => "Oops! Something went wrong.",
                "description" => "Unauthorized",
            ],
            "403" => [
                "title" => "403 Forbidden",
                "oops" => "Oops! Something went wrong.",
                "description" => "Forbidden",
            ],
            "404" => [
                "title" => "404 Not Found",
                "oops" => "Oops! You're lost.",
                "description" => "We can not find the page you're looking for.",
            ],
            "409" => [
                "title" => "409 Conflict",
                "oops" => "Oops! Something went wrong.",
                "description" => "Conflict",
            ],
            "500" => [
                "title" => "500 Internal Server Error",
                "oops" => "Oops! Something went wrong.",
                "description" => "Internal Server Error",
            ],
            "503" => [
                "title" => "503 Service Unavailable",
                "oops" => "Oops! Something went wrong.",
                "description" => "We are fixing it! Please come back in a while.",
            ],
        ]
    ];