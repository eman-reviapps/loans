<?php

return [
    "home" => "Home",
    "lenders" => "Lenders",
    "borrowers" => "Borrowers",
    "active" => "Active",

    'common' => [
        'actions' => 'Actions',
        'enter' => 'Enter',
        'confirm_delete' => 'Are you sure you want to delete',
        'success' => 'Succeeded',
        'failed' => 'Failed',
        'conflict' => 'Already exists',
        'step_of' => 'Step :step of :total',
        'yes' => 'Yes',
        'no' => 'No',
        'welcome' => 'Welcome',
        'select_dropdown' => 'Select...',

        'name' => 'Name',
        'slug' => 'Slug',
        'is_enabled' => 'Is Enabled',

        'step' => 'Step',
        'of' => 'Of',

        'created_at' => 'Created at',

        'updated' => 'Updated successfully',
        'created' => 'Created successfully',
        'deleted' => 'Deleted successfully',
        'succeeded' => 'Operation done successfully',

        'failed' => 'Failed',
        "already_exists" => "Already taken.",

        'forbidden' => 'Forbidden',

        'or' => 'OR',

        'phone_us_format' => 'x-xxx-xxx-xxxx',
    ],

    'public' => [
        "already_have_account" => "Already Have an account ?"
    ],

    'sign_in' => [
        '_title' => 'Sign in',
        'sign_in_text' => 'Welcome Back !!',
        'login_text' => 'Sign in and start using our application',
        'sign_in' => 'Sign in',
        'email' => 'E-Mail',
        'password' => 'Password',
        'keep_me_signed' => 'Keep me signed in',
        'forgot_password' => 'Forgot password',
        'reset_password' => 'Reset password',
        'sign_up' => 'Sign up',
        'new_to_website' => 'New to our website?',
    ],

    'login' => [
        'invalid_login' => 'Invalid username or password.!',
        'too_many_attempts' => 'Too Many Attempts',
        'domain_not_found' => 'Domain Not Found',
        'domain_is_blocked' => 'Domain is blocked',
        'domain_in_activation_process' => 'Domain is still in activation process',
        'user_is_blocked' => 'User is blocked',
        'user_in_activation_process' => 'User is still in activation process',
        'user_not_active' => [
            "title" => "Email is not verified",
            "description" => 'Your acccount has not been verified yet. Please verify your email first in order to be able to login',
        ],
    ],

    'sign_up' => [
        '_title' => 'Create an account',
        'get_started' => 'Get Started',
        'sign_up' => 'Get Started',
        'lets_get_started' => 'Let\'s get started!',
        'look_for' => 'First, tell us what you\'re looking for.',
//        'look_for_loan' => 'Looking for a loan',
        'look_for_loan' => 'Borrower',
//        'aspire_help_borrowers' => 'Aspiring to help borrowers',
        'aspire_help_borrowers' => 'Lender',
        'or' => 'OR',

        'thanks_for_registering' => 'Thank you for registering to ' . config("app.name"),
        'activate_your_account' => 'Activate your account',
        'your_account_not_active' => 'Your account is not currently active. Please check your email for an activation link',
        'account_in_review' => 'Before you can be given access to our application we need to verify your registration, 
        which can take up to 3 working days to process. 
        A notification email will be sent to you once this has been completed.',

    ],

    'sign_up_lender' => [
        '_title' => 'Create an account',
        'sign_up' => 'Sign up',
        'register_lender' => 'Register Lender',
        'get_started' => 'Get Sarted',
        'create_free_account' => 'Create a Free Account',

        'existing' => 'Existing Institution',
        'new' => 'New Institution',

        'institution_setup' => 'Institution',
        'account_setup' => 'Account Setup',

        'institution_signup' => 'Institution',
        'institution_name' => 'Institution Name',
        'business_email' => 'Business E-Mail',

        'email_already_exists' => 'This email is already registered before',
        'failed' => 'Failed to create account',

        'domain_is_blocked' => 'Your domain is blocked',
        'state_not_served_yet' => 'We are not serving this state yet',
        'city_not_served_yet' => 'We are not serving this city yet',

    ],

    'sign_up_borrower' => [
        '_title' => 'Create an account',
        'real_estate_account' => 'Create Real Estate Investor account',
        'business_account' => 'Create Operating Company account',
        'sign_up' => 'Sign up',

//        'select_profile_type' => 'Tell us which profile will you want to create',
        'select_profile_type' => 'Which best describes you?',
//        'real_Estate' => 'Real Estate Loan',
        'real_Estate' => 'Real Estate Investor',
        'business' => 'Operating Company',
//        'business' => 'Business Loan',

        'register_borrower' => 'Register Borrower',

        'account_setup' => 'Account Setup',
        'profile_setup' => 'Profile Setup',
        'business_setup' => 'Business Setup',

        'get_started' => 'Get Sarted',
        'create_free_account' => 'Create a Free Account',

        '201' => 'Created successfully',
        '409' => 'This email is already registered before',
        '500' => 'Failed to create account',
    ],

    'activation' => [
        "_title" => "Activation",
        "activation_failed" => "Activation failed",
        "activation_done" => "Activation done successfully",

        'already_activated' => "This account has already been activated. You can now sign in",
        'user_not_found' => "User Not found",
        'activation_not_found' => "Activation record not found",
        'failed' => "Activation failed, please try again",
    ],

    'forgot_password' => [
        '_title' => 'Forgot password',
        'or' => 'or',
        'reset' => 'Reset',
        'forgot_message' => 'We\'ll help you remember your password',
        'email' => 'E-Mail',
        'continue_login' => 'Continue to Log In',
        'reset_email_sent' => 'Reset Password Email sent',
        'confirm_title' => 'We emailed you a link and instructions for updating your password.',
        'confirm_description' => 'It should be there momentarily. Please check your email and click the link in the message.',
    ],

    'reset_password' => [
        '_title' => 'Reset your password',
        'new_password' => 'New Password',
        'confirm_password' => 'Confirm New Password',
        'update_password' => 'Update Password',
        'start_over' => 'Start Over',
        'reset_error' => 'Password Reset Error',
        'reset_error_descripion' => 'We couldn\'t locate the information needed to recover your password. Please try again.',
    ],

    'operations' => [
        'actions' => 'Actions',
        'new' => 'New',
        'add' => 'Add',
        'view' => 'View',
        'create' => 'Create',
        'save' => 'Save',
        'save_draft' => 'Save Draft',
        'save_changes' => 'Save Changes',
        'save_continue' => 'Save &amp; Continue Edit',
        'edit' => 'Edit',
        'close' => 'Close',
        'update' => 'Update',
        'delete' => 'Delete',
        'cancel' => 'Cancel',
        'back' => 'Back',
        'reset' => 'Reset',
        'remove' => 'Remove',
        'save_continue_edit' => 'Save &amp; Continue Edit',

        'select' => 'Select',

        'select_all' => 'Select All',
        'un_select_all' => 'UnSelect All',

        'select_image' => 'Select image',
        'change' => 'Change',
        'continue' => 'Continue',
        'submit' => 'submit',

        'publish' => 'Publish',
    ],

    'dashboard' => [
        '_title' =>
            [
                "admin" => 'Dashboard',
                "borrower" => 'My loans',
                "lender" => 'My Loans',
            ]
    ],

    'menu' => [
        "admin" => [
            "dashboard" => "Dashboard",
            "institutions" => "Institutions",
            "institutions_management" => "Institutions Management",
            "institutions_types" => "Institutions Types",
            "institutions_sizes" => "Institutions Sizes",
            "all" => "All",
            "requires_approval" => "Requires Approval",
            "roles" => "Roles & Permissions",
            "users" => "Users",
            "lenders" => "Lenders",
            "borrowers" => "Borrowers",
            "users__management" => "Users Management",
            "feedback" => "Feedback",
            "loans" => "Loans",
            "bids" => "Bids",
            "loans__management" => "Loans Management",
            "property_types" => "Property Types",
            "purpose_types" => "Purpose Types",
            "real_estate_types" => "Real Estate Types",
            "business_types" => "Business Types",
            "lease_types" => "Lease Types",
            'settings' => 'Settings',
            'preferences' => 'Preferences',
            'general' => 'General',

            "geo_management" => "Geo-Location Management",
            'states' => 'states',
            'cities' => 'Cities',

        ],
        "borrower" => [
            "dashboard" => "Dashboard",

            "active_deals" => "Active Deals",
            "expired" => "Expired",
            "deleted" => "Deleted",
        ],
        "lender" => [
            "dashboard" => "Placed Bids",

            "find_loans" => "Find Loans",
            "my_work" => "Dashboard",
            "archived" => "Archived",
        ],
    ],

    "header" => [
        'notifications' => 'Notifications',
        'messages' => 'Messages',
        'profile' => 'Profile',
        'settings' => 'Settings',
        'logout' => 'Logout',
        'search' => 'Search',
        'preferences' => 'Preferences',
        'new' => 'New',
        'view_all' => 'view all',
        'mark_all_read' => 'Mark all read',
    ],

    "notifications" => [
        "_title" => "Notifications",
    ],

    'institutions' => [
        '_title' => 'Institutions',
        'singular' => 'Institution',

        'title' => 'Title',
        'domain' => 'Domain',
        'website' => 'Website',
        'status' => 'Status',

        'active' => 'Active ?',

        'address' => 'Address',
        'state' => 'State',
        'city' => 'City',
        'zip_code' => 'Zip Code',
        'phone' => 'Phone',
        'type_id' => 'Type',
        'size_id' => 'Size',

        'institution_have_lenders' => 'Institution have lenders,can\'t delete it',
        'domain_already_exists' => 'The domain has already been taken.',
    ],

    "institution_types" => [
        '_title' => 'Institution Types',
        'singular' => 'Institution Type',
    ],

    "institution_sizes" => [
        '_title' => 'Institution Sizes',
        'singular' => 'Institution Size',
    ],

    'roles' => [
        '_title' => 'Roles',
        'singular' => 'Role',

        'name' => 'Name',
        'slug' => 'Slug',

        'permissions' => 'Permissions',
        'of_role' => 'of role',

        'failed' => 'Failed',
        'updated' => 'Updated',
    ],

    'users' => [
        '_title' => 'Users',
        'singular' => 'User',
        'address' => 'Address',
        'register_date' => 'Register date',

        'user_info' => 'User Info',
        'login_info' => 'Login Info',

        'profile' => 'Profile',
        'overview' => 'Overview',
        'profile_settings' => 'Profile Settings',
        'account' => 'Account',

        'summary' => 'Summary',
        'total_business_loans' => 'Total Business Loans',
        'total_real_estate_loans' => 'Total Real-Estate Loans',
        'total_loans' => 'Total Loans',

        'total_deals' => 'Total deals',
        'total_aspired_borrowers' => 'Total aspired borrowers',

        'last_login' => 'Last login',

        'institution' => 'Institution',
        'profiles' => 'Profiles',
        'loans' => 'Loans',
        'login_history' => 'Login History',
        'messages' => 'Messages',
        'preferences' => 'Preferences',
        'favorites' => 'Favorites',

        'message' => 'Message',

        'personal_info' => 'Account Settings',

        'change_avatar' => 'Change Picture',
        'change_password' => 'Change Password',
        'notification_settings' => 'Notification Settings',
        'permissions' => 'Permissions',

        'name' => 'Name',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'E-Mail',
        'role' => 'Role',
        'status' => 'Status',
        'photo' => 'Photo',
        'phone' => 'Phone',

        "account_setup" => "Account Setup",
        "profile_setup" => "Profile Setup",
        "confirm" => "Confirm",
        "you_have_errors" => "You have some form errors. Please check below.",
        "validation_succeeded" => " Your form validation is successful!",

        'password' => 'Password',
        'confirm_password' => 'Re-type Password',
        'current_password' => 'Current Password',
        'new_password' => 'New Password',
        'confirm_new_password' => 'Re-type New Password',
        'change_password' => 'Change Password',

        'view_profile' => 'View Profile',
        'view_profiles' => 'View Profiles',
        'preferences' => 'Loan Preferences',

        'email_have_taken' => 'Email has already been taken.',
        'email_already_exists' => 'This email is already registered before',

        'domain_blocked' => 'Domain is blocked',
        'domain_is_blocked' => 'Domain is blocked',

        'invalid_old_password' => 'Current password is invalid',

        'member_since' => 'Member Since',

        'my_subscription' => 'My subscription',
        'invoices' => 'Invoices',
    ],

    'profiles' => [
        'profile_info' => 'Profile Settings',
        'profile_info2' => 'Profile Info',
        'latest_loans' => 'Latest Loans',
        'feed' => 'Feed',

        'select_profile_type' => 'Which profile will you want to create',

        'profile_type' => 'Profile type',
        'title' => 'Job Title',
        'is_enabled' => 'Enabled ?',
        'is_active' => 'Is active ?',
        'active' => 'Active',
        'set_active' => 'Set as active',
        "add_profile" => "Add profile",
    ],

    'user_tracks' => [
        '_title' => 'Login History',
        'singular' => 'Login information',

        'date' => 'Date',
        'login_time' => 'Login time',
        'logout_time' => 'Logout time',
        'location' => 'Location',

        'country' => 'Country',
        'city' => 'City',
        'state' => 'State',
        'state_name' => 'State Name',
        'ip' => 'IP',
    ],

    "real_estate_types" => [
        '_title' => 'Real Estate Types',
        'singular' => 'Real Estate Type',
    ],

    "business_types" => [
        '_title' => 'Business Types',
        'singular' => 'Business Type',
    ],

    "property_types" => [
        '_title' => 'Property Types',
        'singular' => 'Property Type',
    ],

    "purpose_types" => [
        '_title' => 'Purpose Types',
        'singular' => 'Purpose Type',
    ],

    "lease_types" => [
        '_title' => 'Lease Types',
        'singular' => 'Lease Type',
    ],

    'settings' => [
        '_title' => 'Settings',
        'singular' => 'Settings',

        'loan_expire_period' => 'Loan expiration period',
        'loan_expire_period_unit' => 'Loan expiration period unit',
        'lender_max_loan_bids' => 'Max bids lender can provide per loan',

        "free_trial_period" => "Free trial period",
        "free_trial_period_unit" => "Free trial period unit",

        "free_lifetime_subscriptions_count" => "Lifetime subscriptions limit",

        "subscribe_reminder_email_first" => "First Email",
        "subscribe_reminder_email_second" => "Second Email",
        "subscribe_reminder_email_third" => "Third Email",

        "subscription_renew_reminder_settings" => "Subscription Renew Reminder Email Settings",

        "subscribe_renew_reminder_email" => "Email",
        "before_subscription_due" => "Before subscription due",

        "loans_settings" => "Loans Settings",
        "subscription_settings" => "Subscription Settings",
        "subscription_reminder_settings" => "Subscription Reminders Email Settings",
        "before_free_trial_ends" => "Before free trial ends",

    ],
    "address" => [
        'state' => 'State',
        'city' => 'City',
        'zip_code' => 'Zip Code',
        'address' => 'Street Address',
        'apt_suite_no' => 'Apt/Suite/Other',
        'full_address' => 'Address',
    ],

    "states" => [
        '_title' => 'States',
        'singular' => 'State',

        'state_code' => 'State code',
        'state' => 'State',
        'no_cities' => 'Cities count',
        'No_counties' => 'Counties count',
    ],
    "counties" => [
        '_title' => 'Counties',
        'singular' => 'County',

        'county' => 'County',
        'state' => 'State',
    ],

    "cities" => [
        '_title' => 'Cities',
        'singular' => 'City',

        'no_zip_codes' => 'Zip codes count',
        'zip_codes' => 'Zip codes',

        'state_code' => 'State code',
        'state' => 'State',
        'city' => 'City',
        'zip_code' => 'Zip Code',
        'latitude' => 'latitude',
        'longitude' => 'longitude',
    ],

    "real_estate_profiles" => [
        'zip_code' => 'Personnel zip code',
        'properties_owned_no' => 'Number of commercial properties owned',

        "properties" => "Properties",
        "optional" => "Optional",
        "address" => "Address",

        "realestate_type" => "Real Estate Type",
        "loan_outstanding" => "Loan Outstanding",
        "loan_maturity_date" => "Loan Maturity Date",

        "estimated_market_value" => "Est. market value ",
        'company_name' => 'Company Name',
    ],

    "business_profiles" => [
        'business_name' => 'Business name',
        'business_address' => 'Business address',
        'business_description' => 'Brief Business description',
        'establish_year' => 'Year Established',
        'fisical_year_end' => 'Fisical year end',

        'last_3_years_annual_revenues' => 'Last 3 years Revenues',
        'last_3_years_net_income_before_tax' => 'Last 3 years Net Income',
        'last_3_years_income_tax_expense' => 'Last 3 years Income Tax Expense',
        'last_3_years_interest_expense' => 'Last 3 years Interest Expense',
        'last_3_years_depreciation_amortization' => 'Last 3 years Depreciation/Amortization',
        'annual_revenue' => 'Annual Revenue',
        'revenue' => 'Revenue',
        'net_income_before_tax' => 'Net Income',
        'interest_expense' => 'Interest Expense',
        'depreciation_amortization' => 'Depreciation Amortization',
        'income_tax_expense' => 'Income Tax Expense',

        'first' => 'Year 1 (most recent)',
        'second' => 'Year 2',
        'third' => 'Year 3',

        'company_EBITDA' => 'EBITDA',
        'company_EBITDA_year_1' => 'Company EBITDA year1',
        'company_EBITDA_year_2' => 'Company EBITDA year2',
        'company_EBITDA_year_3' => 'Company EBITDA year3',

        'revenue_EBITDA_details' => 'Revenue and EBITDA Details',
//        'annual_revenue_year_1' => 'Year 1 (most recent)',
//        'annual_revenue_year_2' => 'Year 2',
//        'annual_revenue_year_3' => 'Year 3',
//
//        'net_income_before_tax_year_1' => 'Year 1 (most recent)',
//        'net_income_before_tax_year_2' => 'Year 2',
//        'net_income_before_tax_year_3' => 'Year 3',
//
//        'interest_expense_year_1' => 'Year 1 (most recent)',
//        'interest_expense_year_2' => 'Year 2',
//        'interest_expense_year_3' => 'Year 3',
//
//        'depreciation_amortization_year_1' => 'Year 1 (most recent)',
//        'depreciation_amortization_year_2' => 'Year 2',
        'depreciation_amortization_year_3' => 'Year 3',

        'your_role' => 'Your role',
        'profile_creator_role' => 'Job title',
        'decision_maker' => 'Decision maker ?',
    ],

    "lender_profiles" => [
        "address" => "Address",
        "title" => "Job Title",
        "bio" => "Biography",
        "linked_link" => "Linkedin link",
        "line_of_business" => "Line of Business",
    ],

    "header_frontend" => [
        "notifications" => "Notifications",
        "messages" => "Messages",
        "profile" => "Profile",
        "profile_settings" => "Profile Settings",
        "overview" => "Overview",
        "account" => "Account",
        "general_account_settings" => "General Account Settings",
        "general_account_overview" => "General Account Overview",
        "settings" => "Settings",
        "logout" => "Logout",
        "find_loans" => "Find Loans",
        "archived_loans" => "Ar`chived loans",
        "my_work" => "My Work",
        "search" => "Search",

        "you_have" => "You have",
        "profiles" => "Profiles",
        "add_profile" => "Add profile",
        "" => "",
        "" => "",
    ],

    "preferences" => [
        "fav_institution_types" => "Only Show to the Following Institution Types : ",
        "fav_lenders" => "Only Show To : ",
        "hide_from" => "Hide From : ",

        "loan_size_range" => "Loan size range",
        'loan_types' => 'Loan Types',
        'profile_types' => 'Profile Types',
        'loan_size_range' => 'Loan size range',
        'state' => 'State',
        'county' => 'County',
        'city' => 'City',
        'geo_location' => 'Zip Code',
        'min' => 'Minimun',
        'max' => 'Maximum',
    ],

    "tenants" => [
        'tenants_name' => 'Name',
        "space_occupied" => "Square Footage Occupied",
        "lease_rate" => "Annual Lease Revenue",
        "lease_type" => "Lease Type",
        "lease_expiration_date" => "Expiration Date",
    ],

    "loans" => [
        '_title' => 'Loans',
        'singular' => 'Loan',

        'new_loan' => 'New Loan',

        'requested_amount' => 'Requested Loan Amount',
        'requested_percent' => 'Requested Percent',
        'ltv' => 'LTV',

        'loan_request_details' => 'Loan request details',
        'estimated_market_price' => 'Estimated Market Price',

        'term_other_loan_type' => 'Loan Type',

        'amount' => 'Amount',
        'percent' => 'Percent',
        'number' => 'Number',
        'purpose_type' => 'Purpose',
        'purpose' => 'Purpose',

        'receivable_range' => 'Accounts Receivable Range Last 12 Months',
        'receivable_range_title' => 'Receivable Range',
        'if_applicable' => ' (if applicable)',
        'receivable_range_from' => 'Receivable Range From',
        'receivable_range_to' => 'Receivable Range To',
        'high' => 'High',
        'low' => 'Low',
        'inventory_range' => 'Inventory Range Last 12 Months',
        'inventory_range_title' => 'Inventory Range',
        'inventory_range_from' => 'Inventory Range From',
        'inventory_range_to' => 'Inventory Range To',

        'inventory_type' => 'Inventory Type',
        'current_ratio' => 'Current Ratio',
        'current_ratio_hint' => 'Current ratio defined as current assets divided by current liabilities',


        'real_estate_type' => 'RealEstate Type',
        'purchase_price' => 'Property Purchase Price',
        'loan_requested' => 'Loan Requested',

        'cash_before_down_payment' => 'Cash on hand before down payment',
        'building_sq_ft' => 'Building sq.ft',
        'building_sq_ft_tips' => 'Sometimes buildings are split into warehouse and office for example 
        and lender would want to know this split so need to direct to think of this',
        'tips' => 'Tips',

        'owner_occupied_percent' => 'Any % Owner Occupied ?',
        'owner_occupied_hint' => ' If more than 50% please use Owner Occupied template',

        'owner_occupied_percent_51' => '% To be owner occupied',
        'owner_occupied_hint_51' => 'If less than 50% please use Investor 0-50% Owner Occupied Template',

        'owner_not_occupied_percent' => '% Not owner occupied',
        'owner_not_occupied_hint' => 'If more than 1%, fill',

//        'gross_annual_revenue' => 'Gross Annual Revenue',
        'gross_annual_revenue' => 'Total Property Annual Income',
        'estimated_annual_expenses' => 'Estimated Annual Expenses',
        'estimated_annual_expenses_hint' => 'Do not include depreciation, amortization or interest expense',

        'annual_income_from_tenants' => 'Annual Income From Tenants',
        'estimated_property_value' => 'Estimated Property Value',
        'net_operating_income' => 'Net Operating Income',

        'cash_liquid_investments' => 'Personal and Business Liquidity',
        'cash_liquid_investments_hint' => 'Cash and/or liquid investments',

        'buildings_no' => 'Number of Buildings',
        'units_no' => 'Number of Units',
        'estimated_net_operating_income' => 'Estimated Net Operating Income',


        'tenants' => 'Tenants',

        'is_loan_outstanding' => 'Current Loan on the Building?',
        'current_loan_outstanding' => 'Current Loan Outstanding on Property',
        'outstanding_value' => 'Amount Outstanding',

        'rental_annual_revenue' => 'Yearly Rental Revenue',

        'is_moving_from_leased' => 'Moving From Currently Leased ?',
        'current_lease_expense' => 'Current lease expense',

        'company_EBITDA' => 'Company EBITDA',
        'company_EBITDA_hint' => 'Calculation: Net Income + income taxes + interest + depreciation + amortization',

        'total_liabilities' => 'Total Liabilities',

        'funded_debt' => 'Funded Debt',
        'funded_debt_hint' => 'Funded debt is defined as bank debt – I.e. debt that accrues interest',

        'tangible_net_worth' => 'Tangible Net Worth (TNW)',
        'tangible_net_worth_hint' => 'TNW is defined as total shareholders’ equity plus subordinated debt less due from related party less intangibles',

        'company_revenue' => 'Company Revenue',

        'rental_revenue_by_tenants' => 'Rental revenue by tenants?',
        'rental_revenue_by_tenants_text' => 'If 1%-50% will be occupied by other tenants, how much is rental revenue by tenants? ',

        'new' => 'New',
        'used' => 'Used',

        'equipment_details' => 'Equipment Details',
        'equipment_purhcase_price' => 'Equipment Purchase Price',

        'equipment_type' => 'Equipment Type',
        'vendor_name' => 'Vendor Name',
        'equipment_purpose' => 'Equipment purpose',
        'equipment_description' => 'Equipment description',

        'less_trade' => 'Less trade',
        'less_down_payment' => 'Less down payment',
        'Financed_amount_requested' => 'Financed amount requested',
        'equipment_finance_type' => 'Equipment finance type',


        'date_posted' => 'Date',
        'loan_request_name' => 'Name',
        'loan_request_size' => 'Size',
        'request_stage' => 'Stage',

        'company_name' => 'Company Name',
        'location' => 'Location',
        'profile_type' => 'Profile Type',
        'requestor' => 'Requestor',
        'loan_type' => 'Loan Type',
        'loan_size' => 'Loan Size',
        'status' => 'Status',

        'post_preferences' => 'Post Preferences',
        'reset_to_default' => 'Reset to default',
        'bid_placed' => 'Bid Placed',

        'delete_reason' => 'We would like to know why you are deleting the post.',

        'loan_details_d' => 'Loan Details',
        'property_information' => 'Property Information',
        'property_address' => 'Property Address',
        'property_type' => 'Property Type',
        'property_income' => 'Property Income',

        'details' => 'Details',

        'borrower' => 'Borrower',

        'attachments' => 'Attachments',
        'place_bid' => 'Place a bid',
        'about_borrower' => 'About the Borrower',

        "archived" => "Archived Loans",
        "archived_title" => "Archived",

        "my_work" => "My Work",
        "dashboard" => "Dashboard",

        "archive" => "Archive",
        "restore" => "Restore",

        "other_required_loans" => "Other required loans by this borrower",
        "similar_loans" => "Similar Loans",

        "loan_details" => "details",
        "place_bid" => "Place bid",

        "terms" => "Terms",
        "attachements" => "Attachements",
        "add_files" => "Add files",
        "drop_text" => "Drop files here or click to upload",

        "user_deleted" => "User Deleted",
    ],
    "feedbacks" => [
        'reason' => 'Reason',
    ],
    "filter" => [
        'filter' => 'Filter',
        'close' => 'Close Filters',
        'save_for_this_time_only' => 'Save For This Time Only',
        'save_to_my_profile' => 'Save to My Profile'
    ],
    "bids" => [
        '_title' => 'Bids',
        'bids' => 'Bids',
        'rate' => 'Rate',
        'term' => 'Term',
        'amortization' => 'Amortization',
        'requires_recourse' => 'Requires recourse',
        'status' => 'Status',

        'description' => 'Description',

        'follow_up' => 'Follow up',
        'bid_details' => 'Bid Details',
        'details' => 'Details',
        'messages' => 'Messages',

        'not_interested' => 'Not Interested',

        'about_lender' => 'About Lender',
        'lender' => 'Lender',

        'borrower_name' => 'Borrower',
        'loan_date' => 'Loan date',
        'loan_type' => 'Loan type',
        'bid_status' => 'Bid status',
        'location' => 'location',
        'requested_amount' => 'Requested amount',
        'bid_date' => 'Bid date',
        'lender_name' => 'Lender',
    ],
    "conversations" => [
        "type_message" => "Type a message here...",
        "cant_follow_up" => "We can see that you followed up twice , let us wait for the borrower to reply."
    ]
];
