<?php
return
    [
        "bid_message" => 'Dear :user_name, I just sent you our offer. Please let us know if you are interested.',
    ];