<?php
use App\Enums\NotificationTypes;

return [
    NotificationTypes::MESSAGE_RECEIEVED => [
        "body" => "New message has been received"
    ],
    NotificationTypes::BID_RECEIVED => [
        "body" => "You have just received new bid for the loan :loan from lender :lender "
    ],
    NotificationTypes::DEAL_ABOUT_TO_EXPIRE => [
        "body" => "Deal about to expire"
    ],
    NotificationTypes::BORROWER_NOT_INTERESTED => [
        "body" => "Borrower :borrower is not interested in your bid"
    ],
    NotificationTypes::DEAL_MATCHING_PREFERENCES => [
        "body" => "Deal matching preferences"
    ],
    NotificationTypes::DOMAIN_REQUIRES_APPROVAL => [
        "body" => "Domain :domain_name requires approval"
    ],
    NotificationTypes::LENDER_REQUIRES_APPROVAL => [
        "body" => "Lender :lender_email requires approval"
    ],
    NotificationTypes::NEW_BORROWER_REGISTERED => [
        "body" => "New borrower has been registered"
    ],
    NotificationTypes::NEW_LENDER_REGISTERED => [
        "body" => "New lender has been registered"
    ],
    NotificationTypes::POST_EDITED => [
        "body" => "A post you've bid on has beed edited"
    ],
];