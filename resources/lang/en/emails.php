<?php
return
    [
        "kind_regards" => "Kind Regards",
        "welcome" => [
            "subject" => ':user_name, a very warm welcome to the :app_name family!',
            "welcome_message" => "Welcome to :app_name, :user_name!",
            "we_are_happy" => "We’re so happy to have you with us. You’re one click away from helping borrowers...",
            "please_check" => "Please click on the button below to confirm we got the right email address.",
            "verify_my_email" => "Verify my email",
            "" => "",
        ],
        "application_received" => [
            "subject" => ':user_name your application has been received!',
            "" => "",
            "" => "",
        ],
        "reset_password" => [
            "subject" => ":app_name Forgotten password request",
            "hi_user" => "Hi, :user_name",
            "reset_required" => ":app_name recently received a request for a forgotten password.",
            "please_click" => "Please click the button below to change your password.",
            "change_password" => "Change Your Password",
            "" => "",
        ],
        "lender_status_changed" => [
            "subject" => "Your account now is :status",
            "visit_account" => "Visit your account",
            "account_status_description" => "Your application is now :status",
        ],
        "lender_requires_approval" => [
            "subject" => ":app_name: :user_name",
            "hello_admin" => 'Hello Admin,',
            "body" => ":email requires approval as a lender, follow the link to get redirected to users' approval panel.",
            "button_text" => "Approve Lender",
        ],
        "institution_requires_approval" => [
            "subject" => ":app_name: :user_name",
            "hello_admin" => 'Hello Admin,',
            "body" => ":email requires approval as an institution, follow the link to get redirected to institutions' approval panel.",
            "button_text" => "Approve Institution",
        ],
    ];