<?php
return
    [
        "index" => "View",
        "view" => "View",
        "create" => "Create",
        "edit" => "Update",
        "update" => "Update",
        "destroy" => "Delete",

        "dashboard" => "Dashboard",
        'settings' => 'Settings',

        "institutions_management" => "Institutions Management",
        "institutions" => "Institutions",
        "institutions_types" => "Institutions Types",
        "institutions_sizes" => "Institutions Sizes",

        "users_management" => "Users Management",
        "users" => "Users",
        "roles" => "Roles & Permissions",

        "posts_management" => "Posts Management",
        "posts" => "Posts",
        "real_estate_types" => "Real Estate Types",
        "business_types" => "Business Types",
        "property_types" => "Property Types",
        "purpose_types" => "Purpose Types",
        "lease_types" => "Lease Types",

        "geo_management" => "Geo-Location Management",
        'states' => 'States',
        'counties' => 'Counties',
        'cities' => 'Cities',
        'zip_codes' => 'Zip Codes',

    ];