@foreach($messages as $conversation)

    @if(Sentinel::inRole(RoleService::ROLE_BORROWER))
        @php
            $url = url('borrower/loans/bids/'.$conversation->bid->id.'/show');
        @endphp
    @elseif(Sentinel::inRole(RoleService::ROLE_LENDER))
        @php
            $url = url('lender/loans/bids/'.$conversation->bid->id.'/show');
        @endphp
    @endif

    @php
        $reply = $conversation->replies()->latest()->first();
        $user_reply = $conversation->replies()->where('receiver_id',Sentinel::getUser()->id)->latest()->first();
    @endphp
    <li style="{{$user_reply->is_read == 0 ? 'background: #f8f9fa;': 'default'}} ">
        @if(Sentinel::inRole(RoleService::ROLE_ADMIN))
            <a href="" class="open_messages"
               data-csrf="{{csrf_token()}}"
               data-action=""
            >
            <span class="photo"><img src="{{isset($reply->sender->photo) && !empty($reply->sender->photo) ? asset($reply->sender->photo) : asset('assets/loan-app/img/avatars/no_image.png')}}"class="img-circle" alt=""> </span>
            <span class="subject">
                <span class="from"> {{$reply->sender->full_name or ''}} </span>
            </span>
            <span class="message"> {{$reply->message}} </span>
            </a>
        @else
            <a href="{{$url}}" class="open_messages"
               data-csrf="{{csrf_token()}}"
               data-action="{{url('replies/' . $user_reply->id.'/mark_read')}}"
            >
                {{--<span class="label label-sm label-icon label-{{$reply->is_read == 0 ? 'success': 'default'}}">--}}

                <span class="photo"><img src="{{isset($reply->sender->photo) && !empty($reply->sender->photo) ? asset($reply->sender->photo) : asset('assets/loan-app/img/avatars/no_image.png')}}"class="img-circle" alt=""> </span>
                <span class="subject">
                    <span class="from"> {{$reply->sender->full_name or ''}} </span>
                </span>
                <span class="message"> {{$reply->message}} </span>

            </a>
        @endif
    </li>
@endforeach