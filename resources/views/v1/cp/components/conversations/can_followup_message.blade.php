@if(!$bid->can_follow_up)
    @if($bid->status == BidStatuses::BORROWER_NOT_INTERESTED)
        @include('v1.cp.components.loans.bid_status')
    @else
        <div class="note note-danger">
            <h4 class="block">{{trans('messages.conversations.cant_follow_up')}}</h4>
        </div>
    @endif
@endif