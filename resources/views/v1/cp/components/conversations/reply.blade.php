@if($reply->sender_id == $bid->lender_id)
    <li class="in">
        <img class="avatar" alt=""
             src="{{isset($reply->user->photo) && !empty($reply->user->photo) ? asset($reply->user->photo) : asset('assets/loan-app/img/avatars/no_image.png')}}"/>
        <div class="message">
            <span class="arrow"> </span>
            <a href="javascript:;" class="name"> {{$bid->lender->full_name}} </a>
            <span class="datetime"> {{$reply->created_at->diffForHumans()}} </span>
            <span class="body"> {{$reply->message}} </span>
        </div>
    </li>
@else
    <li class="out">
        <img class="avatar" alt=""
             src="{{isset($reply->user->photo) && !empty($reply->user->photo) ? asset($reply->user->photo) : asset('assets/loan-app/img/avatars/no_image.png')}}"/>

        <div class="message">
            <span class="arrow"> </span>
            <a href="javascript:;" class="name"> {{$bid->loan->borrower->full_name}} </a>
            <span class="datetime"> {{$reply->created_at->diffForHumans()}} </span>
            <span class="body"> {{$reply->message}} </span>
        </div>
    </li>
@endif