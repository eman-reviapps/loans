<a data-action="{{url('attachment/' .  $attachement->id)}}"
   data-cdata-csrf="{{csrf_token()}}"
   class="{{$view_class.' remove_attachement'}}" target="{{$view_target}}"
   data-title="World Clock Widget<br>by Paul Flavius Nechita">{{trans('messages.operations.remove')}}</a>
