<h5 class="form-section bold">{{trans('messages.loans.attachments')}} ( {{count($attachable->attachments) }}
    )</h5>
<div class="row">
    <div class="col-md-12">
    <div class="portfolio-content portfolio-1">
        @php
            $imageExtensions = ['jpeg','gif','png','bmp','jpg'];
            $videoExtensions = ['flv','mp4','mov','avi','wmv','3gp'];
        @endphp
        <div id="" class="cbp js-grid-juicy-projects">
            @foreach($attachable->attachments as $attachement)

                @php
                    if(in_array(File::extension(asset($attachement->url)), $imageExtensions) )
                    {
                        $thumb = asset($attachement->url);
                        $view_class = "cbp-lightbox cbp-l-caption-buttonRight btn red uppercase";
                        $view_target = "";
                        $a_class = "cbp-lightbox";
                      }
                    else if(in_array(File::extension(asset($attachement->url)), $videoExtensions) )
                    {
                        $thumb = asset('assets/loan-app/img/video.png');
                        $view_class = "cbp-lightbox cbp-l-caption-buttonRight btn red uppercase";
                        $view_target = "";
                        $a_class = "cbp-lightbox";
                     }
                    else if(strpos($attachement->url, 'pdf'))
                    {
                        $thumb = asset('assets/loan-app/img/pdf.jpeg');
                        $view_class = "cbp-l-caption-buttonRight btn red uppercase";
                        $view_target = "_blank";
                        $a_class = "";
                    }
                    else if(strpos($attachement->url, 'doc') || strpos($attachement->url, 'rtf'))
                    {
                        $thumb = asset('assets/loan-app/img/word.png');
                        $view_class = "cbp-l-caption-buttonRight btn red uppercase";
                        $view_target = "_blank";
                        $a_class = "";
                     }
                    else if(strpos($attachement->url, 'xls') )
                    {
                        $thumb = asset('assets/loan-app/img/xls.jpeg');
                        $view_class = "cbp-l-caption-buttonRight btn red uppercase";
                        $view_target = "_blank";
                        $a_class = "";
                     }
                    else if(strpos($attachement->url, 'csv') )
                    {
                        $thumb = asset('assets/loan-app/img/csv.jpeg');
                        $view_class = "cbp-l-caption-buttonRight btn red uppercase";
                        $view_target = "_blank";
                        $a_class = "";
                    }
                    else
                    {
                        $thumb = asset('assets/loan-app/img/no-thumbnail.jpg');
                        $view_class = "cbp-l-caption-buttonRight btn red uppercase";
                        $view_target = "_blank";
                        $a_class = "";
                    }
                @endphp

                <div class="cbp-item graphic logos">
                    <div class="cbp-caption">
                        <div class="cbp-caption-defaultWrap">
                            <img src="{{ $thumb }}" alt="" style="height: 100px;"></div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    @if($view)
                                        @include('v1.cp.components.attachement.view')
                                    @else
                                        @include('v1.cp.components.attachement.remove')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cbp-l-grid-projects-desc uppercase text-center">
                        <a href="{{ asset($attachement->url) }}" alt="{{ asset($attachement->url) }}"
                           target="{{$view_target}}"
                           class="{{$a_class}}">{{basename(asset($attachement->url))}}</a>
                    </div>
                </div>

            @endforeach
        </div>
    </div>
    </div>
</div>