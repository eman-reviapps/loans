<a href="{{ asset($attachement->url) }}"
   class="{{$view_class}}" target="{{$view_target}}"
   data-title="World Clock Widget<br>by Paul Flavius Nechita">{{trans('messages.operations.view')}}</a>
