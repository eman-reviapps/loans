<div class="blog-single-sidebar-recent">
    @if($loan->borrower)
        <p class="bold">{{trans('messages.loans.about_borrower')}}</p>
        <div class="static-info">
            <div class="value"> {{trans('messages.users.name')}}</div>
            <div class="name"> {{$loan->borrower->full_name or ''}}
            </div>
        </div>
        <div class="static-info">
            <div class="value"> {{trans('messages.users.phone')}}</div>
            <div class="name"> {{$loan->borrower_profile->profile->phone or ''}}
            </div>
        </div>

        @if($loan->borrower_profile->profile_type == BorrowerProfileTypes::BUSINESS)
            <div class="static-info">
                <div class="value"> {{trans('messages.business_profiles.business_name')}}</div>
                <div class="name"> {{$loan->borrower_profile->profile->business_name or ''}}
                </div>
            </div>
            <div class="static-info">
                <div class="value"> {{trans('messages.business_profiles.establish_year')}}</div>
                <div class="name"> {{$loan->borrower_profile->profile->establish_year or ''}}
                </div>
            </div>
            <div class="static-info">
                <div class="value"> {{trans('messages.business_profiles.business_description')}}</div>
                <div class="name"> {{$loan->borrower_profile->profile->description or ''}}
                </div>
            </div>
        @endif

        <div class="static-info">
            <div class="value"> {{trans('messages.address.full_address')}}</div>
            <div class="name"> {{$loan->borrower_profile->profile->full_address or ''}}
            </div>
        </div>

        <div class="static-info">
            <div class="value"> {{trans('messages.users.member_since')}}</div>
            <div class="name"> {{$loan->borrower->created_at->toFormattedDateString()}}
            </div>
        </div>

        @if($loan->borrower_profile->profile_type == BorrowerProfileTypes::BUSINESS)
            <div class="static-info">
                <button class="name btn btn-primary" id="ebitda" data-toggle="modal"
                        data-target="#myModal"> {{trans('messages.business_profiles.revenue_EBITDA_details')}}</button>

                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">

                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">{{trans('messages.business_profiles.revenue_EBITDA_details')}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="ebitda-details">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Year 1</th>
                                            <th>Year 2</th>
                                            <th>Year 3</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="bold">{{trans('messages.business_profiles.revenue')}}</td>
                                            <div class="hide">
                                                {{$field = $annual_revenue . '_year_1'}}
                                            </div>
                                            <td>{{$loan->borrower_profile->profile->$field}}</td>


                                            <div class="hide">
                                                {{$field = $annual_revenue . '_year_2'}}
                                            </div>
                                            <td>{{$loan->borrower_profile->profile->$field}}</td>


                                            <div class="hide">
                                                {{$field = $annual_revenue . '_year_3'}}
                                            </div>
                                            <td>{{$loan->borrower_profile->profile->$field}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"></td>
                                        </tr>
                                        @foreach($ebitdaDetails as $detail)
                                            <tr>
                                                <td>{{trans('messages.business_profiles.' . $detail)}}</td>
                                                <div class="hide">
                                                    {{$field = $detail . '_year_1'}}
                                                </div>
                                                <td>{{$loan->borrower_profile->profile->$field}}</td>


                                                <div class="hide">
                                                    {{$field = $detail . '_year_2'}}
                                                </div>
                                                <td>{{$loan->borrower_profile->profile->$field}}</td>


                                                <div class="hide">
                                                    {{$field = $detail . '_year_3'}}
                                                </div>
                                                <td>{{$loan->borrower_profile->profile->$field}}</td>

                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td class="bold">EBITDA</td>
                                            <td>{{$loan->borrower_profile->profile->company_EBITDA_year_1}}</td>
                                            <td>{{$loan->borrower_profile->profile->company_EBITDA_year_2}}</td>
                                            <td>{{$loan->borrower_profile->profile->company_EBITDA_year_3}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        @endif

        @if($loan->borrower_profile->profile_type == BorrowerProfileTypes::REAL_ESTATE)
            <button class="name btn btn-primary" data-toggle="modal"
                    data-target="#properties"> {{trans('messages.real_estate_profiles.properties')}}</button>

            <div class="modal fade" id="properties" role="dialog">
                <div class="modal-dialog">

                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">{{trans('messages.real_estate_profiles.properties')}}</h4>
                        </div>
                        <div class="modal-body">
                            @include('v1.cp.components.real_estate_profile.properties_view',['profile'=>$loan->borrower_profile])
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>

            <button class="name btn btn-primary attachments_show" data-toggle="modal"
                    data-target="#attachments"> {{trans('messages.loans.attachements')}}</button>


            <div class="modal" id="attachments" role="dialog" >

                <div class="modal-dialog ">

                    <div class="modal-content" style="overflow: auto">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">{{trans('messages.loans.attachements')}}</h4>
                        </div>
                        <div class="modal-body">

                            @include('v1.cp.components.attachement.all',['attachable'=>$loan->borrower_profile,'view'=>true])

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        @endif
    @endif
</div>