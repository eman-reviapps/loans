<div class="blog-single-sidebar-recent">
    <p class="bold">{{trans('messages.bids.about_lender')}}</p>
    <div class="static-info">
        <div class="value"> {{trans('messages.users.name')}}</div>
        <div class="name"> {{$bid->lender->full_name or ''}}
        </div>
    </div>

    <div class="static-info">
        <div class="value"> {{trans('messages.users.phone')}}</div>
        <div class="name"> {{$bid->lender->lenderProfile->phone or ''}}
        </div>
    </div>

    <div class="static-info">
        <div class="value"> {{trans('messages.users.institution')}}</div>
        <div class="name"> <a href="">{{$bid->lender->lenderProfile->institution->title or ''}}</a>
        </div>
    </div>

    <div class="static-info">
        <div class="value"> {{trans('messages.institutions.website')}}</div>
        <div class="name"> <a target="_blank" href="{{$bid->lender->lenderProfile->institution->domain or ''}}">{{$bid->lender->lenderProfile->institution->domain or ''}}</a>
        </div>
    </div>

    <div class="static-info">
        <div class="value"> {{trans('messages.address.full_address')}}</div>
        <div class="name"> {{$bid->lender->lenderProfile->full_address or ''}}
        </div>
    </div>

    <div class="static-info">
        <div class="value"> {{trans('messages.users.member_since')}}</div>
        <div class="name"> {{$bid->lender->created_at->toFormattedDateString()}}
        </div>
    </div>
</div>