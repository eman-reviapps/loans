<div class="mt-radio-inline">
    @include('v1.cp.components.general.radio_field',[
    'key'=>$option,
    'value'=>NotificationOptions::TEXT,
    ])

    @include('v1.cp.components.general.radio_field',[
   'key'=>$option,
   'value'=>NotificationOptions::EMAIL,
   ])

    @include('v1.cp.components.general.radio_field',[
  'key'=>$option,
  'value'=>NotificationOptions::BOTH,
  ])

    @include('v1.cp.components.general.radio_field',[
  'key'=>$option,
  'value'=>NotificationOptions::NONE,
  ])

</div>