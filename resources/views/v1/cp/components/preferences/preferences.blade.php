<table class="table table-bordered table-striped table-preferences">

    <tr>
        <td class="width-30"> {{trans('messages.preferences.fav_institution_types')}}</td>

        <td>
            @include('v1.cp.components.preferences.preference_multiselect',
            [
            'option'=>PreferenceOptions::FAVORITE_INSTITUTION_TYPES,
            'items'=>$institutionTypes
            ]
            )
        </td>
    </tr>

    <tr>
        <td class="width-30"> {{trans('messages.preferences.fav_lenders')}}</td>

        <td>
            @include('v1.cp.components.preferences.preference_multiselect',
            [
            'option'=>PreferenceOptions::FAVORITED_LENDERS,
            'items'=>$lenders
            ]
            )
        </td>
    </tr>

    <tr>
        <td class="width-30"> {{trans('messages.preferences.hide_from')}}</td>

        <td>
            @include('v1.cp.components.preferences.preference_multiselect',
            [
            'option'=>PreferenceOptions::NON_FAVORITED_LENDERS,
            'items'=>$lenders
            ]
            )
        </td>
    </tr>

</table>