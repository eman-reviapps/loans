<form role="form" id="borrowerPreferences" action="{{url('borrower/profiles/'.$profile->id.'/preferences')}}"
      method="post">
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="_token" value="<?= csrf_token() ?>">
    <input type="hidden" name="save_continue_tab" id="save_continue_tab" value="">
    @php
        $preferences = $profile->preferences->where('loan_id', null)->keyBy('slug');
    @endphp

    @include('v1.cp.components.preferences.preferences',['preferences'=>$preferences])

    <!--end profile-settings-->
    <div class="margin-top-10">
        <button type="button" class="btn btn-success btn_action_save_continue_tab">
            {{trans('messages.operations.save_changes')}}
        </button>
        <button type="button" class="btn btn-secondary-outline btn_action_back">
            {{trans('messages.operations.cancel')}}
        </button>
    </div>
</form>