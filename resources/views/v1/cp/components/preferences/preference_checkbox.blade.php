<div class="mt-checkbox-inline">
    @foreach($items as $key =>$title)
        @include('v1.cp.components.general.checkbox_field',[
                'option'=>$option,
                'key'=>$key,
                'title'=>$title,
                'name'=>$preferences[$option]['slug'].'[]',
                'value'=>$preferences[$option]['current_value'],
                ])
    @endforeach
</div>