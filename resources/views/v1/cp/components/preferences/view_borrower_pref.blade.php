<table class="table table-bordered table-preferences">

    <tr>
        <td class="width-30"> {{trans('messages.preferences.fav_institution_types')}}</td>

        <td>
            @include('v1.cp.components.preferences.preference_multiselect_view',
       [
       'option'=>PreferenceOptions::FAVORITE_INSTITUTION_TYPES,
       'items'=>\App\Models\InstitutionType::pluck('name', 'id')->all(),
       ]
       )
        </td>
    </tr>

    <tr>
        <td class="width-30"> {{trans('messages.preferences.fav_lenders')}}</td>

        <td>
            @include('v1.cp.components.preferences.preference_multiselect_view',
        [
        'option'=>PreferenceOptions::FAVORITED_LENDERS,
        'items'=>\App\Models\Institution::pluck('domain', 'id')->all(),
        ]
        )
        </td>
    </tr>

    <tr>
        <td class="width-30"> {{trans('messages.preferences.hide_from')}}</td>

        <td>
            @include('v1.cp.components.preferences.preference_multiselect_view',
        [
        'option'=>PreferenceOptions::NON_FAVORITED_LENDERS,
        'items'=>\App\Models\Institution::pluck('domain', 'id')->all(),
        ]
        )
        </td>
    </tr>


</table>