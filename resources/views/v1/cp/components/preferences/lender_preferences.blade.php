<form role="form" id="LenderPreferences" action="{{url('users/'.$user->id.'/preferences')}}" method="post">
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="_token" value="<?= csrf_token() ?>">
    <input type="hidden" name="save_continue_tab" id="save_continue_tab" value="">
    @php
        $preferences = $user->preferences->where('loan_id', null)->where('profile_id', null)->keyBy('slug');

    @endphp
    <table class="table table-bordered table-striped table-preferences">

        <tr>
            <td class="width-30">
                {{$preferences[PreferenceOptions::FAVORITE_PROFILE_TYPES]['description']}}

                <div class="mt-radio-inline">
                    <label class="mt-radio">
                        <input type="radio" name="profile_types_select" id="profile_types_select_all" value="1"
                        > {{trans('messages.operations.select_all')}}
                        <span></span>
                    </label>
                    <label class="mt-radio">
                        <input type="radio" name="profile_types_select" id="profile_types_select_none" value="0">
                        {{trans('messages.operations.un_select_all')}}
                        <span></span>
                    </label>
                </div>
            </td>
            <td>
                @include('v1.cp.components.preferences.preference_checkbox',
                    [
                    'option'=>PreferenceOptions::FAVORITE_PROFILE_TYPES,
                    'items'=>(BorrowerProfileTypes::items())
                    ]
                )
            </td>
        </tr>
        <tr>
            <td class="width-30">
                {{$preferences[PreferenceOptions::FAVORITE_LOAN_TYPES]['description']}}

                <div class="mt-radio-inline">
                    <label class="mt-radio">
                        <input type="radio" name="loan_types_select" id="loan_types_select_all" value="1"
                        > {{trans('messages.operations.select_all')}}
                        <span></span>
                    </label>
                    <label class="mt-radio">
                        <input type="radio" name="loan_types_select" id="loan_types_select_none" value="0">
                        {{trans('messages.operations.un_select_all')}}
                        <span></span>
                    </label>
                </div>
            </td>
            <td>
                @include('v1.cp.components.preferences.preference_checkbox',
                    [
                    'option'=>PreferenceOptions::FAVORITE_LOAN_TYPES,
                    'items'=>LoanTypes::items()
                    ]
                )
            </td>
        </tr>
        <tr>
            <td class="width-30"> {{trans('messages.preferences.loan_size_range')}}</td>
            <td>
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                            Min $
                        </span>
                        <input type="text"
                               class="form-control mask_money"
                               name="{{PreferenceOptions::FAVORITE_LOAN_SIZE_MIN}}"
                               id="{{PreferenceOptions::FAVORITE_LOAN_SIZE_MIN}}"
                               value="{{old(PreferenceOptions::FAVORITE_LOAN_SIZE_MIN) ? old(PreferenceOptions::FAVORITE_LOAN_SIZE_MIN) : $preferences[PreferenceOptions::FAVORITE_LOAN_SIZE_MIN]['current_value']}}"
                               placeholder="{{$preferences[PreferenceOptions::FAVORITE_LOAN_SIZE_MIN]['description']}}"
                        >
                    </div>
                </div>
                <span class="alert-danger"><?php echo $errors->first(PreferenceOptions::FAVORITE_LOAN_SIZE_MIN) ?></span>
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                            Max $
                        </span>
                        <input type="text"
                               class="form-control mask_money"
                               name="{{PreferenceOptions::FAVORITE_LOAN_SIZE_MAX}}"
                               id="{{PreferenceOptions::FAVORITE_LOAN_SIZE_MAX}}"
                               value="{{old(PreferenceOptions::FAVORITE_LOAN_SIZE_MAX) ? old(PreferenceOptions::FAVORITE_LOAN_SIZE_MAX) : $preferences[PreferenceOptions::FAVORITE_LOAN_SIZE_MAX]['current_value']}}"
                               placeholder="{{$preferences[PreferenceOptions::FAVORITE_LOAN_SIZE_MAX]['description']}}"
                        >
                    </div>
                </div>
                <span class="alert-danger"><?php echo $errors->first(PreferenceOptions::FAVORITE_LOAN_SIZE_MAX) ?></span>

            </td>
        </tr>
        <tr>
            <td class="width-30"> {{$preferences[PreferenceOptions::FAVORITE_STATE]['description']}}</td>
            <td>
                <div class="col-md-8">
                    <select id="{{PreferenceOptions::FAVORITE_STATE}}"
                            name="{{PreferenceOptions::FAVORITE_STATE}}[]"
                            class="form-control select2-multiple states" multiple>
                        @if($preferences[PreferenceOptions::FAVORITE_STATE]->value)
                            @foreach($preferences[PreferenceOptions::FAVORITE_STATE]->value as $item)
                                <option selected id="{{$item}}">{{$item}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <td class="width-30"> {{$preferences[PreferenceOptions::FAVORITE_CITY]['description']}}</td>
            <td>
                <div class="col-md-8">
                    <select id="{{PreferenceOptions::FAVORITE_CITY}}"
                            name="{{PreferenceOptions::FAVORITE_CITY}}[]"
                            class="form-control select2-multiple cities" multiple>
                        @if($preferences[PreferenceOptions::FAVORITE_CITY]->value)
                            @foreach($preferences[PreferenceOptions::FAVORITE_CITY]->value as $item)
                                <option selected id="{{$item}}">{{$item}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <td class="width-30"> {{trans('messages.preferences.county')}}</td>
            <td>
                <div class="col-md-8">
                    <select id="{{PreferenceOptions::FAVORITE_LOCATION}}"
                            name="{{PreferenceOptions::FAVORITE_LOCATION}}[]"
                            class="form-control select2-multiple counties" multiple>
                        @if($preferences[PreferenceOptions::FAVORITE_LOCATION]->value)
                            @foreach($preferences[PreferenceOptions::FAVORITE_LOCATION]->value as $item)
                                <option selected id="{{$item}}">{{$item}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </td>
        </tr>
    </table>
    <!--end profile-settings-->
    <div class="margin-top-10">
        <button type="button" class="btn btn-success btn_action_save_continue_tab">
            {{trans('messages.operations.save_changes')}}
        </button>
        <button type="button" class="btn btn-secondary-outline btn_action_back">
            {{trans('messages.operations.cancel')}}
        </button>
    </div>
</form>