<select class="form-control select2-multiple" id="{{$preferences[$option]['slug'].'[]'}}" multiple
        name="{{$preferences[$option]['slug'].'[]'}}">
    <option value="">{{trans('messages.common.select_dropdown')}}</option>
    @foreach($items as $key => $value)
        <option {{ ( is_array($preferences[$option]['current_value']) ? in_array($key,$preferences[$option]['current_value']) : $preferences[$option]['current_value'] == $key) ? 'selected' : ''}}
                value="{{$key}}">
            {{$value}}
        </option>
    @endforeach
</select>