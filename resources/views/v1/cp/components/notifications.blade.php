@foreach($notifications as $notification)
    @php
        $url = '';
        if($notification->notification_type == \App\Enums\NotificationTypes::BID_RECEIVED)
        {
        $bid = \App\Models\Bid::find($notification->referenced_id);
        $url = url('borrower/loans/bids/'.$bid->id.'/show');
        }elseif($notification->notification_type == \App\Enums\NotificationTypes::BORROWER_NOT_INTERESTED)
        {
        $bid = \App\Models\Bid::find($notification->referenced_id);
        $url = url('lender/loans/bids/'.$bid->id.'/show');
        }
        elseif($notification->notification_type == \App\Enums\NotificationTypes::LENDER_REQUIRES_APPROVAL){
            $url = url('admin/users?status=ACTIVATION_REQUIRED');
        }
        elseif($notification->notification_type == \App\Enums\NotificationTypes::NEW_LENDER_REGISTERED){
            $url = url('admin/users');
        }
        elseif($notification->notification_type == \App\Enums\NotificationTypes::DOMAIN_REQUIRES_APPROVAL){
            $url = url('admin/institutions?status=ACTIVATION_REQUIRED');
        }
        elseif($notification->notification_type == \App\Enums\NotificationTypes::POST_EDITED){
            $bid = \App\Models\Bid::whereRaw('loan_id = '.$notification->referenced_id.' and lender_id = '.$notification->user_id)->distinct('id')->get();
            $url = url('lender/loans/bids/'.$bid[0]->id.'/show');
        }
        elseif($notification->notification_type == ""){
            $bid = \App\Models\Bid::whereRaw('loan_id = '.$notification->referenced_id.' and lender_id = '.$notification->user_id)->distinct('id')->get();
            $url = url('lender/loans/bids/'.$bid[0]->id.'/show');
        }
    @endphp
    <li>
        <a href="{{$url}}" class="open_notification"
           data-csrf="{{csrf_token()}}"
           data-action="{{url('notifications/' . $notification->id.'/mark_read')}}"
        >
            <span class="details">
                <span class="label label-sm label-icon label-{{$notification->is_read == 0 ? 'success': 'default'}}">
                </span>
                {{$notification->message}}
            </span>
        </a>
    </li>
@endforeach