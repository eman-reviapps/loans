<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.address.full_address')}}</div>
    <div class="col-md-7 value"> {{$loan->details->full_address}}
    </div>
</div>