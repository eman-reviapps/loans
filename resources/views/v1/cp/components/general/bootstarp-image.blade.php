<label class="control-label bold col-md-4">{{$title}}
    :</label>
<div class="fileinput fileinput-new" data-provides="fileinput">
    <div class="fileinput-new thumbnail"
         style="width: 80px; height: 80px;">
        <img src="{{isset($photo) && !empty($photo) ? asset($photo) : asset('assets/tr-app/img/avatars/no_image.png')}}"
             alt=""/>
    </div>
    <div class="fileinput-preview fileinput-exists thumbnail"
         style="max-width: 80px; max-height: 80px;">
    </div>
    <div>
                <span class="btn default btn-file">
                    <span class="fileinput-new"> {{trans('messages.operations.select_image')}} </span>
                    <span class="fileinput-exists"> {{trans('messages.operations.change')}} </span>
                    <input type="file" name="{{$key}}" id="{{$key}}" class="photos"/>
                    <input type="hidden" name="hidden_{{$key}}" class="hidden_photos"
                           value="{{isset($photo) && !empty($photo) ? $photo : '' }}"/>
                </span>
        <a href="javascript:;" class="btn default fileinput-exists"
           data-dismiss="fileinput"> {{trans('messages.operations.remove')}} </a>
    </div>
</div>
<span class="alert-danger"><?php echo $errors->first($key) ?></span>