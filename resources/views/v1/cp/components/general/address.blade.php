
<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.address.address')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="address" id="address"
               value="{{old('address') ? old('address') : (isset($record) ? $record->address : '')}}"
               placeholder="{{trans('messages.address.address')}}">
        <span class="alert-danger"><?php echo $errors->first('address') ?></span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.address.apt_suite_no')}}
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="apt_suite_no" id="apt_suite_no"
               value="{{old('apt_suite_no') ? old('apt_suite_no') : (isset($record) ? $record->apt_suite_no : '')}}"
               placeholder="{{trans('messages.address.apt_suite_no')}}">
        <span class="alert-danger"><?php echo $errors->first('apt_suite_no') ?></span>
    </div>
</div>
<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.address.state')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">

        <select class="select2 form-control" id="state"
                name="state">
            <option value="">{{trans('messages.common.select_dropdown')}}</option>
            @foreach($states as $state)
                <option {{(old('state') == $state->state_code || (isset($record) && $record->state_code == $state->state_code)) ? 'selected' : ''}} value="{{$state->state_code}}">{{$state->state}}</option>
            @endforeach
        </select>
        <span class="alert-danger"><?php echo $errors->first('state') ?></span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.address.city')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">

        <select class="select2 form-control" id="city"
                name="city">
            <option value="">{{trans('messages.common.select_dropdown')}}</option>
            @if($cities)
                @foreach($cities as $city)
                    <option {{(old('city') == $city->city || (isset($record) && $record->city == $city->city)) ? 'selected' : ''}} value="{{$city->city}}">{{$city->city}}</option>
                @endforeach
            @endif
        </select>
        <span class="alert-danger"><?php echo $errors->first('city') ?></span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.address.zip_code')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">

        <select class="select2 form-control" id="zip_code"
                name="zip_code">
            <option value="">{{trans('messages.common.select_dropdown')}}</option>
            @if(isset($zip_codes))
                @foreach($zip_codes as $zip_code)
                    <option {{(old('$zip_code') == $zip_code->zip_code || (isset($record) && $record->zip_code == $zip_code->zip_code)) ? 'selected' : ''}} value="{{$zip_code->zip_code}}">{{$zip_code->zip_code}}</option>
                @endforeach
            @endif
        </select>
        <span class="alert-danger"><?php echo $errors->first('zip_code') ?></span>
    </div>
</div>