<label class="control-label bold">{{$title}}
    :</label>
<textarea
        class="form-control"
        id="{{$key}}"
        name="{{$key}}"
>{{old($key) ? old($key) : $value}}</textarea>
<span class="alert-danger"><?php echo $errors->first($key) ?></span>s