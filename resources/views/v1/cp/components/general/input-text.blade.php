<input type="text"
       class="form-control"
       name="{{$key}}"
       id="{{$key}}"
       value="{{old($key) ? old($key) : $value}}"
       placeholder="{{trans('messages.courses.title')}}"
>
<span class="alert-danger"><?php echo $errors->first($key) ?></span>