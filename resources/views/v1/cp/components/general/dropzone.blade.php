
<div class="modal fade" id="basic" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">{{trans('messages.loans.add_files')}}</h4>
            </div>
            <div class="modal-body">
                <form action="{{url('borrower/loans/upload_file')}}" class="dropzone dropzone-file-area"
                      id="my-dropzone" style="width: 500px; margin-top: 50px;">
                    <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                    <input type="hidden" name="directory" value="uploads/borrower_profiles"/>
                    <h3 class="bold">{{trans('messages.loans.drop_text')}}</h3>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">{{trans('messages.operations.close')}}</button>
            </div>
        </div>
    </div>
</div>