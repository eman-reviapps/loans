
<label class="mt-checkbox">
    <input type="checkbox" name="{{$name}}"
           value="{{$key}}" {{
           ( is_array($value) ? in_array($key,$value) : $value == $key)
           ? 'checked' : ''}} > {{$title}}
    <span></span>
</label>