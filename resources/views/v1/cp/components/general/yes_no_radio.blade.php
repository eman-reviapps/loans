<label class="control-label bold">{{$title}}</label>
<div class="mt-radio-inline">
    <label class="mt-radio">
        <input type="radio" name="{{$key}}"
               value="1"
                {{ ($value == 1) ? 'checked' : ''}}
        />
        {{trans('messages.common.yes')}}
        <span></span>
    </label>
    <label class="mt-radio">
        <input type="radio" name="{{$key}}"
               value="0"
                {{ ($value == 0) ? 'checked' : ''}}
        />
        {{trans('messages.common.no')}}
        <span></span>
    </label>
</div>

<span class="alert-danger"><?php echo $errors->first($key) ?></span>