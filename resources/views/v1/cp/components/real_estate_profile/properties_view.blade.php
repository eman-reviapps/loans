<table class="table table-striped table-bordered table-advance table-hover">
    <thead>
    <tr>
        <th>
            {{trans('messages.real_estate_profiles.realestate_type')}}
        </th>
        <th class="hidden-xs">
            {{trans('messages.real_estate_profiles.address')}}
        </th>
        <th>
            {{trans('messages.real_estate_profiles.estimated_market_value')}}
        </th>
        <th>
            {{trans('messages.real_estate_profiles.loan_outstanding')}}
        </th>
        <th>
            {{trans('messages.real_estate_profiles.loan_maturity_date')}}
        </th>
    </tr>
    </thead>
    <tbody>

    @foreach($profile->profile->properties as $property)
        <tr>
            <td>{{$property->realestatetype->name  or ''}}</td>
            <td class="hidden-xs">{{$property->address or ''}}</td>
            <td>$ {{$property->estimated_market_value or ''}}</td>
            <td>$ {{$property->loan_outstanding or ''}}</td>
            <td> {{$property->loan_maturity_date ?  $property->loan_maturity_date->toDateString() : ''}}</td>
        </tr>
    @endforeach
    </tbody>
</table>