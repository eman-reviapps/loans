<div data-repeater-item class="mt-repeater-item">

    <div class="mt-repeater-input mt-repeater-textarea">
        <label class="control-label">{{trans('messages.real_estate_profiles.address')}}</label>
        <br/>

        <input type="hidden" id="property_id" name="property_id" value="{{$property->id or ''}}"/>

        <textarea name="address"
                  class="form-control">{{$property->address or ''}}</textarea>
    </div>

    <div class="mt-repeater-input">
        <label class="control-label">{{trans('messages.real_estate_profiles.realestate_type')}}
        </label>
        <br/>

        <select name="realestate_type"
                class="form-control select2 realestate_type">
            <option value="">{{trans('messages.common.select_dropdown')}}</option>
            @foreach($realestate_types as $key => $value)
                <option {{((old('realestate_type') && old('realestate_type') == $key) || ($property && $property->realestate_type_id == $key)) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
            @endforeach
        </select>
    </div>

    <div class="mt-repeater-input">
        <label class="control-label">{{trans('messages.real_estate_profiles.estimated_market_value')}}
        </label>
        <br/>
        <div class="input-group">
            <span class="input-group-addon">
                $
            </span>
            <input type="text" name="estimated_market_value"
                   value="{{old('estimated_market_value') ? old('estimated_market_value') : (isset($property) ? $property->estimated_market_value : '')}}"
                   class="form-control mask_money"/></div>

    </div>
    <br/>
    <div class="mt-repeater-input">
        <label class="control-label">{{trans('messages.real_estate_profiles.loan_outstanding')}}
        </label>
        <br/>
        <div class="input-group">
            <span class="input-group-addon">
                $
            </span>
            <input type="text" name="loan_outstanding"
                   value="{{old('loan_outstanding') ? old('loan_outstanding') : (isset($property) ? $property->loan_outstanding : '')}}"
                   class="form-control mask_money"/></div>

    </div>
    <div class="mt-repeater-input">
        <label class="control-label">{{trans('messages.real_estate_profiles.loan_maturity_date')}}
        </label>
        <br/>
        <div class="input-group">
            <span class="input-group-btn">
                <button class="btn default" type="button">
                    <i class="fa fa-calendar"></i>
                </button>
            </span>
            <input type="text" name="loan_maturity_date"
                   value="{{old('loan_maturity_date') ? old('loan_maturity_date') : (isset($property) ? $property->loan_maturity_date : '')}}"
                   class="form-control date-picker"/></div>

    </div>


    <div class="mt-repeater-input">
        <a href="javascript:;" data-repeater-delete
           class="btn btn-danger btn-sm mt-repeater-delete">
            <i class="fa fa-close"></i>
        </a>
    </div>
</div>