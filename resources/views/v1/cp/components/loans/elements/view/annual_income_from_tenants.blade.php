<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.annual_income_from_tenants')}}</div>
    <div class="col-md-7 value"> $ {{$loan->details->annual_income_from_tenants}}
    </div>
</div>
