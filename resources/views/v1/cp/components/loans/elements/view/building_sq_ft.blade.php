@if(isset($loan->details->building_sq_ft) && $loan->details->building_sq_ft != null)
    <div class="row static-info">
        <div class="col-md-3 name"> {{trans('messages.loans.building_sq_ft')}}</div>
        <div class="col-md-7 value"> {{$loan->details->building_sq_ft}}
        </div>
    </div>
@endif

@if(isset($loan->details->buildings_no) && $loan->details->buildings_no != null)
    <div class="row static-info">
        <div class="col-md-3 name"> {{trans('messages.loans.buildings_no')}}</div>
        <div class="col-md-7 value"> {{$loan->details->buildings_no}}
        </div>
    </div>
    <div class="row static-info">
        <div class="col-md-3 name"> {{trans('messages.loans.units_no')}}</div>
        <div class="col-md-7 value"> {{$loan->details->units_no}}
        </div>
    </div>
@endif
@if($loan->details->tips)
    <div class="col-md-3 name"> {{trans('messages.loans.building_sq_ft_tips')}}</div>
    <div class="col-md-7 value"> {{$loan->details->tips}}
    </div>
@endif
