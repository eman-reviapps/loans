<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.estimated_market_price')}}</div>
    <div class="col-md-7 value"> $ {{$loan->details->estimated_market_price}}
    </div>
</div>
