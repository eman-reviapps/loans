<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.is_moving_from_leased')}}</div>
    <div class="col-md-2 value"> {{ $loan->details->is_moving_from_leased == 1 ?trans('messages.common.yes') : trans('messages.common.no')}}
    </div>
    @if($loan->details->current_lease_expense)
        <div class="col-md-2 name"> {{trans('messages.loans.current_lease_expense')}}</div>
        <div class="col-md-2 value"> {{$loan->details->current_lease_expense}}
        </div>
    @endif
</div>