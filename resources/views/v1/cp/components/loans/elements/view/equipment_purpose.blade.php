
<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.equipment_purpose')}}</div>
    <div class="col-md-7 value"> {{\App\Enums\EquipmentPurposeTypes::all()[$loan->details->equipment_purpose]}}
    </div>
</div>
