<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.loan_requested')}}</div>
    <div class="col-md-2 name"> {{trans('messages.loans.amount')}}</div>
    <div class="col-md-2 value"> {{$loan->details->requested_amount}}
    </div>
    <div class="col-md-2 name"> {{trans('messages.loans.percent')}}</div>
    <div class="col-md-2 value"> {{$loan->details->requested_percent}}
    </div>
</div>