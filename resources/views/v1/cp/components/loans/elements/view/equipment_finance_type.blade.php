<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.equipment_finance_type')}}</div>
    <div class="col-md-7 value"> {{\App\Enums\EquipmentFinanceTypes::all()[$loan->details->equipment_finance_type] }}
    </div>
</div>
