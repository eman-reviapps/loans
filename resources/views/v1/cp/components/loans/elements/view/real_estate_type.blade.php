<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.property_type')}}</div>
    <div class="col-md-7 value"> {{$loan->details->real_estate_type->name or ''}}
    </div>
</div>
