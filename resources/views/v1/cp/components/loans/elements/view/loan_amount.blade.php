<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.requested_amount')}}</div>
    <div class="col-md-7 value">
        $ {{$loan->details->requested_value}}
    </div>
</div>