<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.tangible_net_worth')}}</div>
    <div class="col-md-7 value"> {{$loan->details->tangible_net_worth}}
    </div>
</div>
