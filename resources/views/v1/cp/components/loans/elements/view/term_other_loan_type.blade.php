<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.term_other_loan_type')}}</div>
    <div class="col-md-7 value"> {{\App\Enums\LoanOtherLoanTypes::all()[$loan->details->term_other_loan_type] }}
    </div>
</div>