<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.purpose_type')}}</div>
    <div class="col-md-3 value"> {{$loan->details->purpose_type->name}}
    </div>
    @if($loan->details->purpose_type->name ==  'dOther')
        <div class="col-md-3 value"> ( {{$loan->details->purpose}} )
        </div>
    @endif
</div>