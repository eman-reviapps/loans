<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.estimated_property_value')}}</div>
    <div class="col-md-7 value"> $ {{$loan->details->estimated_property_value}}
    </div>
</div>
