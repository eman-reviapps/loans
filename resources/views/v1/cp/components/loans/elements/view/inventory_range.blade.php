
<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.inventory_range_title')}}</div>
    <div class="col-md-3 value"> $ {{$loan->details->inventory_range_from}}
    </div>
    <div class="col-md-3 value"> $ {{$loan->details->inventory_range_to}}
    </div>
</div>
