<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.funded_debt')}}</div>
    <div class="col-md-7 value"> {{$loan->details->funded_debt}}
    </div>
</div>
