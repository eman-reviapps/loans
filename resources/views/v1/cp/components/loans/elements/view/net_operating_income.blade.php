<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.net_operating_income')}}</div>
    <div class="col-md-7 value"> $ {{$loan->details->net_operating_income}}
    </div>
</div>
