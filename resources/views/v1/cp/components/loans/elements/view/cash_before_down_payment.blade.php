<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.cash_before_down_payment')}}</div>
    <div class="col-md-7 value"> {{$loan->details->cash_before_down_payment}}
    </div>
</div>
