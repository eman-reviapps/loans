<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.equipment_description')}}</div>
    <div class="col-md-7 value"> {{$loan->details->equipment_description}}
    </div>
</div>