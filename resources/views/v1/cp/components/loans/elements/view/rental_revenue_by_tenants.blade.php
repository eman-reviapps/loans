
<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.rental_revenue_by_tenants_text')}}</div>
    <div class="col-md-7 value"> {{$loan->details->rental_revenue_by_tenants}}
    </div>
</div>
