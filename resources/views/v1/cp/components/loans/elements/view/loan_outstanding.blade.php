<div class="row static-info">
    <div class="col-md-2 name"> {{trans('messages.loans.'.$title_key)}}</div>
    <div class="col-md-3 value"> {{$loan->details->is_loan_outstanding == 1 ?trans('messages.common.yes') : trans('messages.common.no')}}
    </div>

    <div class="col-md-2 name"> {{trans('messages.loans.outstanding_value')}}</div>
    <div class="col-md-3 value"> $ s{{$loan->details->outstanding_value}}
    </div>
</div>
