@php
    if(isset($loan_51) && $loan_51)
    {
        $title =trans('messages.loans.owner_occupied_percent_51');
        $hint =trans('messages.loans.owner_occupied_hint_51');
    }
    else
    {
        $title =trans('messages.loans.owner_occupied_percent');
        $hint =trans('messages.loans.owner_occupied_hint');
    }
@endphp

<div class="row static-info">
    <div class="col-md-3 name"> {{$title}}</div>
    <div class="col-md-7 value"> {{$loan->details->owner_occupied_percent}}
    </div>
</div>
