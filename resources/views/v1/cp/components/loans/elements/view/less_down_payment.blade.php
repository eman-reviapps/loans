<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.less_down_payment')}}</div>
    <div class="col-md-7 value">
        $ {{$loan->details->less_down_payment}}
    </div>
</div>