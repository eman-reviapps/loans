<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.owner_not_occupied_percent')}}</div>
    <div class="col-md-7 value"> {{$loan->details->owner_not_occupied_percent}}
    </div>
</div>
