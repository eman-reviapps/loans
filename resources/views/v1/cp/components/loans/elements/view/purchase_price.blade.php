@php
    if(isset($title_key))
    {
    $title = trans('messages.loans.'.$title_key);
    }else
    {
    $title = trans('messages.loans.purchase_price');
    }
@endphp

<div class="row static-info">
    <div class="col-md-3 name"> {{$title}}</div>
    <div class="col-md-7 value"> $ {{$loan->details->purchase_price}}
    </div>
</div>
