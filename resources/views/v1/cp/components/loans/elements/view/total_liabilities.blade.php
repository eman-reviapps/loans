<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.total_liabilities')}}</div>
    <div class="col-md-7 value"> {{$loan->details->total_liabilities}}
    </div>
</div>
