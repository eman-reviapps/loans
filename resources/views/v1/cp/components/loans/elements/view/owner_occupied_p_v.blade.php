<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.owner_occupied_percent')}}</div>
    <div class="col-md-2 name"> {{trans('messages.loans.percent')}}</div>
    <div class="col-md-2 value"> {{$loan->details->owner_occupied_percent}}
    </div>
    <div class="col-md-2 name"> {{trans('messages.loans.number')}}</div>
    <div class="col-md-2 value"> {{$loan->details->owner_occupied_no}}
    </div>
</div>