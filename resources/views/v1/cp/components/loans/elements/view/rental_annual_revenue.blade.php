<div class="row static-info">
    <div class="col-md-3 name"> {{trans('messages.loans.rental_annual_revenue')}}</div>
    <div class="col-md-7 value"> {{$loan->details->rental_annual_revenue}}
    </div>
</div>
