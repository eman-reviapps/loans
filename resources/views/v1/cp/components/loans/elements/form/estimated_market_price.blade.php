<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.estimated_market_price')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">

        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" class="form-control mask_money" name="estimated_market_price" id="estimated_market_price"
                   value="{{old('estimated_market_price') ? old('estimated_market_price') : (isset($loan) ? $loan->details->estimated_market_price : '')}}"
                   placeholder="{{trans('messages.loans.estimated_market_price')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('estimated_market_price') ?></span>
    </div>
</div>