<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.estimated_property_value')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">

        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" class="form-control mask_money" name="estimated_property_value" id="estimated_property_value"
                   value="{{old('estimated_property_value') ? old('estimated_property_value') : (isset($loan) ? $loan->details->estimated_property_value : '')}}"
                   placeholder="{{trans('messages.loans.estimated_property_value')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('estimated_property_value') ?></span>
    </div>
</div>