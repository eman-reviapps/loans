<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.funded_debt')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" class="form-control mask_money" name="funded_debt" id="funded_debt"
                   value="{{old('funded_debt') ? old('funded_debt') : (isset($loan) ? $loan->details->funded_debt : '')}}"
                   placeholder="{{trans('messages.loans.funded_debt')}}">
        </div>
        <span class="help-block">{{trans('messages.loans.funded_debt_hint')}}</span>
        <span class="alert-danger"><?php echo $errors->first('funded_debt') ?></span>
    </div>
</div>