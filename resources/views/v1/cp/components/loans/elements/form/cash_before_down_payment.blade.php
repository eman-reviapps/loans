<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.cash_before_down_payment')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">

        <div class="input-group">
            <span class="input-group-addon">
                $
            </span>
            <input type="text" class="form-control mask_money" name="cash_before_down_payment" id="cash_before_down_payment"
                   value="{{old('cash_before_down_payment') ? old('cash_before_down_payment') : (isset($loan) ? $loan->details->cash_before_down_payment : '')}}"
                   placeholder="{{trans('messages.loans.cash_before_down_payment')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('cash_before_down_payment') ?></span>
    </div>
</div>