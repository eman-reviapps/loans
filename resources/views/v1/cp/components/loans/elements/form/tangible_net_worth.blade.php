<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.tangible_net_worth')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" class="form-control mask_money" name="tangible_net_worth" id="tangible_net_worth"
                   value="{{old('tangible_net_worth') ? old('tangible_net_worth') : (isset($loan) ? $loan->details->tangible_net_worth : '')}}"
                   placeholder="{{trans('messages.loans.tangible_net_worth')}}">
        </div>
        <span class="help-block">{{trans('messages.loans.tangible_net_worth_hint')}}</span>
        <span class="alert-danger"><?php echo $errors->first('tangible_net_worth') ?></span>
    </div>
</div>