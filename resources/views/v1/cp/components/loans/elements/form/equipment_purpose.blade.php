
<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.equipment_purpose')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <select class="select2 form-control" id="equipment_purpose"
                name="equipment_purpose">
            <option value="">{{trans('messages.common.select_dropdown')}}</option>
            @foreach($equipment_purposes as $key => $value)
                <option {{(old('equipment_purpose') == $key || (isset($loan) && $loan->details->equipment_purpose == $key) ) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
            @endforeach
        </select>
        <span class="alert-danger"><?php echo $errors->first('equipment_purpose') ?></span>
    </div>
</div>