<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.'.$title_key)}}</label>
    <div class="col-md-2">
        <div class="mt-radio-inline">
            <label class="mt-radio">
                <input type="radio" name="is_loan_outstanding" value="1"
                        {{
                        (old('is_loan_outstanding') && old('is_loan_outstanding') == 1)
                         || (isset($loan) && $loan->details->is_loan_outstanding == 1)
                         || (!old('is_loan_outstanding') && !isset($loan))
                          ? 'checked': ''}}> {{trans('messages.common.yes')}}
                <span></span>
            </label>
            <label class="mt-radio">
                <input type="radio" name="is_loan_outstanding" value="0"
                        {{
                      (old('is_loan_outstanding') && old('is_loan_outstanding') == 0)
                       || (isset($loan) && $loan->details->is_loan_outstanding == 0)
                       || (!old('is_loan_outstanding') && !isset($loan))
                        ? 'checked': ''}}
                > {{trans('messages.common.no')}}
                <span></span>
            </label>
        </div>
    </div>
    <div class="col-md-3">
        <div class="input-group">

            <input type="text" class="form-control mask_money" name="outstanding_value" id="outstanding_value"
                   value="{{old('outstanding_value') ? old('outstanding_value') : (isset($loan) ? $loan->details->outstanding_value : '')}}"
                   placeholder="{{trans('messages.loans.outstanding_value')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('outstanding_value') ?></span>
    </div>
</div>