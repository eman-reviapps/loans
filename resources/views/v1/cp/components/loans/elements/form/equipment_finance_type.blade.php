
<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.equipment_finance_type')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <select class="select2 form-control" id="equipment_finance_type"
                name="equipment_finance_type">
            <option value="">{{trans('messages.common.select_dropdown')}}</option>
            @foreach($equipment_finance_types as $key => $value)
                <option {{(old('equipment_finance_type') == $key || (isset($loan) && $loan->details->equipment_finance_type == $key) ) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
            @endforeach
        </select>
        <span class="alert-danger"><?php echo $errors->first('equipment_finance_type') ?></span>
    </div>
</div>