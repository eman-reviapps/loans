@php
    if(isset($title_key))
    {
    $title = trans('messages.loans.'.$title_key);
    }else
    {
    $title = trans('messages.loans.purchase_price');
    }
@endphp
<div class="form-group">
    <label class="col-md-4 control-label">{{$title}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
                $
            </span>
            <input type="text" class="form-control mask_money" name="purchase_price" id="purchase_price"
                   value="{{old('purchase_price') ? old('purchase_price') : (isset($loan) ? $loan->details->purchase_price : '')}}"
                   placeholder="{{$title}}">
        </div>

        <span class="alert-danger"><?php echo $errors->first('purchase_price') ?></span>
    </div>
</div>