<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.estimated_net_operating_income')}}
    </label>
    <div class="col-md-5">

        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" readonly class="form-control mask_money" name="estimated_net_operating_income" id="estimated_net_operating_income"
                   value="{{old('estimated_net_operating_income') ? old('estimated_net_operating_income') : (isset($loan) ? $loan->details->estimated_net_operating_income : '')}}"
                   placeholder="{{trans('messages.loans.estimated_net_operating_income')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('estimated_net_operating_income') ?></span>
    </div>
</div>