<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.net_operating_income')}}
    </label>
    <div class="col-md-5">

        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" readonly class="form-control mask_money" name="net_operating_income" id="net_operating_income"
                   value="{{old('net_operating_income') ? old('net_operating_income') : (isset($loan) ? $loan->details->net_operating_income : '')}}"
                   placeholder="{{trans('messages.loans.net_operating_income')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('net_operating_income') ?></span>
    </div>
</div>