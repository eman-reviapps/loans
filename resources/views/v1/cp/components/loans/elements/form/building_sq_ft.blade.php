<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.building_sq_ft')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control building_sq_ft" name="building_sq_ft" id="building_sq_ft"
               value="{{old('building_sq_ft') ? old('building_sq_ft') : (isset($loan) ? $loan->details->building_sq_ft : '')}}"
               placeholder="{{trans('messages.loans.building_sq_ft')}}">
        <span class="alert-danger"><?php echo $errors->first('building_sq_ft') ?></span>
    </div>
    <div class="col-md-3">
        <span class="help-block bold">{{trans('messages.common.or')}}</span>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.loans.buildings_no')}}
        / {{trans('messages.loans.units_no')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-8">
        <div class="col-md-4" style="margin-left: -15px">
            <input type="text" class="form-control" name="buildings_no" id="buildings_no"
                   value="{{old('buildings_no') ? old('buildings_no') : (isset($loan) ? $loan->details->buildings_no : '')}}"
                   placeholder="{{trans('messages.loans.buildings_no')}}">

            <span class="help-block"> {{$errors->first('buildings_no')}} </span>
        </div>
        <div class="col-md-4">
            <input type="text" class="form-control" name="units_no" id="units_no"
                   value="{{old('units_no') ? old('units_no') : (isset($loan) ? $loan->details->units_no : '')}}"
                   placeholder="{{trans('messages.loans.units_no')}}">

            <span class="help-block"> {{$errors->first('units_no')}} </span>
        </div>
    </div>

</div>
@if(isset($display_tips) && $display_tips)
    <span class="help-block">{{trans('messages.loans.building_sq_ft_tips')}}</span>

    <div class="form-group">
        <label class="col-md-4 control-label">{{trans('messages.loans.tips')}}
        </label>
        <div class="col-md-5">
            <input type="text" class="form-control" name="tips" id="tips"
                   value="{{old('tips') ? old('tips') : (isset($loan) ? $loan->details->tips : '')}}"
                   placeholder="{{trans('messages.loans.tips')}}">
            <span class="alert-danger"><?php echo $errors->first('tips') ?></span>
        </div>
    </div>
@endif