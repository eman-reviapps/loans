<div class="form-group">

    <label class="col-md-4 control-label">{{trans('messages.loans.loan_request_details')}}
    </label>

    <div class="col-md-5">

        <textarea class="form-control" id="loan_request_details" name="loan_request_details" placeholder="{{trans('messages.loans.loan_request_details')}}"
                   rows="3">{{old('loan_request_details') ? old('loan_request_details') :  (isset($loan) ? $loan->loan_request_details : '')}}</textarea>

    </div>
</div>