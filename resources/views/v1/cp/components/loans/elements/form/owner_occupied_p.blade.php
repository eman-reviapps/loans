@php
    if(isset($loan_51) && $loan_51)
    {
        $title =trans('messages.loans.owner_occupied_percent_51');
        $hint =trans('messages.loans.owner_occupied_hint_51');
    }
    else
    {
        $title =trans('messages.loans.owner_occupied_percent');
        $hint =trans('messages.loans.owner_occupied_hint');
    }
@endphp
<div class="form-group">
    <label class="col-md-4 control-label">{{$title}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
               %
            </span>
            <input type="text" class="form-control" name="owner_occupied_percent" id="owner_occupied_percent"
                   value="{{old('owner_occupied_percent') ? old('owner_occupied_percent') : (isset($loan) ? $loan->details->owner_occupied_percent : '')}}"
                   placeholder="{{trans('messages.loans.percent')}}">
        </div>
        <span class="help-block">{{$hint}}</span>
        <span class="help-block"> {{$errors->first('owner_occupied_percent')}} </span>
    </div>
</div>
