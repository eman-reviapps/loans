<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.requested_amount')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
                $
            </span>
            <input type="text" class="form-control mask_money" name="requested_amount" id="requested_amount"
                   value="{{old('requested_amount') ? old('requested_amount') : (isset($loan) ? $loan->details->requested_amount : '')}}"
                   placeholder="{{trans('messages.loans.requested_amount')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('requested_amount') ?></span>

    </div>
</div>
