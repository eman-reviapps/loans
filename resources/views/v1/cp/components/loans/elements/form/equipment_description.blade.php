<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.equipment_description')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <textarea class="form-control" id="equipment_description" name="equipment_description" placeholder="{{trans('messages.loans.equipment_description')}}"
                  rows="3">{{old('dd($attributes);') ? old('dd($attributes);') :  (isset($loan) ? $loan->details->equipment_description : '')}}</textarea>


        <span class="alert-danger"><?php echo $errors->first('equipment_description') ?></span>
    </div>
</div>