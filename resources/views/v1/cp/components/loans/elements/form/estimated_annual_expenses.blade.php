<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.estimated_annual_expenses')}}
    </label>
    <div class="col-md-5">

        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" class="form-control mask_money" name="estimated_annual_expenses" id="estimated_annual_expenses"
                   value="{{old('estimated_annual_expenses') ? old('estimated_annual_expenses') : (isset($loan) ? $loan->details->estimated_annual_expenses : '')}}"
                   placeholder="{{trans('messages.loans.estimated_annual_expenses')}}">
        </div>
        <span class="help-block">{{trans('messages.loans.estimated_annual_expenses_hint')}}</span>
        <span class="alert-danger"><?php echo $errors->first('estimated_annual_expenses') ?></span>
    </div>
</div>