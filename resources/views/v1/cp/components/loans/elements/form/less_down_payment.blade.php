<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.less_down_payment')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" class="form-control mask_money" name="less_down_payment" id="less_down_payment"
                   value="{{old('less_down_payment') ? old('less_down_payment') : (isset($loan) ? $loan->details->less_down_payment : '')}}"
                   placeholder="{{trans('messages.loans.less_down_payment')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('less_down_payment') ?></span>
    </div>
</div>