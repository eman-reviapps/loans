

<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.loans.requested_amount')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
                $
            </span>
            <input type="text" class="form-control mask_money" name="requested_amount" id="requested_amount"
                   value="{{old('requested_amount') ? old('requested_amount') : (isset($loan) ? $loan->details->requested_amount : '')}}"
                   placeholder="{{trans('messages.loans.requested_amount')}}">
        </div>
        <span class="help-block"> {{$errors->first('requested_amount')}} </span>
    </div>
    <div class="col-md-3">
        <span class="help-block bold">{{trans('messages.common.or')}}</span>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.loans.ltv')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
                %
            </span>
            <input type="text" class="form-control" name="requested_percent" id="requested_percent"
                   value="{{old('requested_percent') ? old('requested_percent') : (isset($loan) ? $loan->details->requested_percent : '')}}"
                   placeholder="{{trans('messages.loans.ltv')}}">
        </div>
        <span class="help-block"> {{$errors->first('requested_percent')}} </span>
    </div>
</div>

