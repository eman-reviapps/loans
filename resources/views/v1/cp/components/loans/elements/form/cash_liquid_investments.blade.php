<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.cash_liquid_investments')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" class="form-control mask_money" name="cash_liquid_investments" id="cash_liquid_investments"
                   value="{{old('cash_liquid_investments') ? old('cash_liquid_investments') : (isset($loan) ? $loan->details->cash_liquid_investments : '')}}"
                   placeholder="{{trans('messages.loans.cash_liquid_investments')}}">
        </div>
        <span class="help-block">{{trans('messages.loans.cash_liquid_investments_hint')}}</span>
        <span class="alert-danger"><?php echo $errors->first('cash_liquid_investments') ?></span>
    </div>
</div>