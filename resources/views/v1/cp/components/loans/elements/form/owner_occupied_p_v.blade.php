<h5 class="form-section">
    <span class="bold">{{trans('messages.loans.owner_occupied_percent')}}</span>
    <span class="help-block">* {{trans('messages.loans.owner_occupied_hint')}}</span>
</h5>

<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.loans.percent')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
                %
            </span>
            <input type="text" class="form-control" name="owner_occupied_percent" id="owner_occupied_percent"
                   value="{{old('owner_occupied_percent') ? old('owner_occupied_percent') : (isset($loan) ? $loan->details->owner_occupied_percent : '')}}"
                   placeholder="{{trans('messages.loans.percent')}}">
        </div>
        <span class="help-block"> {{$errors->first('owner_occupied_percent')}} </span>
    </div>
</div>
