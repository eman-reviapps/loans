<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.vendor_name')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="vendor_name" id="vendor_name"
               value="{{old('vendor_name') ? old('vendor_name') : (isset($loan) ? $loan->details->vendor_name : '')}}"
               placeholder="{{trans('messages.loans.vendor_name')}}">
        <span class="alert-danger"><?php echo $errors->first('vendor_name') ?></span>
    </div>
</div>