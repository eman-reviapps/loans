<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.term_other_loan_type')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <select class="select2 form-control" id="term_other_loan_type"
                name="term_other_loan_type">
            @foreach($term_other_loan_type as $key => $value)
                <option {{(old('term_other_loan_type') == $key || (isset($loan) && $loan->details->term_other_loan_type == $key) ) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
            @endforeach
        </select>
        <span class="alert-danger"><?php echo $errors->first('term_other_loan_type') ?></span>
    </div>
</div>