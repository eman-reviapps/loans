<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.owner_not_occupied_percent')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
               %
            </span>
            <input type="text" class="form-control" name="owner_not_occupied_percent" id="owner_not_occupied_percent"
                   value="{{old('owner_not_occupied_percent') ? old('owner_not_occupied_percent') : (isset($loan) ? $loan->details->owner_not_occupied_percent : '')}}"
                   placeholder="{{trans('messages.loans.percent')}}">
        </div>
        <span class="help-block"> {{$errors->first('owner_not_occupied_percent')}} </span>
    </div>
</div>
