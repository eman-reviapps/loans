<div class="form-group">
    <label class="col-md-4 control-label">
        <span
                class="{{isset($hide_text) && $hide_text==true ? 'hidden': ''}}
                        purpose_type_text">{{trans('messages.loans.purpose_type')}}
        </span>
        @if(!(isset($hide_text) && $hide_text == true))
        @endif
    </label>
    <div class="col-md-5">

        <input type="text" class="form-control" name="purpose" id="purpose"
               value="{{old('purpose') ? old('purpose') : (isset($loan) ? $loan->details->purpose : '')}}"
               placeholder="{{trans('messages.loans.purpose')}}">
        <span class="alert-danger"><?php echo $errors->first('purpose') ?></span>
    </div>
</div>