<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.total_liabilities')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" class="form-control mask_money" name="total_liabilities" id="total_liabilities"
                   value="{{old('total_liabilities') ? old('total_liabilities') : (isset($loan) ? $loan->details->total_liabilities : '')}}"
                   placeholder="{{trans('messages.loans.total_liabilities')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('total_liabilities') ?></span>
    </div>
</div>