<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.less_trade')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" class="form-control mask_money" name="less_trade" id="less_trade"
                   value="{{old('less_trade') ? old('less_trade') : (isset($loan) ? $loan->details->less_trade : '')}}"
                   placeholder="{{trans('messages.loans.less_trade')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('less_trade') ?></span>
    </div>
</div>