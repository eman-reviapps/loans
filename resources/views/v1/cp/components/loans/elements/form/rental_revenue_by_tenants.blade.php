<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.rental_revenue_by_tenants_text')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" class="form-control mask_money" name="rental_revenue_by_tenants" id="rental_revenue_by_tenants"
                   value="{{old('rental_revenue_by_tenants') ? old('rental_revenue_by_tenants') : (isset($loan) ? $loan->details->rental_revenue_by_tenants : '')}}"
                   placeholder="{{trans('messages.loans.rental_revenue_by_tenants')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('rental_revenue_by_tenants') ?></span>
    </div>
</div>