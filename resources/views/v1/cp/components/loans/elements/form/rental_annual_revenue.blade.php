<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.rental_annual_revenue')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" class="form-control mask_money" name="rental_annual_revenue" id="rental_annual_revenue"
                   value="{{old('rental_annual_revenue') ? old('rental_annual_revenue') : (isset($loan) ? $loan->details->rental_annual_revenue : '')}}"
                   placeholder="{{trans('messages.loans.rental_annual_revenue')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('rental_annual_revenue') ?></span>
    </div>
</div>