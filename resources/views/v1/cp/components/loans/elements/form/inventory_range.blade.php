<h5 class="form-section bold">{{trans('messages.loans.inventory_range')}} {{trans('messages.loans.if_applicable')}}</h5>

<div class="form-group">
    <label class="control-label col-md-1">{{trans('messages.loans.high')}}
    </label>
    <div class="col-md-4">
        <div class="input-group">
            <span class="input-group-addon">
                $
            </span>
            <input type="text" class="form-control mask_money" name="inventory_range_to" id="inventory_range_to"
                   value="{{old('inventory_range_to') ? old('inventory_range_to') : (isset($loan) ? $loan->details->inventory_range_to : '')}}"
                   placeholder="{{trans('messages.loans.inventory_range_to')}}">
        </div>
        <span class="help-block"> {{$errors->first('inventory_range_to')}} </span>
    </div>
    <label class="control-label col-md-1">{{trans('messages.loans.low')}}
    </label>
    <div class="col-md-4">
        <div class="input-group">
            <span class="input-group-addon">
                $
            </span>
            <input type="text" class="form-control mask_money" name="inventory_range_from" id="inventory_range_from"
                   value="{{old('inventory_range_from') ? old('inventory_range_from') : (isset($loan) ? $loan->details->inventory_range_from : '')}}"
                   placeholder="{{trans('messages.loans.inventory_range_from')}}">
        </div>
        <span class="help-block"> {{$errors->first('inventory_range_from')}} </span>
    </div>
</div>