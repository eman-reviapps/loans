<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.gross_annual_revenue')}}:
    </label>
    <div class="col-md-5">
        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" class="form-control mask_money" name="gross_annual_revenue" id="gross_annual_revenue"
                   value="{{old('gross_annual_revenue') ? old('gross_annual_revenue') : (isset($loan) ? $loan->details->gross_annual_revenue : '')}}"
                   placeholder="{{trans('messages.loans.gross_annual_revenue')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('gross_annual_revenue') ?></span>
    </div>
</div>