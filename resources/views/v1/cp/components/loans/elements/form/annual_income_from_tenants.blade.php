<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.annual_income_from_tenants')}}
    </label>
    <div class="col-md-5">

        <div class="input-group">
            <span class="input-group-addon">
               $
            </span>
            <input type="text" class="form-control mask_money" name="annual_income_from_tenants" id="annual_income_from_tenants"
                   value="{{old('annual_income_from_tenants') ? old('annual_income_from_tenants') : (isset($loan) ? $loan->details->annual_income_from_tenants : '')}}"
                   placeholder="{{trans('messages.loans.annual_income_from_tenants')}}">
        </div>
        <span class="alert-danger"><?php echo $errors->first('annual_income_from_tenants') ?></span>
    </div>
</div>