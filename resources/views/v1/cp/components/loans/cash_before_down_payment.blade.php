
<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.cash_before_down_payment')}}:
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="cash_before_down_payment" id="cash_before_down_payment"
               value="{{old('cash_before_down_payment') ? old('cash_before_down_payment') : (isset($loan) ? $loan->details->cash_before_down_payment : '')}}"
               placeholder="{{trans('messages.loans.cash_before_down_payment')}}">
        <span class="alert-danger"><?php echo $errors->first('cash_before_down_payment') ?></span>
    </div>
</div>