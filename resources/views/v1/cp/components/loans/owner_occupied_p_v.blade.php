<h5 class="form-section">
    <span class="bold">{{trans('messages.loans.owner_occupied_percent')}}</span>
    <span class="help-block">* {{trans('messages.loans.owner_occupied_hint')}}</span>
</h5>

<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.loans.percent')}}</label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="owner_occupied_percent" id="owner_occupied_percent"
               value="{{old('owner_occupied_percent') ? old('owner_occupied_percent') : (isset($loan) ? $loan->details->owner_occupied_percent : '')}}"
               placeholder="{{trans('messages.loans.percent')}}">
        <span class="help-block"> {{$errors->first('owner_occupied_percent')}} </span>
    </div>
    <div class="col-md-3">
        <span class="help-block bold">{{trans('messages.common.or')}}</span>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.loans.number')}}</label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="owner_occupied_no" id="owner_occupied_no"
               value="{{old('owner_occupied_no') ? old('owner_occupied_no') : (isset($loan) ? $loan->details->owner_occupied_no : '')}}"
               placeholder="{{trans('messages.loans.number')}}">
        <span class="help-block"> {{$errors->first('owner_occupied_no')}} </span>
    </div>
    <div class="col-md-3">
        <span class="help-block bold">%</span>
    </div>
</div>