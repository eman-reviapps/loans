<div class="blog-single-content bordered blog-container">
    <div class="blog-single-head">
        <h1 class="blog-single-head-title">{{trans('messages.bids.bid_details')}}</h1>
    </div>
    <div class="blog-single-desc">

        <div class="row static-info">
            <div class="col-md-3 name"> {{trans('messages.bids.rate')}}</div>
            <div class="col-md-7 value"> {{$bid->rate}}</div>
        </div>

        <div class="row static-info">
            <div class="col-md-3 name"> {{trans('messages.bids.term')}}</div>
            <div class="col-md-1 value"> {{$bid->term}}</div>
            <div class="col-md-3 value"> {{$bid->term_unit}}</div>
        </div>

        <div class="row static-info">
            <div class="col-md-3 name"> {{trans('messages.bids.amortization')}}</div>
            <div class="col-md-1 value"> {{$bid->amortization}}</div>
            <div class="col-md-3 value"> {{$bid->amortization_unit}}</div>
        </div>

        <div class="row static-info">
            <div class="col-md-3 name"> {{trans('messages.bids.description')}}</div>
            <div class="col-md-7 value"> {{$bid->descripton}}</div>
        </div>


    </div>
</div>