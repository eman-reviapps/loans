<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.building_sq_ft')}}:
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="building_sq_ft" id="building_sq_ft"
               value="{{old('building_sq_ft') ? old('building_sq_ft') : (isset($loan) ? $loan->details->building_sq_ft : '')}}"
               placeholder="{{trans('messages.loans.building_sq_ft')}}">
        <span class="alert-danger"><?php echo $errors->first('building_sq_ft') ?></span>
    </div>
</div>

@if(isset($display_tips) && $display_tips)
    <span class="help-block">{{trans('messages.loans.building_sq_ft_tips')}}</span>

    <div class="form-group">
        <label class="col-md-4 control-label">{{trans('messages.loans.tips')}}:
        </label>
        <div class="col-md-5">
            <input type="text" class="form-control" name="tips" id="tips"
                   value="{{old('tips') ? old('tips') : (isset($loan) ? $loan->details->tips : '')}}"
                   placeholder="{{trans('messages.loans.tips')}}">
            <span class="alert-danger"><?php echo $errors->first('tips') ?></span>
        </div>
    </div>
@endif