<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.gross_annual_revenue')}}:
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="gross_annual_revenue" id="gross_annual_revenue"
               value="{{old('gross_annual_revenue') ? old('gross_annual_revenue') : (isset($loan) ? $loan->details->gross_annual_revenue : '')}}"
               placeholder="{{trans('messages.loans.gross_annual_revenue')}}">
        <span class="alert-danger"><?php echo $errors->first('gross_annual_revenue') ?></span>
    </div>
</div>