<div class="blog-single-content bordered blog-container">
    <div class="blog-single-head">
        <h1 class="blog-single-head-title">{{LoanTypes::items()[$loan->loan_type]}}</h1>
        <div class="blog-single-head-date">
            <i class="icon-calendar font-blue"></i>
            <a href="javascript:;">{{$loan->created_at->diffForHumans()}}</a>
        </div>
    </div>
    <div class="blog-single-desc">

        @if($show_borrower_info)
            <div class="row static-info">
                <div class="col-md-3 name"> {{trans('messages.loans.borrower')}}</div>
                <div class="col-md-7 value">{{$loan->borrower->full_name or ''}}</div>
            </div>

            <div class="row static-info">
                <div class="col-md-3 name"> {{trans('messages.loans.profile_type')}}</div>
                <div class="col-md-7 value">
                    {{BorrowerProfileTypes::items()[$loan->borrower_profile->profile_type] }}
                </div>
            </div>
        @endif

        <div class="row static-info">
            <div class="col-md-3 name"> {{trans('messages.loans.loan_type')}}</div>
            <div class="col-md-7 value"> {{LoanTypes::items()[$loan->loan_type]}}
            </div>
        </div>


        @if($loan->loan_type == LoanTypes::LINE_OF_CREDIT)
            @include('v1.cp.components.loans.loan_templates.view.line_of_credit')
        @elseif($loan->loan_type == LoanTypes::RE_PURCHASE_INVESTOR_0_50)
            @include('v1.cp.components.loans.loan_templates.view.re_purchase_investor')
        @elseif($loan->loan_type == LoanTypes::RE_PURCHASE_OWNER_51_100)
            @include('v1.cp.components.loans.loan_templates.view.re_purchase_owner')
        @elseif($loan->loan_type == LoanTypes::RE_REFINANCE_INVESTOR_0_50)
            @include('v1.cp.components.loans.loan_templates.view.re_refinance_investor')
        @elseif($loan->loan_type == LoanTypes::RE_REFINANCE_OWNER_51_100)
            @include('v1.cp.components.loans.loan_templates.view.re_refinance_owner')
        @elseif($loan->loan_type == LoanTypes::RE_CASH_OUT_INVESTOR_0_50)
            @include('v1.cp.components.loans.loan_templates.view.re_cash_out_investor')
        @elseif($loan->loan_type == LoanTypes::RE_CASH_OUT_OWNER_51_100)
            @include('v1.cp.components.loans.loan_templates.view.re_cash_out_owner')
        @elseif($loan->loan_type == LoanTypes::EQUIPMENT_FINANCE)
            @include('v1.cp.components.loans.loan_templates.view.equipment_finance')
        @elseif($loan->loan_type == LoanTypes::TERM_LOAN_OTHER)
            @include('v1.cp.components.loans.loan_templates.view.term_loan_other')
        @else
            {{$loan->loan_type}}
        @endif

    </div>

    @if(Sentinel::inRole(\App\Services\RoleService::ROLE_BORROWER))
        <div class="blog-comments">
            <h4 class="sbold"> {{trans('messages.loans.post_preferences')}}</h4>

            @include('v1.cp.components.preferences.view_borrower_pref',["preferences"=>$loan->preferences->keyBy('slug')])
        </div>
    @endif

    <div class="blog-comments">
        @include('v1.cp.components.attachement.all',['attachable'=>$loan,'view'=>true])
    </div>
</div>