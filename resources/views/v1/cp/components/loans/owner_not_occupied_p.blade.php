<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.owner_not_occupied_percent')}}:
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="owner_not_occupied_percent" id="owner_not_occupied_percent"
               value="{{old('owner_not_occupied_percent') ? old('owner_not_occupied_percent') : (isset($loan) ? $loan->details->owner_not_occupied_percent : '')}}"
               placeholder="{{trans('messages.loans.percent')}}">
        <span class="help-block"> {{$errors->first('owner_not_occupied_percent')}} </span>
    </div>
</div>
