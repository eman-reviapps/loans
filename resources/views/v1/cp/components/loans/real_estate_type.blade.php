<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.real_estate_type')}}:
    </label>
    <div class="col-md-5">

        <select class="select2 form-control" id="real_estate_type_id"
                name="real_estate_type_id">
            <option value="">{{trans('messages.common.select_dropdown')}}</option>
            @foreach($realestate_types as $realestate_type)
                <option {{(old('real_estate_type_id') == $realestate_type->id|| (isset($loan) && $loan->details->real_estate_type_id == $realestate_type->id)) ? 'selected' : ''}} value="{{$realestate_type->id}}">{{$realestate_type->name}}</option>
            @endforeach
        </select>
        <span class="alert-danger"><?php echo $errors->first('real_estate_type_id') ?></span>
    </div>
</div>