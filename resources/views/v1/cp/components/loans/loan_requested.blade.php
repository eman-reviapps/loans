<h5 class="form-section bold">{{trans('messages.loans.loan_requested')}}</h5>

<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.loans.amount')}}</label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="requested_amount" id="requested_amount"
               value="{{old('requested_amount') ? old('requested_amount') : (isset($loan) ? $loan->details->requested_amount : '')}}"
               placeholder="{{trans('messages.loans.requested_amount')}}">
        <span class="help-block"> {{$errors->first('requested_amount')}} </span>
    </div>
    <div class="col-md-3">
        <span class="help-block bold">{{trans('messages.common.or')}}</span>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.loans.percent')}}</label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="requested_percent" id="requested_percent"
               value="{{old('requested_percent') ? old('requested_percent') : (isset($loan) ? $loan->details->requested_percent : '')}}"
               placeholder="{{trans('messages.loans.requested_percent')}}">
        <span class="help-block"> {{$errors->first('requested_percent')}} </span>
    </div>
    <div class="col-md-3">
        <span class="help-block bold">%</span>
    </div>
</div>

