<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.company_EBITDA')}}
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="company_EBITDA" id="company_EBITDA"
               value="{{old('company_EBITDA') ? old('company_EBITDA') : (isset($loan) ? $loan->details->company_EBITDA : '')}}"
               placeholder="{{trans('messages.loans.company_EBITDA')}}">

        <span class="help-block">{{trans('messages.loans.company_EBITDA_hint')}}</span>
        <span class="alert-danger"><?php echo $errors->first('company_EBITDA') ?></span>
    </div>
</div>