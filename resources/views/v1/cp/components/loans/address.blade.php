<div class="form-group">
    <label class="col-md-3 control-label">{{trans('messages.loans.state')}}:
    </label>
    <div class="col-md-6">

        <select class="select2 form-control" id="state"
                name="state">
            <option value="">{{trans('messages.common.select_dropdown')}}</option>
            @foreach($states as $state)
                <option {{(old('state') && old('state') == $state->state_code) ? 'selected' : ''}} value="{{$state->state_code}}">{{$state->state}}</option>
            @endforeach
        </select>
        <span class="alert-danger"><?php echo $errors->first('state') ?></span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">{{trans('messages.loans.city')}}:
    </label>
    <div class="col-md-6">

        <select class="select2 form-control" id="city"
                name="city">
            <option value="">{{trans('messages.common.select_dropdown')}}</option>

        </select>
        <span class="alert-danger"><?php echo $errors->first('city') ?></span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">{{trans('messages.loans.zip_code')}}:
    </label>
    <div class="col-md-6">
        <select class="select2 form-control" id="zip_code"
                name="zip_code">
            <option value="">{{trans('messages.common.select_dropdown')}}</option>

        </select>
        <span class="alert-danger"><?php echo $errors->first('zip_code') ?></span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 control-label">{{trans('messages.loans.street')}}:
    </label>
    <div class="col-md-6">
        <input type="text" class="form-control" name="street" id="street"
               value="{{old('street') ? old('street') : (isset($loan) ? $loan->details->street : '')}}"
               placeholder="{{trans('messages.loans.street')}}">
        <span class="alert-danger"><?php echo $errors->first('street') ?></span>
    </div>
</div>