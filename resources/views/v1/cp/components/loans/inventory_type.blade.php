<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.inventory_type')}} {{trans('messages.loans.if_applicable')}}
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="inventory_type" id="inventory_type"
               value="{{old('inventory_type') ? old('inventory_type') : (isset($loan) ? $loan->details->inventory_type : '')}}"
               placeholder="{{trans('messages.loans.inventory_type')}}">
        <span class="alert-danger"><?php echo $errors->first('inventory_type') ?></span>
    </div>
</div>