<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.equipment_type')}}
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="equipment_type" id="equipment_type"
               value="{{old('equipment_type') ? old('equipment_type') : (isset($loan) ? $loan->details->equipment_type : '')}}"
               placeholder="{{trans('messages.loans.equipment_type')}}">
        <span class="alert-danger"><?php echo $errors->first('equipment_type') ?></span>
    </div>
</div>