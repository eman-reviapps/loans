<h4 class="block">
    <span class="label label-{{\App\Helpers\ViewHelper::getBidStatusLabel($bid->status)}}"> {{ BidStatuses::items()[$bid->status]}} </span>
</h4>