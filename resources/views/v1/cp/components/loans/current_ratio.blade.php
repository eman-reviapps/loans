<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.current_ratio')}}
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="current_ratio" id="current_ratio"
               value="{{old('current_ratio') ? old('current_ratio') : (isset($loan) ? $loan->details->current_ratio : '')}}"
               placeholder="{{trans('messages.loans.current_ratio')}}">
        <span class="help-block">{{trans('messages.loans.current_ratio_hint')}}</span>
        <span class="alert-danger"><?php echo $errors->first('current_ratio') ?></span>
    </div>
</div>