<h5 class="form-section bold">{{trans('messages.loans.receivable_range')}} {{trans('messages.loans.if_applicable')}}</h5>

<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.loans.low')}}</label>
    <div class="col-md-5">
        <input type="text" class="form-control mask_money" name="receivable_range_from" id="receivable_range_from"
               value="{{old('receivable_range_from') ? old('receivable_range_from') : (isset($loan) ? $loan->details->receivable_range_from : '')}}"
               placeholder="{{trans('messages.loans.receivable_range_from')}}">
        <span class="help-block"> {{$errors->first('receivable_range_from')}} </span>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.loans.high')}}</label>
    <div class="col-md-5">
        <input type="text" class="form-control mask_money" name="receivable_range_to" id="receivable_range_to"
               value="{{old('receivable_range_to') ? old('receivable_range_to') : (isset($loan) ? $loan->details->receivable_range_to : '')}}"
               placeholder="{{trans('messages.loans.receivable_range_to')}}">
        <span class="help-block"> {{$errors->first('receivable_range_to')}} </span>
    </div>
</div>