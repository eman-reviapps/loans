<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.is_moving_from_leased')}}</label>
    <div class="col-md-2">
        <div class="mt-radio-inline">
            <label class="mt-radio">
                <input type="radio" name="is_moving_from_leased" value="1"
                        {{
                        (old('is_moving_from_leased') && old('is_moving_from_leased') == 1)
                         || (isset($loan) && $loan->details->is_moving_from_leased == 1)
                         || (!old('is_moving_from_leased') && !isset($loan))
                          ? 'checked': ''}}
                > {{trans('messages.common.yes')}}
                <span></span>
            </label>
            <label class="mt-radio">
                <input type="radio" name="is_moving_from_leased" value="0"
                        {{
                       (old('is_moving_from_leased') && old('is_moving_from_leased') == 0)
                        || (isset($loan) && $loan->details->is_moving_from_leased == 0)
                        || (!old('is_moving_from_leased') && !isset($loan))
                         ? 'checked': ''}}
                > {{trans('messages.common.no')}}
                <span></span>
            </label>
        </div>
    </div>
    <div class="col-md-3">
        <input type="text" class="form-control" name="current_lease_expense" id="current_lease_expense"
               value="{{old('current_lease_expense') ? old('current_lease_expense') : (isset($loan) ? $loan->details->current_lease_expense : '')}}"
               placeholder="{{trans('messages.loans.current_lease_expense')}}">
        <span class="alert-danger"><?php echo $errors->first('current_lease_expense') ?></span>
    </div>
</div>