<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.funded_debt')}}
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="funded_debt" id="funded_debt"
               value="{{old('funded_debt') ? old('funded_debt') : (isset($loan) ? $loan->details->funded_debt : '')}}"
               placeholder="{{trans('messages.loans.funded_debt')}}">
        <span class="help-block">{{trans('messages.loans.funded_debt_hint')}}</span>
        <span class="alert-danger"><?php echo $errors->first('funded_debt') ?></span>
    </div>
</div>