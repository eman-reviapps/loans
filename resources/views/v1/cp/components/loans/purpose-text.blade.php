<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.purpose_type')}}:
    </label>
    <div class="col-md-5">

        <input type="text" class="form-control" name="purpose" id="purpose"
               value="{{old('purpose') ? old('purpose') : (isset($loan) ? $loan->details->purpose : '')}}"
               placeholder="{{trans('messages.loans.purpose')}}">
        <span class="alert-danger"><?php echo $errors->first('purpose') ?></span>
    </div>
</div>