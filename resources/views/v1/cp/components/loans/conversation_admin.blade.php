@if($bid->conversation->replies)
    <div class="blog-single-content bordered blog-container">
        <div class="blog-single-head">
            <h1 class="blog-single-head-title">{{trans('messages.bids.messages')}}</h1>
        </div>
        <div class="blog-single-desc">
            <div id="chats">
                <div class="scroller" style="height: 525px;" data-always-visible="1" data-rail-visible1="1">
                    <ul class="chats" data-id="{{$bid->id}}">
                        @foreach($bid->conversation->replies as $reply)
                            @include('v1.cp.components.conversations.reply',['reply'=>$reply,"bid"=>$bid])
                        @endforeach
                    </ul>
                    <div id="CanFollowUpMessage"></div>
                </div>
            </div>
        </div>
    </div>
@endif