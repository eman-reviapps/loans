<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.equipment_age')}}
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="equipment_age" id="equipment_age"
               value="{{old('equipment_age') ? old('equipment_age') : (isset($loan) ? $loan->details->equipment_age : '')}}"
               placeholder="{{trans('messages.loans.equipment_age')}}">
        <span class="alert-danger"><?php echo $errors->first('equipment_age') ?></span>
    </div>
</div>