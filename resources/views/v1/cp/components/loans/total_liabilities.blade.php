<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.total_liabilities')}}
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="total_liabilities" id="total_liabilities"
               value="{{old('total_liabilities') ? old('total_liabilities') : (isset($loan) ? $loan->details->total_liabilities : '')}}"
               placeholder="{{trans('messages.loans.total_liabilities')}}">

        <span class="alert-danger"><?php echo $errors->first('total_liabilities') ?></span>
    </div>
</div>