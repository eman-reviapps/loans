<div class="blog-single-content bordered blog-container">
    <div class="blog-single-head">
        <h1 class="blog-single-head-title">{{trans('messages.bids.bids')}} ({{count($loan->bids)}})</h1>
    </div>
    <div class="blog-single-desc">
        @if(isset($loan->bids) && count($loan->bids) > 0)
            @foreach($loan->bids as $bid)
                <table class="table table-bordered table-advance table-hover">
                    <thead>
                    <tr>
                        <th>{{trans('messages.bids.lender')}}</th>
                        <th>{{trans('messages.bids.rate')}}</th>
                        <th>{{trans('messages.bids.term')}}</th>
                        <th>{{trans('messages.bids.amortization')}}</th>
                        <th>{{trans('messages.bids.status')}}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="hidden-xs">{{$bid->lender->full_name}}</td>
                        <td class="hidden-xs">{{$bid->rate}}</td>
                        <td class="hidden-xs">{{$bid->term . ' '.$bid->term_unit}}</td>
                        <td class="hidden-xs">{{$bid->amortization. ' '.$bid->amortization_unit}}</td>
                        <td class="hidden-xs"><span
                                    class="label label-{{ViewHelper::getBidStatusLabel($bid->status)}}">{{\App\Enums\BidStatuses::items()[$bid->status]}}</span>
                        </td>
                        <td>
                            <a class="btn btn-sm grey-salsa btn-outline"
                               href="{{url('admin/loans/bids/'.$bid->id.'/show')}}">
                                <i class="fa fa-eye"></i>
                                <span class="hidden-xs"> View </span>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>

            @endforeach
        @endif
    </div>
</div>