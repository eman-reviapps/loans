<h5 class="form-section bold">{{trans('messages.loans.tenants')}}</h5>

<div class="form-group">
    <div class="col-md-12">
        <div class="form-group mt-repeater" style="margin-left: 0;margin-right: 0">
            <div data-repeater-list="properties">
                @if(isset($loan) && count($loan->tenants) > 0)
                    @foreach($loan->tenants as $tenant)
                        @include('v1.cp.components.loans.tenants.tenant_row',
                                [
                                'tenant'=>$tenant,
                                'lease_types'=>$lease_types
                                ]
                            )
                    @endforeach
                @else
                    @include('v1.cp.components.loans.tenants.tenant_row',
                               [
                               'tenant'=>null,
                               'lease_types'=>$lease_types
                               ]
                           )
                @endif
            </div>
            <a href="javascript:;" data-repeater-create
               class="btn btn-success btn-sm mt-repeater-add">
                <i class="fa fa-plus"></i> {{trans('messages.operations.add')}}
            </a>

        </div>
    </div>
</div>