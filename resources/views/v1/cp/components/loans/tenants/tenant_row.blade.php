<div data-repeater-item class="mt-repeater-item">

    <div class="mt-repeater-input">
        <label class="control-label">{{trans('messages.tenants.tenants_name')}}</label>
        <br/>

        <input type="hidden" id="tenant_id" name="tenant_id" value="{{$tenant->id or ''}}"/>

        <input type="text" name="name" value="{{$tenant->name or ''}}"
               class="form-control"/>
    </div>

    <div class="mt-repeater-input custom-width">
        <label class="control-label">{{trans('messages.tenants.space_occupied')}}</label>
        <span class="required"> * </span>
        <br/>

        <input type="text" name="space_occupied" required value="{{$tenant->space_occupied or ''}}"
               class="form-control mask_number"/>
    </div>

    <div class="mt-repeater-input">
        <label class="control-label">{{trans('messages.tenants.lease_rate')}}
            <span class="required"> * </span>
        </label>
        <br/>
        <div class="input-group">

            <input type="text" name="lease_rate" required value="{{$tenant->lease_rate or ''}}"
                   class="form-control mask_money"/>
            <span class="input-group-addon">
               $
            </span>
        </div>
    </div>

    <div class="mt-repeater-input">
        <label class="control-label">{{trans('messages.tenants.lease_expiration_date')}}
            <span class="required"> * </span>
        </label>
        <br/>
        <input type="text" name="lease_expiration_date" required value="{{$tenant->lease_expiration_date_only or ''}}"
               class="form-control mask_date date date-picker"/>
    </div>

    <div class="mt-repeater-input">
        <label class="control-label">{{trans('messages.tenants.lease_type')}}
            <span class="required"> * </span>
        </label>
        <br/>

        <select name="lease_type" required
                class="form-control lease_type">
            <option value="">{{trans('messages.common.select_dropdown')}}</option>
            @foreach($lease_types as $lease_type)
                <option {{
                (
                (old('lease_type') && (old('lease_type') == $lease_type->slug))
                || ($tenant && $tenant->lease_type_id == $lease_type->id)
                )
                 ? 'selected' : ''}} value="{{$lease_type->slug}}">{{$lease_type->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="mt-repeater-input">
        <a href="javascript:;" data-repeater-delete
           class="btn btn-danger btn-sm mt-repeater-delete">
            <i class="fa fa-close"></i>
        </a>
    </div>
</div>