<div class="row static-info bold">
    <div class="col-md-3 name"> {{trans('messages.loans.tenants')}}</div>
</div>
<div class="row static-info">
    @if(isset($loan) && count($loan->tenants) > 0)
        <table class="table table-striped table-bordered table-advance table-hover">
            <thead>
            <tr>
                <th>{{trans('messages.tenants.tenants_name')}}</th>
                <th>{{trans('messages.tenants.space_occupied')}}</th>
                <th>{{trans('messages.tenants.lease_rate')}}</th>
                <th>{{trans('messages.tenants.lease_expiration_date')}}</th>
                <th>{{trans('messages.tenants.lease_type')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($loan->tenants as $tenant)
                <tr>
                    <td>{{$tenant->name}}</td>
                    <td class="hidden-xs">{{$tenant->space_occupied}}</td>
                    <td class="hidden-xs">$ {{$tenant->lease_rate}}</td>
                    <td class="hidden-xs">{{$tenant->lease_expiration_date_only}}</td>
                    <td class="hidden-xs">{{$tenant->lease_type->name or ''}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
</div>