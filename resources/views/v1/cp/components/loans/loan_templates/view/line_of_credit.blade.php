@include('v1.cp.components.loans.elements.view.loan_amount')

@include('v1.cp.components.loans.elements.view.purpose-dd')

@include('v1.cp.components.loans.elements.view.loan_request_details')

@include('v1.cp.components.loans.elements.view.receivable_range')

@include('v1.cp.components.loans.elements.view.inventory_range')