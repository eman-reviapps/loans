<h5 class="form-section bold m-top-0 ">{{trans('messages.loans.loan_details_d')}}</h5>

@include('v1.cp.components.loans.elements.view.estimated_property_value')

@include('v1.cp.components.loans.elements.view.loan_amount')

@include('v1.cp.components.loans.elements.view.purpose-text')

@include('v1.cp.components.loans.elements.view.loan_request_details')

<h5 class="form-section bold m-top-0 ">{{trans('messages.loans.property_information')}}</h5>

@include('v1.cp.components.general.address-view')

@include('v1.cp.components.loans.elements.view.real_estate_type')

@include('v1.cp.components.loans.elements.view.building_sq_ft',['display_tips'=>false])

@include('v1.cp.components.loans.elements.view.owner_occupied_p')

@include('v1.cp.components.loans.elements.view.loan_outstanding',['title_key'=>'is_loan_outstanding'])

<h5 class="form-section">
    <span class="bold">{{trans('messages.loans.property_income')}}</span>
</h5>

@include('v1.cp.components.loans.elements.view.gross_annual_revenue')

@include('v1.cp.components.loans.elements.view.net_operating_income')

@include('v1.cp.components.loans.tenants.tenants-view')


