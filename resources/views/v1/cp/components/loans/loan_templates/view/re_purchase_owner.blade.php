<h5 class="form-section bold m-top-0 ">{{trans('messages.loans.loan_details_d')}}</h5>

@include('v1.cp.components.loans.elements.view.purchase_price')

@include('v1.cp.components.loans.elements.view.loan_amount')

@include('v1.cp.components.loans.elements.view.loan_request_details')

<h5 class="form-section bold m-top-0 ">{{trans('messages.loans.property_information')}}</h5>

@include('v1.cp.components.general.address-view')

@include('v1.cp.components.loans.elements.view.real_estate_type')

@include('v1.cp.components.loans.elements.view.building_sq_ft')

@include('v1.cp.components.loans.elements.view.owner_occupied_p',['loan_51'=>true])

@include('v1.cp.components.loans.elements.view.annual_income_from_tenants')

@include('v1.cp.components.loans.elements.view.loan_moving_from_leased')

@include('v1.cp.components.loans.tenants.tenants-view')

