<h5 class="form-section bold m-top-0 ">{{trans('messages.loans.equipment_details')}}</h5>

@include('v1.cp.components.loans.elements.view.equipment_type')

@include('v1.cp.components.loans.elements.view.vendor_name')

@include('v1.cp.components.loans.elements.view.equipment_purpose')

@include('v1.cp.components.loans.elements.view.equipment_description')

<h5 class="form-section bold m-top-0 ">{{trans('messages.loans.loan_details_d')}}</h5>

@include('v1.cp.components.loans.elements.view.purchase_price',['title_key'=>'equipment_purhcase_price'])

@include('v1.cp.components.loans.elements.view.less_trade')

@include('v1.cp.components.loans.elements.view.less_down_payment')

@include('v1.cp.components.loans.elements.view.loan_amount')


@include('v1.cp.components.loans.elements.view.equipment_finance_type')
