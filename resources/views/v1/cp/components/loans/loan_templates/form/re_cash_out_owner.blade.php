@component('v1.cp.agent.borrower.loans.form',[
'loan_type_key'=>LoanTypes::RE_CASH_OUT_OWNER_51_100,
'profile'=>$profile,
'logged_in_user'=>$logged_in_user,
'loan'=>isset($loan) ? $loan : null,
])
    <h5 class="form-section bold m-top-0 ">{{trans('messages.loans.loan_details_d')}}</h5>

    @include('v1.cp.components.loans.elements.form.estimated_property_value')

    @include('v1.cp.components.loans.elements.form.loan_amount')

    @include('v1.cp.components.loans.elements.form.purpose-text')

    @include('v1.cp.components.loans.elements.form.loan_request_details')

    <h5 class="form-section bold m-top-0 ">{{trans('messages.loans.property_information')}}</h5>

    <div class="form-group">
        <label class="col-md-4 control-label">{{trans('messages.loans.property_address')}}
        </label>
        <div class="col-md-8">

            @include('v1.cp.components.general.address',['record'=>(isset($loan) ? $loan->details:null)])
        </div>
    </div>

    @include('v1.cp.components.loans.elements.form.real_estate_type')

    @include('v1.cp.components.loans.elements.form.building_sq_ft')

    @include('v1.cp.components.loans.elements.form.loan_outstanding',['title_key'=>'is_loan_outstanding'])

    @include('v1.cp.components.loans.elements.form.annual_income_from_tenants')

    @include('v1.cp.components.loans.tenants.tenants')

@endcomponent
