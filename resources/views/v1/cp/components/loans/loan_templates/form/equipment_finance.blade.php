@component('v1.cp.agent.borrower.loans.form',[
'loan_type_key'=>LoanTypes::EQUIPMENT_FINANCE,
'profile'=>$profile,
'logged_in_user'=>$logged_in_user,
'loan'=>isset($loan) ? $loan : null,
])

    <h5 class="form-section bold m-top-0 ">{{trans('messages.loans.equipment_details')}}</h5>

    @include('v1.cp.components.loans.elements.form.equipment_type')

    @include('v1.cp.components.loans.elements.form.vendor_name')

    @include('v1.cp.components.loans.elements.form.equipment_purpose')

    @include('v1.cp.components.loans.elements.form.equipment_description')

    <h5 class="form-section bold m-top-0 ">{{trans('messages.loans.loan_details_d')}}</h5>
    
    @include('v1.cp.components.loans.elements.form.purchase_price',['title_key'=>'equipment_purhcase_price'])

    @include('v1.cp.components.loans.elements.form.less_trade')

    @include('v1.cp.components.loans.elements.form.less_down_payment')

    @include('v1.cp.components.loans.elements.form.loan_amount')




    @include('v1.cp.components.loans.elements.form.equipment_finance_type')

@endcomponent