@component('v1.cp.agent.borrower.loans.form',[
'loan_type_key'=>LoanTypes::RE_PURCHASE_OWNER_51_100,
'profile'=>$profile,
'logged_in_user'=>$logged_in_user,
'loan'=>isset($loan) ? $loan : null,
])

    <h5 class="form-section bold m-top-0 ">{{trans('messages.loans.loan_details_d')}}</h5>

    @include('v1.cp.components.loans.elements.form.purchase_price')

    @include('v1.cp.components.loans.elements.form.loan_requested')

    @include('v1.cp.components.loans.elements.form.loan_request_details')

    <h5 class="form-section bold m-top-0 ">{{trans('messages.loans.property_information')}}</h5>

    <div class="form-group">
        <label class="col-md-4 control-label">{{trans('messages.loans.property_address')}}
        </label>
        <div class="col-md-8">

            @include('v1.cp.components.general.address',['record'=>(isset($loan) ? $loan->details:null)])
        </div>
    </div>

    @include('v1.cp.components.loans.elements.form.real_estate_type')

    @include('v1.cp.components.loans.elements.form.building_sq_ft')

    @include('v1.cp.components.loans.elements.form.owner_occupied_p',['loan_51'=>true])

    @include('v1.cp.components.loans.elements.form.annual_income_from_tenants')

    @include('v1.cp.components.loans.elements.form.loan_moving_from_leased')

    @include('v1.cp.components.loans.tenants.tenants')

@endcomponent
