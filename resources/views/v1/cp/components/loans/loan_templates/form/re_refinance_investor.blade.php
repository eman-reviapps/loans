@component('v1.cp.agent.borrower.loans.form',[
'loan_type_key'=>LoanTypes::RE_REFINANCE_INVESTOR_0_50,
'profile'=>$profile,
'logged_in_user'=>$logged_in_user,
'loan'=>isset($loan) ? $loan : null,
])

    @slot('form_id')
        form_RE_REFINANCE_INVESTOR_0_50
    @endslot

    @slot('title')
        {{LoanTypes::items()[LoanTypes::RE_REFINANCE_INVESTOR_0_50]}}
    @endslot

    <h5 class="form-section bold m-top-0 ">{{trans('messages.loans.loan_details_d')}}</h5>

    @include('v1.cp.components.loans.elements.form.estimated_market_price')

    @include('v1.cp.components.loans.elements.form.loan_outstanding',['title_key'=>'current_loan_outstanding'])

    @include('v1.cp.components.loans.elements.form.loan_amount')

    @include('v1.cp.components.loans.elements.form.purpose-text')

    @include('v1.cp.components.loans.elements.form.loan_request_details')

    <div class="form-group">
        <label class="col-md-4 control-label">{{trans('messages.loans.property_address')}}
        </label>
        <div class="col-md-8">

            @include('v1.cp.components.general.address',['record'=>(isset($loan) ? $loan->details:null)])
        </div>
    </div>

    @include('v1.cp.components.loans.elements.form.real_estate_type')

    @include('v1.cp.components.loans.elements.form.building_sq_ft')

    @include('v1.cp.components.loans.elements.form.owner_occupied_p')

    <h5 class="form-section">
        <span class="bold">{{trans('messages.loans.property_income')}}</span>
    </h5>

    @include('v1.cp.components.loans.elements.form.gross_annual_revenue')

    @include('v1.cp.components.loans.elements.form.estimated_annual_expenses')

    @include('v1.cp.components.loans.elements.form.net_operating_income')

    @include('v1.cp.components.loans.tenants.tenants')

@endcomponent
