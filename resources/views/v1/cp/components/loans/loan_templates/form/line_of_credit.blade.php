@component('v1.cp.agent.borrower.loans.form',[
'loan_type_key'=>LoanTypes::LINE_OF_CREDIT,
'profile'=>$profile,
'logged_in_user'=>$logged_in_user,
'loan'=>isset($loan) ? $loan : null,
])


    @include('v1.cp.components.loans.elements.form.loan_amount')

    @include('v1.cp.components.loans.elements.form.purpose-dd')

    @include('v1.cp.components.loans.elements.form.loan_request_details')

    @include('v1.cp.components.loans.elements.form.receivable_range')

    @include('v1.cp.components.loans.elements.form.inventory_range')

@endcomponent