@component('v1.cp.agent.borrower.loans.form',[
'loan_type_key'=>LoanTypes::TERM_LOAN_OTHER,
'profile'=>$profile,
'logged_in_user'=>$logged_in_user,
'loan'=>isset($loan) ? $loan : null,
])

    @include('v1.cp.components.loans.elements.form.loan_amount')

    @include('v1.cp.components.loans.elements.form.term_other_loan_type')

    @include('v1.cp.components.loans.elements.form.loan_request_details')

@endcomponent

