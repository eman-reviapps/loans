<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.equipment_state')}}</label>
    <div class="col-md-5">
        <div class="mt-radio-inline">
            <label class="mt-radio">
                <input type="radio" name="equipment_state"
                       value="{{EquipmentStates::NEWW}}"
                        {{(old('equipment_state') && old('equipment_state') == EquipmentStates::NEWW)
                        || (isset($loan) && $loan->details->equipment_state == EquipmentStates::NEWW)
                        || (!old('equipment_state') && !isset($loan))
                         ? 'checked': ''}}
                > {{trans('messages.loans.new')}}
                <span></span>
            </label>
            <label class="mt-radio">
                <input type="radio" name="equipment_state" value="{{EquipmentStates::USED}}"
                        {{(old('equipment_state') && old('equipment_state') == EquipmentStates::USED)
                        || (isset($loan) && $loan->details->equipment_state == EquipmentStates::USED) ? 'checked': ''}}
                > {{trans('messages.loans.used')}}
                <span></span>
            </label>
        </div>
        <span class="alert-danger"><?php echo $errors->first('equipment_state') ?></span>
    </div>
</div>