<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.equipment_name')}}
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="equipment_name" id="equipment_name"
               value="{{old('equipment_name') ? old('equipment_name') : (isset($loan) ? $loan->details->equipment_name : '')}}"
               placeholder="{{trans('messages.loans.equipment_name')}}">
        <span class="alert-danger"><?php echo $errors->first('equipment_name') ?></span>
    </div>
</div>