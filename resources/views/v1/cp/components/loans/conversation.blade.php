@if($bid->conversation->replies)
    <div class="blog-single-content bordered blog-container">
        <div class="blog-single-head">
            <h1 class="blog-single-head-title">{{trans('messages.bids.messages')}}</h1>
        </div>
        <div class="blog-single-desc">
            <div id="chats">
                <div class="scroller" style="height: 525px;" data-always-visible="1" data-rail-visible1="1">
                    <ul class="chats" data-id="{{$bid->id}}">
                        @foreach($bid->conversation->replies as $reply)
                            @include('v1.cp.components.conversations.reply',['reply'=>$reply,"bid"=>$bid])
                        @endforeach
                    </ul>
                    <div id="CanFollowUpMessage"></div>
                </div>
                @if($logged_in_user->status != ActivationStates::SUSPENDED && $bid->loan->is_active)
                    <div class="chat-form">
                        <div class="input-cont">
                            <input class="form-control" type="text" id="message" name="message"
                                   placeholder="{{trans('messages.conversations.type_message')}}"/></div>
                        <div class="btn-cont">
                            <span class="arrow"> </span>

                            <input type="hidden" class="btn-follow-up" id="btn-follow-up" data-id="{{$bid->loan->id}}"
                                   data-csrf="{{csrf_token()}}" data-content="{{$logged_in_user->inRole(\App\Services\RoleService::ROLE_LENDER) ? 1 : 0}}"
                                   data-action="{{url('bids/'.$bid->id.'/follow_up_number')}}" >

                            <a href="" class="btn blue icn-only reply"
                               data-csrf="{{csrf_token()}}"
                               data-user_id="{{$logged_in_user->id}}"
                               data-action="{{url('conversations/' . $bid->conversation->id.'/replies')}}"
                            >
                                <i class="fa fa-check icon-white"></i>
                            </a>
                        </div>

                        @if(!$borrower_replied && $bid->status == BidStatuses::NO_RESPONSE && $logged_in_user->inRole(\App\Services\RoleService::ROLE_BORROWER))
                            <br/>
                            <a class="btn red btn-block not_interested"
                               href="{{url('borrower/loans/bids/' . $bid->id.'/not_interested')}}">{{trans('messages.bids.not_interested')}}</a>
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </div>
@endif