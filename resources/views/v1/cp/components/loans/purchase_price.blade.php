<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.purchase_price')}}:
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="purchase_price" id="purchase_price"
               value="{{old('purchase_price') ? old('purchase_price') : (isset($loan) ? $loan->details->purchase_price : '')}}"
               placeholder="{{trans('messages.loans.purchase_price')}}">
        <span class="alert-danger"><?php echo $errors->first('purchase_price') ?></span>
    </div>
</div>