<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.purpose_type')}}:
    </label>
    <div class="col-md-5">
        <select class="select2 form-control" id="purpose_type_id"
                name="purpose_type_id">
            <option value="">{{trans('messages.common.select_dropdown')}}</option>
            @foreach($purposes as $purpose)
                <option {{(old('purpose_type_id') == $purpose->id || (isset($loan) && $loan->details->purpose_type_id == $purpose->id) ) ? 'selected' : ''}} value="{{$purpose->id}}">{{$purpose->name}}</option>
            @endforeach
        </select>
        <span class="alert-danger"><?php echo $errors->first('purpose_type_id') ?></span>
    </div>
</div>