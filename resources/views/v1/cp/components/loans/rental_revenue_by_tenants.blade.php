<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.loans.rental_revenue_by_tenants_text')}}:
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" name="rental_revenue_by_tenants" id="rental_revenue_by_tenants"
               value="{{old('rental_revenue_by_tenants') ? old('rental_revenue_by_tenants') : (isset($loan) ? $loan->details->rental_revenue_by_tenants : '')}}"
               placeholder="{{trans('messages.loans.rental_revenue_by_tenants')}}">
        <span class="alert-danger"><?php echo $errors->first('rental_revenue_by_tenants') ?></span>
    </div>
</div>