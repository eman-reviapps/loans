<div class="portlet sale-summary">
    <div class="portlet-title">
        <div class="caption font-red sbold"> {{trans('messages.users.summary')}}</div>
    </div>
    <div class="portlet-body">
        <ul class="list-unstyled">
            @if($user->inRole(RoleService::ROLE_BORROWER))
                <li>
                <span class="sale-info"> {{trans('messages.users.total_business_loans')}}
                    <i class="fa fa-img-up"></i>
                </span>
                    <span class="sale-num"> {{$businessLoansCount}} </span>
                </li>
                <li>
                <span class="sale-info"> {{trans('messages.users.total_real_estate_loans')}}
                    <i class="fa fa-img-down"></i>
                </span>
                    <span class="sale-num"> {{$realEstateLoansCount}} </span>
                </li>
                <li>
                    <span class="sale-info"> {{trans('messages.users.total_loans')}} </span>
                    <span class="sale-num"> {{$loansCount}} </span>
                </li>
            @elseif($user->inRole(RoleService::ROLE_LENDER))
                <li>
                <span class="sale-info"> {{trans('messages.users.total_deals')}}
                    <i class="fa fa-img-up"></i>
                </span>
                    <span class="sale-num"> {{\App\Services\LoanService::getLenderLoansCount($user)}} </span>
                </li>
                <li>
                <span class="sale-info"> {{trans('messages.users.total_aspired_borrowers')}}
                    <i class="fa fa-img-down"></i>
                </span>
                    <span class="sale-num"> {{\App\Services\LoanService::getLenderBorrowersCount($user)}} </span>
                </li>
            @endif

        </ul>
    </div>
</div>