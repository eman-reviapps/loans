<form role="form" id="changePicture" action="{{url('users/'.$user->id.'/change_picture')}}" method="post"
      enctype="multipart/form-data">
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="_token" value="<?= csrf_token() ?>">
    <input type="hidden" name="save_continue_tab" id="save_continue_tab" value="">

    <div class="form-group">
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail"
                 style="width: 200px; height: 150px;">
                <img src="{{isset($user->photo) && !empty($user->photo) ? asset($user->photo) : asset('assets/loan-app/img/avatars/no_image.png')}}" alt=""/>
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail"
                 style="max-width: 200px; max-height: 150px;">
            </div>
            <div>
                <span class="btn default btn-file">
                    <span class="fileinput-new"> {{trans('messages.operations.select_image')}} </span>
                    <span class="fileinput-exists"> {{trans('messages.operations.change')}} </span>
                    <input type="file" name="user_picture" id="user_picture"/>
                </span>
                <a href="javascript:;" class="btn default fileinput-exists"
                   data-dismiss="fileinput"> {{trans('messages.operations.remove')}} </a>
            </div>
        </div>
    </div>
    <div class="margin-top-10">
        <button type="button" class="btn btn-success btn_action_save_continue_tab">
            {{trans('messages.operations.save_changes')}}
        </button>
        <button type="button" class="btn btn-secondary-outline btn_action_back">
            {{trans('messages.operations.cancel')}}
        </button>
    </div>
</form>