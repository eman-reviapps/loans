<form class="form-horizontal" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-5">{{trans('messages.users.phone')}}:</label>
                    <div class="col-md-5">
                        <p class="form-control-static"> {{$profile->profile->phone}} </p>
                    </div>
                </div>
            </div>
            <!--/span-->
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-5">{{trans('messages.real_estate_profiles.address')}}
                        :</label>
                    <div class="col-md-5">
                        <p class="form-control-static"> {{$profile->profile->full_address or ''}} </p>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-5">{{trans('messages.real_estate_profiles.properties_owned_no')}}
                        :</label>
                    <div class="col-md-4">
                        <p class="form-control-static"> {{$profile->profile->properties_no}} </p>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>

        @if($profile && count($profile->profile->properties) > 0)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">{{trans('messages.real_estate_profiles.properties')}}
                            : </label>
                        <div class="col-md-9">
                            @include('v1.cp.components.real_estate_profile.properties_view')
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</form>

