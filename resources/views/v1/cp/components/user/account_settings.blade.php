<form role="form" id="personnelInfo" action="{{url('users/'.$user->id.'/account_settings')}}" method="post">
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="_token" value="<?= csrf_token() ?>">
    <input type="hidden" name="save_continue_tab" id="save_continue_tab" value="">

    <div class="form-group">
        <label class="control-label">{{trans('messages.users.first_name')}}</label>
        <input type="text" placeholder="" class="form-control" id="first_name" name="first_name"
               value="{{old('first_name') ? old('first_name') : $user->first_name}}"/>
    </div>
    <div class="form-group">
        <label class="control-label">{{trans('messages.users.last_name')}}</label>
        <input type="text" placeholder="" class="form-control" id="last_name" name="last_name"
               value="{{old('last_name') ? old('last_name') : $user->last_name}}"/>
    </div>
    <div class="form-group">
        <label class="control-label">{{trans('messages.users.email')}}</label>
        <input type="text" placeholder="" class="form-control" id="email" name="email" readonly
               value="{{old('email') ? old('email') : $user->email}}"/>
    </div>
    {{--<div class="form-group">--}}
        {{--<label class="control-label">{{trans('messages.users.phone')}}</label>--}}
        {{--<input type="text" class="form-control" id="phone" name="phone"--}}
               {{--value="{{old('phone') ? old('phone') : $user->phone}}"/>--}}
    {{--</div>--}}
    @if(Sentinel::inRole(RoleService::ROLE_ADMIN))
        <div class="form-group">
            <label class="control-label">{{trans('messages.users.status')}}</label>
            <select class="select2 form-control" id="status" name="status">
                @foreach($activationStates as $activationState)
                    <option {{( (old('status') && old('status') == $activationState->slug) || (isset($user->status) && $user->status == $activationState->slug) ) ? 'selected' : ''}} value="{{$activationState->slug}}">{{$activationState->title}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">{{trans('messages.users.role')}}</label>
            <input type="text" readonly class="form-control" id="role" name="role" value="{{$user->user_role_name}}"/>
        </div>
    @endif
    <div class="margiv-top-10">
        <button type="button" class="btn btn-success btn_action_save_continue_tab">
            {{trans('messages.operations.save_changes')}}
        </button>
        <button type="button" class="btn btn-secondary-outline btn_action_back">
            {{trans('messages.operations.cancel')}}
        </button>
    </div>
</form>