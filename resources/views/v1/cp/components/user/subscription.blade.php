<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject bold uppercase"> Basic Plan </span>
        </div>
        <div class="actions">
            @if($user->status == ActivationStates::SUSPENDED)
                <button type="button"
                        class="btn btn-circle btn-block red">{{(new ActivationStates)->getTitle($user->status)}}</button>
            @else
                @if($user->onTrial())
                    <button type="button" class="btn btn-circle btn-block blue">Trial version</button>
                @else
                    <button type="button" class="btn sbold btn-circle green">Active</button>
                @endif
            @endif
        </div>
    </div>
    <div class="portlet-body">
        @if($user->free_lifetime)
            <div class="table-scrollable table-scrollable-borderless">
                <table class="table table-hover table-light table-subscription">
                    <tbody>
                    <tr>
                        <td class="bold"> Subscription</td>
                        <td><span class="badge badge-info">Free lifetime</span></td>
                    </tr>
                    <tr>
                        <td class="bold"> Subscription Time</td>
                        <td> {{$user->created_at->toDateString()}} </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        @elseif($user->subscribed('main') && $user->status != ActivationStates::SUSPENDED)
            <div class="table-scrollable table-scrollable-borderless">
                <table class="table table-hover table-light table-subscription">
                    <tbody>
                    <tr>
                        <td class="bold"> Price</td>
                        <td> $199/month</td>
                    </tr>
                    <tr>
                        <td class="bold"> Subscription Time</td>
                        <td> {{$user->subscription('main')->created_at->toDateString()}} </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        @elseif($user->status == ActivationStates::SUSPENDED && !$user->subscribed('main'))
            <br/>

            <div class="note note-info">
                <h4 class="block">Your free trial has been ended.</h4>
                <p> We'd love to keep you as a customer , and there is still time to complete your subscription! </p>
            </div>

        @endif

        <br/>

        @if( $user->subscribed('main') && ( $user->status == ActivationStates::SUSPENDED  || (\Carbon\Carbon::now() > $user->subscription('main')->created_at->addMonth()) ) )

            <a href="{{url('subscribe')}}" class="btn btn-primary">Renew</a>

        @elseif( $user->status == ActivationStates::SUSPENDED )

            <a href="{{url('subscribe')}}" class="btn btn-primary">Subscribe</a>
        @endif
    </div>
</div>