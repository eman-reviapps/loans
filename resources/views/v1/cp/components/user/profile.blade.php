<div class="profile">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li>
                <a href="#tab_overview" data-toggle="tab"> {{trans('messages.users.overview')}}</a>
            </li>
            <li>
                <a href="#tab_account" data-toggle="tab"> {{trans('messages.users.account')}}</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="tab_overview">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled profile-nav">
                            <li>
                                <img src="{{isset($user->photo) && !empty($user->photo) ? asset($user->photo) : asset('assets/loan-app/img/avatars/no_image.png')}}"
                                     class="img-responsive pic-bordered" alt=""/>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-8 profile-info">
                                <h1 class="font-green sbold">{{$user->full_name}}</h1>

                                @if(Sentinel::inRole(RoleService::ROLE_ADMIN))
                                    <p class="bold"> {{$user->user_role_name}} </p>
                                @endif
                                <p>
                                    <a href="javascript:;"> {{$user->email}} </a>
                                </p>
                                @if($user->inRole(RoleService::ROLE_LENDER))
                                    <p>
                                        <span class="bold"> {{trans('messages.users.institution')}} : </span>
                                        @if(Sentinel::inRole(RoleService::ROLE_ADMIN))
                                            <a href="{{url('admin/institutions/'.($user->lenderProfile->institution_id))}}"> {{$user->lenderProfile->institution->full_title or ''}} </a>
                                        @else
                                            <span> {{$user->lenderProfile->institution->full_title or ''}}</span>
                                        @endif
                                    </p>
                                    <p>
                                        <span class="bold"> {{trans('messages.lender_profiles.title')}} : </span>
                                        <span> {{$user->lenderProfile->title or ''}}</span>
                                    </p>
                                    <p>
                                        <span class="bold"> {{trans('messages.lender_profiles.line_of_business')}} : </span>
                                        <span> {{$user->lenderProfile->line_of_business or ''}}</span>
                                    </p>
                                    <p>
                                        <span class="bold"> {{trans('messages.users.phone')}} : </span>
                                        <span> {{$user->lenderProfile->phone or ''}}</span>
                                    </p>
                                    <p>
                                        <span class="bold"> {{trans('messages.lender_profiles.address')}} : </span>
                                        <span> {{$user->lenderProfile->address or ''}}</span>
                                    </p>
                                @endif

                                @if(Sentinel::inRole(RoleService::ROLE_ADMIN))
                                    <p>
                                        <strong> {{trans('messages.users.last_login')}}
                                            : </strong> {{$user->last_login}}
                                    </p>
                                @endif
                            </div>
                            <!--end col-md-8-->
                            <div class="col-md-4">
                                @if(!$user->inRole(RoleService::ROLE_ADMIN))
                                    @include('v1.cp.components.user.summary')
                                @endif
                            </div>
                            <!--end col-md-4-->
                        </div>
                        <!--end row-->
                        <div class="tabbable-line tabbable-custom-profile">
                            <ul class="nav nav-tabs">
                                @if(Sentinel::inRole(RoleService::ROLE_ADMIN))
                                    <li class="active">
                                        <a href="#tab_login_history"
                                           data-toggle="tab"> {{trans('messages.users.login_history')}} </a>
                                    </li>
                                @endif
                                @if($user->inRole(RoleService::ROLE_BORROWER))
                                    <li>
                                        <a href="#tab_profiles"
                                           data-toggle="tab"> {{trans('messages.users.profiles')}} </a>
                                    </li>
                                @endif
                            </ul>
                            <div class="tab-content">
                                @if(Sentinel::inRole(RoleService::ROLE_ADMIN))
                                    <div class="tab-pane active" id="tab_login_history">
                                        @include('v1.cp.admin.user.login_history.all')
                                    </div>
                                @endif

                                @if($user->inRole(RoleService::ROLE_BORROWER))
                                    <div class="tab-pane" id="tab_profiles">

                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--tab_1_2-->
            <div class="tab-pane" id="tab_account">
                <div class="row profile-account">
                    <div class="col-md-3">
                        <ul class="ver-inline-menu tabbable margin-bottom-10">
                            <li>
                                <a data-toggle="tab" href="#tab_account_personnel_info">
                                    <i class="fa fa-cog"></i> {{trans('messages.users.personal_info')}} </a>
                                <span class="after"> </span>
                            </li>
                            @if($user->inRole(RoleService::ROLE_LENDER))
                                <li>
                                    <a data-toggle="tab" href="#tab_account_profile_info">
                                        <i class="fa fa-cog"></i> {{trans('messages.profiles.profile_info')}} </a>
                                    <span class="after"> </span>
                                </li>
                            @endif
                            <li>
                                <a data-toggle="tab" href="#tab_account_change_avatar">
                                    <i class="fa fa-picture-o"></i> {{trans('messages.users.change_avatar')}} </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab_account_change_password">
                                    <i class="fa fa-lock"></i> {{trans('messages.users.change_password')}} </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab_account_notification_settings">
                                    <i class="fa fa-cog"></i> {{trans('messages.users.notification_settings')}} </a>
                            </li>
                            @if($user->inRole(RoleService::ROLE_LENDER))
                                <li>
                                    <a data-toggle="tab" href="#tab_account_preferences">
                                        <i class="fa fa-cog"></i> {{trans('messages.users.preferences')}} </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab_account_subscription">
                                        <i class="fa fa-cog"></i> {{trans('messages.users.my_subscription')}} </a>
                                </li>
                                @if($user->subscribed('main'))
                                    <li>
                                        <a data-toggle="tab" href="#tab_account_invoices">
                                            <i class="fa fa-cog"></i> {{trans('messages.users.invoices')}} </a>
                                    </li>
                                @endif
                            @endif

                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_account_personnel_info" class="tab-pane">
                                @include('v1.cp.components.user.account_settings')
                            </div>
                            @if($user->inRole(RoleService::ROLE_LENDER))
                                <div id="tab_account_profile_info" class="tab-pane">
                                    @include('v1.cp.components.user.lender_profile')
                                </div>
                            @endif
                            <div id="tab_account_change_avatar" class="tab-pane">
                                @include('v1.cp.components.user.change_avatar')
                            </div>
                            <div id="tab_account_change_password" class="tab-pane">
                                @include('v1.cp.components.user.change_password')
                            </div>
                            <div id="tab_account_notification_settings" class="tab-pane">
                                @include('v1.cp.components.user.notification_settings')
                            </div>
                            @if($user->inRole(RoleService::ROLE_LENDER))
                                <div id="tab_account_preferences" class="tab-pane">
                                    @include('v1.cp.components.preferences.lender_preferences')
                                </div>
                                <div id="tab_account_subscription" class="tab-pane">
                                    @include('v1.cp.components.user.subscription')
                                </div>
                                @if($user->subscribed('main'))
                                    <div id="tab_account_invoices" class="tab-pane">
                                        @include('v1.cp.components.user.invoices')
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!--end tab-pane-->
        </div>
    </div>
</div>