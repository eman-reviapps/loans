<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.business_profiles.business_name')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <input type="text"
               class="form-control"
               placeholder="{{trans('messages.business_profiles.business_name')}}"
               name="business_name"
               id="business_name"
               value="{{old('business_name') ? old('business_name') : ($profile ? $profile->profile->business_name : '')}}"
        />
        <span class="alert-danger"><?php echo $errors->first('business_name') ?></span>
    </div>

</div>
<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.business_profiles.profile_creator_role')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <input type="text"
               class="form-control"
               placeholder="{{trans('messages.business_profiles.profile_creator_role')}}"
               name="profile_creator_role"
               id="profile_creator_role"
               value="{{old('profile_creator_role') ? old('profile_creator_role') : ($profile ? $profile->profile->profile_creator_role : '')}}"
        />
        <span class="alert-danger"><?php echo $errors->first('profile_creator_role') ?></span>
    </div>

</div>

<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.users.phone')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control mask_phone" id="phone" name="phone"
               value="{{old('phone') ? old('phone') : ($profile ? $profile->profile->phone : '')}}"/>
        <span class="alert-danger"><?php echo $errors->first('phone') ?></span>
    </div>
</div>


@include('v1.cp.components.general.address',['record'=>($profile ? $profile->profile:null)])

<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.business_profiles.business_description')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
            <textarea class="form-control"
                      placeholder="{{trans('messages.business_profiles.business_description')}}"
                      name="description"
                      id="description"
            >{{old('description') ? old('description') : ($profile ? $profile->profile->description : '')}}</textarea>
        <span class="alert-danger"><?php echo $errors->first('description') ?></span>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-4"><?php echo e(trans('messages.business_profiles.establish_year')); ?>
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <input type="text"
               class="form-control establish_year"
               placeholder="<?php echo e(trans('messages.business_profiles.establish_year')); ?>"
               name="establish_year"
               id="establish_year"
               value="<?php echo e(old('establish_year') ? old('establish_year') : ($profile ? $profile->profile->establish_year : '')); ?>"
        />
        <span class="alert-danger"><?php echo $errors->first('establish_year') ?></span>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-4"><?php echo e(trans('messages.business_profiles.fisical_year_end')); ?>
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <input type="text"
               class="form-control fisical_year_end"
               placeholder="<?php echo e(trans('messages.business_profiles.fisical_year_end')); ?>"
               name="fisical_year_end"
               id="fisical_year_end"
               value="<?php echo e(old('fisical_year_end') ? old('fisical_year_end') : ($profile ? $profile->profile->fisical_year_end : '')); ?>"
        />
        <span class="alert-danger"><?php echo $errors->first('fisical_year_end') ?></span>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.business_profiles.last_3_years_annual_revenues')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-9">
        <div class="form-group">
            <div class="col-md-4">
                <input type="text"
                       class="form-control  mask_money"
                       placeholder="{{trans('messages.business_profiles.first')}}"
                       name="annual_revenue_year_1"
                       id="annual_revenue_year_1"
                       value="{{old('annual_revenue_year_1') ? old('annual_revenue_year_1') : ($profile ? $profile->profile->annual_revenue_year_1 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('annual_revenue_year_1') ?></span>
            </div>
            <div class="col-md-4">
                <input type="text"
                       class="form-control  mask_money"
                       placeholder="{{trans('messages.business_profiles.second')}}"
                       name="annual_revenue_year_2"
                       id="annual_revenue_year_2"
                       value="{{old('annual_revenue_year_2') ? old('annual_revenue_year_2') : ($profile ? $profile->profile->annual_revenue_year_2 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('annual_revenue_year_2') ?></span>
            </div>
            <div class="col-md-4">
                <input type="text"
                       class="form-control  mask_money"
                       placeholder="{{trans('messages.business_profiles.third')}}"
                       name="annual_revenue_year_3"
                       id="annual_revenue_year_3"
                       value="{{old('annual_revenue_year_3') ? old('annual_revenue_year_3') : ($profile ? $profile->profile->annual_revenue_year_3 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('annual_revenue_year_3') ?></span>
            </div>
        </div>
    </div>
</div>
<hr/>
<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.business_profiles.last_3_years_net_income_before_tax')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-9">
        <div class="form-group">
            <div class="col-md-4">
                <input type="text"
                       class="form-control calc_EBITDA_year1 mask_money"
                       placeholder="{{trans('messages.business_profiles.first')}}"
                       name="net_income_before_tax_year_1"
                       id="net_income_before_tax_year_1"
                       value="{{old('net_income_before_tax_year_1') ? old('net_income_before_tax_year_1') : ($profile ? $profile->profile->net_income_before_tax_year_1 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('net_income_before_tax_year_1') ?></span>
            </div>
            <div class="col-md-4">
                <input type="text"
                       class="form-control calc_EBITDA_year2 mask_money"
                       placeholder="{{trans('messages.business_profiles.second')}}"
                       name="net_income_before_tax_year_2"
                       id="net_income_before_tax_year_2"
                       value="{{old('net_income_before_tax_year_2') ? old('net_income_before_tax_year_2') : ($profile ? $profile->profile->net_income_before_tax_year_2 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('net_income_before_tax_year_2') ?></span>
            </div>
            <div class="col-md-4">
                <input type="text"
                       class="form-control calc_EBITDA_year3 mask_money"
                       placeholder="{{trans('messages.business_profiles.third')}}"
                       name="net_income_before_tax_year_3"
                       id="net_income_before_tax_year_3"
                       value="{{old('net_income_before_tax_year_3') ? old('net_income_before_tax_year_3') : ($profile ? $profile->profile->net_income_before_tax_year_3 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('net_income_before_tax_year_3') ?></span>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.business_profiles.last_3_years_income_tax_expense')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-9">
        <div class="form-group">
            <div class="col-md-4">
                <input type="text"
                       class="form-control calc_EBITDA_year1 mask_money"
                       placeholder="{{trans('messages.business_profiles.first')}}"
                       name="income_tax_expense_year_1"
                       id="income_tax_expense_year_1"
                       value="{{old('income_tax_expense_year_1') ? old('income_tax_expense_year_1') : ($profile ? $profile->profile->income_tax_expense_year_1 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('income_tax_expense_year_1') ?></span>
            </div>
            <div class="col-md-4">
                <input type="text"
                       class="form-control calc_EBITDA_year2 mask_money"
                       placeholder="{{trans('messages.business_profiles.second')}}"
                       name="income_tax_expense_year_2"
                       id="income_tax_expense_year_2"
                       value="{{old('income_tax_expense_year_2') ? old('income_tax_expense_year_2') : ($profile ? $profile->profile->income_tax_expense_year_2 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('income_tax_expense_year_2') ?></span>
            </div>
            <div class="col-md-4">
                <input type="text"
                       class="form-control calc_EBITDA_year3 mask_money"
                       placeholder="{{trans('messages.business_profiles.third')}}"
                       name="income_tax_expense_year_3"
                       id="income_tax_expense_year_3"
                       value="{{old('income_tax_expense_year_3') ? old('income_tax_expense_year_3') : ($profile ? $profile->profile->income_tax_expense_year_3 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('income_tax_expense_year_3') ?></span>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.business_profiles.last_3_years_interest_expense')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-9">
        <div class="form-group">
            <div class="col-md-4">
                <input type="text"
                       class="form-control calc_EBITDA_year1 mask_money"
                       placeholder="{{trans('messages.business_profiles.first')}}"
                       name="interest_expense_year_1"
                       id="interest_expense_year_1"
                       value="{{old('interest_expense_year_1') ? old('interest_expense_year_1') : ($profile ? $profile->profile->interest_expense_year_1 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('interest_expense_year_1') ?></span>
            </div>
            <div class="col-md-4">
                <input type="text"
                       class="form-control calc_EBITDA_year2 mask_money"
                       placeholder="{{trans('messages.business_profiles.second')}}"
                       name="interest_expense_year_2"
                       id="interest_expense_year_2"
                       value="{{old('interest_expense_year_2') ? old('interest_expense_year_2') : ($profile ? $profile->profile->interest_expense_year_2 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('interest_expense_year_2') ?></span>
            </div>
            <div class="col-md-4">
                <input type="text"
                       class="form-control calc_EBITDA_year3 mask_money"
                       placeholder="{{trans('messages.business_profiles.third')}}"
                       name="interest_expense_year_3"
                       id="interest_expense_year_3"
                       value="{{old('interest_expense_year_3') ? old('interest_expense_year_3') : ($profile ? $profile->profile->interest_expense_year_3 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('interest_expense_year_3') ?></span>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-4">{{trans('messages.business_profiles.last_3_years_depreciation_amortization')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-9">
        <div class="form-group">
            <div class="col-md-4">
                <input type="text"
                       class="form-control calc_EBITDA_year1 mask_money"
                       placeholder="{{trans('messages.business_profiles.first')}}"
                       name="depreciation_amortization_year_1"
                       id="depreciation_amortization_year_1"
                       value="{{old('depreciation_amortization_year_1') ? old('depreciation_amortization_year_1') : ($profile ? $profile->profile->depreciation_amortization_year_1 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('depreciation_amortization_year_1') ?></span>
            </div>
            <div class="col-md-4">
                <input type="text"
                       class="form-control calc_EBITDA_year2 mask_money"
                       placeholder="{{trans('messages.business_profiles.second')}}"
                       name="depreciation_amortization_year_2"
                       id="depreciation_amortization_year_2"
                       value="{{old('depreciation_amortization_year_2') ? old('depreciation_amortization_year_2') : ($profile ? $profile->profile->depreciation_amortization_year_2 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('depreciation_amortization_year_2') ?></span>
            </div>
            <div class="col-md-4">
                <input type="text"
                       class="form-control calc_EBITDA_year3 mask_money"
                       placeholder="{{trans('messages.business_profiles.third')}}"
                       name="depreciation_amortization_year_3"
                       id="depreciation_amortization_year_3"
                       value="{{old('depreciation_amortization_year_3') ? old('depreciation_amortization_year_3') : ($profile ? $profile->profile->depreciation_amortization_year_3 : '')}}"
                />
                <span class="alert-danger"><?php echo $errors->first('depreciation_amortization_year_3') ?></span>
            </div>
        </div>
    </div>
</div>


<div class="form-group">
    <label class="control-label col-md-4"><?php echo e(trans('messages.business_profiles.company_EBITDA')); ?></label>
    <div class="col-md-9">
        <div class="col-md-4">
            <input type="text" readonly
                   class="form-control mask_money"
                   placeholder="<?php echo e(trans('messages.business_profiles.company_EBITDA_year_1')); ?>"
                   name="company_EBITDA_year_1"
                   id="company_EBITDA_year_1"
                   value="<?php echo e(old('company_EBITDA_year_1') ? old('company_EBITDA_year_1') : ($profile ? $profile->profile->company_EBITDA_year_1 : '0')); ?>"
            />
        </div>
        <div class="col-md-4">
            <input type="text" readonly
                   class="form-control mask_money"
                   placeholder="<?php echo e(trans('messages.business_profiles.company_EBITDA_year_2')); ?>"
                   name="company_EBITDA_year_2"
                   id="company_EBITDA_year_2"
                   value="<?php echo e(old('company_EBITDA_year_2') ? old('company_EBITDA_year_2') : ($profile ? $profile->profile->company_EBITDA_year_2 : '0')); ?>"
            />
        </div>
        <div class="col-md-4">
            <input type="text" readonly
                   class="form-control mask_money"
                   placeholder="<?php echo e(trans('messages.business_profiles.company_EBITDA_year_3')); ?>"
                   name="company_EBITDA_year_3"
                   id="company_EBITDA_year_3"
                   value="<?php echo e(old('company_EBITDA_year_3') ? old('company_EBITDA_year_3') : ($profile ? $profile->profile->company_EBITDA_year_3 : '0')); ?>"
            />
        </div>
    </div>
</div>


<div class="form-group">
    <div class="col-md-9">
        <input type="hidden" readonly
               class="form-control mask_money"
               placeholder="<?php echo e(trans('messages.business_profiles.company_EBITDA')); ?>"
               name="company_EBITDA"
               id="company_EBITDA"
               value="<?php echo e(old('company_EBITDA') ? old('company_EBITDA') : ($profile ? $profile->profile->company_EBITDA : '0')); ?>"
        />
        <span class="alert-danger"><?php echo $errors->first('company_EBITDA') ?></span>
    </div>
</div>
