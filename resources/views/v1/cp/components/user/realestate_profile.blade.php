<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.real_estate_profiles.company_name')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control" id="company_name" name="company_name"
               placeholder="{{trans('messages.real_estate_profiles.company_name')}}"
               value="{{old('company_name') ? old('company_name') : ($profile ? $profile->profile->company_name : '')}}"/>
        <span class="alert-danger"><?php echo $errors->first('company_name') ?></span>
    </div>

</div>

@include('v1.cp.components.general.address',['record'=>($profile ? $profile->profile:null)])

<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.users.phone')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control mask_phone" id="phone" name="phone"
               placeholder="{{trans('messages.users.phone')}}"
               value="{{old('phone') ? old('phone') : ($profile ? $profile->profile->phone : '')}}"/>
        <span class="alert-danger"><?php echo $errors->first('phone') ?></span>
    </div>

</div>

<div class="form-group">
    <label class="col-md-4 control-label">{{trans('messages.real_estate_profiles.properties_owned_no')}}
        <span class="required"> * </span>
    </label>
    <div class="col-md-5">
        <input type="text" class="form-control mask_number"
               value="{{old('properties_owned_no') ? old('properties_owned_no') :  ($profile ? $profile->profile->properties_no : '')}}"
               placeholder="{{trans('messages.real_estate_profiles.properties_owned_no')}}"
               name="properties_owned_no"
               id="properties_owned_no"/>
        <span class="alert-danger"><?php echo $errors->first('properties_owned_no') ?></span>
    </div>
</div>

<div class="form-group">
    <label class="col-md-4 control-label bold">
        {{trans('messages.real_estate_profiles.properties')}}
    </label>
    <br/>
    <br/>
    <div class="col-md-12">
        <div class="form-group mt-repeater" style="margin-left: 0;margin-right: 0">
            <div data-repeater-list="properties">
                @if($profile && count($profile->profile->properties) > 0)
                    @foreach($profile->profile->properties as $property)
                        @include('v1.cp.components.real_estate_profile.property_row',
                                [
                                '$property'=>$property,
                                'realestate_types'=>$realestate_types
                                ]
                            )
                    @endforeach
                @else
                    @include('v1.cp.components.real_estate_profile.property_row',
                               [
                               'property'=>null,
                               'realestate_types'=>$realestate_types
                               ]
                           )
                @endif
            </div>
            <a href="javascript:;" data-repeater-create
               class="btn btn-success btn-sm mt-repeater-add">
                <i class="fa fa-plus"></i> {{trans('messages.operations.add')}}
            </a>

        </div>
    </div>

    <div class="col-md-12">

        @if($profile && count($profile->attachments) > 0)
            @include('v1.cp.components.attachement.all',['attachable'=>$profile,'view'=>false])
        @endif
        <br/>
        <a class="btn green btn-outline sbold" data-toggle="modal"
           href="#basic"> {{trans('messages.loans.add_files')}} </a>
    </div>
</div>

