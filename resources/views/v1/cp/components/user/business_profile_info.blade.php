<form class="form-horizontal" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-5">{{trans('messages.business_profiles.business_name')}}:</label>
                    <div class="col-md-5">
                        <p class="form-control-static"> {{$profile->profile->business_name or ''}} </p>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-5">{{trans('messages.users.phone')}}
                        :</label>
                    <div class="col-md-5">
                        <p class="form-control-static"> {{$profile->profile->phone}} </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-5">{{trans('messages.business_profiles.profile_creator_role')}}
                        :</label>
                    <div class="col-md-5">
                        <p class="form-control-static"> {{$profile->profile->profile_creator_role or ''}} </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-5">{{trans('messages.business_profiles.business_address')}}:</label>
                    <div class="col-md-5">
                        <p class="form-control-static"> {{$profile->profile->full_address or ''}} </p>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-5">{{trans('messages.business_profiles.business_description')}}
                        :</label>
                    <div class="col-md-5">
                        <p class="form-control-static"> {{$profile->profile->description}} </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-5">{{trans('messages.business_profiles.establish_year')}}:</label>
                    <div class="col-md-5">
                        <p class="form-control-static"> {{$profile->profile->establish_year or ''}} </p>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label col-md-5">{{trans('messages.business_profiles.fisical_year_end')}}
                        :</label>
                    <div class="col-md-5">
                        <p class="form-control-static"> {{$profile->profile->fisical_year_end}} </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                    <tr>
                        <th class="bold"> </th>
                        <th class="bold">{{trans('messages.business_profiles.first')}}</th>
                        <th class="bold">{{trans('messages.business_profiles.second')}}</th>
                        <th class="bold">{{trans('messages.business_profiles.third')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="bold">
                        <td>
                            {{trans('messages.business_profiles.revenue')}}
                        </td>
                        <td>{{$profile->profile->annual_revenue_year_1 or ''}}</td>
                        <td>{{$profile->profile->annual_revenue_year_2 or ''}}</td>
                        <td>{{$profile->profile->annual_revenue_year_3 or ''}}</td>
                    </tr>

                    <tr>
                        <td>
                            {{trans('messages.business_profiles.last_3_years_net_income_before_tax')}}
                        </td>
                        <td>{{$profile->profile->net_income_before_tax_year_1 or ''}}</td>
                        <td>{{$profile->profile->net_income_before_tax_year_2 or ''}}</td>
                        <td>{{$profile->profile->net_income_before_tax_year_3 or ''}}</td>
                    </tr>

                    <tr>
                        <td>
                            {{trans('messages.business_profiles.last_3_years_income_tax_expense')}}
                        </td>
                        <td>{{$profile->profile->income_tax_expense_year_1 or ''}}</td>
                        <td>{{$profile->profile->income_tax_expense_year_2 or ''}}</td>
                        <td>{{$profile->profile->income_tax_expense_year_3 or ''}}</td>
                    </tr>

                    <tr>
                        <td>
                            {{trans('messages.business_profiles.last_3_years_interest_expense')}}
                        </td>
                        <td>{{$profile->profile->interest_expense_year_1 or ''}}</td>
                        <td>{{$profile->profile->interest_expense_year_2 or ''}}</td>
                        <td>{{$profile->profile->interest_expense_year_3 or ''}}</td>
                    </tr>

                    <tr>
                        <td>
                            {{trans('messages.business_profiles.last_3_years_depreciation_amortization')}}
                        </td>
                        <td>{{$profile->profile->depreciation_amortization_year_1 or ''}}</td>
                        <td>{{$profile->profile->depreciation_amortization_year_2 or ''}}</td>
                        <td>{{$profile->profile->depreciation_amortization_year_3 or ''}}</td>
                    </tr>

                    <tr class="bold">
                        <td>
                            {{trans('messages.business_profiles.company_EBITDA')}}
                        </td>
                        <td>{{$profile->profile->company_EBITDA_year_1 or ''}}</td>
                        <td>{{$profile->profile->company_EBITDA_year_2 or ''}}</td>
                        <td>{{$profile->profile->company_EBITDA_year_3 or ''}}</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</form>

