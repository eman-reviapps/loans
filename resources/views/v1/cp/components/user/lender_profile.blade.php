<form role="form" id="LenderProfile" action="{{url('lender/profiles/'.$user->lenderProfile->id)}}"
      method="post">
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="_token" value="<?= csrf_token() ?>">
    <input type="hidden" name="save_continue_tab" id="save_continue_tab" value="">

    <div class="form-group">
        <label class="control-label">{{trans('messages.lender_profiles.title')}}</label>
        <input type="text" placeholder="" class="form-control" id="title" name="title"
               value="{{old('title') ? old('title') : $user->lenderProfile->title}}"/>
    </div>
    <div class="form-group">
        <label class="control-label">{{trans('messages.lender_profiles.line_of_business')}}</label>
        <input type="text" placeholder="" class="form-control" id="line_of_business" name="line_of_business"
               value="{{old('line_of_business') ? old('line_of_business') : $user->lenderProfile->line_of_business}}"/>
    </div>
    <div class="form-group">
        <label class="control-label">{{trans('messages.lender_profiles.address')}}</label>
        <input type="text" placeholder="" class="form-control" id="address" name="address"
               value="{{old('address') ? old('address') : $user->lenderProfile->address}}"/>
    </div>
    <div class="form-group">
        <label class="control-label">{{trans('messages.lender_profiles.bio')}}</label>
        <textarea class="form-control" id="bio" name="bio"
                  rows="3">{{old('bio') ? old('bio') : $user->lenderProfile->bio}}</textarea>
    </div>
    <div class="form-group">
        <label class="control-label">{{trans('messages.lender_profiles.linked_link')}}</label>
        <input type="text" placeholder="" class="form-control" id="linked_link" name="linked_link"
               value="{{old('linked_link') ? old('linked_link') : $user->lenderProfile->linked_link}}"/>
    </div>
    <div class="form-group">
        <label class="control-label">{{trans('messages.users.phone')}}</label>
        <input type="text" class="form-control mask_phone" id="phone" name="phone"
               value="{{old('phone') ? old('phone') : $user->lenderProfile->phone}}"/>
    </div>
    <div class="margiv-top-10">
        <button type="button" class="btn btn-success btn_action_save_continue_tab">
            {{trans('messages.operations.save_changes')}}
        </button>
        <button type="button" class="btn btn-secondary-outline btn_action_back">
            {{trans('messages.operations.cancel')}}
        </button>
    </div>
</form>