<div class="mt-radio-inline">
    @include('v1.cp.components.user.preference_notification_radio_option',[
    'key'=>$notificationType,
    'value'=>NotificationOptions::TEXT,
    ])

    @include('v1.cp.components.user.preference_notification_radio_option',[
   'key'=>$notificationType,
   'value'=>NotificationOptions::EMAIL,
   ])

    @include('v1.cp.components.user.preference_notification_radio_option',[
  'key'=>$notificationType,
  'value'=>NotificationOptions::BOTH,
  ])

    @include('v1.cp.components.user.preference_notification_radio_option',[
  'key'=>$notificationType,
  'value'=>NotificationOptions::NONE,
  ])

</div>