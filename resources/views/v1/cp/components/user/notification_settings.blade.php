<form role="form" id="NotificationSettings" action="{{url('users/'.$user->id.'/preferences')}}" method="post">
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="_token" value="<?= csrf_token() ?>">
    <input type="hidden" name="save_continue_tab" id="save_continue_tab" value="">
    <?php
    $preferences = $user->preferences->keyBy('slug'); ;
    ?>
    <table class="table table-bordered table-striped">

        <tr>
            <td> {{$preferences[PreferenceOptions::NOTIFICATION_PREFERENCE]['description']}}</td>
            <td>
                @include('v1.cp.components.preferences.preference_notification_radio',
                    ['option'=>PreferenceOptions::NOTIFICATION_PREFERENCE]
                )
            </td>
        </tr>
    </table>
    <!--end profile-settings-->
    <div class="margin-top-10">
        <button type="button" class="btn btn-success btn_action_save_continue_tab">
            {{trans('messages.operations.save_changes')}}
        </button>
        <button type="button" class="btn btn-secondary-outline btn_action_back">
            {{trans('messages.operations.cancel')}}
        </button>
    </div>
</form>