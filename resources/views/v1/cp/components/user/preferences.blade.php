<form role="form" id="changePassword" action="{{url('users/'.$user->id.'/preferences')}}" method="post">
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="_token" value="<?= csrf_token() ?>">
    <input type="hidden" name="save_continue_tab" id="save_continue_tab" value="">

    <table class="table table-bordered table-striped">

        <tr>
            <td> {{$preferences[NotificationTypes::MESSAGE_RECEIEVED]['description']}}</td>
            <td>
                @include('v1.cp.components.user.preference_notification_radio',
                    ['notificationType'=>NotificationTypes::MESSAGE_RECEIEVED]
                )
            </td>
        </tr>

        @if($user->inRole(RoleService::ROLE_BORROWER))
            <tr>
                <td> {{$preferences[NotificationTypes::BID_RECEIVED]['description']}}</td>
                <td>
                    @include('v1.cp.components.user.preference_notification_radio',
                        ['notificationType'=>NotificationTypes::BID_RECEIVED]
                    )
                </td>
            </tr>
        @endif

        @if($user->inRole(RoleService::ROLE_BORROWER))
            <tr>
                <td> {{$preferences[NotificationTypes::DEAL_ABOUT_TO_EXPIRE]['description']}}</td>
                <td>
                    @include('v1.cp.components.user.preference_notification_radio',
                        ['notificationType'=>NotificationTypes::DEAL_ABOUT_TO_EXPIRE]
                    )
                </td>
            </tr>
        @endif

        @if($user->inRole(RoleService::ROLE_LENDER))
            <tr>
                <td> {{$preferences[NotificationTypes::BORROWER_NOT_INTERESTED]['description']}}</td>
                <td>
                    @include('v1.cp.components.user.preference_notification_radio',
                        ['notificationType'=>NotificationTypes::BORROWER_NOT_INTERESTED]
                    )
                </td>
            </tr>
        @endif

        @if($user->inRole(RoleService::ROLE_LENDER))
            <tr>
                <td> {{$preferences[NotificationTypes::DEAL_MATCHING_PREFERENCES]['description']}}</td>
                <td>
                    @include('v1.cp.components.user.preference_notification_radio',
                        ['notificationType'=>NotificationTypes::DEAL_MATCHING_PREFERENCES]
                    )
                </td>
            </tr>
        @endif
    </table>
    <!--end profile-settings-->
    <div class="margin-top-10">
        <button type="button" class="btn btn-success btn_action_save_continue_tab">
            {{trans('messages.operations.save_changes')}}
        </button>
        <button type="button" class="btn btn-secondary-outline btn_action_back">
            {{trans('messages.operations.cancel')}}
        </button>
    </div>
</form>