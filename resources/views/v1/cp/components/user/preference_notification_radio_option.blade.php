<label class="mt-radio">
    <input type="radio" name="{{$preferences[$key]['slug']}}"
           value="{{$value}}"
           {{ ($preferences[$key]['current_value'] == $value) ? 'checked' : ''}}
    />
    {{$notificationOptions[$value]}}
    <span></span>
</label>