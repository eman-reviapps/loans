<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject bold uppercase"> Your payment history </span>
        </div>
    </div>

    <div class="portlet-body">
        <div class="table-scrollable table-scrollable-borderless">
            <table class="table table-hover table-light">
                <thead>
                <tr class="uppercase">
                    <th> Date</th>
                    <th> Payment</th>
                    <th> Invoice</th>
                </tr>
                </thead>
                <tbody>
                @foreach($invoices as $invoice)
                    <tr>
                        <td> Mar{{ $invoice->date()->toFormattedDateString() }}</td>
                        <td> {{ $invoice->total() }}</td>
                        <td><a class="btn btn-sm grey-salsa btn-outline" href="{{url('user/invoice/'.$invoice->id)}}"> View </a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>