<div class="modal fade delete_modal" id="deleteModal<?= $record->id ?>" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ui-draggable-handle">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?= trans('messages.operations.delete') ?> <?= trans('messages.' . $messagesKey . '.singular') ?></h4>
            </div>
            <form id="deleteForm<?= $record->id ?>" class="form-horizontal"
                  action="<?= url($root_url . '/' . $record->id) ?>" method="DELETE">
                <div class="modal-body">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="<?= csrf_token() ?>">

                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        <span class="msg_error"></span>
                    </div>
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button>
                        <span class="msg_success"></span>
                    </div>

                    <p><?= trans('messages.common.confirm_delete') ?></p>
                    <div class="form-group">
                        <div class="col-md-6">
                            <input class="form-control" readonly type="text" value="<?= $record->name ?>">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline"
                                data-dismiss="modal"><?= trans('messages.operations.close') ?></button>
                        <button type="button"
                                class="btn red delete_record_btn"><?= trans('messages.operations.delete') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>