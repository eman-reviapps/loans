@extends('v1.layout.admin')

@section('page-title')
    <?= trans('messages.' . $messagesKey . '._title') ?>
@endsection

@section('page-css-pugins')
    <link href="<?= asset('assets/global/plugins/datatables/datatables.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') ?>"
          rel="stylesheet" type="text/css"/>
    <link href="<?= asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1><?= trans('messages.' . $messagesKey . '._title') ?></h1>
        </div>
        <!-- END PAGE TITLE -->
        <!-- BEGIN PAGE TOOLBAR -->
        <div class="page-toolbar">
            @if (Sentinel::hasAccess($messagesKey . '.create'))
                <a class="btn btn-success" data-toggle="modal" href="#createModal">
                    <i class="fa fa-plus"></i>
                    <span class="hidden-xs"> <?= trans('messages.operations.new') ?> </span>
                </a>
            @endif
            @include('v1.cp.admin.types_general.create')
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>

@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{url('admin//')}}"><?= trans('messages.home') ?></a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active"><?= trans('messages.' . $messagesKey . '._title') ?></span>
        </li>
    </ul>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-blue">
                        <form id="DeleteBulkForm"
                              action="{{url($root_url.'/deleteBulk/')}}" method="DELETE">
                            @if (Sentinel::hasAccess($messagesKey . '.destroy'))
                                <a class="btn btn-sm red btn_action_delete_bulk" href="">
                                    <span class="hidden-xs"> <?= trans('messages.operations.delete') ?> </span>
                                </a>
                            @endif
                        </form>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="FilteredData">
                        @include('v1.cp.admin.types_general.filter')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="<?= asset('assets/global/scripts/datatable.js') ?>" type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/datatables/datatables.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') ?>"
            type="text/javascript"></script>

    <script src="<?= asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>

@endsection

@section('page-scripts')
    <script src="<?= asset('assets/loan-app/scripts/datatable-columns-checkboxs.js') ?>"></script>
    <script src="<?= asset('assets/loan-app/scripts/name-slug-codes/index.js') ?>"></script>
@endsection