<table class="table table-striped table-bordered table-hover" id="data_table">
    <thead class="bg-grey">
    <tr>
        <th width="2%">
            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                <input type="checkbox" class="group-checkable" data-set=".checkboxes"/>
                <span></span>
            </label>
        </th>
        <th><?= trans('messages.common.name') ?></th>
        <th><?= trans('messages.common.slug') ?></th>
        <th><?= trans('messages.common.is_enabled') ?></th>
        <th><?= trans('messages.common.actions') ?></th>
    </tr>
    </thead>
    <tbody>
    @if($types)
        @foreach($types as $record)
            <tr>
                <td class="mt-checkbox-td">
                    <label style="margin-left: 8px"
                           class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                        <input type="checkbox" class="checkboxes" value="{{$record->id}}"/>
                        <span></span>
                    </label>
                </td>
                <td>{{$record->name}}</td>
                <td>{{$record->slug}}</td>
                <td>
                    @if (Sentinel::hasAccess($messagesKey . '.edit'))
                        <form id="changeStateForm<?= $record->id ?>" class="form-horizontal"
                              action="<?= url($root_url . '/' . $record->id . '/change_status') ?>" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                            <input type="hidden" name="is_enabled" value="{{$record->is_enabled}}">

                            <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-mini bootstrap-switch-animate bootstrap-switch-off">
                                <div class="bootstrap-switch-container">
                                    <input type="checkbox" class="make-switch" data-bind="<?= $record->id ?>"
                                           <?= $record->is_enabled ? "checked" : "" ?> data-on-color='success'
                                           data-on-text="<i class='fa fa-check'></i>"
                                           data-off-text="<i class='fa fa-times'></i>">
                                </div>
                            </div>
                        </form>
                    @else
                        <span class="badge badge-{{ViewHelper::getIsEnabledLabel($record->is_enabled)}} badge-roundless">{{($record->is_enabled ? trans('messages.common.yes') : trans('messages.common.no'))}}</span>
                    @endif
                </td>
                <td>
                    @if (Sentinel::hasAccess($messagesKey . '.edit'))
                        <a class="btn btn-success btn-xs" data-toggle="modal" href="#editModal<?= $record->id ?>">
                            <i class="fa fa-edit"></i>
                            <span class="hidden-xs"> <?= trans('messages.operations.edit') ?> </span>
                        </a>
                    @endif
                    @include('v1.cp.admin.types_general.edit')

                    @if (Sentinel::hasAccess($messagesKey . '.destroy'))
                        <a class="btn btn-danger btn-xs" data-toggle="modal" href="#deleteModal{{$record->id}}">
                            <i class="fa fa-close"></i>
                            <span class="hidden-xs"> <?= trans('messages.operations.delete') ?> </span>
                        </a>
                    @endif
                    @include('v1.cp.admin.types_general.delete')
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>