<div class="modal fade add_modal" id="createModal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ui-draggable-handle">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> <?= trans('messages.operations.new') ?> <?= trans('messages.' . $messagesKey . '.singular') ?></h4>
            </div>
            <form id="createForm" class="form-horizontal" action="<?= url($root_url) ?>" method="POST">
                <div class="modal-body">
                    <input type="hidden" name="_token" value="<?= csrf_token() ?>">

                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        <span class="msg_error"></span>
                    </div>
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button>
                        <span class="msg_success"></span>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label bold"><?= trans('messages.common.name') ?></label>
                        <div class="col-md-6">
                            <input class="form-control" id="name" name="name" type="text"
                                   placeholder="<?= trans('messages.common.enter') ?> <?= trans('messages.common.name') ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label bold"><?= trans('messages.common.slug') ?></label>
                        <div class="col-md-6">
                            <input class="form-control" readonly id="slug" name="slug" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline"
                                data-dismiss="modal"><?= trans('messages.operations.close') ?></button>
                        <button type="button"
                                class="btn green add_record_btn"><?= trans('messages.operations.save') ?></button>
                    </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>