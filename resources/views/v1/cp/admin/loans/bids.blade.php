@extends('v1.layout.admin')
@section('page-title')
    <?= trans('messages.bids._title') ?>
@endsection
@section('page-css-pugins')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
          rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}"
          rel="stylesheet"
          type="text/css"/>

    <link href="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1><?= trans('messages.bids._title') ?></h1>
        </div>
    </div>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{url('admin//')}}"><?= trans('messages.home') ?></a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active"><?= trans('messages.bids._title') ?></span>
        </li>
    </ul>
@endsection

@section('content')
    <div class="portlet light ">
        <div class="portlet-body">

            <div class="table-container">
                <table class="table table-striped table-bordered table-hover" id="bids-table">
                    <thead>
                    <tr>
                        <th>{{trans('messages.bids.borrower_name')}}</th>
                        <th>{{trans('messages.bids.loan_date')}}</th>
                        <th>{{trans('messages.bids.location')}}</th>
                        <th>{{trans('messages.bids.loan_type')}}</th>
                        <th>{{trans('messages.bids.requested_amount')}}</th>
                        <th>{{trans('messages.bids.bid_status')}}</th>
                        <th>{{trans('messages.bids.bid_date')}}</th>
                        <th>{{trans('messages.bids.lender_name')}}</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection


@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>

    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/loans/bids.js')}}"></script>
@endsection
