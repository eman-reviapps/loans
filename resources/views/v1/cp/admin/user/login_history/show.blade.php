@extends('v1.layout.admin')

@section('page-title')
    {{trans('messages.user_tracks.singular')}}
@endsection

@section('page-css-pugins')
    <link href="<?= asset('assets/pages/css/invoice-2.min.css') ?>" rel="stylesheet" type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>{{trans('messages.user_tracks.singular')}}</h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{url('admin//')}}">{{trans('messages.home')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/users')}}">{{trans('messages.users._title')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/users/'.$user->id)}}">{{$user->full_name}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">{{trans('messages.user_tracks.singular')}}</span>
        </li>
    </ul>
@endsection

@section('content')
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption">
                </i>{{$user_track->login_at}}
            </div>
            <div class="actions btn-set">
                <button type="button" class="btn btn-secondary-outline btn_action_back">
                    <i class="fa fa-angle-left"></i> {{trans('messages.operations.back')}}
                </button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <form class="form-horizontal" role="form">
                                <div class="form-body">
                                    <h4 class="form-section"><strong>{{trans('messages.users.user_info')}}</strong></h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">{{trans('messages.users.first_name')}}
                                                    :</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$user->first_name}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">{{trans('messages.users.last_name')}}
                                                    :</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$user->last_name}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">{{trans('messages.users.email')}}
                                                    :</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><a
                                                                href="javascript:;"> {{$user->email}}</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">{{trans('messages.users.last_login')}}
                                                    :</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">{{$user->last_login}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <h4 class="form-section"><strong>{{trans('messages.users.login_info')}}</strong>
                                    </h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">{{trans('messages.user_tracks.date')}}
                                                    :</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$user_track->date}} </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">{{trans('messages.user_tracks.login_time')}}
                                                    :</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$user_track->login_time}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">{{trans('messages.user_tracks.logout_time')}}
                                                    :</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$user_track->logout_time}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">{{trans('messages.user_tracks.country')}}
                                                    :</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$user_track->country}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">{{trans('messages.user_tracks.city')}}
                                                    :</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$user_track->city}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div><!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">{{trans('messages.user_tracks.state')}}
                                                    :</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$user_track->state}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">{{trans('messages.user_tracks.state_name')}}
                                                    :</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$user_track->state_name}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="portlet-body">
                                            <div id="gmap_basic" class="gmaps"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4RjwwCYnwOy2fImDEkUQjYaUs4gEuIwY"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/gmaps/gmaps.min.js') ?>" type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script>
        $(document).ready(function () {
            var map = new GMaps({
                div: '#gmap_basic',
                lat: <?= $user_track->latitude ?>,
                lng: <?= $user_track->longitude ?>
            });
            map.addMarker({
                lat: <?= $user_track->latitude ?>,
                lng: <?= $user_track->longitude ?>,
                title: 'Lima',
                click: function (e) {
//                    if (console.log) console.log(e);
//                    alert('You clicked in this marker');
                }
            });
        });
    </script>
@endsection