<table class="table table-striped table-bordered table-advance table-hover" id="data_table_simple">
    <thead>
    <tr>
        <th>{{trans('messages.user_tracks.date')}}</th>
        <th>{{trans('messages.user_tracks.login_time')}}</th>
        <th>{{trans('messages.user_tracks.logout_time')}}</th>
        <th>{{trans('messages.user_tracks.location')}}</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($user->tracks as $track)
        <tr>
            <td>{{$track->date}}</td>
            <td>{{$track->login_time}}</td>
            <td>{{$track->logout_time}}</td>
            <td>
                <a target="_blank"
                   href="http://maps.google.com/maps?q=<?php echo $track->latitude ?>,<?php echo $track->longitude ?>&ll=<?php echo $track->latitude ?>,<?php echo $track->longitude ?>&z=17">{{$track->latitude}}
                    , {{$track->longitude}}</a>
            </td>
            <td>
                <a class="btn btn-sm grey-salsa btn-outline"
                   href="{{url('admin/users/'.$track->user_id.'/tracks/'.$track->id.'')}}"> {{trans('messages.operations.view')}} </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
