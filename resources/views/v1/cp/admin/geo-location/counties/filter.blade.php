<table class="table table-striped table-bordered table-hover" id="data_table">
    <thead class="bg-grey">
    <tr>
        <th><?= trans('messages.counties.county') ?></th>
        <th><?= trans('messages.states.no_cities') ?></th>
        <th><?= trans('messages.common.is_enabled') ?></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @if($counties)
        @foreach($counties as $record)
            <tr>
                <td>{{$record->county}}</td>
                <td>{{$record->county_cities_count}}</td>
                <td>
                    @if (Sentinel::hasAccess('counties.change_status'))
                        <form id="changeStateForm_{{$record->state_code}}_{{ str_replace(' ', '_', $record->county)}}" class="form-horizontal"
                              action="<?= url('admin/states/' . $record->state_code . '/counties/' . $record->county . '/change_status') ?>"
                              method="POST">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                            <input type="hidden" name="is_enabled" value="{{$record->is_enabled}}">

                            <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-mini bootstrap-switch-animate bootstrap-switch-off">
                                <div class="bootstrap-switch-container">
                                    <input type="checkbox" class="make-switch" data-bind="<?= $record->county ?>"
                                           <?= $record->is_enabled ? "checked" : "" ?> data-on-color='success'
                                           data-on-text="<i class='fa fa-check'></i>"
                                           data-off-text="<i class='fa fa-times'></i>">
                                </div>
                            </div>
                        </form>
                    @else
                        <span class="badge badge-{{ViewHelper::getIsEnabledLabel($record->is_enabled)}} badge-roundless">{{($record->is_enabled ? trans('messages.common.yes') : trans('messages.common.no'))}}</span>
                    @endif
                </td>
                <td>
                    @if (Sentinel::hasAccess('cities.index'))
                        <a class="btn btn-info btn-xs"
                           href="{{url('admin/states/'.$record->state_code.'/counties/'.$record->county.'/cities')}}">
                            <i class="fa fa-edit"></i>
                            <span class="hidden-xs"> <?= trans('messages.cities._title') ?> </span>
                        </a>
                    @endif
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>