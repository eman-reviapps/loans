<table class="table table-striped table-bordered table-hover" id="data_table">
    <thead class="bg-grey">
    <tr>
        <th><?= trans('messages.cities.zip_code') ?></th>
        <th><?= trans('messages.cities.latitude') ?></th>
        <th><?= trans('messages.cities.longitude') ?></th>
        <th><?= trans('messages.common.is_enabled') ?></th>
    </tr>
    </thead>
    <tbody>
    @if($zip_codes)
        @foreach($zip_codes as $record)
            <tr>
                <td>{{$record->zip_code}}</td>
                <td>{{$record->latitude}}</td>
                <td>{{$record->longitude}}</td>
                <td>
                    @if (Sentinel::hasAccess('zip_codes.change_status'))
                        <form id="changeStateForm_{{$record->state_code}}_{{ str_replace(' ', '_', $record->county)}}" class="form-horizontal"
                              action="<?= url('admin/states/' . $record->state_code . '/counties/' . $record->county . '/cities/' . $record->city . '/zip_codes/' . $record->zip_code . '/change_status') ?>"
                              method="POST">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                            <input type="hidden" name="is_enabled" value="{{$record->is_enabled}}">

                            <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-mini bootstrap-switch-animate bootstrap-switch-off">
                                <div class="bootstrap-switch-container">
                                    <input type="checkbox" class="make-switch" data-bind="<?= $record->id ?>"
                                           <?= $record->is_enabled ? "checked" : "" ?> data-on-color='success'
                                           data-on-text="<i class='fa fa-check'></i>"
                                           data-off-text="<i class='fa fa-times'></i>">
                                </div>
                            </div>
                        </form>
                    @else
                        <span class="badge badge-{{ViewHelper::getIsEnabledLabel($record->is_enabled)}} badge-roundless">{{($record->is_enabled ? trans('messages.common.yes') : trans('messages.common.no'))}}</span>
                    @endif
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>