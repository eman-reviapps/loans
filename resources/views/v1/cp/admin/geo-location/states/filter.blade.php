<table class="table table-striped table-bordered table-hover" id="data_table">
    <thead class="bg-grey">
    <tr>
        <th><?= trans('messages.states.state_code') ?></th>
        <th><?= trans('messages.states.state') ?></th>
        <th><?= trans('messages.states.no_cities') ?></th>
        <th><?= trans('messages.states.No_counties') ?></th>
        <th><?= trans('messages.common.is_enabled') ?></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @if($states)
        @foreach($states as $record)
            <tr>
                <td>{{$record->state_code}}</td>
                <td>{{$record->state}}</td>
                <td>{{count($record->cities)}}</td>
                <td>{{$record->state_counties_count}}</td>
                <td>
                    @if (Sentinel::hasAccess('states.edit'))
                        <form id="changeStateForm<?= $record->state_code ?>" class="form-horizontal"
                              action="<?= url('admin/states/' . $record->state_code . '/change_status') ?>" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                            <input type="hidden" name="is_enabled" value="{{$record->is_enabled}}">

                            <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-mini bootstrap-switch-animate bootstrap-switch-off">
                                <div class="bootstrap-switch-container">
                                    <input type="checkbox" class="make-switch" data-bind="<?= $record->state_code ?>"
                                           <?= $record->is_enabled ? "checked" : "" ?> data-on-color='success'
                                           data-on-text="<i class='fa fa-check'></i>"
                                           data-off-text="<i class='fa fa-times'></i>">
                                </div>
                            </div>
                        </form>
                    @else
                        <span class="badge badge-{{ViewHelper::getIsEnabledLabel($record->is_enabled)}} badge-roundless">{{($record->is_enabled ? trans('messages.common.yes') : trans('messages.common.no'))}}</span>
                    @endif
                </td>
                <td>
                    @if (Sentinel::hasAccess('counties.index'))
                        <a class="btn btn-info btn-xs" href="{{url('admin/states/'.$record->state_code.'/counties')}}">
                            <i class="fa fa-edit"></i>
                            <span class="hidden-xs"> <?= trans('messages.counties._title') ?> </span>
                        </a>
                    @endif
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>