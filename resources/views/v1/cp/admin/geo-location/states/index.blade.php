@extends('v1.layout.admin')

@section('page-title')
    <?= trans('messages.states._title') ?>
@endsection

@section('page-css-pugins')
    <link href="<?= asset('assets/global/plugins/datatables/datatables.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') ?>"
          rel="stylesheet" type="text/css"/>
    <link href="<?= asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1><?= trans('messages.states._title') ?></h1>
        </div>
        <!-- END PAGE TITLE -->
        <!-- BEGIN PAGE TOOLBAR -->
        <div class="page-toolbar">

        </div>
        <!-- END PAGE TOOLBAR -->
    </div>

@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{url('admin//')}}"><?= trans('messages.home') ?></a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active"><?= trans('messages.states._title') ?></span>
        </li>
    </ul>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div style="margin-right: 10px;" class="caption font-blue">
                        @if (Sentinel::hasAccess('states.edit'))
                            <form action="{{url('admin/states/select_all')}}" method="POST">
                                <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                                <input type="submit" class="btn btn-success" name="submit" value="<?= trans('messages.operations.select_all') ?> ">
                            </form>
                        @endif
                    </div>
                    <div class="caption font-blue">
                        @if (Sentinel::hasAccess('states.edit'))
                            <form action="{{url('admin/states/unselect_all')}}" method="POST">
                                <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                                <input type="submit" class="btn btn-default" name="submit" value="<?= trans('messages.operations.un_select_all') ?> ">
                            </form>
                        @endif
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="FilteredData">
                        @include('v1.cp.admin.geo-location.states.filter')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="<?= asset('assets/global/scripts/datatable.js') ?>" type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/datatables/datatables.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.js') ?>"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="<?= asset('assets/loan-app/scripts/datatable-columns-checkboxs.js') ?>"></script>
@endsection