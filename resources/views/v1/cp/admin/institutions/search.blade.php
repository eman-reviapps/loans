<form id="filterForm">
    <div class="portlet light bordered" style="margin-bottom: 15px">
        <div class="search-bar bordered">
            <div class="row">

                <div class="col-md-2">
                    <div class="input-group">
                        <select class="select2 form-control"
                                name="status">
                            <option value=""> -- {{trans('messages.institutions.status')}} --</option>
                            @foreach($activationStates as $activationState)
                                <option {{old('status') && old('status') == $activationState->slug ? 'selected' : ''}} value="{{$activationState->slug}}">{{$activationState->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="input-group">
                        <select class="select2 form-control"
                                name="type_id">
                            <option value=""> -- {{trans('messages.institutions.type_id')}} --</option>
                            @foreach($institution_types as $key => $value)
                                <option {{old('type') && old('type') == $key ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-2 hidden">
                    <div class="input-group">
                        <select class="select2 form-control"
                                name="size_id">
                            <option value=""> -- {{trans('messages.institutions.size_id')}} --</option>
                            @foreach($institution_sizes as $key => $value)
                                <option {{old('size') && old('size') == $key ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="input-group">
                        <select class="select2 form-control"
                                name="state">
                            <option value=""> -- {{trans('messages.institutions.state')}} --</option>
                            @foreach($states as $key => $value)
                                <option {{old('state') && old('state') == $key ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="input-group">
                        <select class="select2 form-control" id="city"
                                name="city">
                            <option value=""> -- {{trans('messages.institutions.city')}} --</option>
                        </select>
                    </div>
                </div>

            </div>
        </div>
    </div>
</form>