<table class="table table-striped table-bordered table-hover" id="data_table">
    <thead class="bg-grey">
    <tr>
        <th width="2%">
            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                <input type="checkbox" class="group-checkable" data-set=".checkboxes"/>
                <span></span>
            </label>
        </th>
        <th><?= trans('messages.institutions.title') ?></th>
        <th><?= trans('messages.institutions.domain') ?></th>
        <th><?= trans('messages.institutions.type_id') ?></th>
        <th><?= trans('messages.institutions.size_id') ?></th>
        <th><?= trans('messages.institutions.status') ?></th>
        <th><?= trans('messages.common.actions') ?></th>
    </tr>
    </thead>
    <tbody>
    @if($institutions)
        @foreach($institutions as $record)
            <tr>
                <td class="mt-checkbox-td">
                    <label style="margin-left: 8px"
                           class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                        <input type="checkbox" class="checkboxes" value="{{$record->id}}"/>
                        <span></span>
                    </label>
                </td>
                <td>{{$record->title}}</td>
                <td>{{$record->domain}}</td>
                <td>{{$record->institutionType->name or ''}}</td>
                <td>{{$record->institutionSize->name or ''}}</td>
                <td>
                    @if (Sentinel::hasAccess('institutions.edit'))
                        <form id="ChangeStatusForm{{$record->id}}"
                              action="{{url('admin/institutions/'.$record->id.'/change_status')}}" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                            <input type="hidden" name="status" value="">
                            {{--bs-select style="height: 30px" --}}
                            <select id="status{{$record->id}}"
                                    class="form-control input-small" data-bind="{{$record->id}}"
                                    data-style="{{ViewHelper::getActivationDataStyle($record->status)}}">
                                @foreach($activationStates as $state)
                                    <option value="{{$state->slug}}" {{$record->status == $state->slug ? 'selected' : ''}}>{{$state->title}}</option>
                                @endforeach
                            </select>
                        </form>
                    @else
                        <span class="badge badge-{{ViewHelper::getActivationStatusLabel($record->status)}} badge-roundless">{{(new ActivationStates)->getTitle($record->status)}}</span>
                    @endif
                </td>
                <td>
                    @if (Sentinel::hasAccess('institutions.index'))
                        <a class="btn btn-success btn-xs" href="{{url('admin/institutions/'.$record->id)}}">
                            <i class="fa fa-eye"></i>
                            <span class="hidden-xs"> <?= trans('messages.operations.view') ?> </span>
                        </a>
                    @endif
                    @if (Sentinel::hasAccess('institutions.edit'))
                        <a class="btn btn-info btn-xs" href="{{url('admin/institutions/'.$record->id.'/edit')}}">
                            <i class="fa fa-edit"></i>
                            <span class="hidden-xs"> <?= trans('messages.operations.edit') ?> </span>
                        </a>
                    @endif
                    @if (Sentinel::hasAccess('institutions.destroy'))
                        <a class="btn btn-danger btn-xs hidden" data-toggle="modal" href="#deleteModal{{$record->id}}">
                            <i class="fa fa-close"></i>
                            <span class="hidden-xs"> <?= trans('messages.operations.delete') ?> </span>
                        </a>
                    @endif
                    @include('v1.cp.admin.institutions.delete')

                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>