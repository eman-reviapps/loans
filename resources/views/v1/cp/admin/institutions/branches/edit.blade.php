@extends('v1.layout.admin')

@section('page-title')
    {{trans('messages.institutions.singular')}} {{$institution->title}} - {{trans('messages.institutions.branch')}} {{$branch->title}}
@endsection

@section('page-css-pugins')
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>{{$institution->title}} {{trans('messages.institutions.branches')}}</h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{url('admin//')}}">{{trans('messages.home')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/institutions')}}">{{trans('messages.institutions._title')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/institutions/'.$institution->id)}}">{{$institution->title}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/institutions/'.$institution->id.'/branches')}}">{{trans('messages.institutions.branches')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">{{$branch->title}}</span>
        </li>
    </ul>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form id="editInstitutionBranchForm" class="form-horizontal form-row-seperated"
                  action="{{url('admin/institutions/'.$institution->id.'/branches/'.$branch->id)}}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                <input type="hidden" name="save_continue" id="save_continue" value="no">

                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            </i>{{$branch->title}}
                        </div>
                        <div class="actions btn-set">
                            <button type="button" class="btn btn-secondary-outline btn_action_back">
                                <i class="fa fa-angle-left"></i> {{trans('messages.operations.back')}}
                            </button>
                            <button class="btn btn-secondary-outline btn_action_reload">
                                <i class="fa fa-reply"></i> {{trans('messages.operations.reset')}}
                            </button>
                            <button type="button" class="btn btn-success btn_action_save">
                                <i class="fa fa-check"></i> {{trans('messages.operations.save')}}
                            </button>
                            <button type="button" class="btn btn-success btn_action_save_continue">
                                <i class="fa fa-check-circle"></i> {{trans('messages.operations.save_continue')}}
                            </button>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable-bordered">
                            <div class="tab-content">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">{{trans('messages.institutions.title')}}:
                                        </label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="title" id="title"
                                                   value="{{old('title') ? old('title') : $branch->title}}"
                                                   placeholder="{{trans('messages.institutions.title')}}">
                                            <span class="alert-danger"><?php echo $errors->first('title') ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">{{trans('messages.institutions.active')}}
                                            :
                                        </label>
                                        <div class="col-md-6">
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="active"
                                                           value="1" {{ ($branch->active == 1) ? 'checked' : ''}}
                                                    />
                                                    {{trans('messages.common.yes')}}
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="active"
                                                           value="0" {{ ($branch->active == 0) ? 'checked' : ''}}
                                                    />
                                                    {{trans('messages.common.no')}}
                                                    <span></span>
                                                </label>
                                            </div>
                                            <span class="alert-danger"><?php echo $errors->first('active') ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">{{trans('messages.institutions.address')}}
                                            :
                                        </label>
                                        <div class="col-md-6">
                                                <textarea class="form-control" id="address"
                                                          name="address">{{old('address') ? old('address') : $branch->address}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">{{trans('messages.institutions.state')}}:
                                        </label>
                                        <div class="col-md-6">
                                            <select class="select2 form-control" id="state"
                                                    name="state">
                                                <option value="">{{trans('messages.common.select_dropdown')}}</option>
                                                @foreach($states as $key => $value)
                                                    <option {{ ( (old('state') && old('state') == $key) || (isset($branch->state) && $branch->state == $key) ) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                            <span class="alert-danger"><?php echo $errors->first('state') ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">{{trans('messages.institutions.city')}}:
                                        </label>
                                        <div class="col-md-6">
                                            <select class="select2 form-control" id="city"
                                                    name="city">
                                                <option value="">{{trans('messages.common.select_dropdown')}}</option>
                                                @foreach($cities as $key => $value)
                                                    <option {{isset($branch->zip_code) && $branch->zip_code == $key ? 'selected' : ''}} value="{{$key}}">{{$value." - ".$key}}</option>
                                                @endforeach
                                            </select>
                                            <span class="alert-danger"><?php echo $errors->first('city') ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">{{trans('messages.institutions.zip_code')}}
                                            :
                                        </label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="zip_code" id="zip_code" readonly
                                                   value="{{old('zip_code') ? old('zip_code') : $branch->zip_code}}"
                                                   placeholder="{{trans('messages.institutions.zip_code')}}">
                                            <span class="alert-danger"><?php echo $errors->first('zip_code') ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">{{trans('messages.institutions.phone')}}:
                                        </label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="phone" id="phone"
                                                   value="{{old('phone') ? old('phone') : $branch->phone}}"
                                                   placeholder="{{trans('messages.institutions.phone')}}">
                                            <span class="alert-danger"><?php echo $errors->first('phone') ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </form>
    </div>
@endsection

@section('page-js-pugins')

    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>


@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/institutions/branches/index.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/institutions/branches/forms.js')}}"></script>
@endsection