@extends('v1.layout.admin')

@section('page-title')
    {{trans('messages.institutions.singular')}} {{$institution->title}} - {{trans('messages.institutions.branch')}} {{$branch->title}}
@endsection

@section('page-css-pugins')
    <link href="<?= asset('assets/pages/css/invoice-2.min.css') ?>" rel="stylesheet" type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>{{$institution->title}} {{trans('messages.institutions.branches')}}</h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{url('admin//')}}">{{trans('messages.home')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/institutions')}}">{{trans('messages.institutions._title')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/institutions/'.$institution->id)}}">{{$institution->title}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/institutions/'.$institution->id.'/branches')}}">{{trans('messages.institutions.branches')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">{{$branch->title}}</span>
        </li>
    </ul>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form id="editInstitutionForm" class="form-horizontal form-row-seperated"
                  action="{{url('admin/institutions/'.$institution->id)}}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                <input type="hidden" name="save_continue" id="save_continue" value="no">

                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            </i>{{$branch->title}}
                        </div>
                        <div class="actions btn-set">
                            <button type="button" class="btn btn-secondary-outline btn_action_back">
                                <i class="fa fa-angle-left"></i> {{trans('messages.operations.back')}}
                            </button>
                            @if (Sentinel::hasAccess('institutions.update'))
                                <a href="{{url('admin/institutions/'.$institution->id.'/branches/'.$branch->id.'/edit')}}"
                                   class="btn btn-success btn_action_save">
                                    <i class="fa fa-check"></i> {{trans('messages.operations.edit')}}
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="invoice-content-2 bordered">
                            <div class="row">
                                <div class="col-md-7 col-xs-6">
                                    <h3 class="bold">{{$branch->title}}</h3>
                                </div>
                                <div class="col-md-5 col-xs-6">
                                    <div class="details">
                                        <span class="bold">{{trans('messages.institutions.address')}}
                                            : </span> {{$branch->address}}
                                        <br>
                                        <br>
                                        <span class="bold">{{trans('messages.institutions.state')}}
                                            : </span> {{$branch->branchState->state or ''}}
                                        <br>
                                        <br>
                                        <span class="bold">{{trans('messages.institutions.city')}}
                                            : </span> {{$branch->city}}
                                        <br>
                                        <br>
                                        <span class="bold">{{trans('messages.institutions.zip_code')}}</span>
                                        : {{$branch->zip_code}}
                                        <br>
                                        <br>
                                        <span class="bold">{{trans('messages.institutions.active')}}</span>
                                        :
                                        <span class="label"></span>{{$branch->active ? trans('messages.common.yes') : trans('messages.common.no') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('page-js-pugins')

@endsection

@section('page-scripts')

@endsection