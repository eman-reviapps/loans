<table class="table table-striped table-bordered table-hover" id="data_table">
    <thead class="bg-grey">
    <tr>
        <th width="2%">
            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                <input type="checkbox" class="group-checkable" data-set=".checkboxes"/>
                <span></span>
            </label>
        </th>
        <th><?= trans('messages.institutions.title') ?></th>
        <th><?= trans('messages.institutions.state') ?></th>
        <th><?= trans('messages.institutions.city') ?></th>
        <th><?= trans('messages.institutions.address') ?></th>
        <th><?= trans('messages.institutions.active') ?></th>
        <th><?= trans('messages.common.actions') ?></th>
    </tr>
    </thead>
    <tbody>
    @if($branches)
        @foreach($branches as $record)
            <tr>
                <td class="mt-checkbox-td">
                    <label style="margin-left: 8px"
                           class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                        <input type="checkbox" class="checkboxes" value="{{$record->id}}"/>
                        <span></span>
                    </label>
                </td>
                <td>{{$record->title}}</td>
                <td>{{$record->branchState->state or ''}}</td>
                <td>{{$record->branchCity->city or ''}} - {{$record->zip_code}}</td>
                <td>{{$record->address}}</td>
                <td>
                    @if (Sentinel::hasAccess('institutions.update'))
                        <form id="ChangeStateForm{{$record->id}}"
                              action="{{url('admin/institutions/'.$institution->id.'/branches/'.$record->id.'/change_status')}}"
                              method="POST">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                            <input type="hidden" name="status" value="<?= csrf_token() ?>">
                            {{--bs-select style="height: 30px" --}}
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="active" id="active{{$record->id}}"
                                           value="1" {{ ($record->active == 1) ? 'checked' : ''}}
                                    />
                                    {{trans('messages.common.yes')}}
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="active" id="active{{$record->id}}"
                                           value="0" {{ ($record->active == 0) ? 'checked' : ''}}
                                    />
                                    {{trans('messages.common.no')}}
                                    <span></span>
                                </label>
                            </div>
                        </form>
                    @else
                        <span class="badge badge-{{ViewHelper::getActivationStatusLabel($record->status)}} badge-roundless">{{(new ActivationStates)->getTitle($record->status)}}</span>
                    @endif
                </td>
                <td>
                    @if (Sentinel::hasAccess('institutions.index'))
                        <a class="btn btn-success btn-xs" href="{{url('admin/institutions/'.$institution->id.'/branches/'.$record->id)}}">
                            <i class="fa fa-eye"></i>
                            <span class="hidden-xs"> <?= trans('messages.operations.view') ?> </span>
                        </a>
                    @endif

                    @if (Sentinel::hasAccess('institutions.update'))
                        <a class="btn btn-info btn-xs" href="{{url('admin/institutions/'.$institution->id.'/branches/'.$record->id.'/edit')}}">
                            <i class="fa fa-edit"></i>
                            <span class="hidden-xs"> <?= trans('messages.operations.edit') ?> </span>
                        </a>
                    @endif

                    @if (Sentinel::hasAccess('institutions.delete'))
                        <a class="btn btn-danger btn-xs" data-toggle="modal" href="#deleteModal{{$record->id}}">
                            <i class="fa fa-close"></i>
                            <span class="hidden-xs"> <?= trans('messages.operations.delete') ?> </span>
                        </a>
                    @endif
                    @include('v1.cp.admin.institutions.branches.delete')

                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>