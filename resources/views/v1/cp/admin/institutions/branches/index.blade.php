@extends('v1.layout.admin')

@section('page-title')
    {{$institution->title}} {{trans('messages.institutions.branches')}}
@endsection

@section('page-css-pugins')
    <link href="<?= asset('assets/global/plugins/datatables/datatables.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') ?>"
          rel="stylesheet" type="text/css"/>
    <link href="<?= asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') ?>"
          rel="stylesheet" type="text/css"/>

    <link href="<?= asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') ?>" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1><?= trans('messages.institutions.branches') ?></h1>
        </div>
        <!-- END PAGE TITLE -->
        <!-- BEGIN PAGE TOOLBAR -->
        <div class="page-toolbar">
            @if (Sentinel::hasAccess('institutions.create'))
                <a class="btn btn-success " href="{{url('admin/institutions/'.$institution->id.'/branches/create')}}">
                    <i class="fa fa-plus"></i>
                    <span class="hidden-xs"> {{trans('messages.operations.new')}} </span>
                </a>
            @endif
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{url('admin//')}}"><?= trans('messages.home') ?></a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/institutions')}}">{{trans('messages.institutions._title')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/institutions/'.$institution->id)}}">{{$institution->title}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active"><?= trans('messages.institutions.branches') ?></span>
        </li>
    </ul>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-blue">
                        @if (Sentinel::hasAccess('institutions.delete'))
                            <form id="DeleteBulkForm"
                                  action="{{url('admin/institutions/deleteBulk/')}}" method="DELETE">
                                <a class="btn btn-sm red btn_action_delete_bulk" href="">
                                    <span class="hidden-xs"> <?= trans('messages.operations.delete') ?> </span>
                                </a>
                            </form>
                        @endif
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="FilteredData">
                        @include('v1.cp.admin.institutions.branches.filter')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="<?= asset('assets/global/scripts/datatable.js') ?>" type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/datatables/datatables.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') ?>"
            type="text/javascript"></script>

    <script src="<?= asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') ?>"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="<?= asset('assets/pages/scripts/components-bootstrap-select.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/loan-app/scripts/datatable-columns-checkboxs.js') ?>"></script>
    <script src="<?= asset('assets/loan-app/scripts/change_status.js') ?>"></script>

    <script src="{{asset('assets/loan-app/scripts/institutions/branches/index.js')}}"></script>
@endsection