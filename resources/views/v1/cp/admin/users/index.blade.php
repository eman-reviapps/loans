@extends('v1.layout.admin')

@section('page-title')
    @include('v1.cp.admin.users._index_title')
@endsection

@section('page-css-pugins')
    <link href="<?= asset('assets/global/plugins/datatables/datatables.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') ?>"
          rel="stylesheet" type="text/css"/>
    <link href="<?= asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') ?>"
          rel="stylesheet" type="text/css"/>

    <link href="<?= asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') ?>" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <div class="page-title">
            <h1>
                @include('v1.cp.admin.users._index_title')
            <h1/>
        </div>
        <div class="page-toolbar">
            @if (Sentinel::hasAccess('users.create'))
                <a class="btn btn-success " href="{{url('admin/users/create')}}">
                    <i class="fa fa-plus"></i>
                    <span class="hidden-xs"> {{trans('messages.operations.new')}} </span>
                </a>
            @endif
        </div>
    </div>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{url('admin//')}}"><?= trans('messages.home') ?></a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">@include('v1.cp.admin.users._index_title')</span>
        </li>
    </ul>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-blue">

                    </div>
                </div>
                <div class="portlet-body">
                    <div id="FilteredData">
                        @include('v1.cp.admin.users.filter')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="<?= asset('assets/global/scripts/datatable.js') ?>" type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/datatables/datatables.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') ?>"
            type="text/javascript"></script>

    <script src="<?= asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') ?>"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="<?= asset('assets/pages/scripts/components-bootstrap-select.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/loan-app/scripts/datatable-users.js') ?>"></script>
    <script src="<?= asset('assets/loan-app/scripts/change_status.js') ?>"></script>
    <script src="<?= asset('assets/loan-app/scripts/users/index.js') ?>"></script>
@endsection