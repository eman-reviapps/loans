@extends('v1.layout.admin')

@section('page-title')
    {{trans('messages.operations.new')}} {{trans('messages.users.singular')}}
@endsection

@section('page-css-pugins')
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="<?= asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') ?>" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>{{trans('messages.operations.new')}} {{trans('messages.users.singular')}}</h1>
        </div>
    </div>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{url('admin//')}}">{{ trans('messages.home')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/users')}}">{{ trans('messages.users._title')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">{{trans('messages.operations.new')}} {{trans('messages.users.singular')}}</span>
        </li>
    </ul>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="create_user_form_wizard">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold">
                            {{trans('messages.operations.create')}} {{trans('messages.users.singular')}}
                            -
                            <span class="step-title"> {{trans('messages.common.step')}}
                                1 {{trans('messages.common.of')}} 3 </span>
                            </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form class="form-horizontal" action="{{url('admin/users')}}" id="create_user_form" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                        <div class="form-wizard">
                            <div class="form-body">
                                <ul class="nav nav-pills nav-justified steps">
                                    <li>
                                        <a href="#tab_account" data-toggle="tab" class="step">
                                            <span class="number"> 1 </span>
                                            <span class="desc">
                                               <i class="fa fa-check"></i> {{trans('messages.users.account_setup')}}
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab_profile" data-toggle="tab" class="step">
                                            <span class="number"> 2 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> {{trans('messages.users.profile_setup')}}
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab_confirm" data-toggle="tab" class="step">
                                            <span class="number"> 3 </span>
                                            <span class="desc">
                                                                <i class="fa fa-check"></i> {{trans('messages.users.confirm')}} </span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="bar" class="progress progress-striped" role="progressbar">
                                    <div class="progress-bar progress-bar-success"></div>
                                </div>
                                <div class="tab-content">
                                    <div class="alert alert-danger display-none">
                                        <button class="close" data-dismiss="alert"></button>
                                        {{trans('messages.users.you_have_errors')}}
                                    </div>
                                    <div class="alert alert-success display-none">
                                        <button class="close" data-dismiss="alert"></button>
                                        {{trans('messages.users.validation_succeeded')}}
                                    </div>
                                    <div class="tab-pane active" id="tab_account">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.email')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" value="{{old('email')}}"
                                                       placeholder="{{trans('messages.users.email')}}" name="email"/>
                                                <span class="alert-danger"><?php echo $errors->first('email') ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.password')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="password"
                                                       class="form-control"
                                                       placeholder="{{trans('messages.users.password')}}"
                                                       name="password"
                                                       id="password"
                                                />
                                                <span class="alert-danger"><?php echo $errors->first('password') ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.confirm_password')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="password"
                                                       class="form-control"
                                                       placeholder="{{trans('messages.users.confirm_password')}}"
                                                       name="password_confirmation"
                                                       id="password_confirmation"
                                                />
                                                <span class="alert-danger"><?php echo $errors->first('confirm_password') ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.status')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <select name="status" id="status" class="form-control select2">
                                                    <option value=""></option>
                                                    @foreach($activationStates as $activationState)
                                                        <option {{ (old('status') && old('status') == $activationState->slug ) ? 'selected' : ''}} value="{{$activationState->slug}}">{{$activationState->title}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="alert-danger"><?php echo $errors->first('status') ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.role')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <select name="role" id="role" class="form-control select2">
                                                    <option value=""></option>
                                                    @foreach($roles as $role)
                                                        <option {{ (old('role') && old('role') == $role->slug)  ? 'selected' : ''}} value="{{$role->slug}}">{{$role->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="alert-danger"><?php echo $errors->first('role') ?></span>
                                            </div>
                                        </div>

                                        <div class="form-group institution-div">
                                            <label class="control-label col-md-3">{{trans('messages.users.institution')}}
                                            </label>
                                            <div class="col-md-4">
                                                <select name="institution" id="institution" class="form-control select2">
                                                    <option value=""></option>
                                                    @foreach($institutions as $institution)
                                                        <option {{ (old('institution') && old('institution') == $institution->id)  ? 'selected' : ''}} value="{{$institution->id}}">{{$institution->full_title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane" id="tab_profile">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.first_name')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text"
                                                       class="form-control"
                                                       placeholder="{{trans('messages.users.first_name')}}"
                                                       name="first_name"
                                                       id="first_name"
                                                       value="{{old('first_name')}}"
                                                />
                                                <span class="alert-danger"><?php echo $errors->first('first_name') ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.last_name')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text"
                                                       class="form-control"
                                                       placeholder="{{trans('messages.users.last_name')}}"
                                                       name="last_name"
                                                       id="last_name"
                                                       value="{{old('last_name')}}"
                                                />
                                                <span class="alert-danger"><?php echo $errors->first('last_name') ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.photo')}}
                                            </label>
                                            <div class="col-md-6">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail"
                                                         style="width: 200px; height: 150px;">
                                                        <img src="{{isset($user->photo) && !empty($user->photo) ? asset($user->photo) : asset('assets/loan-app/img/avatars/no_image.png')}}"
                                                             alt=""/>
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                         style="max-width: 200px; max-height: 150px;">

                                                    </div>
                                                    <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> {{trans('messages.operations.select_image')}} </span>
                                                        <span class="fileinput-exists"> {{trans('messages.operations.change')}} </span>
                                                        <input type="file" name="user_picture" id="user_picture"/>
                                                    </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists"
                                                           data-dismiss="fileinput"> {{trans('messages.operations.remove')}} </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_confirm">
                                        <h4 class="form-section bold">{{trans('messages.users.account')}}</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.email')}}
                                                :</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="email"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.status')}}
                                                :</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="status"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.role')}}
                                                :</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="role"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.institution')}}
                                                :</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="institution"></p>
                                            </div>
                                        </div>
                                        <h4 class="form-section bold">{{trans('profiles')}}</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.first_name')}}
                                                :</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="first_name"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans('messages.users.last_name')}}
                                                :</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="last_name"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <a href="javascript:;" class="btn default button-previous">
                                            <i class="fa fa-angle-left"></i> {{trans('messages.operations.back')}} </a>
                                        <a href="javascript:;"
                                           class="btn btn-outline green button-next"> {{trans('messages.operations.continue')}}
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                        <a href="javascript:;"
                                           class="btn green button-submit"> {{trans('messages.operations.submit')}}
                                            <i class="fa fa-check"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script>
        var role_lender = '<?= \App\Services\RoleService::ROLE_LENDER ?>';
        var old_institution = '<?= old('institution')?>';
    </script>

    <script src="{{asset('assets/loan-app/scripts/users/create.js')}}" type="text/javascript"></script>

@endsection