<table class="table table-striped table-bordered table-hover" id="data_table">
    <thead class="bg-grey">
    <tr>
        <th><?= trans('messages.users.name') ?></th>
        <th><?= trans('messages.users.email') ?></th>
        <th><?= trans('messages.users.role') ?></th>
        <th><?= trans('messages.users.status') ?></th>
        <th><?= trans('messages.users.register_date') ?></th>
        <th><?= trans('messages.common.actions') ?></th>
    </tr>
    </thead>
    <tbody>
    @if($users)
        @foreach($users as $record)
            <tr>
                <td>{{$record->full_name}}</td>
                <td>{{$record->email}}</td>
                <td>{{$record->user_role_name}}</td>
                <td>
                    @if (Sentinel::hasAccess('users.edit'))
                        <form id="ChangeStatusForm{{$record->id}}"
                              action="{{url('admin/users/'.$record->id.'/change_status')}}" method="PUT">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                            <input type="hidden" name="status" value="<?= csrf_token() ?>">
                            {{--bs-select--}}
                            <select id="status{{$record->id}}"
                                    class="form-control input-small" data-bind="{{$record->id}}"
                                    data-style="{{ViewHelper::getActivationDataStyle($record->status)}}">
                                @foreach($activationStates as $state)
                                    <option value="{{$state->slug}}" {{$record->status == $state->slug ? 'selected' : ''}}>{{$state->title}}</option>
                                @endforeach
                            </select>
                        </form>
                    @else
                        <span class="badge badge-{{ViewHelper::getActivationStatusLabel($record->status)}} badge-roundless">{{(new ActivationStates)->getTitle($record->status)}}</span>
                    @endif
                </td>
                <td>{{$record->created_at}}</td>
                <td>
                    @if (Sentinel::hasAccess('users.index'))
                        <a class="btn btn-success btn-xs" href="{{url('admin/users/'.$record->id)}}">
                            <i class="fa fa-eye"></i>
                            <span class="hidden-xs"> <?= trans('messages.operations.view') ?> </span>
                        </a>
                    @endif
                    @if (Sentinel::hasAccess('users.edit'))
                        <a class="btn btn-info btn-xs" href="{{url('admin/users/'.$record->id.'/edit')}}">
                            <i class="fa fa-edit"></i>
                            <span class="hidden-xs"> <?= trans('messages.operations.edit') ?> </span>
                        </a>
                    @endif
                    @if (Sentinel::hasAccess('users.destroy'))
                        <a class="btn btn-danger btn-xs" data-toggle="modal" href="#deleteModal{{$record->id}}">
                            <i class="fa fa-close"></i>
                            <span class="hidden-xs"> <?= trans('messages.operations.delete') ?> </span>
                        </a>
                    @endif
                    @include('v1.cp.admin.users.delete')
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>