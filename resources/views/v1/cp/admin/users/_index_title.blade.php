@empty(request()->role)

    <?= trans('messages.users._title') ?>

    @isset(request()->status)
        {{trans('messages.menu.admin.requires_approval')}}
    @endisset
@endempty

@isset(request()->role)

    @isset(request()->status)
        {{trans('messages.active')}}
    @endisset

    @if(request()->role == \App\Services\RoleService::ROLE_LENDER)
        {{trans('messages.lenders')}}
    @elseif(request()->role == \App\Services\RoleService::ROLE_BORROWER)
        {{trans('messages.borrowers')}}
    @endif
@endisset