@extends('v1.layout.admin')

@section('page-title')
    {{ $user->full_name ." ". trans('messages.users.profile')}}
@endsection

@section('page-css-pugins')
    <link href="<?= asset('assets/global/plugins/datatables/datatables.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') ?>"
          rel="stylesheet" type="text/css"/>

    <link href="<?= asset('assets/pages/css/profile-2.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') ?>" rel="stylesheet"
          type="text/css"/>


    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>{{ $user->full_name ." ". trans('messages.users.profile')}}</h1>
        </div>
        <!-- END PAGE TITLE -->
        <!-- BEGIN PAGE TOOLBAR -->
        <div class="page-toolbar">
            <a class="btn btn-success " href="{{url('admin/users/create')}}">
                <i class="fa fa-plus"></i>
                <span class="hidden-xs"> {{trans('messages.operations.new')}} </span>
            </a>
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{url('admin//')}}">{{trans('messages.home')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/users')}}">{{trans('messages.users._title')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">{{ $user->full_name ." ". trans('messages.users.profile')}}</span>
        </li>
    </ul>
@endsection

@section('content')

    @include('v1.cp.components.user.profile')

@endsection

@section('page-js-pugins')
    <script src="<?= asset('assets/global/scripts/datatable.js') ?>" type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/datatables/datatables.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') ?>"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="<?= asset('assets/loan-app/scripts/datatable-simple.js') ?>"></script>
    <script src="{{asset('assets/loan-app/scripts/general/state_select_multiple.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/general/city_select_multiple.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/general/zip_code_select_multiple.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/general/county_select_multiple.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/users/account.js')}}"></script>
@endsection