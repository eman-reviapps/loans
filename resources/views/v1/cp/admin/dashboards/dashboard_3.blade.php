@extends('v1.layout.admin')

@section('page-title')
    <?= trans('messages.dashboard._title.lender') ?>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <h2>Lenders Dashboard</h2>
        </div>
    </div>

    <br>

    <div class="row">

        <div class="col-lg-6 col-xs-12 col-sm-12">
            <!-- BEGIN CHART PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green-haze"></i>
                        <span class="caption-subject bold uppercase font-green-haze">LENDERS PER INSTITUTION</span>
                        <span class="caption-helper"></span>
                    </div>
                    <div class="tools">
                        <a href="javascript:" class="collapse"> </a>
                        <a href="javascript:" class="reload"> </a>
                        <a href="javascript:" class="fullscreen"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="chart_7777" class="chart" style="height: 400px;"></div>
                </div>
            </div>
            <!-- END CHART PORTLET-->
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-12">
            <!-- BEGIN CHART PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green-haze"></i>
                        <span class="caption-subject bold uppercase font-green-haze"> Lenders Per State </span>
                        <span class="caption-helper"></span>
                    </div>
                    <div class="tools">
                        <a href="javascript:" class="collapse"> </a>
                        <a href="javascript:" class="reload"> </a>
                        <a href="javascript:" class="fullscreen"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="chart_11" class="chart" style="height: 400px;"></div>
                </div>
            </div>
            <!-- END CHART PORTLET-->
        </div>
    </div>

@endsection

@section('page-js-pugins')
    <script src="<?= asset('assets/global/plugins/amcharts/amcharts/amcharts.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/amcharts/amcharts/serial.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/amcharts/amcharts/pie.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/amcharts/amcharts/radar.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/amcharts/amcharts/themes/light.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/amcharts/ammap/ammap.js') ?>" type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/amcharts/amstockcharts/amstock.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/amcharts/amcharts/plugins/dataloader/dataloader.min.js') ?>"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="<?= asset('assets/pages/scripts/charts-amcharts.js') ?>" type="text/javascript"></script>
@endsection