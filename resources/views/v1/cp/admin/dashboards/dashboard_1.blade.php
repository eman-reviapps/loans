@extends('v1.layout.admin')

@section('page-title')
    <?= trans('messages.dashboard._title.lender') ?>
@endsection

@section('content')
    <!-- BEGIN DASHBOARD STATS 1-->
    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <h2>Statistics</h2>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
            <a class="dashboard-stat dashboard-stat-v2 grey" href="{{url('admin/users?role='.\App\Services\RoleService::ROLE_BORROWER)}}">
                <div class="visual">
                    <i class="fa fa-user"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="1349">{{$borrowers_count}}</span>
                    </div>
                    <div class="desc"> Borrowers </div>
                </div>
            </a>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
            <a class="dashboard-stat dashboard-stat-v2 grey" href="{{url('admin/users?role='.\App\Services\RoleService::ROLE_LENDER)}}">
                <div class="visual">
                    <i class="fa fa-user"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="1349">{{$lenders_count}}</span>
                    </div>
                    <div class="desc"> Lenders </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
            <a class="dashboard-stat dashboard-stat-v2 green" href="{{url('admin/users?role='.\App\Services\RoleService::ROLE_BORROWER.'&status=active')}}">
                <div class="visual">
                    <i class="fa fa-user"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="549">{{$active_borrowers_count}}</span>
                    </div>
                    <div class="desc"> Active Borrowers </div>
                </div>
            </a>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
            <a class="dashboard-stat dashboard-stat-v2 green" href="{{url('admin/users?role='.\App\Services\RoleService::ROLE_LENDER.'&status=active')}}">
                <div class="visual">
                    <i class="fa fa-user"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="549">{{$active_lenders_count}}</span>
                    </div>
                    <div class="desc"> Active Lender </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
            <a class="dashboard-stat dashboard-stat-v2 yellow" href="{{url('admin/loans/bids')}}">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="12,5">{{$bids_count}}</span></div>
                    <div class="desc"> Bids </div>
                </div>
            </a>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
            <a class="dashboard-stat dashboard-stat-v2 red" href="{{url('admin/loans')}}">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="12,5">{{$loan_count}}</span></div>
                    <div class="desc"> Loans </div>
                </div>
            </a>
        </div>
    </div>

@endsection