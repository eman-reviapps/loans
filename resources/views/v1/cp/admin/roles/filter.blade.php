<table class="table table-striped table-bordered table-hover" id="data_table">
    <thead class="bg-grey">
    <tr>
        <th width="2%">
            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                <input type="checkbox" class="group-checkable" data-set=".checkboxes"/>
                <span></span>
            </label>
        </th>
        <th><?= trans('messages.roles.name') ?></th>
        <th><?= trans('messages.roles.slug') ?></th>
        <th><?= trans('messages.common.actions') ?></th>
    </tr>
    </thead>
    <tbody>
    @if($roles)
        @foreach($roles as $record)
            <tr>
                <td class="mt-checkbox-td">
                    <label style="margin-left: 8px"
                           class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                        <input type="checkbox" class="checkboxes" value="{{$record->id}}"/>
                        <span></span>
                    </label>
                </td>
                <td>{{$record->name}}</td>
                <td>{{$record->slug}}</td>
                <td>
                    @if (Sentinel::hasAccess('roles.edit'))
                        <a class="btn btn-info btn-xs" href="{{url('admin/roles/'.$record->id.'/edit')}}">
                            <i class="fa fa-edit"></i>
                            <span class="hidden-xs"> <?= trans('messages.roles.permissions') ?> </span>
                        </a>
                    @endif
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>