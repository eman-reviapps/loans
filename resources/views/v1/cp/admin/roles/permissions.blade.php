@extends('v1.layout.admin')

@section('page-title')
    {{trans('messages.roles.permissions')}}
@endsection

@section('page-css-pugins')
    <link href="{{asset('assets/global/plugins/jstree/dist/themes/default/style.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>{{trans('messages.roles.permissions')}}</h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{url('admin//')}}">{{trans('messages.home')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{url('admin/roles')}}">{{trans('messages.roles._title')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">{{$role->name}}</span>
        </li>
    </ul>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form id="editPermissionsForm" class="form-horizontal form-row-seperated"
                  action="{{url('admin/roles/'.$role->id)}}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                <input type="hidden" name="save_continue" id="save_continue" value="no">

                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            {{$role->full_title}}
                        </div>
                        <div class="actions btn-set">
                            <button type="button" class="btn btn-secondary-outline btn_action_back">
                                <i class="fa fa-angle-left"></i> {{trans('messages.operations.back')}}
                            </button>
                            <button class="btn btn-secondary-outline btn_action_reload">
                                <i class="fa fa-reply"></i> {{trans('messages.operations.reset')}}
                            </button>
                            <button type="button" class="btn btn-success btn_action_save">
                                <i class="fa fa-check"></i> {{trans('messages.operations.save')}}
                            </button>
                            <button type="button" class="btn btn-success btn_action_save_continue">
                                <i class="fa fa-check-circle"></i> {{trans('messages.operations.save_continue')}}
                            </button>
                        </div>
                    </div>
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject bold uppercase">{{trans('messages.roles.permissions')}} {{trans('messages.roles.of_role')}} {{$role->name}}</span>
                            </div>
                            <div class="actions">

                            </div>
                        </div>
                        <div class="portlet-body">
                            <div id="jstree1" class="tree-demo"></div>
                            <input type="hidden" id="privilages" name="privilages" />
                            <input type="hidden" id="un_checked_parent" name="un_checked_parent" />
                        </div>
                    </div>
                </div>
        </div>
        </form>
    </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/jstree/dist/jstree.min.js')}}" type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script>
        var privilages = <?php print_r($permissions_tree) ?>;
    </script>
    <script src="{{asset('assets/loan-app/scripts/roles/permissions.js')}}"></script>
@endsection