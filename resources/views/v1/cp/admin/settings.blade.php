@extends('v1.layout.admin')

@section('page-title')
    {{trans('messages.settings._title')}}
@endsection

@section('page-css-pugins')
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>{{trans('messages.settings._title')}}</h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{url('admin//')}}">{{trans('messages.home')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">{{trans('messages.settings._title')}}</span>
        </li>
    </ul>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form id="settingForm" class="form-horizontal form-row-seperated"
                  action="{{url('admin/settings/'.$setting->id)}}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                <input type="hidden" name="save_continue" id="save_continue" value="no">

                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                        </div>
                        <div class="actions btn-set">
                            <button type="button" class="btn btn-secondary-outline btn_action_back">
                                <i class="fa fa-angle-left"></i> {{trans('messages.operations.back')}}
                            </button>
                            <button class="btn btn-secondary-outline btn_action_reload">
                                <i class="fa fa-reply"></i> {{trans('messages.operations.reset')}}
                            </button>
                            @if (Sentinel::hasAccess('settings.edit'))
                                <button type="button" class="btn btn-success btn_action_save">
                                    <i class="fa fa-check"></i> {{trans('messages.operations.save')}}
                                </button>
                            @endif
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable-bordered">
                            <div class="tab-content">
                                <div class="form-body">

                                    <h5 class="form-section bold">{{trans('messages.settings.loans_settings')}}</h5>
                                    <hr/>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label">{{trans('messages.settings.loan_expire_period')}}
                                            :
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control mask_number"
                                                   name="loan_expire_period" id="loan_expire_period"
                                                   value="{{old('loan_expire_period') ? old('loan_expire_period') : $setting->loan_expire_period}}"
                                                   placeholder="{{trans('messages.settings.loan_expire_period')}}">
                                            <span class="alert-danger"><?php echo $errors->first('loan_expire_period') ?></span>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="select2 form-control" id="loan_expire_period_unit"
                                                    name="loan_expire_period_unit">
                                                @foreach($periodUnits as $key=>$value)
                                                    <option {{( (old('loan_expire_period_unit') && old('loan_expire_period_unit') == $key) || (isset($setting->loan_expire_period_unit) && $setting->loan_expire_period_unit == $key) ) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                            <span class="alert-danger"><?php echo $errors->first('loan_expire_period_unit') ?></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label">{{trans('messages.settings.lender_max_loan_bids')}}
                                            :
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mask_number"
                                                   name="lender_max_loan_bids" id="lender_max_loan_bids"
                                                   value="{{old('lender_max_loan_bids') ? old('lender_max_loan_bids') : $setting->lender_max_loan_bids}}"
                                                   placeholder="{{trans('messages.settings.lender_max_loan_bids')}}">
                                            <span class="alert-danger"><?php echo $errors->first('lender_max_loan_bids') ?></span>
                                        </div>
                                    </div>

                                    <br/>
                                    <h5 class="form-section bold">{{trans('messages.settings.subscription_settings')}}</h5>
                                    <hr/>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label">{{trans('messages.settings.free_trial_period')}}
                                            :
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control mask_number"
                                                   name="free_trial_period" id="free_trial_period"
                                                   value="{{old('free_trial_period') ? old('free_trial_period') : $setting->free_trial_period}}"
                                                   placeholder="{{trans('messages.settings.free_trial_period')}}">
                                            <span class="alert-danger"><?php echo $errors->first('free_trial_period') ?></span>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="select2 form-control" id="free_trial_period_unit"
                                                    name="free_trial_period_unit">
                                                @foreach($daysMonthsUnits as $key=>$value)
                                                    <option {{( (old('free_trial_period_unit') && old('free_trial_period_unit') == $key) || (isset($setting->free_trial_period_unit) && $setting->free_trial_period_unit == $key) ) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                            <span class="alert-danger"><?php echo $errors->first('free_trial_period_unit') ?></span>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-5 control-label">{{trans('messages.settings.free_lifetime_subscriptions_count')}}
                                            :
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control mask_number"
                                                   name="free_lifetime_subscriptions_count" id="free_lifetime_subscriptions_count"
                                                   value="{{old('free_lifetime_subscriptions_count') ? old('free_lifetime_subscriptions_count') : $setting->free_lifetime_subscriptions_count}}"
                                                   placeholder="{{trans('messages.settings.free_lifetime_subscriptions_count')}}">
                                            <span class="alert-danger"><?php echo $errors->first('free_lifetime_subscriptions_count') ?></span>
                                        </div>
                                    </div>

                                    <br/>
                                    <h5 class="form-section bold">{{trans('messages.settings.subscription_reminder_settings')}}</h5>
                                    <hr/>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label">{{trans('messages.settings.subscribe_reminder_email_first')}}
                                            :
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control mask_number"
                                                   name="subscribe_reminder_email_first" id="subscribe_reminder_email_first"
                                                   value="{{old('subscribe_reminder_email_first') ? old('subscribe_reminder_email_first') : $setting->subscribe_reminder_email_first}}"
                                                   placeholder="{{trans('messages.settings.subscribe_reminder_email_first')}}">
                                            <span class="alert-danger"><?php echo $errors->first('subscribe_reminder_email_first') ?></span>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="select2 form-control" id="subscribe_reminder_email_first_unit"
                                                    name="subscribe_reminder_email_first_unit">
                                                @foreach($daysUnits as $key=>$value)
                                                    <option {{( (old('subscribe_reminder_email_first_unit') && old('subscribe_reminder_email_first_unit') == $key) || (isset($setting->subscribe_reminder_email_first_unit) && $setting->subscribe_reminder_email_first_unit == $key) ) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                            <span class="alert-danger"><?php echo $errors->first('subscribe_reminder_email_first_unit') ?></span>
                                        </div>

                                        <span class="help-inline col-md-2"> {{trans('messages.settings.before_free_trial_ends')}} </span>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label">{{trans('messages.settings.subscribe_reminder_email_second')}}
                                            :
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control mask_number"
                                                   name="subscribe_reminder_email_second" id="subscribe_reminder_email_second"
                                                   value="{{old('subscribe_reminder_email_second') ? old('subscribe_reminder_email_second') : $setting->subscribe_reminder_email_second}}"
                                                   placeholder="{{trans('messages.settings.subscribe_reminder_email_second')}}">
                                            <span class="alert-danger"><?php echo $errors->first('subscribe_reminder_email_second') ?></span>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="select2 form-control" id="subscribe_reminder_email_second_unit"
                                                    name="subscribe_reminder_email_second_unit">
                                                @foreach($daysUnits as $key=>$value)
                                                    <option {{( (old('subscribe_reminder_email_second_unit') && old('subscribe_reminder_email_second_unit') == $key) || (isset($setting->subscribe_reminder_email_second_unit) && $setting->subscribe_reminder_email_second_unit == $key) ) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                            <span class="alert-danger"><?php echo $errors->first('subscribe_reminder_email_second_unit') ?></span>
                                        </div>

                                        <span class="help-inline col-md-2"> {{trans('messages.settings.before_free_trial_ends')}} </span>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-5 control-label">{{trans('messages.settings.subscribe_reminder_email_third')}}
                                            :
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control mask_number"
                                                   name="subscribe_reminder_email_third" id="subscribe_reminder_email_third"
                                                   value="{{old('subscribe_reminder_email_third') ? old('subscribe_reminder_email_third') : $setting->subscribe_reminder_email_third}}"
                                                   placeholder="{{trans('messages.settings.subscribe_reminder_email_third')}}">
                                            <span class="alert-danger"><?php echo $errors->first('subscribe_reminder_email_third') ?></span>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="select2 form-control" id="subscribe_reminder_email_third_unit"
                                                    name="subscribe_reminder_email_third_unit">
                                                @foreach($daysUnits as $key=>$value)
                                                    <option {{( (old('subscribe_reminder_email_third_unit') && old('subscribe_reminder_email_third_unit') == $key) || (isset($setting->subscribe_reminder_email_third_unit) && $setting->subscribe_reminder_email_third_unit == $key) ) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                            <span class="alert-danger"><?php echo $errors->first('subscribe_reminder_email_third_unit') ?></span>
                                        </div>

                                        <span class="help-inline col-md-2"> {{trans('messages.settings.before_free_trial_ends')}} </span>
                                    </div>

                                    <br/>
                                    <h5 class="form-section bold">{{trans('messages.settings.subscription_renew_reminder_settings')}}</h5>
                                    <hr/>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label">{{trans('messages.settings.subscribe_renew_reminder_email')}}
                                            :
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control mask_number"
                                                   name="subscribe_renew_reminder_email" id="subscribe_renew_reminder_email"
                                                   value="{{old('subscribe_renew_reminder_email') ? old('subscribe_renew_reminder_email') : $setting->subscribe_renew_reminder_email}}"
                                                   placeholder="{{trans('messages.settings.subscribe_renew_reminder_email')}}">
                                            <span class="alert-danger"><?php echo $errors->first('subscribe_renew_reminder_email') ?></span>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="select2 form-control" id="subscribe_renew_reminder_email_unit"
                                                    name="subscribe_renew_reminder_email_unit">
                                                @foreach($daysUnits as $key=>$value)
                                                    <option {{( (old('subscribe_renew_reminder_email_unit') && old('subscribe_renew_reminder_email_unit') == $key) || (isset($setting->subscribe_renew_reminder_email_unit) && $setting->subscribe_renew_reminder_email_unit == $key) ) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                            <span class="alert-danger"><?php echo $errors->first('subscribe_reminder_email_first_unit') ?></span>
                                        </div>

                                        <span class="help-inline col-md-2"> {{trans('messages.settings.before_subscription_due')}} </span>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('page-js-pugins')

    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>

    <script src="{{asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>

@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/settings.js')}}"></script>
@endsection