@extends('v1.layout.agent')

@section('page-title')
    Subscribe
@endsection

@section('page-head')
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Subscribe</h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">

            @include('v1.layout.__partials.notification.agent')

            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="{{ url('subscribe') }}" method="post">
                        <div id="dropin-container"></div>
                        {{ csrf_field() }}
                        <hr>
                        <button id="payment-button" class="btn btn-primary btn-flat hidden" type="submit">Pay now
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="https://js.braintreegateway.com/js/braintree-2.30.0.min.js"></script>
@endsection

@section('page-scripts')
    <script>
        $.ajax({
            url: '{{ url('braintree/token') }}'
        }).done(function (response) {
            braintree.setup(response.data.token, 'dropin', {
                container: 'dropin-container',
                onReady: function () {
                    $('#payment-button').removeClass('hidden');
                }
            });
        });
    </script>
@endsection
