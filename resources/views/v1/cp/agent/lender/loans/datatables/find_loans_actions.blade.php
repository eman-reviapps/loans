@if(count($loan->bids) == 0)
    <a class="btn btn-xs blue btn-outline archive"
       data-id="{{$loan->id}}"
       data-csrf="{{csrf_token()}}"
       data-action="{{url('lender/loans/' . $loan->id.'/user/'.$logged_in_user->id.'/archive')}}"
       data-toggle="tooltip">{{trans('messages.loans.archive')}}</a>
@else
    <span class="badge badge-roundless badge-success">{{trans('messages.loans.bid_placed')}}</span>
@endif