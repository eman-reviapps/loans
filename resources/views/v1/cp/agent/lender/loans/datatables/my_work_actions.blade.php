@php
    $bid = $loan->bids[0];
    $bid_status = $bid->status;
@endphp

@if($logged_in_user->status != ActivationStates::SUSPENDED)
    @if($bid_status == BidStatuses::NO_RESPONSE
                    &&
                    count($bid->conversation->replies->where('sender_id', $logged_in_user->id)) < 3
                    &&
                    count($bid->conversation->replies->where('receiver_id',$logged_in_user->id)) == 0)
        <a class="btn btn-xs blue btn-outline btn-follow-up"
           data-id="{{$loan->id}}"
           data-csrf="{{csrf_token()}}"
           data-action="{{url('bids/'.$bid->id.'/follow_up_number')}}"
           data-href="{{url('lender/loans/bids/'.$bid->id.'/show')}}"
           data-toggle="tooltip">{{trans('messages.bids.follow_up')}}</a>

    @elseif($bid_status == BidStatuses::BORROWER_RESPONDED)
        <a class="btn btn-xs blue btn-outline"
           href="{{url('lender/loans/bids/' . $bid->id . '/show')}}">{{trans('messages.bids.details')}}</a>
    @elseif($bid_status == BidStatuses::BORROWER_NOT_INTERESTED)
        <a class="btn btn-xs blue btn-outline archive"
           data-id="{{$loan->id}}"
           data-csrf="{{csrf_token()}}"
           data-action="{{url('lender/loans/' . $loan->id.'/user/'.$logged_in_user->id.'/archive')}}"
           data-toggle="tooltip">{{trans('messages.loans.archive')}}</a>
    @endif
@endif