<a class="btn btn-xs blue btn-outline restore"
   data-id="{{$record->id}}"
   data-csrf="{{csrf_token()}}"
   data-action="{{url('lender/loans/archived/' . $record->id.'/restore')}}"
   data-toggle="tooltip">{{trans('messages.loans.restore')}}</a>