
<form id="filterLoansLender">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <label class="sbold">{{trans('messages.preferences.loan_types')}}</label>
                <div class="input-group m-top-30">
                    <div class="icheck-list">
                        @foreach(LoanTypes::items() as $key=>$value)
                            <label>
                                <input value="{{$key}}"
                                       type="checkbox"
                                       class="icheck"
                                       id="{{PreferenceOptions::FAVORITE_LOAN_TYPES}}"
                                       name="{{PreferenceOptions::FAVORITE_LOAN_TYPES}}[]"
                                       data-checkbox="icheckbox_square-blue"
                                        {{
                                        (is_array($lender_preference_loan_types->current_value) ?
                                        in_array($key,$lender_preference_loan_types['current_value']) : $value == $key)
                                        ? 'checked' : ''
                                        }}
                                >
                                {{$value}} </label>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="form-group">
            <div class="row">
                <label class="col-md-2 control-label">{{trans('messages.preferences.loan_size_range')}}</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">
                            Min $
                        </span>
                        <input type="text"
                               class="form-control mask_money"
                               name="{{PreferenceOptions::FAVORITE_LOAN_SIZE_MIN}}"
                               id="{{PreferenceOptions::FAVORITE_LOAN_SIZE_MIN}}"
                               value="{{$lender_preference_loan_min->current_value}}"
                               placeholder="{{trans('messages.preferences.min')}}"
                        >
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">
                            Max $
                        </span>
                        <input type="text"
                               class="form-control mask_money"
                               name="{{PreferenceOptions::FAVORITE_LOAN_SIZE_MAX}}"
                               id="{{PreferenceOptions::FAVORITE_LOAN_SIZE_MAX}}"
                               value="{{$lender_preference_loan_max->current_value}}"
                               placeholder="{{trans('messages.preferences.max')}}"
                        >
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label class="col-md-2 control-label">{{trans('messages.preferences.state')}}</label>
                <div class="col-md-6">
                    <select id="{{PreferenceOptions::FAVORITE_STATE}}"
                            name="{{PreferenceOptions::FAVORITE_STATE}}[]"
                            class="form-control select2-multiple states" multiple>
                        @if($lender_preference_state->value)
                            @foreach($lender_preference_state->value as $item)
                                <option selected id="{{$item}}">{{$item}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label class="col-md-2 control-label">{{trans('messages.preferences.city')}}</label>
                <div class="col-md-6">
                    <select id="{{PreferenceOptions::FAVORITE_CITY}}"
                            name="{{PreferenceOptions::FAVORITE_CITY}}[]"
                            class="form-control select2-multiple cities" multiple>
                        @if($lender_preference_city->value)
                            @foreach($lender_preference_city->value as $item)
                                <option selected id="{{$item}}">{{$item}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label class="col-md-2 control-label">{{trans('messages.preferences.county')}}</label>
                <div class="col-md-6">
                    <select id="{{PreferenceOptions::FAVORITE_LOCATION}}"
                            name="{{PreferenceOptions::FAVORITE_LOCATION}}[]"
                            class="form-control select2-multiple counties" multiple>
                        @if($lender_preference_counties->value)
                            @foreach($lender_preference_counties->value as $item)
                                <option selected id="{{$item}}">{{$item}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 pull-right">
                <button type="button"
                        class="btn blue-madison btn-close-filter"
                        data-toggle="collapse"
                        data-target="#collapse"
                >{{trans('messages.filter.close')}}
                </button>
            </div>
            <div class="col-md-2 pull-right">
                <button type="button"
                        class="btn blue-madison btn-save-to_profile"
                >{{trans('messages.filter.save_to_my_profile')}}
                </button>
            </div>
        </div>
        <hr/>
    </div>
</form>