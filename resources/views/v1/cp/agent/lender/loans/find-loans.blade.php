@extends('v1.cp.agent.lender.loans.layout')

@section('page-title')
    <?= trans('messages.dashboard._title.lender') ?>
@endsection

@section('page-css-pugins')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
          rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}"
          rel="stylesheet"
          type="text/css"/>

    <link href="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet"
          type="text/css"/>

    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>

    <link href="{{asset('assets/global/plugins/icheck/skins/all.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb m-top-30">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{url('/')}}">{{trans('messages.home')}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <span class="active">{{trans('messages.menu.lender.find_loans')}}</span>
        </li>
    </ul>
@endsection

@section('content_loans')
    <div class="portlet" style="margin-bottom: 0" >
        <div class="portlet-title" style="border-bottom: none;margin-bottom: 0">
            <div class="actions" >
                <button type="button" class="btn blue-madison btn-block" data-toggle="collapse"
                        data-target="#collapse">
                    <i class="fa fa-filter"></i> {{trans('messages.filter.filter')}}
                </button>
            </div>
        </div>
        <div class="portlet-body collapse" id="collapse" >
            <div id="context2" data-toggle="context" data-target="#context-menu">

                @include('v1.cp.agent.lender.loans._filter')

            </div>
        </div>
    </div>

    <div class="portlet ">
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover" id="loans-table">
                    <thead>
                    <tr>
                        <th>{{trans('messages.loans.date_posted')}}</th>
                        <th>{{trans('messages.loans.company_name')}}</th>
                        <th>{{trans('messages.loans.location')}}</th>
                        <th>{{trans('messages.loans.requestor')}}</th>
                        <th>{{trans('messages.loans.loan_type')}}</th>
                        <th>{{trans('messages.loans.loan_size')}}</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>

    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/icheck/icheck.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/masks/jquery.maskMoney.min.js')}}"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>


    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/masks/jquery.maskMoney.min.js')}}"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/loans/archive.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/general/zip_code_select_multiple.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/general/city_select_multiple.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/general/state_select_multiple.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/general/county_select_multiple.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/lender/find_loans.js')}}"></script>
@endsection