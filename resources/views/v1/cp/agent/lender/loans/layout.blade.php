@extends('v1.layout.agent')

@section('content')


            @include('v1.layout.__partials.navigation.lender')

            @yield('page-breadcrumb')

            <div class="page-content-agent">
                @yield('content_loans')
            </div>


@endsection