@extends('v1.cp.agent.lender.loans.layout')

@section('page-title')
    <?= trans('messages.dashboard._title.lender') ?>
@endsection

@section('page-css-pugins')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
          rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}"
          rel="stylesheet"
          type="text/css"/>

    <link href="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb m-top-30">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{url('/')}}">{{trans('messages.home')}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <span class="active">{{trans('messages.menu.lender.dashboard')}}</span>
        </li>
    </ul>
@endsection

@section('content_loans')
    <div class="page-head">
    </div>
    <div class="portlet light">
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover" id="my_work-table">
                    <thead>
                    <tr>
                        <th>{{trans('messages.loans.date_posted')}}</th>
                        <th>{{trans('messages.loans.company_name')}}</th>
                        <th>{{trans('messages.loans.location')}}</th>
                        <th>{{trans('messages.loans.loan_type')}}</th>
                        <th>{{trans('messages.loans.loan_size')}}</th>
                        <th>{{trans('messages.loans.status')}}</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>

    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/loans/archive.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/lender/my_work.js')}}"></script>
@endsection