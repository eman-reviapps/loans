@extends('v1.layout.agent')

@section('page-title')
    {{trans('messages.loans.loan_details')}}
@endsection

@section('page-css-pugins')
    <link href="{{asset('assets/loan-app/css/view_page.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/global/plugins/cubeportfolio/css/cubeportfolio.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{url('/')}}">{{trans('messages.home')}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <span class="active">{{trans('messages.loans.loan_details')}}</span>
        </li>
    </ul>
@endsection

@section('content')

    <div class="blog-page blog-content-2">
        <div class="row">
            <div class="col-lg-9">

                @include('v1.cp.components.loans.details',['show_borrower_info'=>true])

            </div>
            <div class="col-lg-3">
                <div class=" blog-container">

                    @include('v1.cp.components.agent.about_borrower')

                    <hr/>

                    @if(count($archive) != 0)
                        <h4 class="block">
                            <span class="label label-warning"> {{trans('messages.loans.archived_title')}} </span>
                        </h4>
                    @endif
                    @if(count($loan->bids) == 0 && count($archive) == 0)
                        <a data-href="{{url('lender/loans/' . $loan->id.'/place_bid')}}"
                           class="btn green-meadow btn-block place_bid"
                           data-id="{{$loan->id}}"
                           data-csrf="{{csrf_token()}}"
                           data-action="{{url('lender/loans/' . $loan->id.'/is_there_other_bids_placed/'.$logged_in_user->id.'')}}"
                        >{{trans('messages.loans.place_bid')}}</a>
                        <br/>

                        <button type="button" class="btn green-meadow btn-outline btn-block archive"
                                data-id="{{$loan->id}}"
                                data-csrf="{{csrf_token()}}"
                                data-action="{{url('lender/loans/' . $loan->id.'/user/'.$logged_in_user->id.'/archive')}}"
                        >{{trans('messages.loans.archive')}}</button>
                    @else
                        <button type="button" class="btn green-meadow btn-outline btn-block restore"
                                data-id="{{$archive[0]->id}}"
                                data-csrf="{{csrf_token()}}"
                                data-action="{{url('lender/loans/archived/' . $archive[0]->id.'/restore')}}"
                        >{{trans('messages.loans.restore')}}</button>
                    @endif
                    {{--<p class="text-center sbold">Bids : 2</p>--}}

                    
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js')}}" type="text/javascript"></script>

@endsection

@section('page-scripts')
    <script src="{{asset('assets/pages/scripts/portfolio-1.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/loans/archive.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/loans/conversation.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/lender/loan_details.js')}}"></script>

@endsection