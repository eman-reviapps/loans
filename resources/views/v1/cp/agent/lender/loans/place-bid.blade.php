@extends('v1.layout.agent')

@section('page-title')
    {{trans('messages.loans.place_bid')}}
@endsection

@section('page-css-pugins')
    <link href="{{asset('assets/loan-app/css/view_page.css')}}" rel="stylesheet" type="text/css"/>

    <link href="{{asset('assets/global/plugins/icheck/skins/all.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{url('/')}}">{{trans('messages.home')}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <a href="{{url('lender/loans/' . $loan->id . '/details') }}">{{LoanTypes::items()[$loan->loan_type]}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <span class="active">{{trans('messages.loans.place_bid')}}</span>
        </li>
    </ul>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold uppercase">{{trans('messages.loans.terms')}}</span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body form">
            <div class="row">
                <div class="col-lg-8">

                    @include('v1.layout.__partials.validation_errors')

                    <form id="place_bid_form"
                          action="{{url('lender/loans/' . $loan->id.'/lender/'.$logged_in_user->id.'/place_bid')}}"
                          class="form-horizontal form-row-seperated" method="POST">

                        <input type="hidden" name="_token" value="<?= csrf_token() ?>">

                        <div class="form-body">
                            <div class="form-group padding-30">
                                <div class="col-md-3">
                                    <label class="control-label sbold">{{trans('messages.bids.rate')}}</label>
                                </div>

                                <div class="col-md-6">
                                    <input type="text" class="form-control numeric" name="rate"
                                           id="rate"
                                           value="{{old('rate') ? old('rate') : (isset($bid) ? $bid->rate : '')}}"
                                           placeholder="%">
                                </div>
                            </div>

                            <div class="form-group padding-30">
                                <div class="col-md-3">
                                    <label class="control-label sbold">{{trans('messages.bids.term')}}</label>
                                </div>

                                <div class="col-md-3">
                                    <input type="text" class="form-control mask_number"
                                           name="term" id="term"
                                           value="{{old('term') ? old('term') : (isset($bid) ? $bid->term : '')}}">
                                </div>
                                <div class="col-md-3">
                                    <select class="select2 form-control" id="term_unit"
                                            name="term_unit">
                                        <option value="">{{trans('messages.common.select_dropdown')}}</option>
                                        @foreach($periodUnits as $key=>$value)
                                            <option {{( (old('term_unit') && old('term_unit') == $key) || (isset($bid->term_unit) && $bid->term_unit == $key) ) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group padding-30">
                                <div class="col-md-3">
                                    <label class="control-label sbold">{{trans('messages.bids.amortization')}}</label>
                                </div>

                                <div class="col-md-3">
                                    <input type="text" class="form-control mask_number"
                                           name="amortization" id="amortization"
                                           value="{{old('amortization') ? old('amortization') : (isset($bid) ? $bid->amortization : '')}}">
                                </div>
                                <div class="col-md-3">
                                    <select class="select2 form-control" id="amortization_unit"
                                            name="amortization_unit">
                                        <option value="">{{trans('messages.common.select_dropdown')}}</option>
                                        @foreach($periodUnits as $key=>$value)
                                            <option {{( (old('amortization_unit') && old('amortization_unit') == $key) || (isset($bid->amortization_unit) && $bid->amortization_unit == $key) ) ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group padding-30">
                                <div class="col-md-3">
                                    <label class="control-label sbold">{{trans('messages.bids.description')}}</label>
                                </div>

                                <div class="col-md-9">
                                    <textarea
                                            class="form-control"
                                            rows="6"
                                            id="description"
                                            name="description">{{old('description') ? old('description') : (isset($bid) ? $bid->description : '')}}</textarea>
                                </div>
                            </div>

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="button" class="btn btn-secondary-outline btn_action_back">
                                        <i class="fa fa-angle-left"></i> {{trans('messages.operations.back')}}
                                    </button>

                                    <button type="button" class="btn btn-success btn_action_save">
                                        <i class="fa fa-check"></i> {{trans('messages.operations.save')}}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>

    <script src="{{asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/masks/jquery.maskMoney.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/icheck/icheck.min.js')}}" type="text/javascript"></script>

    <script type="text/javascript" src="{{asset('assets/global/plugins/jquery-numeric/jquery.numeric.js')}}"></script>
@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/lender/place_bid.js')}}"></script>
@endsection