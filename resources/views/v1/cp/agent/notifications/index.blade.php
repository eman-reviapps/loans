@extends('v1.layout.agent')

@section('page-title')
    Notifications
@endsection

@section('page-head')

@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">{{trans('messages.notifications._title')}}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <ul>
                        @include('v1.cp.components.notifications')
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')

@endsection

@section('page-scripts')

@endsection
