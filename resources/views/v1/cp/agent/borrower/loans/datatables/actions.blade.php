<a href="{{url('borrower/loans/' . $loan->id)}}" class="btn btn-xs green btn-outline" data-toggle="tooltip"><i
            class="fa fa-eye"></i></a>
@if($loan->is_active)

<a href="{{url('borrower/loans/' . $loan->id.'/edit')}}" class="btn btn-xs blue btn-outline" data-toggle="tooltip"><i
            class="fa fa-edit"></i></a>

    <a href="{{url('borrower/loans/' . $loan->id)}}" class="btn btn-xs red btn-outline delete"
       data-id="{{$loan->id}}" data-action="{{url('borrower/loans/' . $loan->id)}}"
       data-toggle="tooltip"><i
                class="fa fa-close"></i></a>
@endif