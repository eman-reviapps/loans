<div class="modal fade delete_modal" id="deleteModal{{$loan->id}}" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ui-draggable-handle">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">{{trans('messages.operations.delete')}} {{trans('messages.loans.singular')}}</h4>
            </div>
            <form id="deleteForm{{$loan->id}}" class="form-horizontal"
                  action="{{url('borrower/loans/' . $loan->id)}}" method="DELETE">
                <div class="modal-body">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        <span class="msg_error"></span>
                    </div>
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button>
                        <span class="msg_success"></span>
                    </div>

                    <p class="bold">{{trans('messages.loans.delete_reason')}}</p>
                    <div class="form-group">
                        <label class="col-md-1 control-label"></label>
                        <div class="col-md-9">
                            <div class="mt-radio-list">
                                @foreach(PostFeedbackList::items() as $key=>$value)
                                    <label class="mt-radio">
                                        <input type="radio" name="reason" class="reason" id="{{$key}}" value="{{$key}}"
                                        > {{$value}}
                                        <span></span>
                                    </label>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="form-group" class="feedback_description">
                        <div class="col-md-9">
                            <label class="sbold">{{trans('messages.feedbacks.reason')}}</label>
                            <textarea id="description" name="description" class="form-control" rows="3"></textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline"
                            data-dismiss="modal">{{trans('messages.operations.cancel')}}</button>
                    <button type="button"
                            class="btn btn-danger delete_record_btn">{{trans('messages.operations.delete')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>