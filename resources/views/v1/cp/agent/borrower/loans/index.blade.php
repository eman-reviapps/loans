@extends('v1.cp.agent.borrower.loans.layout')

@section('page-title')
    <?= trans('messages.dashboard._title.borrower') ?>
@endsection

@section('page-css-pugins')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
          rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}"
          rel="stylesheet"
          type="text/css"/>

    <link href="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb m-top-30">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{url('/')}}">{{trans('messages.home')}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <span class="active">{{trans('messages.menu.borrower.'.$title_key)}}</span>
        </li>
    </ul>
@endsection

@section('content_loans')
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject font-green sbold uppercase"> {{trans('messages.menu.borrower.'.$title_key)}} </span>
            </div>
            <div class="actions">
                <a href="{{url('borrower/loans/create')}}"
                   class="btn btn-success btn-sm">{{trans('messages.operations.new')}}</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="modal fade delete_modal" id="deleteModal" tabindex="-1" role="basic" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header ui-draggable-handle">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">{{trans('messages.operations.delete')}} {{trans('messages.loans.singular')}}</h4>
                        </div>
                        <form id="deleteForm" class="form-horizontal"
                              action="" method="DELETE">
                            <div class="form-body">
                                <div class="modal-body">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="id" value="">
                                    <input type="hidden" name="status" value="{{LoanStatuses::DELETED}}">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                                    <div class="alert alert-danger display-hide">
                                        <button class="close" data-close="alert"></button>
                                        <span class="msg_error"></span>
                                    </div>
                                    <div class="alert alert-success display-hide">
                                        <button class="close" data-close="alert"></button>
                                        <span class="msg_success"></span>
                                    </div>

                                    <p class="bold">{{trans('messages.loans.delete_reason')}}</p>
                                    <div class="form-group">
                                        <label class="col-md-1 control-label"></label>
                                        <div class="col-md-6">
                                            <div class="mt-radio-list">
                                                @foreach(PostFeedbackList::items() as $key=>$value)
                                                    <label class="mt-radio">
                                                        <input type="radio" name="reason" class="reason" id="{{$key}}"
                                                               value="{{$key}}"
                                                        > {{$value}}
                                                        <span></span>
                                                    </label>
                                                @endforeach
                                            </div>
                                            <span class="alert-danger"></span>
                                        </div>

                                        <div class="feedback_description">
                                            <div class="col-md-9">
                                                <label class="sbold">{{trans('messages.feedbacks.reason')}}</label>
                                                <textarea id="description" name="description" class="form-control"
                                                          rows="3"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn dark btn-outline"
                                            data-dismiss="modal">{{trans('messages.operations.cancel')}}</button>
                                    <button type="button"
                                            class="btn btn-danger delete_loan_btn">{{trans('messages.operations.delete')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="table-container">
                <table class="table table-striped table-bordered table-hover" id="loans-table">
                    <thead>
                    <tr>
                        <th>{{trans('messages.loans.date_posted')}}</th>
                        <th>{{trans('messages.loans.loan_request_name')}}</th>
                        <th>{{trans('messages.loans.loan_request_size')}}</th>
                        <th>{{trans('messages.loans.request_stage')}}</th>
                        <th style="width:150px;"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>

    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/loans/index.js')}}"></script>
@endsection