@extends('v1.layout.agent')

@section('content')
    <div class="row">

        @include('v1.layout.__partials.navigation.borrower')

        @yield('page-breadcrumb')

        <div class="page-content-agent">
            @yield('content_loans')
        </div>

    </div>
@endsection