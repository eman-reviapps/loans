@extends('v1.layout.agent')

@section('page-title')
    <?= trans('messages.dashboard._title.borrower') ?>
@endsection

@section('page-css-pugins')

    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>

    <link href="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css"/>

    <link href="{{asset('assets/global/plugins/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/dropzone/basic.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <div class="page-title">
            <h1>{{trans('messages.loans.new_loan')}}</h1>
        </div>
    </div>
@endsection
@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb m-top-30">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{url('/')}}">{{trans('messages.home')}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <a href="{{url('/')}}">{{trans('messages.menu.borrower.active_deals')}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <span class="active">{{trans('messages.loans.new_loan')}}</span>
        </li>
    </ul>
@endsection

@section('content')

    <div class="portlet">
        <div class="portlet-body form">
            <form action="{{url('borrower/loans/get-template')}}" class="form-horizontal" method="GET">
                <input type="hidden" name="_token" value="<?= csrf_token() ?>">

                <div class="form-body" style="padding-bottom: 0">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-percent-9">Loan Type</label>
                        <div class="col-md-7">
                            <select class="form-control select2 loan_type" id="loan_type" name="loan_type">
                                @foreach($loanTypes as $key=>$value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="background-white">

        <div id="template_LINE_OF_CREDIT">
            @include('v1.cp.components.loans.loan_templates.form.line_of_credit')
        </div>
        <div id="template_RE_PURCHASE_INVESTOR_0_50">
            @include('v1.cp.components.loans.loan_templates.form.re_purchase_investor')
        </div>
        <div id="template_RE_PURCHASE_OWNER_51_100">
            @include('v1.cp.components.loans.loan_templates.form.re_purchase_owner')
        </div>
        <div id="template_RE_REFINANCE_INVESTOR_0_50">
            @include('v1.cp.components.loans.loan_templates.form.re_refinance_investor')
        </div>
        <div id="template_RE_REFINANCE_OWNER_51_100">
            @include('v1.cp.components.loans.loan_templates.form.re_refinance_owner')
        </div>
        <div id="template_RE_CASH_OUT_INVESTOR_0_50">
            @include('v1.cp.components.loans.loan_templates.form.re_cash_out_investor')
        </div>
        <div id="template_RE_CASH_OUT_OWNER_51_100">
            @include('v1.cp.components.loans.loan_templates.form.re_cash_out_owner')
        </div>
        <div id="template_EQUIPMENT_FINANCE">
            @include('v1.cp.components.loans.loan_templates.form.equipment_finance')
        </div>
        <div id="template_TERM_LOAN_OTHER">
            @include('v1.cp.components.loans.loan_templates.form.term_loan_other')
        </div>

        @include('v1.cp.components.general.dropzone')

    </div>


@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/jquery-repeater/jquery.repeater.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>

    <script src="{{asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>

    {{--<script src="{{asset('assets/global/plugins/masks/jquery.maskMoney.min.js')}}"--}}
            {{--type="text/javascript"></script>--}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>

    <script src="{{asset('assets/global/plugins/dropzone/dropzone.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')}}"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        loan_type = '{{$loan_type}}';
        is_moving_from_leased = '{{old('is_moving_from_leased') ? old('is_moving_from_leased'):0}}';
        is_loan_outstanding = '{{old('is_loan_outstanding') ? old('is_loan_outstanding'):0}}';
    </script>
    <script src="{{asset('assets/loan-app/scripts/loans/validation.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/general/address.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/loans/form.js')}}"></script>
@endsection