@extends('v1.layout.agent')

@section('page-title')
    {{trans('messages.loans.loan_details')}}
@endsection

@section('page-css-pugins')
    <link href="{{asset('assets/loan-app/css/view_page.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet"
          type="text/css"/>

    <link href="{{asset('assets/global/plugins/cubeportfolio/css/cubeportfolio.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('page-css')
    <link href="{{asset('assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{asset('assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{url('/')}}">{{trans('messages.home')}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <a href="{{url('/')}}">{{trans('messages.menu.borrower.active_deals')}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <span class="active">{{trans('messages.loans.loan_details')}}</span>
        </li>
    </ul>
@endsection

@section('content')
    <div class="blog-page blog-content-2">
        <div class="row">
            <div class="col-lg-9">

                @include('v1.cp.components.loans.details',['show_borrower_info'=>false])

                @include('v1.cp.components.loans.bids')
            </div>
            <div class="col-lg-3">
                <div class=" blog-container">

                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js')}}" type="text/javascript"></script>

@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/conversation.js')}}"></script>
    <script src="{{asset('assets/pages/scripts/portfolio-1.js')}}" type="text/javascript"></script>
@endsection