<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-equalizer font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">{{LoanTypes::items()[$loan_type_key]}}</span>
        </div>
        <div class="actions">
            @if($loan)
                <form action="{{url('borrower/loans/'.$loan->id.'/change_status')}}" class="form-horizontal"
                      id="form_change_status"
                      method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                    <input type="hidden" name="status" value="">
                    <input type="hidden" name="save_continue" id="save_continue" value="yes">

                    <span class="badge badge-{{ViewHelper::getLoanStatusLabel($loan->status)}}">{{LoanStatuses::items()[$loan->status]}}</span>
                    @if($loan->status == LoanStatuses::DRAFT)
                        <button type="button" class="btn green btn_loan_publish">
                            {{trans('messages.operations.publish')}}
                        </button>
                    @endif
                </form>
            @endif
        </div>
    </div>
    <div class="portlet-body form">
        <form id="form_{{$loan_type_key}}" action="{{url('borrower/loans'.(isset($loan) ? '/'.$loan->id: ''))}}"
              class="form-horizontal" method="POST">
            <input type="hidden" name="_token" value="<?= csrf_token() ?>">
            <input type="hidden" name="save_continue" id="save_continue" value="no">
            <input type="hidden" name="loan_type_slug" id="loan_type_slug" value="">
            <input type="hidden" name="loan_status" id="loan_status" value="">
            @if($loan)
                <input type="hidden" name="_method" value="PUT">
            @endif

            <div class="form-body">
                {{ $slot }}

                @if(isset($loan))
                @include('v1.cp.components.attachement.all',['attachable'=>$loan,'view'=>false])
                @endif
                <div class="row padding-b-30">
                    <div class="col-md-12" >
                        <h5 class="form-section bold">{{trans('messages.loans.attachements')}}</h5>
                        <a class="btn green btn-outline sbold" data-toggle="modal" href="#basic"> {{trans('messages.loans.add_files')}} </a>
                    </div>
                </div>



                <a class="btn default blue-stripe" data-toggle="modal" id="manual-ajax"
                   data-key_by="slug"
                   data-user_id="{{$logged_in_user ? $logged_in_user->id : ''}}"
                   data-profile_id="{{$profile ? $profile->id : ''}}"
                   data-loan_id="{{$loan ? $loan->id : ''}}"
                   data-csrf="{{csrf_token()}}"
                   data-action="{{url('preferences')}}"
                   href="#post_preferences_{{$loan_type_key}}"> {{trans('messages.loans.post_preferences')}} </a>

                <div id="post_preferences_{{$loan_type_key}}" class="modal fade" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title sbold">{{trans('messages.loans.post_preferences')}}</h4>
                            </div>
                            <div class="modal-body">

                                <div id="data"></div>

                            </div>
                            <div class="modal-footer">

                                <button type="button"
                                        class="btn dark btn-outline reset_default_preferneces">{{trans('messages.loans.reset_to_default')}}
                                </button>

                                <button type="button" data-dismiss="modal"
                                        class="btn dark btn-outline">{{trans('messages.operations.close')}}</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="form-actions" style="padding-top: 10px">
                <div class="row">
                    <div class="col-md-offset-4 col-md-4s">
                        <button type="button" class="btn btn-secondary-outline btn_action_back">
                            <i class="fa fa-angle-left"></i> {{trans('messages.operations.back')}}
                        </button>
                        <button class="btn btn-secondary-outline btn_action_reload">
                            <i class="fa fa-reply"></i> {{trans('messages.operations.reset')}}
                        </button>

                        @if($loan)
                            <button type="button" class="btn btn-success btn_loan_save_continue">
                                <i class="fa fa-check-circle"></i> {{trans('messages.operations.save_continue')}}
                            </button>
                        @else
                            <button type="button" class="btn btn-success btn_loan_save_draft">
                                <i class="fa fa-check"></i> {{trans('messages.operations.save_draft')}}
                            </button>
                        @endif
                        <button type="button" class="btn btn-success btn_loan_save">
                            <i class="fa fa-check"></i> {{trans('messages.operations.save')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
