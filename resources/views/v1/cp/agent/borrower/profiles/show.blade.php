@extends('v1.layout.agent')

@section('page-title')
    {{ $user->full_name ." ". trans('messages.users.profile')}}
@endsection

@section('page-css-pugins')
    <link href="{{asset('assets/pages/css/profile-2.min.css')}}" rel="stylesheet" type="text/css"/>

    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>

    <link href="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('assets/global/plugins/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/dropzone/basic.min.css')}}" rel="stylesheet" type="text/css"/>

    <link href="{{asset('assets/global/plugins/cubeportfolio/css/cubeportfolio.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{url('/')}}">{{trans('messages.home')}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <span class="active">{{trans('messages.header.profile')}}</span>
        </li>
    </ul>
@endsection

@section('content')
    <div class="profile">
        <div class="tabbable-line tabbable-full-width">
            <ul class="nav nav-tabs">
                <li>
                    <a href="#tab_overview" data-toggle="tab"> {{trans('messages.users.overview')}}</a>
                </li>
                <li>
                    <a href="#tab_account" data-toggle="tab"> {{trans('messages.users.profile_settings')}}</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="tab_overview">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="list-unstyled profile-nav">
                                <li>
                                    <img src="{{isset($user->photo) && !empty($user->photo) ? asset($user->photo) : asset('assets/loan-app/img/avatars/no_image.png')}}"
                                         class="img-responsive pic-bordered" alt=""/>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-8 profile-info">
                                    <h1 class="font-green sbold">{{$user->full_name}}</h1>

                                    <p>
                                        <a href="javascript:;"> {{$user->email}} </a>
                                    </p>
                                    <h5>
                                        <strong>{{$profile->profile_type}} {{trans('messages.users.profile')}}</strong>
                                    </h5>
                                    <br/>
                                    @if($profile->is_enabled &&  $profile->id != session('borrower_profile_id'))
                                        <a class="btn btn-circle red"
                                           href="{{url('borrower/profiles/'.$profile->id.'/select')}}">
                                            <span class="hidden-xs"> {{trans('messages.profiles.set_active')}} </span>
                                        </a>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    @include('v1.cp.components.user.summary')
                                </div>
                            </div>

                            <div class="tabbable-line tabbable-custom-profile">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_profile_info"
                                           data-toggle="tab"> {{trans('messages.profiles.profile_info2')}} </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_profile_info">
                                        <div class="portlet-body">
                                            @if($profile->profile_type == BorrowerProfileTypes::REAL_ESTATE)
                                                @include('v1.cp.components.user.realestate_profile_info')
                                            @else
                                                @include('v1.cp.components.user.business_profile_info')
                                            @endif
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="tab_latest_bids">
                                        {{trans('messages.profiles.latest_loans')}}
                                    </div>

                                    <div class="tab-pane" id="tab_feed">
                                        {{trans('messages.profiles.feed')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--tab_1_2-->
                <div class="tab-pane" id="tab_account">
                    <div class="row profile-account">
                        <div class="col-md-3">
                            <ul class="ver-inline-menu tabbable margin-bottom-10">
                                <li>
                                    <a data-toggle="tab" href="#tab_account_profile_info">
                                        <i class="fa fa-cog"></i> {{trans('messages.profiles.profile_info2')}} </a>
                                    <span class="after"> </span>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab_account_preferences">
                                        <i class="fa fa-cog"></i> {{trans('messages.users.preferences')}} </a>
                                </li>

                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="tab-content">
                                <div id="tab_account_profile_info" class="tab-pane">
                                    @include('v1.cp.agent.borrower.profiles.edit')
                                </div>

                                <div id="tab_account_preferences" class="tab-pane">
                                    @include('v1.cp.components.preferences.borrower_preferences')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end tab-pane-->
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/jquery-repeater/jquery.repeater.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>

    <script src="{{asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/masks/jquery.maskMoney.min.js')}}"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>

    <script src="{{asset('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/dropzone/dropzone.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js')}}" type="text/javascript"></script>

@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/datatable-simple.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/general/address.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/users/borrower_profile.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/attachment.js')}}"></script>
    <script src="{{asset('assets/pages/scripts/portfolio-1.js')}}" type="text/javascript"></script>
@endsection