@extends('v1.layout.agent')

@section('page-title')
    {{ $user->full_name ." ". trans('messages.users.profile')}}
@endsection

@section('page-css-pugins')
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/pages/css/profile-2.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <div class="page-title">
            <h1>{{ $user->full_name ." ". trans('messages.users.profiles')}}</h1>
        </div>
        <div class="page-toolbar">
            <a class="btn btn-success " href="{{url('borrower/profiles/create')}}">
                <i class="fa fa-plus"></i>
                <span class="hidden-xs"> {{trans('messages.header_frontend.add_profile')}} </span>
            </a>

        </div>
    </div>
@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{url('/')}}">{{trans('messages.home')}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <span class="active">{{ $user->full_name ." ". trans('messages.users.profiles')}}</span>
        </li>
    </ul>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-advance table-hover"
                           id="data_table_no_actions">
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{trans('messages.profiles.profile_type')}}</th>
                            <th>{{trans('messages.profiles.title')}}</th>
                            <th>{{trans('messages.profiles.is_enabled')}}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($profiles)
                            @foreach($profiles as $record)
                                <tr>
                                    <td>
                                        @if($record->id == session('borrower_profile_id'))
                                            <span class="label label-sm label-success"> {{trans('messages.profiles.active')}} </span>
                                        @endif
                                    </td>
                                    <td>{{$record->profile_type}}</td>
                                    <td>{{$record->profile_title or ''}}</td>
                                    <td>
                                        <form id="changeStateForm{{$record->id}}" class="form-horizontal"
                                              action="{{url('borrower/profiles/' . $record->id . '/change_status')}}"
                                              method="POST">
                                            <input type="hidden" name="_method" value="PUT">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <input type="hidden" name="is_enabled" value="{{$record->is_enabled}}">

                                            <input type="checkbox" class="make-switch" data-size="small"
                                                   data-bind="{{$record->id}}"
                                                   {{$record->is_enabled ? "checked" : ""}} data-on-color='success'
                                                   data-on-text="<i class='fa fa-check'></i>"
                                                   data-off-text="<i class='fa fa-times'></i>">
                                        </form>
                                    </td>
                                    <td>
                                        <a class="btn btn-sm grey-salsa btn-outline"
                                           href="{{url('borrower/profiles/'.$record->id)}}">
                                            <i class="fa fa-eye"></i>
                                            <span class="hidden-xs"> {{trans('messages.operations.view')}} </span>
                                        </a>
                                        <a class="btn btn-sm grey-salsa btn-outline"
                                           href="{{url('borrower/profiles/'.$record->id.'/edit')}}">
                                            <i class="fa fa-edit"></i>
                                            <span class="hidden-xs"> {{trans('messages.operations.edit')}} </span>
                                        </a>
                                        <a class="btn btn-sm grey-salsa btn-outline" data-toggle="modal"
                                           href="#deleteModal{{$record->id}}">
                                            <i class="fa fa-close"></i>
                                            <span class="hidden-xs"> {{trans('messages.operations.delete')}} </span>
                                        </a>
                                        @include('v1.cp.agent.borrower.profiles.delete')

                                        @if($record->is_enabled &&  $record->id != session('borrower_profile_id'))
                                            <a class="btn btn-sm grey-salsa btn-outline"
                                               href="{{url('borrower/profiles/'.$record->id.'/select')}}">
                                                <i class="fa fa-bookmark"></i>
                                                <span class="hidden-xs"> {{trans('messages.profiles.set_active')}} </span>
                                            </a>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}"
            type="text/javascript"></script>

@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/datatable-no-options.js')}}"></script>
@endsection