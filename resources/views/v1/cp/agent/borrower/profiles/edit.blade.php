@php
    if($profile->profile_type == BorrowerProfileTypes::REAL_ESTATE)
    {
    $form_id = 'borrower_realEstate_profile';
    }
    else
    {
    $form_id = 'borrower_business_profile';
    }

@endphp


<form role="form" id="{{$form_id}}" action="{{url('borrower/profiles/'.$profile->id)}}" class="form-horizontal form-row-seperated"
      method="post">
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="_token" value="<?= csrf_token() ?>">
    <input type="hidden" name="save_continue_tab" id="save_continue_tab" value="">
    <div class="portlet">
        <div class="portlet-body">
            <div class="form-body">
                @if($profile->profile_type == BorrowerProfileTypes::REAL_ESTATE)
                    @include('v1.cp.components.user.realestate_profile')
                @else
                    @include('v1.cp.components.user.business_profile')
                @endif
            </div>
        </div>
    </div>

    <div class="margiv-top-20">
        <button type="button" class="btn btn-success btn_action_save_continue_tab">
            {{trans('messages.operations.save_changes')}}
        </button>
        <button type="button" class="btn btn-secondary-outline btn_action_back">
            {{trans('messages.operations.cancel')}}
        </button>
    </div>

</form>

@include('v1.cp.components.general.dropzone')