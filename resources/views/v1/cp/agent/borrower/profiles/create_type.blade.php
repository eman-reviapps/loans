@extends('v1.layout.agent')

@section('page-title')
    {{ $user->full_name ." ". trans('messages.profiles.add_profile')}}
@endsection

@section('page-css-pugins')
    <link href="{{asset('assets/pages/css/profile-2.min.css')}}" rel="stylesheet" type="text/css"/>

    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/pages/css/about.min.css')}}" rel="stylesheet" type="text/css"/>

@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{url('/')}}">{{trans('messages.home')}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <span class="active">{{trans('messages.profiles.add_profile')}}</span>
        </li>
    </ul>
@endsection

@section('content')
    <div class="margin-top-40">
    <div class="col-md-8 col-md-offset-2">
        <div class="text-center">
            <h1 class="m-xs-top-bottom tell_us"><?= trans('messages.profiles.select_profile_type')?></h1>
        </div>
    </div>
    <div class="col-md-8 col-md-offset-2" style="margin-top: 30px">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="portlet">
                    <div class="card-icon card-icon-signup">
                        <img class="img-icon-signup" src="{{asset('assets/loan-app/img/bussniss-Loan.png')}}">
                    </div>
                    <div class="card-title">
                        <span> {{trans('messages.sign_up_borrower.business')}} </span>
                    </div>
                    <div class="card-desc">
                        <a class="btn btn-default btn-circle uppercase"
                           href="{{url('borrower/profiles/create?profile_type=business')}}"><?= trans('messages.sign_up_borrower.get_started')?></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="portlet">
                    <div class="card-icon card-icon-signup">
                        <img class="img-icon-signup" src="{{asset('assets/loan-app/img/realstate-Loan.png')}}">
                    </div>
                    <div class="card-title">
                        <span> {{trans('messages.sign_up_borrower.real_Estate')}} </span>
                    </div>
                    <div class="card-desc">
                        <a class="btn btn-default btn-circle uppercase"
                           href="{{url('borrower/profiles/create?profile_type=real_estate')}}"><?= trans('messages.sign_up_borrower.get_started')?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/jquery-repeater/jquery.repeater.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>

    <script src="{{asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/datatable-simple.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/users/borrower_profile.js')}}"></script>
@endsection