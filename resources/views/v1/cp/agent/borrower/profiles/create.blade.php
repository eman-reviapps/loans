@extends('v1.layout.agent')

@section('page-title')
    {{ $user->full_name ." ". trans('messages.users.profile')}}
@endsection

@section('page-css-pugins')
    <link href="{{asset('assets/pages/css/profile-2.min.css')}}" rel="stylesheet" type="text/css"/>

    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>

@endsection

@section('page-breadcrumb')
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{url('/')}}">{{trans('messages.home')}}</a>
            <span class="separator">/</span>
        </li>
        <li>
            <span class="active">{{trans('messages.profiles.add_profile')}}</span>
        </li>
    </ul>
@endsection

@php
    if($profile_type == BorrowerProfileTypes::REAL_ESTATE)
    {
    $form_id = 'borrower_realEstate_profile';
    }
    else
    {
    $form_id = 'borrower_business_profile';
    }

@endphp

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form id="{{$form_id}}" class="form-horizontal form-row-seperated"
                  action="{{url('borrower/profiles')}}" method="POST">
                <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                <input type="hidden" name="save_continue" id="save_continue" value="no">
                <input type="hidden" name="profile_type" id="profile_type" value="{{$profile_type}}">

                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            </i>{{trans('messages.operations.add')}}
                            {{$profile_type == BorrowerProfileTypes::REAL_ESTATE ? trans('messages.sign_up_borrower.real_Estate') : trans('messages.sign_up_borrower.business')}}
                            {{trans('messages.users.profile')}}
                        </div>
                        <div class="actions btn-set">
                            <button type="button" class="btn btn-secondary-outline btn_action_back">
                                <i class="fa fa-angle-left"></i> {{trans('messages.operations.back')}}
                            </button>
                            <button class="btn btn-secondary-outline btn_action_reload">
                                <i class="fa fa-reply"></i> {{trans('messages.operations.reset')}}
                            </button>
                            <button type="button" class="btn btn-success btn_action_save">
                                <i class="fa fa-check"></i> {{trans('messages.operations.save')}}
                            </button>
                        </div>
                    </div>
                    {{--col-md-10 col-md-offset-1--}}
                    <div class="portlet-body">
                        <div class="tabbable-bordered">
                            <div class="tab-content">
                                <div class="form-body">
                                    @if($profile_type == BorrowerProfileTypes::REAL_ESTATE)
                                        @include('v1.cp.components.user.realestate_profile',['profile'=>null])
                                    @else
                                        @include('v1.cp.components.user.business_profile',['profile'=>null])
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/jquery-repeater/jquery.repeater.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>

    <script src="{{asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/masks/jquery.maskMoney.min.js')}}"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>

@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/datatable-simple.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/general/address.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/users/borrower_profile.js')}}"></script>
@endsection