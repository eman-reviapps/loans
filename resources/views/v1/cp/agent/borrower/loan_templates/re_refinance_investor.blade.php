@component('v1.cp.agent.borrower.loans.form',[
'loan_type_key'=>LoanTypes::RE_REFINANCE_INVESTOR_0_50,
'profile'=>$profile,
'logged_in_user'=>$logged_in_user,
'loan'=>isset($loan) ? $loan : null,
])

    @slot('form_id')
        form_RE_REFINANCE_INVESTOR_0_50
    @endslot

    @slot('title')
        {{LoanTypes::items()[LoanTypes::RE_REFINANCE_INVESTOR_0_50]}}
    @endslot

    @include('v1.cp.components.general.address',['record'=>(isset($loan) ? $loan->details:null)])

    @include('v1.cp.components.loans.loan_amount')

    @include('v1.cp.components.loans.purpose-text')

    @include('v1.cp.components.loans.real_estate_type')

    @include('v1.cp.components.loans.building_sq_ft')

    @include('v1.cp.components.loans.owner_occupied_p')

    @include('v1.cp.components.loans.gross_annual_revenue')

    @include('v1.cp.components.loans.estimated_annual_expenses')

    @include('v1.cp.components.loans.tenants.tenants')

    @include('v1.cp.components.loans.cash_liquid_investments')

@endcomponent
