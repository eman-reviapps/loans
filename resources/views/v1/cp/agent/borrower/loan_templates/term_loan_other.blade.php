@component('v1.cp.agent.borrower.loans.form',[
'loan_type_key'=>LoanTypes::TERM_LOAN_OTHER,
'profile'=>$profile,
'logged_in_user'=>$logged_in_user,
'loan'=>isset($loan) ? $loan : null,
])

    @include('v1.cp.components.loans.loan_amount')

    @include('v1.cp.components.loans.purpose-text')

    @include('v1.cp.components.loans.company_EBITDA')

    @include('v1.cp.components.loans.total_liabilities')

    @include('v1.cp.components.loans.funded_debt')

    @include('v1.cp.components.loans.tangible_net_worth')

@endcomponent

