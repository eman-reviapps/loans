@component('v1.cp.agent.borrower.loans.form',[
'loan_type_key'=>LoanTypes::EQUIPMENT_FINANCE,
'profile'=>$profile,
'logged_in_user'=>$logged_in_user,
'loan'=>isset($loan) ? $loan : null,
])

    @include('v1.cp.components.loans.equipment_type')

    @include('v1.cp.components.loans.equipment_name')

    @include('v1.cp.components.loans.equipment_state')

    @include('v1.cp.components.loans.equipment_age')

    @include('v1.cp.components.loans.purchase_price')

    @include('v1.cp.components.loans.company_EBITDA')

    @include('v1.cp.components.loans.total_liabilities')

    @include('v1.cp.components.loans.funded_debt')

    @include('v1.cp.components.loans.tangible_net_worth')

@endcomponent