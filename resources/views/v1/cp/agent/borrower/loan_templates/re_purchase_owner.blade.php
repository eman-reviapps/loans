@component('v1.cp.agent.borrower.loans.form',[
'loan_type_key'=>LoanTypes::RE_PURCHASE_OWNER_51_100,
'profile'=>$profile,
'logged_in_user'=>$logged_in_user,
'loan'=>isset($loan) ? $loan : null,
])

    @include('v1.cp.components.general.address',['record'=>(isset($loan) ? $loan->details:null)])

    @include('v1.cp.components.loans.real_estate_type')

    @include('v1.cp.components.loans.purchase_price')

    @include('v1.cp.components.loans.loan_requested')

    @include('v1.cp.components.loans.cash_before_down_payment')

    @include('v1.cp.components.loans.building_sq_ft')

    @include('v1.cp.components.loans.owner_occupied_p',['loan_51'=>true])

    @include('v1.cp.components.loans.owner_not_occupied_p')

    <span class="help-block">{{trans('messages.loans.owner_not_occupied_hint')}}</span>

    @include('v1.cp.components.loans.tenants.tenants')

    @include('v1.cp.components.loans.loan_moving_from_leased')

    @include('v1.cp.components.loans.company_EBITDA')

    @include('v1.cp.components.loans.total_liabilities')

    @include('v1.cp.components.loans.funded_debt')

    @include('v1.cp.components.loans.tangible_net_worth')

@endcomponent
