@component('v1.cp.agent.borrower.loans.form',[
'loan_type_key'=>LoanTypes::RE_CASH_OUT_OWNER_51_100,
'profile'=>$profile,
'logged_in_user'=>$logged_in_user,
'loan'=>isset($loan) ? $loan : null,
])

    @include('v1.cp.components.general.address',['record'=>(isset($loan) ? $loan->details:null)])

    @include('v1.cp.components.loans.real_estate_type')

    @include('v1.cp.components.loans.loan_outstanding')

    @include('v1.cp.components.loans.loan_amount')

    @include('v1.cp.components.loans.purpose-text')

    @include('v1.cp.components.loans.building_sq_ft')

    @include('v1.cp.components.loans.company_revenue')

    @include('v1.cp.components.loans.company_EBITDA')

    @include('v1.cp.components.loans.rental_revenue_by_tenants')

    @include('v1.cp.components.loans.cash_liquid_investments')

@endcomponent
