@component('v1.cp.agent.borrower.loans.form',[
'loan_type_key'=>LoanTypes::RE_CASH_OUT_INVESTOR_0_50,
'profile'=>$profile,
'logged_in_user'=>$logged_in_user,
'loan'=>isset($loan) ? $loan : null,
])

    @include('v1.cp.components.general.address',['record'=>(isset($loan) ? $loan->details:null)])

    @include('v1.cp.components.loans.real_estate_type')

    @include('v1.cp.components.loans.loan_outstanding')

    @include('v1.cp.components.loans.loan_amount')

    @include('v1.cp.components.loans.purpose-text')

    @include('v1.cp.components.loans.building_sq_ft',['display_tips'=>true])

    @include('v1.cp.components.loans.owner_occupied_p')

    @include('v1.cp.components.loans.rental_annual_revenue')

    @include('v1.cp.components.loans.estimated_annual_expenses')

    @include('v1.cp.components.loans.tenants.tenants')

    @include('v1.cp.components.loans.cash_liquid_investments')

@endcomponent
