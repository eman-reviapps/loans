@component('v1.cp.agent.borrower.loans.form',[
'loan_type_key'=>LoanTypes::LINE_OF_CREDIT,
'profile'=>$profile,
'logged_in_user'=>$logged_in_user,
'loan'=>isset($loan) ? $loan : null,
])

    @include('v1.cp.components.loans.loan_amount')

    @include('v1.cp.components.loans.purpose-dd')

    @include('v1.cp.components.loans.receivable_range')

    @include('v1.cp.components.loans.inventory_range')

    @include('v1.cp.components.loans.inventory_type')

    @include('v1.cp.components.loans.current_ratio')

@endcomponent