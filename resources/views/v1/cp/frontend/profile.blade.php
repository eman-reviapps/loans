@extends('v1.layout.frontend')

@section('page-title')
    {{ $user->full_name ." ". trans('messages.users.profile')}}
@endsection

@section('page-css-pugins')
    <link href="<?= asset('assets/global/plugins/datatables/datatables.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') ?>"
          rel="stylesheet" type="text/css"/>

    <link href="<?= asset('assets/pages/css/profile-2.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') ?>" rel="stylesheet"
          type="text/css"/>
@endsection

@section('page-head')
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>{{ $user->full_name}}</h1>
        </div>
    </div>
@endsection

@section('content')
    @include('v1.cp.components.user.profile')
@endsection

@section('page-js-pugins')
    <script src="<?= asset('assets/global/scripts/datatable.js') ?>" type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/datatables/datatables.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') ?>"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="<?= asset('assets/loan-app/scripts/datatable-simple.js') ?>"></script>

    <script src="{{asset('assets/loan-app/scripts/users/account.js')}}"></script>
@endsection