@extends('v1.layout.public')

@section('page-title')
    <?= trans('messages.sign_in._title') ?>
@endsection

@section('page-css')
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?= asset('assets/loan-app/css/public.css') ?>" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
@endsection

@section('header-right')
    <div class="top-right-btns pull-right">
        <div class="btn-group btn-group-circle btn-group-justified">
            <a href="javascript:;" class="btn btn-default btn-header"> {{trans('messages.sign_in.sign_up')}} </a>
            <a href="javascript:;" class="btn btn-default btn-header"> {{trans('messages.sign_in.sign_in')}} </a>
        </div>
    </div>

    <ul class="nav navbar-nav pull-right">
        <li class="dropdown dropdown-user dropdown-dark">
            <a class="dropdown-toggle" href="{{url('login')}}">
                <span class="username">
                    <?= trans('messages.public.already_have_account') ?> <?= trans('messages.sign_in.sign_in') ?>
                </span>
            </a>
        </li>
    </ul>
@endsection

@section('content')
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form id="form-login" name="form-login" class="login-form" method="post" action="{{url('login')}}">
            <input type="hidden" name="_token" value="<?= csrf_token() ?>">
            <h3 class="form-title font-green"><?= trans('messages.sign_in._title') ?></h3>
            <br/>

            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9"><?= trans('messages.sign_in.email') ?></label>
                <input class="form-control form-control-solid placeholder-no-fix"
                       type="text"
                       autocomplete="off"
                       name="email"
                       id="email"
                       value="{{ old('email') }}"
                       placeholder="<?= trans('messages.sign_in.email') ?>"
                />
                <span class="alert-danger"><?php echo $errors->first('email') ?></span>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"><?= trans('messages.sign_in.password') ?></label>

                <input class="form-control form-control-solid placeholder-no-fix"
                       type="password"
                       autocomplete="off"
                       placeholder="<?= trans('messages.sign_in.password') ?>"
                       name="password"
                       id="password"
                />
                <span class="alert-danger"><?php echo $errors->first('password') ?></span>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn green uppercase"><?= trans('messages.sign_in.sign_in') ?></button>
                <label class="rememberme check mt-checkbox mt-checkbox-outline">
                    <input type="checkbox" name="remember"
                           value="1"/><?= trans('messages.sign_in.keep_me_signed') ?>
                    <span></span>
                </label>
                <a href="{{url('forgot/password')}}" id="forget-password"
                   class="forget-password"><?= trans('messages.sign_in.forgot_password') ?>?</a>
            </div>
            <div class="login-options">

            </div>
            <div class="create-account">
                <p>
                    <a href="{{url('signup')}}" id="register-btn"
                       class="uppercase"><?= trans('messages.sign_in.sign_up') ?></a>
                </p>
            </div>
        </form>
        <!-- END LOGIN FORM -->
    </div>
@endsection

@section('page-js-pugins')
    <script src="<?= asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') ?>"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="<?= asset('assets/loan-app/scripts/login.js') ?>"></script>
@endsection