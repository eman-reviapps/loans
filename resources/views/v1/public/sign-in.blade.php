@extends('v1.layout.public')

@section('page-title')
    <?= trans('messages.sign_in._title') ?>
@endsection

@section('page-css')
    <link href="{{asset('assets/loan-app/css/themes/green.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('header-right')
    @include('v1.public.header-right.signin_signup')
@endsection

@section('content')
    <div class="content m-top-sign login_div">
        <form id="form-login" name="form-login" class="login-form" method="post" action="{{url('login')}}">
            <input type="hidden" name="_token" value="<?= csrf_token() ?>">
            <h3 class="form-title"><?= trans('messages.sign_in.sign_in_text') ?></h3>
            <br/>

            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"><?= trans('messages.sign_in.email') ?></label>
                <div class="input-icon">
                    <i class="fa fa-user icon-login-custom"></i>
                    <input class="form-control form-control-solid placeholder-no-fix"
                           type="text"
                           autocomplete="off"
                           name="email"
                           id="email"
                           value="{{ old('email') }}"
                           placeholder="<?= trans('messages.sign_in.email') ?>"
                    />
                </div>
                <span class="alert-danger"><?php echo $errors->first('email') ?></span>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9"><?= trans('messages.sign_in.password') ?></label>
                <div class="input-icon">
                    <i class="fa fa-lock icon-login-custom"></i>
                    <input class="form-control form-control-solid placeholder-no-fix"
                           type="password"
                           autocomplete="off"
                           placeholder="<?= trans('messages.sign_in.password') ?>"
                           name="password"
                           id="password"
                    />
                </div>
                <span class="alert-danger"><?php echo $errors->first('password') ?></span>
            </div>
            <div class="form-actions">
                <label class="rememberme check mt-checkbox mt-checkbox-outline">
                    <input type="checkbox" name="remember"
                           value="1"/><?= trans('messages.sign_in.keep_me_signed') ?>
                    <span></span>
                </label>
                <a href="{{url('forgot/password')}}" id="forget-password"
                   class="forget-password"><?= trans('messages.sign_in.forgot_password') ?>?</a>

                <button type="submit"
                        class="btn btn-default btn-circle uppercase btn-block"><?= trans('messages.sign_in.sign_in') ?></button>

            </div>
        </form>
    </div>
@endsection

@section('page-js-pugins')
    <script src="<?= asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') ?>"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="<?= asset('assets/loan-app/scripts/login.js') ?>"></script>
@endsection