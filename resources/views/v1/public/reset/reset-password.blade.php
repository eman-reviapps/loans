@extends('v1.layout.public')

@section('page-title')
    <?= trans('messages.reset_password._title') ?>
@endsection

@section('page-css')
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?= asset('assets/loan-app/css/public.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/loan-app/css/themes/green.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
@endsection

@section('header-right')
    @include('v1.public.header-right.signin_signup')
@endsection

@section('content')
    <div class="content m-top-sign login_div">
        <form id="form-reset_password" name="form-reset_password" class="reset-form"
              method="post"
              action="{{url('reset/password')}}">
            <input type="hidden" name="_token" value="<?= csrf_token() ?>">
            <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}"/>
            <input type="hidden" name="reset_code" id="reset_code" value="{{$reset_code}}"/>

            <h3 class="form-title"><?= trans('messages.reset_password.new_password') ?></h3>

            <br/>

            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-lock icon-login-custom"></i>
                    <input id="new_password"
                           name="new_password"
                           type="password"
                           class="form-control placeholder-no-fix"
                           placeholder="<?= trans('messages.reset_password.new_password') ?>"/>
                </div>
            </div>
            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-lock icon-login-custom"></i>
                    <input id="new_password_confirmation"
                           name="new_password_confirmation"
                           type="password"
                           class="form-control placeholder-no-fix"
                           placeholder="<?= trans('messages.reset_password.confirm_password') ?>"
                </div>
            </div>

            <div class="form-actions" style="margin-top: 10px">
                <button type="submit" id="register-submit-btn"
                        class="btn btn-default btn-circle btn-block uppercase btn-block"><?= trans('messages.reset_password.update_password') ?>
                </button>
            </div>
        </form>
    </div>
@endsection

@section('page-js-pugins')
    <script src="<?= asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') ?>"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="<?= asset('assets/loan-app/scripts/reset_password.js') ?>"></script>
@endsection
