@extends('v1.layout.public')

@section('page-title')
    <?= trans('messages.forgot_password._title') ?>
@endsection

@section('page-css')
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?= asset('assets/loan-app/css/public.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/loan-app/css/themes/green.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
@endsection

@section('header-right')
    @include('v1.public.header-right.signin_signup')
@endsection

@section('content')
    <div class="content m-top-sign login_div">
        <form id="form-forgot_password" name="form-forgot_password" class="forget-form"
              method="post"
              action="{{url('forgot/password')}}">
            <input type="hidden" name="_token" value="<?= csrf_token() ?>">

            <h3 class="form-title"><?= trans('messages.forgot_password.forgot_message') ?></h3>

            <br/>

            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-envelope icon-login-custom"></i>
                    <input id="email"
                           name="email"
                           type="text"
                           class="form-control"
                           value="{{ old('email') }}"
                           placeholder="<?= trans('messages.forgot_password.email') ?>"
                    />
                </div>
                <span class="alert-danger"><?php echo $errors->first('email') ?></span>
            </div>

            <div class="form-actions">
                <button type="submit" id="register-submit-btn"
                        class="btn btn-default btn-circle btn-block uppercase pull-left"><?= trans('messages.forgot_password.reset') ?>
                </button>
            </div>
        </form>
    </div>
@endsection

@section('page-js-pugins')
    <script src="<?= asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') ?>"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="<?= asset('assets/loan-app/scripts/forgot_password.js') ?>"></script>
@endsection
