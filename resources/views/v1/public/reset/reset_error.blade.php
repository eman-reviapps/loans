@extends('v1.layout.public')

@section('page-title')
    <?= trans('messages.reset_password.reset_error') ?>
@endsection


@section('page-css')
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?= asset('assets/loan-app/css/public.css') ?>" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
@endsection

@section('header-right')
    @include('v1.public.header-right.signin_signup')
@endsection

@section('content')
    <div class="col-md-8 m-top-30  col-md-offset-2">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold"><?= trans('messages.reset_password.reset_error') ?></span>
                </div>
            </div>
            <div class="portlet-body">
                <p><?= trans('messages.reset_password.reset_error_descripion') ?></p>

                <a href="{{url('forgot/password')}}"
                   class="btn btn-success uppercase"><?= trans('messages.reset_password.start_over') ?></a>

            </div>
        </div>
    </div>
@endsection
