@extends('v1.layout.public')

@section('page-title')
    <?= trans('messages.forgot_password.reset_email_sent') ?>
@endsection

@section('page-css')
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?= asset('assets/loan-app/css/public.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/loan-app/css/themes/green.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
@endsection

@section('header-right')
    @include('v1.public.header-right.signin_signup')
@endsection

@section('content')
    <div class="col-md-8 m-top-30  col-md-offset-2">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold"><?= trans('messages.forgot_password.reset_email_sent') ?></span>
                </div>
            </div>
            <div class="portlet-body">
                <p><?= trans('messages.forgot_password.confirm_title') ?></p>
                <p><?= trans('messages.forgot_password.confirm_description') ?></p>

                <a href="{{url('/')}}"
                   class="btn btn-success uppercase"><?= trans('messages.forgot_password.continue_login') ?></a>
            </div>
        </div>
    </div>
@endsection
