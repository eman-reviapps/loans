@extends('v1.layout.public')

@section('page-title')
    <?= $title ?>
@endsection


@section('page-css')
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?= asset('assets/loan-app/css/public.css') ?>" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
@endsection

@section('header-right')
    @include('v1.public.header-right.signin_signup')
@endsection

@section('content')
    <div class="col-md-8 m-top-30  col-md-offset-2">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold"><?= $title ?></span>
                </div>
            </div>
            <div class="portlet-body">
                <p><?= $description ?></p>

                <a href="{{url('/')}}" class="btn btn-success uppercase"><?= trans('errors.main_page') ?></a>
            </div>
        </div>
    </div>
@endsection
