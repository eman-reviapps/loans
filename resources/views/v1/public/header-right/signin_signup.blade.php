<div class="top-right-btns pull-right">
    <div class="btn-group btn-group-circle btn-group-justified ">
        <a href="{{url('signup')}}" class="btn btn-default btn-header btn-signup"> {{trans('messages.sign_in.sign_up')}} </a>
        <a href="{{url('login')}}" class="btn btn-default btn-header btn-signin "> {{trans('messages.sign_in.sign_in')}} </a>
    </div>
</div>