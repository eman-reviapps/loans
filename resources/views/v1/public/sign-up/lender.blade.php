@extends('v1.layout.public')

@section('page-title')
    <?= trans('messages.sign_up_lender._title') ?>
@endsection


@section('page-css')
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="<?= asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') ?>" rel="stylesheet"
          type="text/css"/>
@endsection

@section('header-right')
    @include('v1.public.header-right.signin_signup')
@endsection

@section('content')
    <div class="content-signup">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light" id="signup_lender_form_wizard">
                    <div class="portlet-title">
                        <div class="caption">
                        <span class="caption-subject bold">
                            {{trans('messages.sign_up_lender.register_lender')}}
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="portlet-body form">
                        <form class="form-horizontal" action="{{url('signup/lender')}}" id="signup_lender_form"
                              method="POST"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                            <div class="form-body no-padding">

                                <div class="alert alert-danger display-none">
                                    <button class="close" data-dismiss="alert"></button>
                                    {{trans('messages.users.you_have_errors')}}
                                </div>
                                <div class="alert alert-success display-none">
                                    <button class="close" data-dismiss="alert"></button>
                                    {{trans('messages.users.validation_succeeded')}}
                                </div>

                                <div class="form-group ">
                                    <label class="control-label col-md-4">{{trans('messages.institutions.state')}}
                                    </label>
                                    <div class="col-md-4">
                                        <select class="select2 form-control" id="state"
                                                name="state">
                                            <option value="">{{trans('messages.common.select_dropdown')}}</option>
                                            @foreach($states as $key => $value)
                                                <option {{old('state') && old('state') == $key ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                        <span class="alert-danger"><?php echo $errors->first('state') ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{trans('messages.institutions.city')}}
                                        :
                                    </label>
                                    <div class="col-md-4">
                                        <select class="select2 form-control" id="city"
                                                name="city">
                                            <option value="">{{trans('messages.common.select_dropdown')}}</option>
                                        </select>
                                        <span class="alert-danger"><?php echo $errors->first('city') ?></span>
                                    </div>
                                </div>
                                <div class="existing-institution-div">

                                    <div class="form-group">
                                        <label class="control-label col-md-4">{{trans('messages.users.institution')}}
                                        </label>
                                        <div class="col-md-4">
                                            <input type="hidden" name="institution_exists" value="1"
                                                   id="institution_exists">
                                            <select name="institution" id="institution"
                                                    class="select2 form-control">
                                                <option value=""></option>
                                                @foreach($institutions as $institution)
                                                    <option data-content="{{$institution->domain}}"
                                                            {{ (old('institution') && old('institution') == $institution->id)  ? 'selected' : ''}} value="{{$institution->id}}">{{$institution->title}}</option>
                                                @endforeach
                                            </select>
                                            <span class="alert-danger"><?php echo $errors->first('institution') ?></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group new-institution-div">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">{{trans('messages.institutions.domain')}}
                                            :
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="domain"
                                                   id="domain"
                                                   value="{{old('domain')}}"
                                                   placeholder="{{trans('messages.institutions.domain')}}">
                                            <span class="alert-danger"><?php echo $errors->first('domain') ?></span>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="help-block">( ex: bofa.com )</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{trans('messages.sign_up_lender.business_email')}}
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input type="text" class="form-control"
                                                   value="{{old('user_email')}}"
                                                   placeholder="{{trans('messages.sign_up_lender.business_email')}}"
                                                   name="user_email" id="user_email"/>
                                            <input type="hidden" name="email" id="email">
                                            <span id="institution_domain"
                                                  class="input-group-addon input-circle-right"></span>
                                        </div>
                                        <span class="alert-danger"><?php echo $errors->first('user_email') ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{trans('messages.users.password')}}
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="password"
                                               class="form-control"
                                               placeholder="{{trans('messages.users.password')}}"
                                               name="password"
                                               id="password"
                                        />
                                        <span class="alert-danger"><?php echo $errors->first('password') ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{trans('messages.users.confirm_password')}}
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="password"
                                               class="form-control"
                                               placeholder="{{trans('messages.users.confirm_password')}}"
                                               name="password_confirmation"
                                               id="password_confirmation"
                                        />
                                        <span class="alert-danger"><?php echo $errors->first('confirm_password') ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{trans('messages.users.phone')}}

                                    </label>
                                    <div class="col-md-4">
                                        <input type="text"
                                               class="form-control mask_phone"
                                               placeholder="{{trans('messages.users.phone')}}"
                                               name="phone"
                                               id="phone"
                                               value="{{old('phone')}}"
                                        />

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{trans('messages.lender_profiles.address')}}

                                    </label>
                                    <div class="col-md-4">
                                        <input type="text"
                                               class="form-control"
                                               placeholder="{{trans('messages.users.address')}}"
                                               name="address"
                                               id="address"
                                               value="{{old('address')}}"
                                        />

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{trans('messages.users.first_name')}}
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text"
                                               class="form-control"
                                               placeholder="{{trans('messages.users.first_name')}}"
                                               name="first_name"
                                               id="first_name"
                                               value="{{old('first_name')}}"
                                        />
                                        <span class="alert-danger"><?php echo $errors->first('first_name') ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{trans('messages.users.last_name')}}
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text"
                                               class="form-control"
                                               placeholder="{{trans('messages.users.last_name')}}"
                                               name="last_name"
                                               id="last_name"
                                               value="{{old('last_name')}}"
                                        />
                                        <span class="alert-danger"><?php echo $errors->first('last_name') ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{trans('messages.profiles.title')}}

                                    </label>
                                    <div class="col-md-4">
                                        <input type="text"
                                               class="form-control"
                                               placeholder="{{trans('messages.profiles.title')}}"
                                               name="jobTitle"
                                               id="jobTitle"
                                               value="{{old('jobTitle')}}"
                                        />

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{trans('messages.lender_profiles.line_of_business')}}

                                    </label>
                                    <div class="col-md-4">
                                        <input type="text"
                                               class="form-control"
                                               placeholder="{{trans('messages.lender_profiles.line_of_business')}}"
                                               name="line_of_business"
                                               id="line_of_business"
                                               value="{{old('line_of_business')}}"
                                        />

                                    </div>
                                </div>

                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-4">
                                        <input type="submit" title="{{trans('messages.operations.submit')}}" class="btn green" />

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script>
        function addNewInstitution() {
            $('#institution_exists').val("0");

            $('.existing-institution-div').hide();
            $('.confirm-existing-institution-div').hide();

            $('.new-institution-div').show();
            $('.confirm-new-institution-div').show();

            $('select').select2("close");
        }
    </script>
    <script src="<?= asset('assets/loan-app/scripts/general/address.js') ?>"></script>
    <script src="<?= asset('assets/loan-app/scripts/signup_lender.js') ?>"></script>
@endsection


