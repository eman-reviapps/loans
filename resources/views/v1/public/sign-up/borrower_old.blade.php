@extends('v1.layout.public')

@section('page-title')
    <?= trans('messages.sign_up_borrower._title') ?>
@endsection

@section('page-css')
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?= asset('assets/loan-app/css/public.css') ?>" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
@endsection

@section('header-right')
    <ul class="nav navbar-nav pull-right">
        <li class="dropdown dropdown-user dropdown-dark">
            <a class="dropdown-toggle" href="{{url('login')}}">
                <span class="username">
                    <?= trans('messages.public.already_have_account') ?> <?= trans('messages.sign_in.sign_in') ?>
                </span>
            </a>
        </li>
    </ul>
@endsection

@section('content')
    <div class="content">
        <form id="form-signup_borrower" name="form-signup_borrower" class="register-form-borrower" method="post"
              action="{{url('signup/borrower')}}">

            <input type="hidden" name="_token" value="<?= csrf_token() ?>">

            <h3 class="font-green"><?= trans('messages.sign_up_borrower.create_free_account') ?></h3>
            <br/>
            @include('v1.layout.__partials.partials.public.errors')
            <div class="form-group">
                <input id="first_name"
                       name="first_name"
                       type="text"
                       class="form-control placeholder-no-fix"
                       value="{{ old('first_name') }}"
                       placeholder="<?= trans('messages.sign_up_borrower.first_name') ?>"
                />
                <span class="alert-danger"><?php echo $errors->first('first_name') ?></span>
            </div>

            <div class="form-group">
                <input id="last_name"
                       name="last_name"
                       type="text"
                       class="form-control placeholder-no-fix"
                       value="{{ old('last_name') }}"
                       placeholder="<?= trans('messages.sign_up_borrower.last_name') ?>"
                />
                <span class="alert-danger"><?php echo $errors->first('last_name') ?></span>
            </div>

            <div class="form-group">
                <input id="email"
                       name="email"
                       type="text"
                       class="form-control placeholder-no-fix"
                       value="{{ old('email') }}"
                       placeholder="<?= trans('messages.sign_up_borrower.email') ?>"
                />
                <span class="alert-danger"><?php echo $errors->first('email') ?></span>
            </div>

            <div class="form-group">
                <input id="password"
                       name="password"
                       type="password"
                       class="form-control placeholder-no-fix"
                       placeholder="<?= trans('messages.sign_up_borrower.password') ?>"
                />
                <span class="alert-danger"><?php echo $errors->first('password') ?></span>
            </div>

            <div class="form-group">
                <input id="password_confirmation"
                       name="password_confirmation"
                       type="password"
                       class="form-control placeholder-no-fix"
                       placeholder="<?= trans('messages.sign_up_borrower.repeat_password') ?>"
                />
                <span class="alert-danger"><?php echo $errors->first('password_confirmation') ?></span>
            </div>

            <div class="form-actions">
                <button type="submit" id="register-submit-btn"
                        class="btn btn-success uppercase pull-left"><?= trans('messages.sign_up_borrower.get_started') ?></button>
            </div>
            <div class="clearfix"></div>
            <p class="sign-note"><?= trans('messages.public.already_have_account') ?> <a
                        href="{{url('login')}}"><?= trans('messages.sign_in.sign_in') ?></a></p>
        </form>
    </div>
@endsection

@section('page-js-pugins')
    <script src="<?= asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') ?>"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="<?= asset('assets/loan-app/scripts/signup_borrower.js') ?>"></script>
@endsection


