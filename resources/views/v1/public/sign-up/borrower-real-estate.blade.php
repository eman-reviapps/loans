@extends('v1.layout.public')

@section('page-title')
    <?= trans('messages.sign_up_borrower.real_estate_account') ?>
@endsection

@section('page-css')
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="<?= asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') ?>" rel="stylesheet"
          type="text/css"/>
@endsection

@section('header-right')
    @include('v1.public.header-right.signin_signup')
@endsection

@section('content')
    <div class="content-signup">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light" id="signup_borrower_form_wizard">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold">
                            {{trans('messages.sign_up_borrower.real_estate_account')}}
                            -
                            <span class="step-title"> {{trans('messages.common.step')}}
                                1 {{trans('messages.common.of')}} 2 </span>
                            </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form class="form-horizontal" action="{{url('signup/borrower')}}" id="signup_borrower_form"
                          method="POST"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                        <input type="hidden" name="profile_type" value="{{$profile_type}}">
                        <div class="form-wizard">
                            <div class="form-body no-padding">
                                <ul class="nav nav-pills nav-justified steps">
                                    <li>
                                        <a href="#tab_account" data-toggle="tab" class="step">
                                            <span class="number"> 1 </span>
                                            <span class="desc">
                                               <i class="fa fa-check"></i> {{trans('messages.sign_up_borrower.account_setup')}}
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab_profile" data-toggle="tab" class="step">
                                            <span class="number"> 2 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> {{trans('messages.sign_up_borrower.profile_setup')}}
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="bar" class="progress progress-striped" role="progressbar">
                                    <div class="progress-bar progress-bar-success"></div>
                                </div>
                                <div class="tab-content">
                                    <div class="alert alert-danger display-none">
                                        <button class="close" data-dismiss="alert"></button>
                                        {{trans('messages.users.you_have_errors')}}
                                    </div>
                                    <div class="alert alert-success display-none">
                                        <button class="close" data-dismiss="alert"></button>
                                        {{trans('messages.users.validation_succeeded')}}
                                    </div>
                                    <div class="tab-pane active" id="tab_account">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">{{trans('messages.users.email')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control"
                                                       value="{{old('email')}}"
                                                       placeholder="{{trans('messages.users.email')}}"
                                                       name="email" id="email"/>
                                                <span class="alert-danger"><?php echo $errors->first('email') ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4">{{trans('messages.users.password')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="password"
                                                       class="form-control"
                                                       placeholder="{{trans('messages.users.password')}}"
                                                       name="password"
                                                       id="password"
                                                />
                                                <span class="alert-danger"><?php echo $errors->first('password') ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4">{{trans('messages.users.confirm_password')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="password"
                                                       class="form-control"
                                                       placeholder="{{trans('messages.users.confirm_password')}}"
                                                       name="password_confirmation"
                                                       id="password_confirmation"
                                                />
                                                <span class="alert-danger"><?php echo $errors->first('confirm_password') ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4">{{trans('messages.users.first_name')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text"
                                                       class="form-control"
                                                       placeholder="{{trans('messages.users.first_name')}}"
                                                       name="first_name"
                                                       id="first_name"
                                                       value="{{old('first_name')}}"
                                                />
                                                <span class="alert-danger"><?php echo $errors->first('first_name') ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4">{{trans('messages.users.last_name')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text"
                                                       class="form-control"
                                                       placeholder="{{trans('messages.users.last_name')}}"
                                                       name="last_name"
                                                       id="last_name"
                                                       value="{{old('last_name')}}"
                                                />
                                                <span class="alert-danger"><?php echo $errors->first('last_name') ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4">{{trans('messages.users.phone')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text"
                                                       class="form-control mask_phone"
                                                       placeholder="{{trans('messages.users.phone')}}"
                                                       name="phone"
                                                       id="phone"
                                                       value="{{old('phone')}}"
                                                />
                                                <span class="alert-danger"><?php echo $errors->first('phone') ?></span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane" id="tab_profile">


                                        <div class="form-group">
                                            <label class="col-md-4 control-label">{{trans('messages.real_estate_profiles.company_name')}}

                                            </label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="company_name" id="company_name"
                                                       value="{{old('company_name') ? old('company_name') : (isset($record) ? $record->company_name : '')}}"
                                                       placeholder="{{trans('messages.real_estate_profiles.company_name')}}">
                                                <span class="alert-danger"><?php echo $errors->first('company_name') ?></span>
                                            </div>
                                        </div>

                                        @include('v1.cp.components.general.address',['record'=>null])

                                        <div class="form-group">
                                            <label class="control-label col-md-4">{{trans('messages.real_estate_profiles.properties_owned_no')}}
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-5">
                                                <input type="text"
                                                       class="form-control mask_number"
                                                       placeholder="{{trans('messages.real_estate_profiles.properties_owned_no')}}"
                                                       name="properties_owned_no"
                                                       id="properties_owned_no"
                                                       value="{{old('properties_owned_no')}}"
                                                />
                                                <span class="alert-danger"><?php echo $errors->first('properties_owned_no') ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-4">
                                        <a href="javascript:;" class="btn default button-previous">
                                            <i class="fa fa-angle-left"></i> {{trans('messages.operations.back')}} </a>
                                        <a href="javascript:;"
                                           class="btn btn-outline green button-next"> {{trans('messages.operations.continue')}}
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                        <a href="javascript:;"
                                           class="btn green button-submit"> {{trans('messages.operations.submit')}}
                                            <i class="fa fa-check"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/jquery-repeater/jquery.repeater.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/general/address.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/signup_borrower_real_estate.js')}}"></script>
@endsection


