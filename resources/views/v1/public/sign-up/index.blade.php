@extends('v1.layout.public')

@section('page-title')
    <?= trans('messages.sign_up._title') ?>
@endsection

@section('page-css')
    <link href="<?= asset('assets/loan-app/css/signup.css') ?>" rel="stylesheet" type="text/css"/>
@endsection

@section('header-right')
    @include('v1.public.header-right.signin_signup')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-md-12 m-lg-bottom m-top-30">
            <hgroup class="text-center">
                <h1 class="m-xs-top-bottom font-green-custom"><?= trans('messages.sign_up.lets_get_started')?></h1>
            </hgroup>
        </div>

        <div class="col-md-12 text-center m-top-30">
            <div class="col-md-5">
                <div>
                    <img class="img-icon-signup" src="{{asset('assets/loan-app/img/lender.png')}}">
                </div>
                <div class="text-muted">
                    <div class="o-user-type-selection font-blue-custom"><?= trans('messages.sign_up.aspire_help_borrowers')?></div>
                </div>
                <a class="btn btn-default btn-circle uppercase"
                   href="{{url('signup/lender')}}"><?= trans('messages.sign_up.sign_up')?></a>
            </div>

            <div class="col-md-2 o-or-divider"><?= trans('messages.sign_up.or')?></div>

            <div class="col-md-5">
                <div>
                    <img class="img-icon-signup" src="{{asset('assets/loan-app/img/borrower.png')}}">
                </div>
                <div class="text-muted">
                    <div class="o-user-type-selection font-green-custom"><?= trans('messages.sign_up.look_for_loan')?></div>
                </div>
                <a class="btn btn-default btn-circle uppercase"
                   href="{{url('signup/borrower')}}"><?= trans('messages.sign_up.sign_up')?></a>
            </div>


        </div>
    </div>
@endsection