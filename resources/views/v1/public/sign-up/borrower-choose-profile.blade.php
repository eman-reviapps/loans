@extends('v1.layout.public')

@section('page-title')
    <?= trans('messages.sign_up_borrower._title') ?>
@endsection

@section('page-css')
    <link href="<?= asset('assets/pages/css/about.min.css') ?>" rel="stylesheet" type="text/css"/>
@endsection

@section('header-right')
    @include('v1.public.header-right.signin_signup')
@endsection

@section('content')
    <div class="">
        <div class="content custom-content">
            <div class="col-md-12 m-bottom-60">
                <div class="text-center">
                    <h1 class="m-xs-top-bottom tell_us"><?= trans('messages.sign_up_borrower.select_profile_type')?></h1>
                </div>
            </div>

            <div class="row margin-bottom-20">
                <div class="col-lg-6 col-md-6">
                    <div class="portlet">
                        <div class="card-icon card-icon-signup">
                            <img class="img-icon-signup" src="{{asset('assets/loan-app/img/bussniss-Loan.png')}}">
                        </div>
                        <div class="card-title">
                            <span class="font-blue-custom"> {{trans('messages.sign_up_borrower.business')}} </span>
                        </div>
                        <div class="card-desc">
                            <a class="btn btn-default btn-circle uppercase"
                               href="{{url('signup/borrower/business')}}"><?= trans('messages.sign_up_borrower.get_started')?></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="portlet">
                        <div class="card-icon card-icon-signup">
                            <img class="img-icon-signup" src="{{asset('assets/loan-app/img/realstate-Loan.png')}}">
                        </div>
                        <div class="card-title">
                            <span class="font-green-custom"> {{trans('messages.sign_up_borrower.real_Estate')}} </span>
                        </div>
                        <div class="card-desc">
                            <a class="btn btn-default btn-circle uppercase"
                               href="{{url('signup/borrower/real-estate')}}"><?= trans('messages.sign_up_borrower.get_started')?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="<?= asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') ?>"
            type="text/javascript"></script>
    <script src="<?= asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') ?>"
            type="text/javascript"></script>
@endsection

@section('page-scripts')
    <script src="<?= asset('assets/loan-app/scripts/signup_borrower.js') ?>"></script>
@endsection


