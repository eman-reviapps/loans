@extends('v1.layout.public')

@section('page-title')
    <?= trans('messages.sign_up_borrower.business_account') ?>
@endsection

@section('page-css')
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="<?= asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') ?>" rel="stylesheet"
          type="text/css"/>
@endsection

@section('header-right')
    @include('v1.public.header-right.signin_signup')
@endsection

@section('content')
    <div class="content-signup">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light" id="signup_borrower_form_wizard">
                    <div class="portlet-title">
                        <div class="caption">
                        <span class="caption-subject bold">
                            {{trans('messages.sign_up_borrower.business_account')}}
                            -
                            <span class="step-title"> {{trans('messages.common.step')}}
                                1 {{trans('messages.common.of')}} 2 </span>
                            </span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form class="form-horizontal" action="{{url('signup/borrower')}}" id="signup_borrower_form"
                              method="POST"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="<?= csrf_token() ?>">
                            <input type="hidden" name="profile_type" value="{{$profile_type}}">
                            <div class="form-wizard">
                                <div class="form-body no-padding">
                                    <ul class="nav nav-pills nav-justified steps">
                                        <li>
                                            <a href="#tab_account" data-toggle="tab" class="step">
                                                <span class="number"> 1 </span>
                                                <span class="desc">
                                               <i class="fa fa-check"></i> {{trans('messages.sign_up_borrower.account_setup')}}
                                            </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab_profile" data-toggle="tab" class="step">
                                                <span class="number"> 2 </span>
                                                <span class="desc">
                                                <i class="fa fa-check"></i> {{trans('messages.sign_up_borrower.business_setup')}}
                                            </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div id="bar" class="progress progress-striped" role="progressbar">
                                        <div class="progress-bar progress-bar-success"></div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="alert alert-danger display-none">
                                            <button class="close" data-dismiss="alert"></button>
                                            {{trans('messages.users.you_have_errors')}}
                                        </div>
                                        <div class="alert alert-success display-none">
                                            <button class="close" data-dismiss="alert"></button>
                                            {{trans('messages.users.validation_succeeded')}}
                                        </div>
                                        <div class="tab-pane active" id="tab_account">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.users.email')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control"
                                                           value="{{old('email')}}"
                                                           placeholder="{{trans('messages.users.email')}}"
                                                           name="email" id="email"/>
                                                    <span class="alert-danger"><?php echo $errors->first('email') ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.users.password')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="password"
                                                           class="form-control"
                                                           placeholder="{{trans('messages.users.password')}}"
                                                           name="password"
                                                           id="password"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('password') ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.users.confirm_password')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="password"
                                                           class="form-control"
                                                           placeholder="{{trans('messages.users.confirm_password')}}"
                                                           name="password_confirmation"
                                                           id="password_confirmation"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('confirm_password') ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.users.first_name')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text"
                                                           class="form-control"
                                                           placeholder="{{trans('messages.users.first_name')}}"
                                                           name="first_name"
                                                           id="first_name"
                                                           value="{{old('first_name')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('first_name') ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.users.last_name')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text"
                                                           class="form-control"
                                                           placeholder="{{trans('messages.users.last_name')}}"
                                                           name="last_name"
                                                           id="last_name"
                                                           value="{{old('last_name')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('last_name') ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.business_profiles.profile_creator_role')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text"
                                                           class="form-control"
                                                           placeholder="{{trans('messages.business_profiles.profile_creator_role')}}"
                                                           name="profile_creator_role"
                                                           id="profile_creator_role"
                                                           value="{{old('profile_creator_role')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('profile_creator_role') ?></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.users.phone')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text"
                                                           class="form-control mask_phone"
                                                           placeholder="{{trans('messages.common.phone_us_format')}}"
                                                           name="phone"
                                                           id="phone"
                                                           value="{{old('phone')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('phone') ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_profile">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.business_profiles.business_name')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text"
                                                           class="form-control"
                                                           placeholder="{{trans('messages.business_profiles.business_name')}}"
                                                           name="business_name"
                                                           id="business_name"
                                                           value="{{old('business_name')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('business_name') ?></span>
                                                </div>
                                            </div>

                                            @include('v1.cp.components.general.address',['record'=>null])

                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.business_profiles.business_description')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                               <textarea class="form-control"
                                                         placeholder="{{trans('messages.business_profiles.business_description')}}"
                                                         name="description"
                                                         id="description"
                                               >{{old('description')}}</textarea>
                                                    <span class="alert-danger"><?php echo $errors->first('description') ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.business_profiles.establish_year')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text"
                                                           class="form-control establish_year"
                                                           placeholder="{{trans('messages.business_profiles.establish_year')}}"
                                                           name="establish_year"
                                                           id="establish_year"
                                                           value="{{old('establish_year')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('establish_year') ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.business_profiles.fisical_year_end')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text"
                                                           class="form-control fisical_year_end"
                                                           placeholder="{{trans('messages.business_profiles.fisical_year_end')}}"
                                                           name="fisical_year_end"
                                                           id="fisical_year_end"
                                                           value="{{old('fisical_year_end')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('fisical_year_end') ?></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.business_profiles.last_3_years_annual_revenues')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control mask_money"
                                                           placeholder="{{trans('messages.business_profiles.first')}}"
                                                           name="annual_revenue_year_1"
                                                           id="annual_revenue_year_1"
                                                           value="{{old('annual_revenue_year_1')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('annual_revenue_year_1') ?></span>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control mask_money"
                                                           placeholder="{{trans('messages.business_profiles.second')}}"
                                                           name="annual_revenue_year_2"
                                                           id="annual_revenue_year_2"
                                                           value="{{old('annual_revenue_year_2')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('annual_revenue_year_2') ?></span>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control mask_money"
                                                           placeholder="{{trans('messages.business_profiles.third')}}"
                                                           name="annual_revenue_year_3"
                                                           id="annual_revenue_year_3"
                                                           value="{{old('annual_revenue_year_3')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('annual_revenue_year_3') ?></span>
                                                </div>
                                            </div>
<hr/>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.business_profiles.last_3_years_net_income_before_tax')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control calc_EBITDA_year1 mask_money"
                                                           placeholder="{{trans('messages.business_profiles.first')}}"
                                                           name="net_income_before_tax_year_1"
                                                           id="net_income_before_tax_year_1"
                                                           value="{{old('net_income_before_tax_year_1')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('net_income_before_tax_year_1') ?></span>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control calc_EBITDA_year2 mask_money"
                                                           placeholder="{{trans('messages.business_profiles.second')}}"
                                                           name="net_income_before_tax_year_2"
                                                           id="net_income_before_tax_year_2"
                                                           value="{{old('net_income_before_tax_year_2')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('net_income_before_tax_year_2') ?></span>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control calc_EBITDA_year3 mask_money"
                                                           placeholder="{{trans('messages.business_profiles.third')}}"
                                                           name="net_income_before_tax_year_3"
                                                           id="net_income_before_tax_year_3"
                                                           value="{{old('net_income_before_tax_year_3')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('net_income_before_tax_year_3') ?></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.business_profiles.last_3_years_income_tax_expense')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control calc_EBITDA_year1 mask_money"
                                                           placeholder="{{trans('messages.business_profiles.first')}}"
                                                           name="income_tax_expense_year_1"
                                                           id="income_tax_expense_year_1"
                                                           value="{{old('income_tax_expense_year_1')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('income_tax_expense_year_1') ?></span>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control calc_EBITDA_year2 mask_money"
                                                           placeholder="{{trans('messages.business_profiles.second')}}"
                                                           name="income_tax_expense_year_2"
                                                           id="income_tax_expense_year_2"
                                                           value="{{old('income_tax_expense_year_2')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('income_tax_expense_year_2') ?></span>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control calc_EBITDA_year3 mask_money"
                                                           placeholder="{{trans('messages.business_profiles.third')}}"
                                                           name="income_tax_expense_year_3"
                                                           id="income_tax_expense_year_3"
                                                           value="{{old('income_tax_expense_year_3')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('income_tax_expense_year_3') ?></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.business_profiles.last_3_years_interest_expense')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control calc_EBITDA_year1 mask_money"
                                                           placeholder="{{trans('messages.business_profiles.first')}}"
                                                           name="interest_expense_year_1"
                                                           id="interest_expense_year_1"
                                                           value="{{old('interest_expense_year_1')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('interest_expense_year_1') ?></span>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control calc_EBITDA_year2 mask_money"
                                                           placeholder="{{trans('messages.business_profiles.second')}}"
                                                           name="interest_expense_year_2"
                                                           id="interest_expense_year_2"
                                                           value="{{old('interest_expense_year_2')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('interest_expense_year_2') ?></span>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control calc_EBITDA_year3 mask_money"
                                                           placeholder="{{trans('messages.business_profiles.third')}}"
                                                           name="interest_expense_year_3"
                                                           id="interest_expense_year_3"
                                                           value="{{old('interest_expense_year_3')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('interest_expense_year_3') ?></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.business_profiles.last_3_years_depreciation_amortization')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control calc_EBITDA_year1 mask_money"
                                                           placeholder="{{trans('messages.business_profiles.first')}}"
                                                           name="depreciation_amortization_year_1"
                                                           id="depreciation_amortization_year_1"
                                                           value="{{old('depreciation_amortization_year_1')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('depreciation_amortization_year_1') ?></span>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control calc_EBITDA_year2 mask_money"
                                                           placeholder="{{trans('messages.business_profiles.second')}}"
                                                           name="depreciation_amortization_year_2"
                                                           id="depreciation_amortization_year_2"
                                                           value="{{old('depreciation_amortization_year_2')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('depreciation_amortization_year_2') ?></span>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text"
                                                           class="form-control calc_EBITDA_year3 mask_money"
                                                           placeholder="{{trans('messages.business_profiles.third')}}"
                                                           name="depreciation_amortization_year_3"
                                                           id="depreciation_amortization_year_3"
                                                           value="{{old('depreciation_amortization_year_3')}}"
                                                    />
                                                    <span class="alert-danger"><?php echo $errors->first('depreciation_amortization_year_3') ?></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-4">{{trans('messages.business_profiles.company_EBITDA')}}
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-2">
                                                    <input type="text" readonly
                                                           class="form-control mask_money"
                                                           placeholder="<?php echo e(trans('messages.business_profiles.company_EBITDA_year_1')); ?>"
                                                           name="company_EBITDA_year_1"
                                                           id="company_EBITDA_year_1"
                                                           value="{{old('company_EBITDA_year_1')}}"
                                                    />
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" readonly
                                                           class="form-control mask_money"
                                                           placeholder="<?php echo e(trans('messages.business_profiles.company_EBITDA_year_2')); ?>"
                                                           name="company_EBITDA_year_2"
                                                           id="company_EBITDA_year_2"
                                                           value="{{old('company_EBITDA_year_2')}}"
                                                    />
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" readonly
                                                           class="form-control mask_money"
                                                           placeholder="<?php echo e(trans('messages.business_profiles.company_EBITDA_year_3')); ?>"
                                                           name="company_EBITDA_year_3"
                                                           id="company_EBITDA_year_3"
                                                           value="{{old('company_EBITDA_year_3')}}"
                                                    />
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="hidden" readonly
                                                           class="form-control mask_money"
                                                           placeholder="<?php echo e(trans('messages.business_profiles.company_EBITDA')); ?>"
                                                           name="company_EBITDA"
                                                           id="company_EBITDA"
                                                           value="{{old('company_EBITDA')}}"
                                                    />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-4 col-md-4">
                                            <a href="javascript:;" class="btn default button-previous">
                                                <i class="fa fa-angle-left"></i> {{trans('messages.operations.back')}}
                                            </a>
                                            <a href="javascript:;"
                                               class="btn btn-outline green button-next"> {{trans('messages.operations.continue')}}
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                            <a href="javascript:;"
                                               class="btn green button-submit"> {{trans('messages.operations.submit')}}
                                                <i class="fa fa-check"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js-pugins')
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/loan-app/scripts/custom_validation_methods.js')}}"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>

    <script src="{{asset('assets/global/plugins/masks/jquery.maskMoney.min.js')}}"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>

@endsection

@section('page-scripts')
    <script src="{{asset('assets/loan-app/scripts/general/address.js')}}"></script>
    <script src="{{asset('assets/loan-app/scripts/signup_borrower_business.js')}}"></script>
@endsection


