<!DOCTYPE html>
<html>
<head lang="en">
    @include('v1.layout.partials.head')
    @yield('css');
</head>
<body class="horizontal-navigation">

<!--.site-header-->
@include('v1.layout.includes.headers.borrower')
<!--.site-header-->

<!--.side-menu-->
@include('v1.layout.includes.navigation.borrower')
<!--.side-menu-->

<!--.page-content-->
<div class="page-content">
    <div class="container-fluid">
        @yield('content')
    </div>
</div>
<!--.page-content-->

@include('v1.layout.partials.scripts')

@yield('scripts');

</body>
</html>