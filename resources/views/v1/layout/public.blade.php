<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- BEGIN HEAD -->
@include('v1.layout.__partials.head.public')
</head>
<!-- END HEAD -->

<body class=" login">

<!--.site-header-->
@include('v1.layout.__partials.headers.public')
<!--.site-header-->

<!--.page-content-->
<div class="page-center">
    <div class="page-center-in">
        @include('v1.layout.__partials.notification.public')
        @yield('content')
    </div>
</div>
<!--.page-content-->

<script>
    var messages_validations_array = <?php print_r(json_encode(trans('validation.custom'))) ?>;
    var app_url = '<?=url("/")?>';
</script>

@include('v1.layout.__partials.scripts.public')

<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>

</body>
</html>