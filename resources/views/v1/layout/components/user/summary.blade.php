<div class="portlet sale-summary">
    <div class="portlet-title">
        <div class="caption font-red sbold"> {{trans('messages.users.summary')}}</div>
        <div class="tools">
            <a class="reload" href="javascript:;"> </a>
        </div>
    </div>
    <div class="portlet-body">
        <ul class="list-unstyled">
            <li>
                <span class="sale-info"> TODAY SOLD
                    <i class="fa fa-img-up"></i>
                </span>
                <span class="sale-num"> 23 </span>
            </li>
            <li>
                <span class="sale-info"> WEEKLY SALES
                    <i class="fa fa-img-down"></i>
                </span>
                <span class="sale-num"> 87 </span>
            </li>
            <li>
                <span class="sale-info"> TOTAL SOLD </span>
                <span class="sale-num"> 2377 </span>
            </li>
        </ul>
    </div>
</div>