<form role="form" id="changePassword" action="{{url('users/'.$user->id.'/change_password')}}" method="post">
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="_token" value="<?= csrf_token() ?>">
    <input type="hidden" name="save_continue_tab" id="save_continue_tab" value="">

    <div class="form-group">
        <label class="control-label">{{trans('messages.users.current_password')}}</label>
        <input id="current_password"
               name="current_password"
               type="password"
               class="form-control"
               placeholder="<?= trans('messages.users.current_password') ?>"
        />
    </div>
    <div class="form-group">
        <label class="control-label">{{trans('messages.users.new_password')}}</label>
        <input id="new_password"
               name="new_password"
               type="password"
               class="form-control"
               placeholder="<?= trans('messages.users.new_password') ?>"
        />
    </div>
    <div class="form-group">
        <label class="control-label">{{trans('messages.users.confirm_new_password')}}</label>
        <input id="new_password_confirmation"
               name="new_password_confirmation"
               type="password"
               class="form-control placeholder-no-fix"
               placeholder="<?= trans('messages.users.confirm_new_password') ?>"
        />
    </div>
    <div class="margin-top-10">

        <button type="button" class="btn btn-success btn_action_save_continue_tab">
            {{trans('messages.users.change_password')}}
        </button>
        <button type="button" class="btn btn-secondary-outline btn_action_back">
            {{trans('messages.operations.cancel')}}
        </button>

    </div>
</form>