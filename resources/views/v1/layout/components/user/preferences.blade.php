<form action="#">
    <table class="table table-bordered table-striped">
        <tr>
            <td> Anim pariatur cliche reprehenderit, enim eiusmod high life
                accusamus..
            </td>
            <td>
                <div class="mt-radio-inline">
                    <label class="mt-radio">
                        <input type="radio" name="optionsRadios1" value="option1"/>
                        Yes
                        <span></span>
                    </label>
                    <label class="mt-radio">
                        <input type="radio" name="optionsRadios1" value="option2"
                               checked/> No
                        <span></span>
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td> Enim eiusmod high life accusamus terry richardson ad squid wolf
                moon
            </td>
            <td>
                <div class="mt-radio-inline">
                    <label class="mt-radio">
                        <input type="radio" name="optionsRadios21" value="option1"/>
                        Yes
                        <span></span>
                    </label>
                    <label class="mt-radio">
                        <input type="radio" name="optionsRadios21" value="option2"
                               checked/> No
                        <span></span>
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td> Enim eiusmod high life accusamus terry richardson ad squid wolf
                moon
            </td>
            <td>
                <div class="mt-radio-inline">
                    <label class="mt-radio">
                        <input type="radio" name="optionsRadios31" value="option1"/>
                        Yes
                        <span></span>
                    </label>
                    <label class="mt-radio">
                        <input type="radio" name="optionsRadios31" value="option2"
                               checked/> No
                        <span></span>
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td> Enim eiusmod high life accusamus terry richardson ad squid wolf
                moon
            </td>
            <td>
                <div class="mt-radio-inline">
                    <label class="mt-radio">
                        <input type="radio" name="optionsRadios41" value="option1"/>
                        Yes
                        <span></span>
                    </label>
                    <label class="mt-radio">
                        <input type="radio" name="optionsRadios41" value="option2"
                               checked/> No
                        <span></span>
                    </label>
                </div>
            </td>
        </tr>
    </table>
    <!--end profile-settings-->
    <div class="margin-top-10">
        <a href="javascript:;" class="btn green"> Save Changes </a>
        <a href="javascript:;" class="btn default"> Cancel </a>
    </div>
</form>