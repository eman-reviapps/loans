<header class="site-header">
    <nav role="navigation" class="primary-navbar navbar navbar-default">
        <div class="container">
            <div class="nav navbar-header navbar-left">
                <a href="{{url('/')}}" class="site-logo">
                    {{--<img class="hidden-md-down" src="{{asset('assets/pages/img/logo-invert.png')}}" alt="">--}}
                </a>
            </div>
            @yield('header-right')
        </div>
    </nav>
</header>

<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="{{url('/')}}">
                {{--<img src="{{asset('assets/layouts/layout4/img/logo-light.png')}}" alt="logo" class="logo-default">--}}
            </a>
        </div>
        <!-- END LOGO --
        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                @yield('header-right')
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>