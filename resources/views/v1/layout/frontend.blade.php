<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    @include('v1.layout.__partials.head.frontend')
</head>

<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">

@include('v1.layout.__partials.headers.frontend')

<div class="clearfix"></div>


<div class="page-container">

    @include('v1.layout.__partials.navigation.frontend')

    <div class="page-content-wrapper">
        <div class="page-content">

            @yield('page-head')

            @yield('page-breadcrumb')

            @yield('content')

        </div>
    </div>

    @include('v1.layout.__partials.quick-sidebar.frontend')

</div>

@include('v1.layout.__partials.footers.frontend')

<script>
    var messages_validations_array = <?php print_r(json_encode(trans('validation.custom'))) ?>;
    var app_url = '<?=url("/")?>';
</script>

@include('v1.layout.__partials.scripts.frontend')

</body>
</html>