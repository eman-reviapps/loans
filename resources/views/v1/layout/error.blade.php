<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- BEGIN HEAD -->
@include('v1.layout.__partials.head.error')
@yield('css')
</head>
<!-- END HEAD -->

<body class="page-500-full-page">

<!--.page-content-->
@yield('content')
<!--.page-content-->

@include('v1.layout.__partials.scripts.error')

@yield('scripts')

</body>
</html>