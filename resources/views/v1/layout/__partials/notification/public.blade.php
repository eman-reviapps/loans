@if (session()->has('flash_notification.message') && !empty(session('flash_notification.message')))
    <div class="sign-box m-t-md alert alert-{{ session('flash_notification.level') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {!! session('flash_notification.message') !!}
    </div>
@endif