<nav role="navigation" class="primary-navbar navbar navbar-default">
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{url('/')}}">
                    <img src="{{asset('assets/loan-app/img/logo.png')}}" alt="logo" class="logo-default">
                </a>
            </div>

            <div class="page-top">
                <div class="top-menu">
                    @yield('header-right')
                </div>
            </div>
        </div>
    </div>
</nav>

