<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <div class="page-logo">
            <a href="{{url('')}}">
                <img src="{{asset('assets/loan-app/img/logo.png')}}" alt="logo" class="logo-default">
            </a>
        </div>

        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse"> </a>


        <div class="page-top">

            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="separator hide"></li>

                    <li class="dropdown dropdown-extended dropdown-notification"
                        id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <i class="icon-bell"></i>
                            <span class="badge badge-success"> {{$unread_notifications_count}} </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3><span class="bold"></span> {{trans('messages.header.notifications')}}</h3>
                                {{--<a href="{{url('notifications')}}"></a>--}}
                                <a
                                        class="mark_all_notifications_read"
                                        data-csrf="{{csrf_token()}}"
                                        data-action="{{url('notifications/mark_all_read')}}"
                                >{{trans('messages.header.mark_all_read')}}</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;"
                                    data-handle-color="#637283">
                                    @include('v1.cp.components.notifications')
                                </ul>
                            </li>
                        </ul>
                    </li>
                    @if(!Sentinel::inRole(RoleService::ROLE_ADMIN))
                        <li class="separator hide"></li>

                        <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                               data-close-others="true">
                                <i class="icon-envelope-open"></i>
                                <span class="badge badge-danger"> {{$unread_messages_count}} </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3>{{trans('messages.header.messages')}}</h3>
                                    {{--<a href="{{url('messages')}}"></a>--}}
                                    <a class="mark_all_messages_read"
                                       data-csrf="{{csrf_token()}}"
                                       data-action="{{url('conversations/mark_all_read')}}">{{trans('messages.header.mark_all_read')}}</a>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list scroller" style="height: 275px;"
                                        data-handle-color="#637283">
                                        @include('v1.cp.components.messages')
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    @endif
                    <li class="separator hide"></li>
                    @if(Sentinel::inRole(RoleService::ROLE_BORROWER))
                        <li class="dropdown dropdown-extended dropdown-inbox dropdown-user dropdown-borrower">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                               data-close-others="true">
                                <span class="username username-hide-on-mobile"> {{$logged_in_user->first_name}} </span>
                                <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                <img alt="" class="img-circle"
                                     src="{{isset($logged_in_user->photo) && !empty($logged_in_user->photo) ? asset($logged_in_user->photo) : asset('assets/loan-app/img/avatars/no_image.png')}}"/>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li class="external">
                                    <h3>
                                        {{trans('messages.header_frontend.you_have')}}
                                        <span class="bold">{{count($logged_in_user->borrowerProfiles)}} </span> {{trans('messages.header_frontend.profiles')}}
                                    </h3>
                                    <a href="{{url('borrower/profiles')}}">{{trans('messages.header.view_all')}}</a>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list"
                                        data-handle-color="#637283">
                                        @foreach ($logged_in_user->borrowerProfiles()->enabled()->get() as $borrowerProfile)
                                            <li>
                                                <a href="{{url('borrower/profiles/'.$borrowerProfile->id)}}">
                                                <span class="subject">
                                                    <span class="from">
                                                        @if($borrowerProfile->id == session('borrower_profile_id'))
                                                            <i class="fa fa-bookmark"></i>
                                                        @endif
                                                        {{$borrowerProfile->profile_title}} </span>
                                                    <span class="time"> {{$borrowerProfile->profile_type}} </span>
                                                </span>
                                                </a>
                                            </li>
                                        @endforeach
                                        <li>
                                            <a href="{{url('borrower/profiles/create')}}">
                                                <i class="icon-plus"></i> {{trans('messages.header_frontend.add_profile')}}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{url('borrower/profiles/'.session('borrower_profile_id').'/edit')}}">
                                                <i class="icon-settings"></i> {{trans('messages.header_frontend.profile_settings')}}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('profile')}}">
                                                <i class="icon-bell"></i> {{trans('messages.header_frontend.general_account_overview')}}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('account')}}">
                                                <i class="icon-settings"></i> {{trans('messages.header_frontend.general_account_settings')}}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{url('logout')}}">
                                                <i class="icon-logout"></i> {{trans('messages.header.logout')}}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                               data-close-others="true">
                                <span class="username username-hide-on-mobile"> {{$logged_in_user->first_name}} </span>
                                <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                <img alt="" class="img-circle"
                                     src="{{isset($logged_in_user->photo) && !empty($logged_in_user->photo) ? asset($logged_in_user->photo) : asset('assets/loan-app/img/avatars/no_image.png')}}"/>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="{{route('profile')}}">
                                        <i class="icon-user"></i> {{trans('messages.header.profile')}}</a>
                                </li>
                                <li>
                                    <a href="{{route('account')}}">
                                        <i class="icon-settings"></i> {{trans('messages.header_frontend.account')}} </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="{{url('logout')}}">
                                        <i class="icon-logout"></i> {{trans('messages.header.logout')}}
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif

                    {{--<li class="dropdown dropdown-user">--}}
                    {{--<a class="dropdown-toggle m-t-10" style="margin-top: 4px" href="{{url('logout')}}">--}}
                    {{--<i class="icon-logout"></i>--}}
                    {{--<span class="username username-hide-on-mobile">{{trans('messages.header.logout')}}</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}
                </ul>
            </div>
        </div>
    </div>
</div>