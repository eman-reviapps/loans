<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">

        <div class="page-logo">
            <a href="{{url('')}}">
                <img src="{{asset('assets/loan-app/img/logo.png')}}" alt="logo" class="logo-default">
            </a>
        </div>

        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse"> </a>

        <div class="page-actions">
            <div class="btn-group">
            </div>
        </div>

        <div class="page-top">
            {{--<form class="search-form" action="page_general_search_2.html" method="GET">--}}
                {{--<div class="input-group">--}}
                    {{--<input type="text" class="form-control input-sm" placeholder="Search..." name="query">--}}
                    {{--<span class="input-group-btn">--}}
                        {{--<a href="javascript:;" class="btn submit">--}}
                            {{--<i class="icon-magnifier"></i>--}}
                        {{--</a>--}}
                    {{--</span>--}}
                {{--</div>--}}
            {{--</form>--}}

            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="separator hide"></li>

                    <li class="dropdown dropdown-extended dropdown-notification"
                        id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <i class="icon-bell"></i>
                            <span class="badge badge-success"> {{$unread_notifications_count}} </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>
                                    <span class="bold"></span> {{trans('messages.header.notifications')}}</h3>
                                    <a
                                            class="mark_all_notifications_read"
                                            data-csrf="{{csrf_token()}}"
                                            data-action="{{url('notifications/mark_all_read')}}"
                                    >{{trans('messages.header.mark_all_read')}}</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;"
                                    data-handle-color="#637283">
                                    @include('v1.cp.components.notifications')
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="separator hide"></li>

                    {{--<li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">--}}
                        {{--<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"--}}
                           {{--data-close-others="true">--}}
                            {{--<i class="icon-envelope-open"></i>--}}
                            {{--<span class="badge badge-danger"> {{$unread_messages_count}} </span>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li class="external">--}}
                                {{--<h3>You have--}}
                                    {{--<span class="bold">{{$unread_messages_count}} {{trans('messages.header.new')}}</span> {{trans('messages.header.messages')}}--}}
                                {{--</h3>--}}
                                {{--<a href="">{{trans('messages.header.view_all')}}</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<ul class="dropdown-menu-list scroller" style="height: 275px;"--}}
                                    {{--data-handle-color="#637283">--}}

                                {{--</ul>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}

                    <li class="separator hide"></li>

                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <span class="username username-hide-on-mobile"> {{$logged_in_user->first_name}} </span>
                            <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                            <img alt="" class="img-circle"
                                 src="{{isset($logged_in_user->photo) && !empty($logged_in_user->photo) ? asset($logged_in_user->photo) : asset('assets/loan-app/img/avatars/no_image.png')}}"/>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="{{url('admin/users/'.$logged_in_user->id)}}">
                                    <i class="icon-user"></i> {{trans('messages.header.profile')}} </a>
                            </li>
                            {{--<li class="divider"></li>--}}
                            @if (Sentinel::hasAccess('settings.*'))
                                <li>
                                    <a href="{{url('admin/settings')}}">
                                        <i class="icon-settings"></i> {{trans('messages.header.settings')}} </a>
                                </li>
                            @endif
                            {{--<li>--}}
                            {{--<a href="{{url('admin/users/'.$logged_in_user->id.'/preferences')}}">--}}
                            {{--<i class="icon-wrench"></i> {{trans('messages.header.preferences')}}--}}
                            {{--</a>--}}
                            {{--</li>--}}
                            <li class="divider"></li>
                            <li>
                                <a href="{{url('logout')}}">
                                    <i class="icon-logout"></i> {{trans('messages.header.logout')}}
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle m-t-10" style="margin-top: 4px" href="{{url('logout')}}">
                            <i class="icon-logout"></i>
                            <span class="username username-hide-on-mobile">{{trans('messages.header.logout')}}</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>