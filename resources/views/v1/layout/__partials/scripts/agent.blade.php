<!--[if lt IE 9]>
<script src="<?= asset('assets/global/plugins/respond.min.js') ?>"></script>
<script src="<?= asset('assets/global/plugins/excanvas.min.js') ?>"></script>
<script src="<?= asset('assets/global/plugins/ie8.fix.min.js') ?>"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?= asset('assets/global/plugins/jquery.min.js') ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/js.cookie.min.js') ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>"
        type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/jquery.blockui.min.js') ?>" type="text/javascript"></script>
<script src="<?= asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>"
        type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?= asset('assets/global/plugins/bootstrap-toastr/toastr.min.js') ?>" type="text/javascript"></script>

@yield('page-js-pugins')
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= asset('assets/global/scripts/app.min.js') ?>" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<script>
    var message = '<?= session('flash_notification.message') ?>';
    var level = '<?= session('flash_notification.level') ?>';
    var active_tab = '<?= session('active_tab') ?>';
</script>

<script src="<?= asset('assets/loan-app/scripts/helpers.js') ?>"></script>
<script src="<?= asset('assets/loan-app/scripts/common.js') ?>"></script>
<script src="<?= asset('assets/loan-app/scripts/admin.js') ?>"></script>

<script src="<?= asset('assets/loan-app/scripts/notifications.js') ?>"></script>
<script src="<?= asset('assets/loan-app/scripts/conversations.js') ?>"></script>



@yield('page-scripts')

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?= asset('assets/layouts/layout4/scripts/layout.min.js') ?>" type="text/javascript"></script>
<script src="<?= asset('assets/layouts/layout4/scripts/demo.min.js') ?>" type="text/javascript"></script>
<script src="<?= asset('assets/layouts/global/scripts/quick-sidebar.min.js') ?>" type="text/javascript"></script>
<script src="<?= asset('assets/layouts/global/scripts/quick-nav.min.js') ?>" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->



