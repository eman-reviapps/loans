<meta charset="utf-8"/>
<title>@yield('page-title') - <?php echo config('app.name') ?></title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="Preview page of Metronic Admin Theme #4 for blank page layout" name="description"/>
<meta content="" name="author"/>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
      type="text/css"/>
<link href="<?= asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet"
      type="text/css"/>
<link href="<?= asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet"
      type="text/css"/>
<link href="<?= asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet"
      type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?= asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') ?>"  rel="stylesheet" type="text/css" />
<link href="<?= asset('assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css') ?>" />
<link href="<?= asset('assets/global/plugins/fullcalendar/fullcalendar.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="<?= asset('assets/global/plugins/jqvmap/jqvmap/jqvmap.css') ?>" />
<link href="<?= asset('assets/global/plugins/bootstrap-toastr/toastr.min.css') ?>" rel="stylesheet" type="text/css"/>
@yield('page-css-pugins')
<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= asset('assets/global/css/components.min.css') ?>" rel="stylesheet" id="style_components"
      type="text/css"/>
<link href="<?= asset('assets/global/css/plugins.min.css') ?>" rel="stylesheet" type="text/css"/>
<!-- END THEME GLOBAL STYLES -->

<link rel="shortcut icon" href="<?= asset('favicon.ico') ?>"/>

<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?= asset('assets/layouts/layout4/css/layout.min.css') ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/layouts/layout4/css/themes/light.min.css') ?>" rel="stylesheet" type="text/css"
      id="style_color"/>
<link href="<?= asset('assets/layouts/layout4/css/custom.min.css') ?>" rel="stylesheet" type="text/css"/>
<!-- END THEME LAYOUT STYLES -->

{{--Custom CSS FOR ADMIN--}}
<link href="<?= asset('assets/loan-app/css/common.css') ?>" rel="stylesheet" type="text/css"/>
<link href="<?= asset('assets/loan-app/css/admin.css') ?>" rel="stylesheet" type="text/css"/>



@yield('page-css')


