<div class="container-fluid">


<div class="page-sidebar navbar-collapse collapse second-nav-bar">
    <ul class=" page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true"
        data-slide-speed="200">
        {{--<li class="divider divider-margin"></li>--}}

        <li class="nav-item col-lg-4 col-md-4 col-sm-4 {{(Request::is('borrower/loans*') && !Request::has('status') ? 'active' : '')}} ">
            <a href="{{url('borrower/loans')}}" class="nav-link">
                <i class="icon-docs"></i>
                <span class="title">{{trans('messages.menu.borrower.active_deals')}}</span>
                <span class="badge badge-{{(Request::is('borrower/loans*') && !Request::has('status') ? 'info' : 'default')}}">{{$active_loans_count}}</span>
            </a>
        </li>

        <li class="nav-item col-lg-4 col-md-4 col-sm-4 {{(Request::is('borrower/loans*') && Request::has('status') && Request::input('status') == 'expired' ? 'active' : '')}}  ">
            <a href="{{url('borrower/loans?status='.strtolower(LoanStatuses::EXPIRED))}}" class="nav-link">
                <i class="icon-briefcase"></i>
                <span class="title">{{trans('messages.menu.borrower.expired')}}</span>
                <span class="badge badge-{{(Request::is('borrower/loans*') && Request::has('status') && Request::input('status') == 'expired' ? 'warning' : 'default')}}">{{$expired_loans_count}}</span>
            </a>
        </li>

        <li class="nav-item col-lg-4 col-md-4 col-sm-4 {{(Request::is('borrower/loans*') && Request::has('status') && Request::input('status') == 'deleted' ? 'active' : '')}}">
            <a href="{{url('borrower/loans?status='.strtolower(LoanStatuses::DELETED))}}" class="nav-link">
                <i class="icon-trash"></i>
                <span class="title">{{trans('messages.menu.borrower.deleted')}}</span>
                <span class="badge badge-{{(Request::is('borrower/loans*') && Request::has('status') && Request::input('status') == 'deleted' ? 'danger' : 'default')}}">{{$deleted_loans_count}}</span>
            </a>
        </li>
    </ul>
</div>

</div>