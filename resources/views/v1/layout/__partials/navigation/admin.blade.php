<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            @if (Sentinel::hasAccess('dashboard'))
                <li class="nav-item {{(Request::is('admin/dashboard*') ? 'active' : '')}}">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">{{trans('messages.menu.admin.dashboard')}}</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="{{url('admin/dashboard')}}" class="nav-link ">
                                <span class="title">Statistics</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('admin/dashboard_2')}}" class="nav-link ">
                                <span class="title">{{trans('messages.menu.admin.loans')}} {{trans('messages.menu.admin.dashboard')}}</span>
                            </a>
                        </li><li class="nav-item">
                            <a href="{{url('admin/dashboard_3')}}" class="nav-link ">
                                <span class="title">{{trans('messages.menu.admin.lenders')}} {{trans('messages.menu.admin.dashboard')}}</span>
                            </a>
                        </li>

                    </ul>
                </li>
            @endif

            @if (Sentinel::hasAnyAccess(['institutions.*', 'institution_types.*', 'institution_sizes.*']))
                <li class="heading">
                    <h3 class="uppercase">{{trans('messages.menu.admin.institutions_management')}}</h3>
                </li>
                @if (Sentinel::hasAccess('institutions.*'))
                    <li class="nav-item {{(Request::is('admin/institutions*') ? 'active' : '')}} ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-docs"></i>
                            <span class="title">{{trans('messages.menu.admin.institutions')}}</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item">
                                <a href="{{url('admin/institutions')}}" class="nav-link ">
                                    <span class="title">{{trans('messages.menu.admin.all')}}</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="{{url('admin/institutions?status='.\App\Enums\ActivationStates::ACTIVATION_REQUIRED)}}"
                                   class="nav-link ">
                                    <span class="title">{{trans('messages.menu.admin.requires_approval')}}</span>
                                    <span class="badge badge-danger">{{$domains_activation_required_count}}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if (Sentinel::hasAccess('institution_types.*'))
                    <li class="nav-item {{(Request::is('admin/institution_types*') ? 'active' : '')}} ">
                        <a href="{{url('admin/institution_types')}}" class="nav-link ">
                            <i class="icon-docs"></i>
                            <span class="title">{{trans('messages.menu.admin.institutions_types')}}</span>
                        </a>
                    </li>
                @endif
                @if (Sentinel::hasAccess('institution_sizes.*'))
                    <li class="nav-item {{(Request::is('admin/institution_sizes*') ? 'active' : '')}} ">
                        <a href="{{url('admin/institution_sizes')}}" class="nav-link ">
                            <i class="icon-docs"></i>
                            <span class="title">{{trans('messages.menu.admin.institutions_sizes')}}</span>
                        </a>
                    </li>
                @endif
            @endif

            @if (Sentinel::hasAnyAccess(['users.*', 'roles.*']))
                <li class="heading">
                    <h3 class="uppercase">{{trans('messages.menu.admin.users__management')}}</h3>
                </li>
                @if (Sentinel::hasAccess('users.*'))
                    <li class="nav-item {{(Request::is('admin/users*') ? 'active' : '')}} ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-users"></i>
                            <span class="title">{{trans('messages.menu.admin.users')}}</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="{{url('admin/users')}}" class="nav-link ">
                                    <span class="title">{{trans('messages.menu.admin.all')}}</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="{{url('admin/users?status='.\App\Enums\ActivationStates::ACTIVATION_REQUIRED)}}"
                                   class="nav-link ">
                                    <span class="title">{{trans('messages.menu.admin.requires_approval')}}</span>
                                    <span class="badge badge-danger">{{$users_activation_required_count}}</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="{{url('admin/users?role='.\App\Services\RoleService::ROLE_LENDER)}}"
                                   class="nav-link ">
                                    <span class="title">{{trans('messages.menu.admin.lenders')}}</span>
                                    <span class="badge badge-success">{{$lenders_count}}</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="{{url('admin/users?role='.\App\Services\RoleService::ROLE_BORROWER)}}"
                                   class="nav-link ">
                                    <span class="title">{{trans('messages.menu.admin.borrowers')}}</span>
                                    <span class="badge badge-success">{{$borrowers_count}}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if (Sentinel::hasAccess('roles.*'))
                    <li class="nav-item   {{(Request::is('admin/roles*') ? 'active' : '')}} ">
                        <a href="{{url('admin/roles')}}" class="nav-link">
                            <i class="icon-users"></i>
                            <span class="title">{{trans('messages.menu.admin.roles')}}</span>
                        </a>
                    </li>
                @endif
            @endif

            @if (Sentinel::hasAnyAccess(['loans.*', 'real_estate_types.*', 'property_types.*', 'purpose_types.*', 'lease_types.*']))
                <li class="heading">
                    <h3 class="uppercase">{{trans('messages.menu.admin.loans__management')}}</h3>
                </li>
                @if (Sentinel::hasAccess('loans.*'))
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-docs"></i>
                            <span class="title">{{trans('messages.menu.admin.loans')}}</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="{{url('admin/loans')}}" class="nav-link ">
                                    <span class="title">{{trans('messages.menu.admin.all')}}</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="{{url('admin/loans/bids')}}" class="nav-link ">
                                    <span class="title">{{trans('messages.menu.admin.bids')}}</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                @endif
                @if (Sentinel::hasAccess('real_estate_types.*'))
                    <li class="nav-item {{(Request::is('admin/real_estate_types*') ? 'active' : '')}} ">
                        <a href="{{url('admin/real_estate_types')}}" class="nav-link ">
                            <i class="icon-docs"></i>
                            <span class="title">{{trans('messages.menu.admin.real_estate_types')}}</span>
                        </a>
                    </li>
                @endif
                @if (Sentinel::hasAccess('property_types.*'))
                    <li class="nav-item {{(Request::is('property_types*') ? 'active' : '')}} ">
                        <a href="{{url('admin/property_types')}}" class="nav-link ">
                            <i class="icon-docs"></i>
                            <span class="title">{{trans('messages.menu.admin.property_types')}}</span>
                        </a>
                    </li>
                @endif
                @if (Sentinel::hasAccess('purpose_types.*'))
                    <li class="nav-item {{(Request::is('admin/purpose_types*') ? 'active' : '')}} ">
                        <a href="{{url('admin/purpose_types')}}" class="nav-link ">
                            <i class="icon-docs"></i>
                            <span class="title">{{trans('messages.menu.admin.purpose_types')}}</span>
                        </a>
                    </li>
                @endif
                @if (Sentinel::hasAccess('lease_types.*'))
                    <li class="nav-item {{(Request::is('admin/lease_types*') ? 'active' : '')}} ">
                        <a href="{{url('admin/lease_types')}}" class="nav-link ">
                            <i class="icon-docs"></i>
                            <span class="title">{{trans('messages.menu.admin.lease_types')}}</span>
                        </a>
                    </li>
                @endif
            @endif

            <li class="heading">
                <h3 class="uppercase">{{trans('messages.menu.admin.general')}}</h3>
            </li>
            @if (Sentinel::hasAccess('settings.*'))
                <li class="nav-item {{(Request::is('admin/settings*') ? 'active' : '')}} ">
                    <a href="{{url('admin/settings')}}" class="nav-link">
                        <i class="icon-settings"></i>
                        <span class="title">{{trans('messages.menu.admin.settings')}}</span>
                    </a>
                </li>
            @endif
            @if (Sentinel::hasAnyAccess(['states.*', 'cities.*']))
                <li class="heading">
                    <h3 class="uppercase">{{trans('messages.menu.admin.geo_management')}}</h3>
                </li>
                @if (Sentinel::hasAccess('states.*'))
                    <li class="nav-item {{(Request::is('admin/states*') ? 'active' : '')}} ">
                        <a href="{{url('admin/states')}}" class="nav-link">
                            <i class="icon-docs"></i>
                            <span class="title">{{trans('messages.menu.admin.states')}}</span>
                        </a>
                    </li>
                @endif
            @endif
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>