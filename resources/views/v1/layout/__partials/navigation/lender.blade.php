<div class="container-fluid">


    <div class="page-sidebar navbar-collapse collapse second-nav-bar">
        <ul class=" page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true"
            data-slide-speed="200">
            {{--<li class="divider divider-margin"></li>--}}

            @if($logged_in_user->status != ActivationStates::SUSPENDED)

                <li class="nav-item col-lg-4 col-md-4 col-sm-4 {{(Request::is('lender/find-loans*') ? 'active' : '')}} ">
                    <a href="{{url('lender/find-loans')}}" class="nav-link">
                        <i class="icon-magnifier"></i>
                        <span class="title">{{trans('messages.menu.lender.find_loans')}}</span>
                        <span class="badge badge-{{(Request::is('lender/find-loans')? 'warning' : 'default')}}">{{$loans_count}}</span>
                    </a>
                </li>
                <li class="nav-item col-lg-4 col-md-4 col-sm-4 {{(Request::is('lender/my-work*')  ? 'active' : '')}}  ">
                    <a href="{{url('lender/my-work')}}" class="nav-link">
                        <i class="icon-docs"></i>
                        <span class="title">{{trans('messages.menu.lender.dashboard')}}</span>
                        <span class="badge badge-{{(Request::is('lender/my-work')? 'info' : 'default')}}">{{$bids_placed_count}}</span>
                    </a>
                </li>

                <li class="nav-item col-lg-4 col-md-4 col-sm-4 {{(Request::is('lender/archived-loans*') ? 'active' : '')}}">
                    <a href="{{url('lender/archived-loans')}}" class="nav-link">
                        <i class="icon-briefcase"></i>
                        <span class="title">{{trans('messages.menu.lender.archived')}}</span>
                        <span class="badge badge-{{(Request::is('lender/archived-loans')? 'danger' : 'default')}}">{{$archived_count}}</span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>