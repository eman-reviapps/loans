<div class="page-footer">
    <div class="page-footer-inner"> 2017 &copy; <a target="_blank" href="http://www.seamlabs.com/">SeamLabs</a> &nbsp;
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>