@component('mail::message')
# {{trans('emails.welcome.welcome_message',['app_name' => config('app.name'),'user_name' => $user->first_name])}}

{{trans('emails.welcome.we_are_happy')}}

{{trans('emails.welcome.please_check')}}
@php
    $url = url('activate/'.$user->id.'/'.$activation->code);
@endphp

@component('mail::button', ['url' =>$url])
{{strtoupper(trans('emails.welcome.verify_my_email'))}}
@endcomponent

{{trans('emails.kind_regards')}},<br>
{{ config('app.name') }}
@endcomponent
