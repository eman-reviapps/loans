@component('mail::message')
# Hello {{$user->first_name}},

{{trans('emails.lender_status_changed.account_status_description',['status' => $user->status])}}

@php
    $url = url('/');
@endphp

@component('mail::button', ['url' =>$url])
{{trans('emails.lender_status_changed.visit_account')}}
@endcomponent

{{trans('emails.kind_regards')}},<br>
{{ config('app.name') }}
@endcomponent
