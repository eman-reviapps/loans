@component('mail::message')
# {{trans('emails.lender_requires_approval.hello_admin')}}
{{trans('emails.lender_requires_approval.body', ['email' => $user->email])}}

@php
    $url = url('admin/users?status=ACTIVATION_REQUIRED');
@endphp

@component('mail::button', ['url' =>$url])
{{trans('emails.lender_requires_approval.button_text')}}
@endcomponent

{{trans('emails.kind_regards')}},<br>
{{ config('app.name') }}
@endcomponent
