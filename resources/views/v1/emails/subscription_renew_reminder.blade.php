@component('mail::message')
# Hi,

We hope you have enjoyed being a {{ config('app.name') }} user for the past month.

Your monthly subscription is ending in {{$days}} days.

If you have any questions or feedback just reply to this email and we'll get right back to you.

{{trans('emails.kind_regards')}},<br>
{{ config('app.name') }}

@endcomponent
