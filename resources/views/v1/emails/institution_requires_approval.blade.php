@component('mail::message')
# {{trans('emails.institution_requires_approval.hello_admin')}}
{{trans('emails.institution_requires_approval.body', ['email' => $institution->title])}}

@php
    $url = url('admin/institutions?status=ACTIVATION_REQUIRED');
@endphp

@component('mail::button', ['url' =>$url])
{{trans('emails.institution_requires_approval.button_text')}}
@endcomponent

{{trans('emails.kind_regards')}},<br>
{{ config('app.name') }}
@endcomponent
