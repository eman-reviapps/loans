@component('mail::message')
# Hello {{$user->first_name}},

Thank you for your application. Our team will review it thoroughly
and will let you know about the result within approximately 7 days depending on volume of applicants.
When your application is approved, you will then be able to start!

Kind Regards,<br>
{{ config('app.name') }}
@endcomponent
