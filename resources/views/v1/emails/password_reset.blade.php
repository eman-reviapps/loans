@component('mail::message')
# # {{trans('emails.reset_password.hi_user',['user_name' => $user->first_name])}}

{{trans('emails.reset_password.reset_required',['app_name' => config('app.name')])}}

{{trans('emails.reset_password.please_click')}}
@php
    $url = url('reset/password/'.$user->id.'/'.$reminder->code);
@endphp

@component('mail::button', ['url' =>$url])
{{trans('emails.reset_password.change_password')}}
@endcomponent

{{trans('emails.kind_regards')}},<br>
{{ config('app.name') }}
@endcomponent
