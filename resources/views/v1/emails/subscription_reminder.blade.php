@component('mail::message')
# Hi,

Thanks for signing up for {{ config('app.name') }}. We hope you have been enjoying your free trial.

Unfortunately, your free trial is ending in {{$days}} days.

We'd love to keep you as a customer , and there is still time to complete your subscription! Simply visit your account dashboard to subscribe.

@php
    $url = url('account');
@endphp

@component('mail::button', ['url' =>$url])
    Subscribe Now
@endcomponent
If you have any questions or feedback just reply to this email and we'll get right back to you.


{{trans('emails.kind_regards')}},<br>
{{ config('app.name') }}

@endcomponent
