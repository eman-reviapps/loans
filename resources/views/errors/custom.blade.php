@extends('v1.layout.error')

@section('page-title')
    <?= $title ?>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 page-500">
            <div class=" details">
                <h3><?= $title ?></h3>
                <p> <?= $description ?>
                    <br/>
                </p>
                <p>
                    <a href="{{url('/')}}" class="btn red btn-outline"> <?= trans('errors.main_page') ?> </a>
                    <br>
                </p>
            </div>
        </div>
    </div>
@endsection