@extends('v1.layout.error')

@section('page-title')
    <?= trans('errors.http_status_codes.400.title') ?>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 page-500">
            <div class=" number font-red"> 400</div>
            <div class=" details">
                <h3><?= trans('errors.http_status_codes.400.oops') ?></h3>
                <p> <?= trans('errors.http_status_codes.400.description') ?>
                    <br/>
                </p>
                <p>
                    <a href="{{url('/')}}" class="btn red btn-outline"> <?= trans('errors.main_page') ?> </a>
                    <br>
                </p>
            </div>
        </div>
    </div>
@endsection