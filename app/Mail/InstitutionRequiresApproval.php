<?php

namespace App\Mail;

use App\Models\Institution;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InstitutionRequiresApproval extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $institution;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Institution $institution)
    {
        $this->institution = $institution;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->getSubject())->markdown('v1.emails.institution_requires_approval');
    }


    private function getSubject()
    {
        return trans('emails.institution_requires_approval.subject',
            [
                'app_name' => config('app.name'),
                'user_name' => 'An Institution Requires Approval'
            ]
        );
    }
}
