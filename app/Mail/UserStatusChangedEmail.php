<?php

namespace App\Mail;

use App\Models\Institution;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserStatusChangedEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $institution;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Institution $institution = null)
    {
        $this->user = $user;
        $this->institution = $institution;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->getSubject())
            ->markdown('v1.emails.users.status_changed');
    }

    private function getSubject()
    {
        return trans('emails.lender_status_changed.subject',
            [
                'status' => $this->user->status,
            ]
        );
    }
}
