<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LenderRequiresApproval extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->getSubject())
            ->markdown('v1.emails.lender_requires_approval');
    }

    private function getSubject()
    {
        return trans('emails.lender_requires_approval.subject',
            [
                'app_name' => config('app.name'),
                'user_name' => 'A User Requires Approval'
            ]
        );
    }
}
