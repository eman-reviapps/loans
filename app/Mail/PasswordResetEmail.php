<?php

namespace App\Mail;

use App\Models\User;
use Cartalyst\Sentinel\Reminders\EloquentReminder;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PasswordResetEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $reminder;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, EloquentReminder $reminder)
    {
        $this->user = $user;
        $this->reminder = $reminder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject($this->getSubject())
            ->markdown('v1.emails.password_reset');
    }

    /**
     * @return string
     */
    private function getSubject()
    {
        return trans('emails.reset_password.subject', ['app_name' => config('app.name')]);
    }
}
