<?php

namespace App\Mail;

use App\Models\User;
use Cartalyst\Sentinel\Activations\EloquentActivation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $activation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, EloquentActivation $activation)
    {
        $this->user = $user;
        $this->activation = $activation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject($this->getSubject())
            ->markdown('v1.emails.welcome_email');
    }

    /**
     * @return string
     */
    private function getSubject()
    {
        return trans('emails.welcome.subject',
            [
                'user_name' => $this->user->first_name,
                'app_name' => config('app.name')
            ]
        );
    }
}
