<?php namespace App\Traits;

use App\Helpers\Output;
use Illuminate\Http\Request;
use Sentinel;

trait ControllersTrait
{
    protected $messages_key = 'common';
    protected $success_redirect_to = '';
    protected $fail_redirect_to = '';
    private $response;

    private $save_continue = '';
    private $save_continue_tab = '';

    public function __construct(Request $request)
    {

    }

    function getMessagesKey()
    {
        return $this->messages_key;
    }

    function setMessagesKey($messages_key)
    {
        $this->messages_key = $messages_key;
    }

    function getSuccessRedirectTo()
    {
        return $this->success_redirect_to;
    }

    function setSuccessRedirectTo($success_redirect_to)
    {
        $this->success_redirect_to = $success_redirect_to;
    }

    public function handleAbortReponse(Request $request, Output $response)
    {
        $this->response = $response;
        if ($request->ajax() || $request->wantsJson()) {
            return $this->ajaxResponse();
        }
        abort($this->response->http_status);
    }

    public function handleReponse(Request $request, Output $response)
    {
        $this->response = $response;
        if ($request->ajax() || $request->wantsJson()) {
            return $this->ajaxResponse();

        } else {
            $this->flashMessage();
            if ($request->has('save_continue')) {
                $this->save_continue = $request->save_continue;
            }
            if ($request->has('save_continue_tab')) {
                $this->save_continue_tab = $request->save_continue_tab;
            }
            return $this->handleRedirect();
        }
    }

    public function flashMessage()
    {
        $message = $this->getMessage();
        flash($message, $this->response->status ? 'success' : 'danger');
    }

    public function getMessage()
    {
        $message_key = $this->getMessagesKey();

        $key = $this->response->key ? $this->response->key : $message_key;

        $sub_key = $this->response->sub_key ? '.' . $this->response->sub_key : '';

        return trans("messages.$key" . $sub_key);
    }

//    public function getMessage()
//    {
//        $message_key = $this->getMessagesKey();
//        $key = $this->response->key;
//        $sub_key = $this->response->sub_key ? '.' . $this->response->sub_key : '';
//
//        return trans("messages.$message_key.$key" . $sub_key);
//    }

    public function handleRedirect()
    {
        if ($this->response->status) {
            return $this->handleSuccess();
        }
        return $this->handleFail();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleSuccess()
    {
        if (isset($this->save_continue) && !empty($this->save_continue) && $this->save_continue == 'yes') {
            return back();
        }
        if (isset($this->save_continue_tab) && !empty($this->save_continue_tab)) {
            return back()->with('active_tab', $this->save_continue_tab);
        }
        return redirect($this->getSuccessRedirectTo());
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleFail()
    {
        return (isset($this->fail_redirect_to) && !empty($this->fail_redirect_to)) ?
            redirect($this->getFailRedirectTo()) :
            back()->withInput();
    }

    public function unFlashMessage()
    {
        session()->forget('flash_notification.message');
    }

    /**
     * @return string
     */
    public function getFailRedirectTo()
    {
        return $this->fail_redirect_to;
    }

    /**
     * @param string $fail_redirect_to
     */
    public function setFailRedirectTo($fail_redirect_to)
    {
        $this->fail_redirect_to = $fail_redirect_to;
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    private function ajaxResponse()
    {
        $message = $this->getMessage();
        return response()->json(
            [
                "message" => $message,
                "level" => $this->response->status ? 'success' : 'danger',
                "object" => $this->response->object ? $this->response->object : ''
            ], $this->response->http_status);
    }

    public function returnAjaxResponse($response, $status)
    {
        return response()->json($response, $status);
    }

    public function saveSessionLenderPrefer(Request $request)
    {
        if (session()->has('lender.preferences.FAVORITE_LOAN_TYPES')) {
            $temp = session('lender.preferences.FAVORITE_LOAN_TYPES');
            $temp->value = $request->FAVORITE_LOAN_TYPES;
            session(['lender.preferences.FAVORITE_LOAN_TYPES' => $temp]);
        }

        if (session()->has('lender.preferences.FAVORITE_LOAN_SIZE_MIN')) {
            $temp = session('lender.preferences.FAVORITE_LOAN_SIZE_MIN');
            $temp->value = $request->FAVORITE_LOAN_SIZE_MIN;
            session(['lender.preferences.FAVORITE_LOAN_SIZE_MIN' => $temp]);
        }

        if (session()->has('lender.preferences.FAVORITE_LOAN_SIZE_MAX')) {
            $temp = session('lender.preferences.FAVORITE_LOAN_SIZE_MAX');
            $temp->value = $request->FAVORITE_LOAN_SIZE_MAX;
            session(['lender.preferences.FAVORITE_LOAN_SIZE_MAX' => $temp]);
        }

        if (session()->has('lender.preferences.FAVORITE_STATE')) {
            $temp = session('lender.preferences.FAVORITE_STATE');
            $temp->value = $request->FAVORITE_STATE;
            session(['lender.preferences.FAVORITE_STATE' => $temp]);
        }

        if (session()->has('lender.preferences.FAVORITE_CITY')) {
            $temp = session('lender.preferences.FAVORITE_CITY');
            $temp->value = $request->FAVORITE_CITY;
            session(['lender.preferences.FAVORITE_CITY' => $temp]);
        }

        if (session()->has('lender.preferences.FAVORITE_LOCATION')) {
            $temp = session('lender.preferences.FAVORITE_LOCATION');
            $temp->value = $request->FAVORITE_LOCATION;
            session(['lender.preferences.FAVORITE_LOCATION' => $temp]);
        }
    }
}