<?php

namespace App\Console;

use App\Jobs\SubscribeReminder;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\SyncPlans::class,
        Commands\FirstSubscribeReminder::class,
        Commands\RenewSubscriptionReminder::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('subscribe:reminder')
            ->dailyAt('09:00')
            ->before(function () {
                Log::info('Susbcription reminder cron starts at : ' . Carbon::now());
            })
            ->after(function () {
                Log::info('Susbcription reminder cron ends at: ' . Carbon::now());
            });

        $schedule->command('subscribe:renew_reminder')
            ->dailyAt('09:00')
            ->before(function () {
                Log::info('Susbcription renew reminder cron starts at : ' . Carbon::now());
            })
            ->after(function () {
                Log::info('Susbcription renew reminder cron ends at: ' . Carbon::now());
            });
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
