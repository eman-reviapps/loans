<?php

namespace App\Console\Commands;

use App\Enums\ActivationStates;
use App\Mail\SubscriptionRenewReminder;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Setting;
use App\Models\User;
use App\Services\RoleService;

class RenewSubscriptionReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscribe:renew_reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscription renew for lenders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users_due = User::whereHas('roles', function ($query) {
            $query->where('slug', RoleService::ROLE_LENDER);
        })
            ->whereDate('next_payment_due',Carbon::now()->toDateString())
            ->get();

        foreach ($users_due as $user)
        {
            $user->status = ActivationStates::SUSPENDED;
            $user->save();
        }

        $settings = Setting::first();

        $users = User::whereHas('roles', function ($query) {
            $query->where('slug', RoleService::ROLE_LENDER);
        })
            ->whereDate('next_payment_due',Carbon::now()->addDays($settings->subscribe_renew_reminder_email)->toDateString())
            ->get();

        if (count($users) > 0)
            \Mail::bcc($users)->send(new SubscriptionRenewReminder($settings->subscribe_renew_reminder_email));


    }
}
