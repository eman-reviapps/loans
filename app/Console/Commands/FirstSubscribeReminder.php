<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Enums\ActivationStates;
use App\Mail\SubscriptionReminder;
use App\Models\Setting;
use App\Models\User;
use App\Services\RoleService;

class FirstSubscribeReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscribe:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscription reminder for lenders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $users_due = User::whereHas('roles', function ($query) {
            $query->where('slug', RoleService::ROLE_LENDER);
        })
            ->whereDate('trial_ends_at',Carbon::now()->toDateString())
            ->get();

        foreach ($users_due as $user)
        {
            $user->status = ActivationStates::SUSPENDED;
            $user->save();
        }
        
        $settings = Setting::first();

        $first_reminder_users = User::with(['roles' => function ($q) {
            $q->where('slug', RoleService::ROLE_LENDER);
        }])
            ->active()->firstReminder()->trialEndsWithinDays($settings->subscribe_reminder_email_first)
            ->get();

//        $this->info("Building {$first_reminder_users}!");

        if (count($first_reminder_users) > 0)
        {
            \Mail::bcc($first_reminder_users)->send(new SubscriptionReminder($settings->subscribe_reminder_email_first));
            foreach ($first_reminder_users as $user)
            {
                $user->first_reminder_email_sent = 1;
                $user->save();
            }
        }
//        $this->info("Building {$first_reminder_users}!");

        $second_reminder_users = User::with(['roles' => function ($q) {
            $q->where('slug', RoleService::ROLE_LENDER);
        }])
            ->active()->secondReminder()->trialEndsWithinDays($settings->subscribe_reminder_email_second)
            ->get();

        if (count($second_reminder_users) > 0) {
            \Mail::bcc($second_reminder_users)->send(new SubscriptionReminder($settings->subscribe_reminder_email_second));
            foreach ($second_reminder_users as $user)
            {
                $user->second_reminder_email_sent = 1;
                $user->save();
            }
        }

        $third_reminder_users = User::with(['roles' => function ($q) {
            $q->where('slug', RoleService::ROLE_LENDER);
        }])
            ->where('status', ActivationStates::ACTIVE)
            ->active()->thirdReminder()->trialEndsWithinDays($settings->subscribe_reminder_email_third)
            ->get();

        if (count($third_reminder_users) > 0) {
            \Mail::bcc($third_reminder_users)->send(new SubscriptionReminder($settings->subscribe_reminder_email_third));
            foreach ($third_reminder_users as $user)
            {
                $user->third_reminder_email_sent = 1;
                $user->save();
            }
        }

    }
}
