<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserLoggedIn' => [
            'App\Listeners\RecordUserTrack',
        ],
        'App\Events\UserLoggedOut' => [
            'App\Listeners\UpdateUserTrack',
        ],
        'App\Events\UserRegistered' => [
            'App\Listeners\NotifyAdmin',
            'App\Listeners\SendWelcomeEmail',
            'App\Listeners\SendApplicationReceivedEmail',
            'App\Listeners\SendUserRequiresApprovalMail',
            'App\Listeners\SendInstitutionRequiresApprovalMail',
        ],

        'App\Events\UserRequestedEmailReset' => [
            'App\Listeners\SendPasswordResetEmail',
        ],

        'App\Events\DomainStatusChanged' => [
            'App\Listeners\SendStatusChangedEmailToUsers',
        ],

        'App\Events\UserStatusChanged' => [
            'App\Listeners\SendStatusChangedEmailToUser',
        ],

        'App\Events\BidPlaced' => [
            'App\Listeners\NotifyBorrower',
        ],

        'App\Events\BorrowerNotInterested' => [
            'App\Listeners\NotifyLender',
        ],
        'App\Events\PostEdited' => [
            'App\Listeners\NotifyLenderPostEdited'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
