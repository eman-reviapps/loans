<?php

namespace App\Providers;

use App\Enums\BorrowerProfileTypes;
use App\Models\BorrowerProfile;
use App\Models\BusinessProfile;
use App\Models\Loan;
use App\Models\RealEstateProfile;
use App\Models\Reply;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

use Braintree_Configuration;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        //register morph borrower profile types
        Relation::morphMap([
            BorrowerProfileTypes::BUSINESS => BusinessProfile::class,
            BorrowerProfileTypes::REAL_ESTATE => RealEstateProfile::class
        ]);

        Relation::morphMap([
            'loans' => Loan::class,
            'replies' => Reply::class,
            'borrower_profiles' => BorrowerProfile::class,
        ]);

        Braintree_Configuration::environment(config('services.braintree.environment'));
        Braintree_Configuration::merchantId(config('services.braintree.merchant_id'));
        Braintree_Configuration::publicKey(config('services.braintree.public_key'));
        Braintree_Configuration::privateKey(config('services.braintree.private_key'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
