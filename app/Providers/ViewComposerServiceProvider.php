<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //v1.components.headers.admin
        view()->composer(
            ['v1.layout.*', 'v1.cp.admin.users.show', 'v1.cp.agent.*'], 'App\Http\ViewComposers\UserComposer'
        );
        view()->composer(
            ['v1.layout.admin'], 'App\Http\ViewComposers\AdminComposer'
        );
        view()->composer(
            ['v1.cp.admin.institutions.index'], 'App\Http\ViewComposers\InstitutionsSearchComposer'
        );
        view()->composer(
            ['v1.cp.agent.borrower.loans.*'], 'App\Http\ViewComposers\BorrowerComposer'
        );
        view()->composer(
            ['v1.cp.agent.lender.loans.*'], 'App\Http\ViewComposers\LenderComposer'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
