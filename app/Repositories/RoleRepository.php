<?php

namespace App\Repositories;

use App\Models\Role;
use App\Services\FilterServices;
use Sentinel;
use Activation;
use DB;

class RoleRepository extends Repository
{
    function model()
    {
        return Role::class;
    }
}