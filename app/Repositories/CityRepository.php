<?php
namespace App\Repositories;

use App\Models\City;

class CityRepository extends Repository
{
    function model()
    {
        return City::class;
    }
}