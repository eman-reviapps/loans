<?php

namespace App\Repositories;

use App\Models\UserTrack;
class UserTrackRepository extends Repository
{

    function model()
    {
        return UserTrack::class;
    }
}