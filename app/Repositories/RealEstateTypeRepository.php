<?php
namespace App\Repositories;

use App\Models\RealEstateType;

class RealEstateTypeRepository extends Repository
{
    function model()
    {
        return RealEstateType::class;
    }
}