<?php

namespace App\Repositories;

use App\Contracts\CriteriaInterface;
use App\Contracts\RepositoryInterface;
use App\Contracts\SortCriteriaInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

abstract class Repository implements RepositoryInterface, CriteriaInterface,SortCriteriaInterface
{
    protected $model;
    protected $tableColumns;
    protected $newModel;

    //key value array
    protected $criteria;
    protected $sort_criteria;

    public function __construct(App $app)
    {
        $this->makeModel();
        $this->tableColumns = Schema::getColumnListing($this->model->getTable());
    }

    public function getTableColumns()
    {
        return $this->tableColumns;
    }

    public abstract function model();

    public function makeModel()
    {
        return $this->setModel($this->model());
    }

    public function setModel($eloquentModel)
    {
        $this->newModel = App::make($eloquentModel);
        if (!$this->newModel instanceof Model)
            throw new \Exception("Class {$this->newModel} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        return $this->model = $this->newModel;
    }


    function prepareData(array $attributes)
    {
        $data = [];

        foreach ($attributes as $key => $value) {
            if (in_array($key, $this->tableColumns)) {
                $data[$key] = $attributes[$key];
            }
        }

        return $data;
    }

    public function count()
    {
        $model = $this->applyCriteria();
        return $model->count();
    }

    public function all($columns = array('*'))
    {
        $model = $this->applyCriteria();
        return $model->get($columns);
    }

    public function first($columns = array('*'))
    {
        $model = $this->applyCriteria();
        return $model->first();
    }

    public function with(array $relations)
    {
        $model = $this->model;
        return $model->with($relations);
    }

    public function lists($value, $key = null)
    {
        $model = $this->applyCriteria();

        $lists = $model->lists($value, $key);
        if (is_array($lists)) {
            return $lists;
        }
        return $lists->all();
    }

    public function paginate($perPage = 25, $columns = array('*'))
    {
        $model = $this->applyCriteria();
        return $model->paginate($perPage, $columns);
    }

    public function create(array $data)
    {
        $model = $this->model;
        $prepared = $this->prepareData($data);

        return $model->create($prepared);
    }

    public function saveModel(array $data)
    {
        $prepared = $this->prepareData($data);

        $model = $this->model;
        foreach ($prepared as $k => $v) {
            $model->$k = $v;
        }
        return $model->save();
    }

    public function update(array $data, $id, $attribute = "id")
    {
        $model = $this->model;

        $prepared = $this->prepareData($data);

        return $model->where($attribute, '=', $id)->update($prepared);
    }

//    public function updateRich(array $data, $id)
//    {
//        if (!($model = $this->model->find($id))) {
//            return false;
//        }
//        $prepared = $this->prepareData($data);
//
//        return $model->fill($prepared)->save();
//    }

    public function delete($id)
    {
        $model = $this->model;
        return $model->destroy($id);
    }

    public function find($id, $columns = array('*'))
    {
        $model = $this->applyCriteria();
        return $model->find($id, $columns);
    }

    public function findBy($attribute, $value, $columns = array('*'))
    {
        $model = $this->applyCriteria();
        return $model->where($attribute, '=', $value)->first($columns);
    }

    public function findAllBy($attribute, $value, $columns = array('*'))
    {
        $model = $this->applyCriteria();
        return $model->where($attribute, '=', $value)->get($columns);
    }

    public function findWhere($where, $columns = ['*'], $or = false)
    {
        $this->applyCriteria();
        $model = $this->model;
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = (!$or)
                    ? $model->where($value)
                    : $model->orWhere($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = (!$or)
                        ? $model->where($field, $operator, $search)
                        : $model->orWhere($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = (!$or)
                        ? $model->where($field, '=', $search)
                        : $model->orWhere($field, '=', $search);
                }
            } else {
                $model = (!$or)
                    ? $model->where($field, '=', $value)
                    : $model->orWhere($field, '=', $value);
            }
        }
        return $model->get($columns);
    }

    public function pushCriteria($criteria)
    {
        $this->resetCriteria();

        if (count($criteria) > 0) {
            foreach ($criteria as $key => $value) {
                $this->criteria[] = [
                    'key' => $key,
                    'value' => $value
                ];
            }
        }
    }

    public function resetCriteria()
    {
        $this->criteria = [];
    }

    public function getCriteria()
    {
        return $this->criteria;
    }

    public function getByCriteria($criteria)
    {
        return $this->apply($this->model, $criteria);
    }

    public function applyCriteria()
    {
        $model = $this->model;
        if (count($this->getCriteria()) > 0) {
            foreach ($this->getCriteria() as $criteria) {
                if (in_array($criteria['key'], $this->tableColumns)) {
                    $model = $this->apply($model, $criteria);
                }
            }
        }
        $model = $this->applySortCriteria($model);

        return $model;
    }

    public function apply($model, $criteria)
    {
        $key = $criteria['key'];
        $value = $criteria['value'];

        try {
            if ($value != null && isset($value)) {
                if (is_array($value)) {
                    $value = array_filter($value, function ($k) {
                        return $k != '' && !empty($k);
                    });
                    if (count($value) > 0) {
                        $query = $model->whereIn($key, $value);
                    }
                } else {
                    $query = $model->where($key, $value);
                }
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
        return $query;
    }


    public function pushSortCriteria($sort_criteria)
    {
        $this->resetSortCriteria();

        if (count($sort_criteria) > 0) {
            foreach ($sort_criteria as $key => $value) {
                $this->sort_criteria[] = [
                    'column' => $key,
                    'direction' => $value
                ];
            }
        }
    }

    public function resetSortCriteria()
    {
        $this->sort_criteria = [];
    }

    public function getSortCriteria()
    {
        return $this->sort_criteria;
    }

    public function applySortCriteria($model)
    {
        if (count($this->getSortCriteria()) > 0) {
            foreach ($this->getSortCriteria() as $sort_criteria) {
                if (in_array($sort_criteria['column'], $this->tableColumns)) {
                    $model = $this->applySort($model, $sort_criteria);
                }
            }
        }
        return $model;
    }

    public function applySort($model, $sort_criteria)
    {
        $column = $sort_criteria['column'];
        $direction = $sort_criteria['direction'];

        try {
            $query = $model->orderBy($column, $direction);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
        return $query;
    }
}