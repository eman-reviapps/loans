<?php
/**
 * Created by PhpStorm.
 * User: Eman
 * Date: 3/12/2017
 * Time: 3:31 PM
 */

namespace App\Repositories;

use App\Models\LeaseType;

class LeaseTypeRepository extends Repository
{
    function model()
    {
        return LeaseType::class;
    }
}