<?php
namespace App\Repositories;
use App\Models\BusinessProfile;

class BusinessProfileRepository extends Repository
{
    function model()
    {
        return BusinessProfile::class;
    }
}