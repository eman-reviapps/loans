<?php

namespace App\Repositories;

use App\Contracts\CriteriaInterface;
use App\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;

abstract class Repository_working implements RepositoryInterface, CriteriaInterface
{
    protected $model;
    protected $tableColumns;
    protected $newModel;

    //key value array
    protected $criteria;

    public function __construct(App $app)
    {
        $this->makeModel();
        $this->tableColumns = Schema::getColumnListing($this->model->getTable());
    }

    public function getTableColumns()
    {
        return $this->tableColumns;
    }

    public abstract function model();

    public function makeModel()
    {
        return $this->setModel($this->model());
    }

    public function setModel($eloquentModel)
    {
        $this->newModel = App::make($eloquentModel);
        if (!$this->newModel instanceof Model)
            throw new \Exception("Class {$this->newModel} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        return $this->model = $this->newModel;
    }


    function prepareData(array $attributes)
    {
        $data = [];

        foreach ($attributes as $key => $value) {
            if (in_array($key, $this->tableColumns)) {
                $data[$key] = $attributes[$key];
            }
        }

        return $data;
    }

    public function count()
    {
        $this->applyCriteria();
        return $this->model->count();
    }

    public function all($columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->get($columns);
    }

    public function first($columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->first();
    }

    public function with(array $relations)
    {
        $this->model = $this->model->with($relations);
        return $this;
    }

    public function lists($value, $key = null)
    {
        $this->applyCriteria();
        $lists = $this->model->lists($value, $key);
        if (is_array($lists)) {
            return $lists;
        }
        return $lists->all();
    }

    public function paginate($perPage = 25, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->paginate($perPage, $columns);
    }

    public function create(array $data)
    {
        $prepared = $this->prepareData($data);

        return $this->model->create($prepared);
    }

    public function saveModel(array $data)
    {
        $prepared = $this->prepareData($data);

        foreach ($prepared as $k => $v) {
            $this->model->$k = $v;
        }
        return $this->model->save();
    }

    public function update(array $data, $id, $attribute = "id")
    {
        $prepared = $this->prepareData($data);

        return $this->model->where($attribute, '=', $id)->update($prepared);
    }

    public function updateRich(array $data, $id)
    {
        if (!($model = $this->model->find($id))) {
            return false;
        }
        $prepared = $this->prepareData($data);

        return $model->fill($prepared)->save();
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function find($id, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->find($id, $columns);
    }

    public function findBy($attribute, $value, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->where($attribute, '=', $value)->first($columns);
    }

    public function findAllBy($attribute, $value, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->where($attribute, '=', $value)->get($columns);
    }

    public function findWhere($where, $columns = ['*'], $or = false)
    {
        $this->applyCriteria();
        $model = $this->model;
        foreach ($where as $field => $value) {
            if ($value instanceof \Closure) {
                $model = (!$or)
                    ? $model->where($value)
                    : $model->orWhere($value);
            } elseif (is_array($value)) {
                if (count($value) === 3) {
                    list($field, $operator, $search) = $value;
                    $model = (!$or)
                        ? $model->where($field, $operator, $search)
                        : $model->orWhere($field, $operator, $search);
                } elseif (count($value) === 2) {
                    list($field, $search) = $value;
                    $model = (!$or)
                        ? $model->where($field, '=', $search)
                        : $model->orWhere($field, '=', $search);
                }
            } else {
                $model = (!$or)
                    ? $model->where($field, '=', $value)
                    : $model->orWhere($field, '=', $value);
            }
        }
        return $model->get($columns);
    }

    public function pushCriteria($criteria)
    {
        $this->criteria = [];

        if (count($criteria) > 0) {
            foreach ($criteria as $key => $value) {
                $this->criteria[] = [
                    'key' => $key,
                    'value' => $value
                ];
            }
        }
    }

    public function resetCriteria()
    {
        $this->criteria = [];
    }

    public function getCriteria()
    {
        return $this->criteria;
    }

    public function getByCriteria($criteria)
    {
        $this->model = $this->apply($this->model, $criteria);
        return $this;
    }

    public function applyCriteria()
    {
        if (count($this->getCriteria()) > 0) {
            foreach ($this->getCriteria() as $criteria) {
                if (in_array($criteria['key'], $this->tableColumns)) {
                    $this->model = $this->apply($this->model, $criteria);
                }
            }
        }
        return $this;
    }

    public function apply($model, $criteria)
    {
        $key = $criteria['key'];
        $value = $criteria['value'];

        try {
            if ($value != null && isset($value)) {
                if (is_array($value)) {
                    $value = array_filter($value, function ($k) {
                        return $k != '' && !empty($k);
                    });
                    if (count($value) > 0) {
                        $query = $model->whereIn($key, $value);
                    }
                } else {
                    $query = $model->where($key, $value);
                }
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
        return $query;
    }
}