<?php
namespace App\Repositories;

use App\Models\Setting;

class SettingRepository extends Repository
{
    function model()
    {
        return Setting::class;
    }
}