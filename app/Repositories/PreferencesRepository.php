<?php
/**
 * Created by PhpStorm.
 * User: Eman
 * Date: 3/30/2017
 * Time: 12:50 PM
 */

namespace App\Repositories;

use App\Models\User;
use App\Models\UserPreference;
use DB;

class PreferencesRepository
{
    public function all()
    {
        return UserPreference::latest()->get();
    }

    public function query()
    {
        return UserPreference::where('id', '>', 0);
    }

    public function findUserPreferenceBySlug(User $user, $slug)
    {
        return UserPreference::where('user_id', $user->id)
            ->where('slug', $slug)->first();
    }

    public function create(array $attributes)
    {
        try {
            $userPreference = new UserPreference();
            $userPreference = $this->construct($userPreference, $attributes);

            $created = $userPreference->save();

            if (!$created) {
                return false;
            }
            return $userPreference;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function update(UserPreference $userPreference, array $attributes)
    {
        try {
            $userPreference = $this->construct($userPreference, $attributes);

            $updated = $userPreference->save();

            if (!$updated) {
                return false;
            }

            return $userPreference;

        } catch (\Exception $e) {
            return false;
        }
    }

    public function updateBulk($preferencesArray)
    {
        DB::beginTransaction();
        try {
            foreach ($preferencesArray as $record) {
                $updated = $this->update($record['preference'], $record['attributes']);
                if (!$updated)
                    return false;
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    private function construct(UserPreference $userPreference, array $attributes)
    {
        if (isset($attributes['user_id'])) {
            $userPreference->user_id = $attributes['user_id'];
        }
        if (isset($attributes['slug'])) {
            $userPreference->slug = $attributes['slug'];
        }
        if (isset($attributes['name'])) {
            $userPreference->name = $attributes['name'];
        }
        if (isset($attributes['description'])) {
            $userPreference->description = $attributes['description'];
        }
        if (isset($attributes['value'])) {
            $userPreference->value = $attributes['value'];
        }
        if (isset($attributes['default_value'])) {
            $userPreference->default_value = $attributes['default_value'];
        }
        if (isset($attributes['data_type'])) {
            $userPreference->data_type = $attributes['data_type'];
        }
        if (isset($attributes['field_type'])) {
            $userPreference->field_type = $attributes['field_type'];
        }
        if (isset($attributes['is_enabled'])) {
            $userPreference->is_enabled = $attributes['is_enabled'];
        }

        return $userPreference;
    }


}