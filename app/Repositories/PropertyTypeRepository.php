<?php
namespace App\Repositories;

use App\Models\PropertyType;

class PropertyTypeRepository extends Repository
{
    function model()
    {
        return PropertyType::class;
    }
}