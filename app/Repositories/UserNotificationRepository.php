<?php

namespace App\Repositories;

use App\Models\UserNotification;
use DB;
class UserNotificationRepository
{
    public function create($attributes)
    {

        try {
            $notification = new UserNotification;

            $notification->user_id = $attributes['user_id'];
            $notification->referenced_id = $attributes['referenced_id'];;
            $notification->notification_type = $attributes['notification_type'];
            $notification->message = $attributes['message'];
            $notification->Is_read = $attributes['is_read'];

            $notification->save();

            return array(true, ['notification' => $notification]);
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return array(false);
        }
    }
}