<?php

namespace App\Repositories;

use App\Models\Institution;
use App\Models\City;
use App\Enums\ActivationStates;

class InstitutionRepository extends Repository
{
    function model()
    {
        return Institution::class;
    }

    public function getByDomain($domain_name)
    {
        return Institution::where('domain', 'like', '%' . $domain_name . '%')->first();
    }

    public function isDomainExists($domain_name, $id = null)
    {
        $query = Institution::where('domain', 'like', '%' . $domain_name . '%');
        if ($id != null && $id) {
            $query = $query->where('id', '<>', $id);
        }

        return $query->first();
    }

    public function isBlocked($institution_id)
    {
        $domain_blocked = Institution::where('id', $institution_id)
            ->status(ActivationStates::BLOCKED)
            ->first();

        return $domain_blocked;
    }

    public function isActive($institution_id)
    {
        $domain_active = Institution::where('id', $institution_id)
            ->status(ActivationStates::ACTIVE)
            ->first();

        return $domain_active;
    }

//    public function update(Institution $institution, array $attributes)
//    {
//        try {
//            $institution = $this->construct($institution, $attributes);
//
//            $updated = $institution->save();
//
//            if (!$updated) {
//                return false;
//            }
//
//            return $institution;
//
//        } catch (\Exception $e) {
//            dd($e->getMessage());
//            return false;
//        }
//    }

//    private function construct(Institution $institution, array $attributes)
//    {
//        if (isset($attributes['title'])) {
//            $institution->title = $attributes['title'];
//        }
//        if (isset($attributes['domain'])) {
//            $institution->domain = $attributes['domain'];
//        }
//        if (isset($attributes['status'])) {
//            $institution->status = $attributes['status'];
//        }
//        if (isset($attributes['type'])) {
//            $institution->type_id = $attributes['type'];
//        }
//        if (isset($attributes['size'])) {
//            $institution->size_id = $attributes['size'];
//        }
//        if (isset($attributes['state'])) {
//            $institution->state = $attributes['state'];
//        }
//
//        if (isset($attributes['city']) && !empty($attributes['city'])) {
//            $city = City::where('zip_code', $attributes['city'])->first();
//            $institution->city = $city ? $city->city : null;
//        }
//        if (isset($attributes['zip_code'])) {
//            $institution->zip_code = $attributes['zip_code'];
//        }
//        if (isset($attributes['phone'])) {
//            $institution->phone = $attributes['phone'];
//        }
//        return $institution;
//    }
}