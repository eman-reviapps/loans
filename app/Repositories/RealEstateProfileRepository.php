<?php
namespace App\Repositories;
use App\Models\RealEstateProfile;

class RealEstateProfileRepository extends Repository
{
    function model()
    {
        return RealEstateProfile::class;
    }
}