<?php

namespace App\Repositories;

use App\Models\State;

class StateRepository extends Repository
{
    function model()
    {
        return State::class;
    }
}