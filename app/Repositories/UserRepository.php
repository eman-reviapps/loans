<?php

namespace App\Repositories;

use App\Models\User;
use Sentinel;
use Activation;
use DB;

class UserRepository extends Repository
{
    protected $preferencesService;

    function model()
    {
        return User::class;
    }

    public function create(array $data)
    {
        $role_name = $data['role'];

        $role = Sentinel::findRoleBySlug($role_name);

        $credentials = [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'status' => $data['status'],
        ];

        $user = Sentinel::register($credentials);

        $role->users()->attach($user);

        $activation = Activation::create($user);

        $user->photo = isset($data['photo']) ? $data['photo'] : '';
        $user->phone = isset($data['phone']) ? $data['phone'] : '';

        if (isset($data['free_lifetime'])) {
            $user->free_lifetime = $data['free_lifetime'];
        }
        if (isset($data['trial_ends_at'])) {
            $user->trial_ends_at = $data['trial_ends_at'];
        }
        $user->save();

        if (!$user) {
            return array(false);
        }
        return array(true, ['user' => $user, 'activation' => $activation]);
    }

    public function isUserExists($email, $id = null)
    {
        $query = $this->model->where('email', $email);
        if ($id != null && $id) {
            $query = $query->where('id', '<>', $id);
        }

        return $query->first();
    }
}