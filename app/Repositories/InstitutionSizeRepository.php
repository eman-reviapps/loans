<?php
namespace App\Repositories;

use App\Models\InstitutionSize;
class InstitutionSizeRepository extends Repository
{
    function model()
    {
        return InstitutionSize::class;
    }

}