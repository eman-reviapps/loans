<?php

namespace App\Repositories;

use App\Models\InstitutionBranch;
use App\Models\City;

class InstitutionBranchRepository extends Repository
{
    function model()
    {
        return InstitutionBranch::class;
    }

//    public function isExists($title, $id = null)
//    {
//        $query = InstitutionBranch::where('title', 'like', '%' . $title . '%');
//        if ($id != null && $id) {
//            $query = $query->where('id', '<>', $id);
//        }
//
//        return $query->first();
//    }

//    public function update(InstitutionBranch $branch, array $attributes)
//    {
//        try {
//            $branch = $this->construct($branch, $attributes);
//
//            $updated = $branch->save();
//
//            if (!$updated) {
//                return false;
//            }
//
//            return $branch;
//
//        } catch (\Exception $e) {
//            return false;
//        }
//    }
//
//    private function construct(InstitutionBranch $branch, array $attributes)
//    {
//        if (isset($attributes['institution_id'])) {
//            $branch->institution_id = $attributes['institution_id'];
//        }
//        if (isset($attributes['title'])) {
//            $branch->title = $attributes['title'];
//        }
//        if (isset($attributes['active'])) {
//            $branch->active = $attributes['active'];
//        }
//        if (isset($attributes['state'])) {
//            $branch->state = $attributes['state'];
//        }
//        if (isset($attributes['city']) && !empty($attributes['city'])) {
//            $city = City::where('zip_code', $attributes['city'])->first();
//            $branch->city = $city ? $city->city : null;
//        }
//        if (isset($attributes['zip_code'])) {
//            $branch->zip_code = $attributes['zip_code'];
//        }
//        if (isset($attributes['address'])) {
//            $branch->address = $attributes['address'];
//        }
//        if (isset($attributes['phone'])) {
//            $branch->phone = $attributes['phone'];
//        }
//
//        return $branch;
//    }


}