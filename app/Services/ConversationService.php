<?php

namespace App\Services;


use App\Enums\BidStatuses;
use App\Enums\LoanStatuses;
use App\Helpers\Output;
use App\Models\Bid;
use App\Models\Conversation;
use App\Models\Loan;
use App\Models\Reply;
use App\Models\User;

class ConversationService
{

    public function addReply(Conversation $conversation, array $attributes)
    {
        $reply = new Reply;

        $reply->conversation_id = $conversation->id;

        $reply->sender_id = $attributes['user_id'];
        $reply->receiver_id = $attributes['user_id'] == $conversation->sender_id ? $conversation->receiver_id : $conversation->sender_id;

        $reply->message = $attributes['message'];
        $reply->is_read = 0;

        $reply->save();

        if (!$reply) {
            return new Output(false, 500, 'common', 'failed');
        }

        if ($attributes['user_id'] == $conversation->bid->loan->borrower_id) {
            $bid = Bid::find($conversation->bid->id);
            $bid->status = BidStatuses::BORROWER_RESPONDED;
            $bid->save();

            $loan = Loan::find($conversation->bid->loan->id);
            $loan->status = LoanStatuses::BORROWER_RESPONDED;
            $loan->save();
        }
        return new Output(true, 200, 'common', 'updated', $reply);
    }

    public function canFollowUp(User $user, Bid $bid)
    {
        if (
            $bid->status == BidStatuses::BORROWER_NOT_INTERESTED
            ||
            (
                $bid->status == BidStatuses::NO_RESPONSE
                &&
                $user->inRole(RoleService::ROLE_LENDER)
                &&
                count($bid->conversation->replies->where('sender_id', $user->id)) >= 3
                &&
                count($bid->conversation->replies->where('receiver_id', $user->id)) == 0
            )
        ) {
            return new Output(false, 500, 'common', 'failed');
        } else {
            return new Output(true, 200, 'common', 'succeeded');
        }
    }

    public function markAsRead(Reply $reply)
    {
        $reply->is_read = 1;
        $reply->save();

        if (!$reply) {
            return new Output(false, 500, 'common', 'failed');
        }

        return new Output(true, 200, 'common', 'updated');
    }

    public function markAllRead(User $user)
    {
        Reply::where('receiver_id',$user->id)->update(['is_read'=>1]);

        return new Output(true, 200, 'common', 'updated');
    }

    public function getLenderFollowUpNumber(User $user, Bid $bid)
    {
        $count =  $bid->conversation->replies->where('sender_id',$user->id)->count();

        return new Output(true, 200, 'common', 'succeeded', $count);

    }

}