<?php
namespace App\Services;

use App\Events\UserRequestedEmailReset;
use Sentinel;
use Reminder;
use App\Models\User;
use App\Helpers\Output;

class PasswordService
{

    public function forgotPassword(array $attributes)
    {
        $user = $this->validateCredentials($attributes);
        if (!$user) {
            return new Output(false, 401, 'invalid_login');
        }

        ($reminder = Reminder::exists($user)) || ($reminder = Reminder::create($user));
        event(new UserRequestedEmailReset($user, $reminder));
        return new Output(true, 200, 200);
    }

    public function resetPassword(array $attributes)
    {
        $user = User::find($attributes['user_id']);
        $reset_code = $attributes['reset_code'];

        $check = $this->isValidResetCode($user, $reset_code);
        if (!$check->status) {
            return $check;
        }

        if (!Reminder::exists($user, $reset_code)) {
            return new Output(false, 404, '', 404);
        }

        if (!Reminder::complete($user, $reset_code, $attributes['new_password'])) {
            // Reminder was successfull
            return new Output(false, 500, '', 500);
        }
        Sentinel::login($user);
        return new Output(true, 200, '', 200, $user);
    }

    protected function validateCredentials($attributes)
    {
        $credentials = [
            'login' => $attributes['email'],
        ];

        $user = Sentinel::findByCredentials($credentials);
        if (!$user) {
            return false;
        }
        return $user;
    }

    public function isValidResetCode(User $user, $reset_code)
    {
        if (!$user) {
            return new Output(false, 401, '', 401);
        }

        if (!Reminder::exists($user, $reset_code)) {
            return new Output(false, 404, '', 404);
        }

        return new Output(true, 200, '', 200);
    }
}
