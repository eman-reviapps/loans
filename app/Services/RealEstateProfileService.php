<?php
/**
 * Created by PhpStorm.
 * User: Eman
 * Date: 4/6/2017
 * Time: 1:37 PM
 */

namespace App\Services;

use App\Helpers\Output;
use App\Models\BorrowerProfile;
use App\Models\RealEstateProfile;
use App\Repositories\PropertiesRepository;
use App\Repositories\PropertyRepository;
use App\Repositories\RealEstateProfileRepository;
use DB;

class RealEstateProfileService extends Service
{
    protected $repository;
    protected $propertyRepository;

    public function __construct(RealEstateProfileRepository $repository, PropertyRepository $propertyRepository)
    {
        $this->repository = $repository;
        $this->propertyRepository = $propertyRepository;
    }

    public function editRealEstateProfile(BorrowerProfile $profile,array $attributes)
    {

    }

    public function addRealEstateProfile(array $attributes)
    {
        DB::beginTransaction();

        try {
            $created = $this->createRealEstateProfile($attributes);
            if (!$created) {
                DB::rollback();
                return new Output(false, 409, 'common', 'failed');
            }

            DB::commit();

            return new Output(true, 201, 'common', 'created');
        } catch (\Exception $e) {
            DB::rollback();
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function createRealEstateProfile(array $attributes)
    {
        $profile = $this->repository->create(
            [
                "zip_code" => $attributes['zip_code'],
                "phone" => $attributes['phone'],
                "properties_no" => $attributes['properties_owned_no'],
            ]
        );
        if (!$profile) {
            return false;
        }

        if (isset($attributes['properties']) && count($attributes['properties']) > 0) {
            $properties = $attributes['properties'];

            $created = $this->createProperties($profile, $properties);
            if (!$created)
                return false;
        }
        return $profile;
    }

    private function createProperties(RealEstateProfile $profile, $properties)
    {
        foreach ($properties as $property) {
            $this->propertyRepository->create(
                [
                    "profile_id" => $profile->id,
                    "property_type_id" => $property['property_type'],
                    "address" => $property['address'],
                    "estimated_market_value" => $property['estimated_market_value'],
                ]
            );
        }
        return true;
    }
}