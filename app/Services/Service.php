<?php

namespace App\Services;

use App\Contracts\ServiceInterface;
use App\Helpers\Output;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Model;

abstract class Service implements ServiceInterface
{
    protected $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function all()
    {
        return $this->get([]);
    }

    public function get(array $attributes, $count = false,$sort_criteria = [])
    {
        $this->repository->pushCriteria($attributes);

        $this->repository->pushSortCriteria($sort_criteria);

        $records = $this->repository->all();

        $records = $this->filterRelations($records, $attributes);

        return $count ? count($records) : $records;
    }

    public function filterRelations($data, $attributes)
    {
        return $data;
    }

    public function first(array $attributes)
    {
        $this->repository->pushCriteria($attributes);

        return $this->repository->first();
    }

    public function create(array $attributes)
    {
        try {
            $attributes = $this->prepareCreate($attributes);

            $created = $this->repository->create($attributes);

            if (!$created)
                return new Output(false, 500, 'common', 'failed');

            return new Output(true, 200, 'common', 'created');

        } catch (\Exception $e) {
            return new Output(false, 500, 'common', 'failed');
        }

    }

    public function prepareCreate(array $attributes)
    {
        return $this->prepare($attributes);
    }

    public function prepareUpdate(Model $model, array $attributes)
    {
        return $this->prepare($attributes);
    }

    public function prepare(array $attributes)
    {
        return $attributes;
    }

    public function update(Model $model, array $attributes)
    {
        try {

            $attributes = $this->prepareUpdate($model, $attributes);

            $updated = $this->repository->update($attributes, $model->id);

            if (!$updated)
                return new Output(false, 500, 'common', 'failed');


            return new Output(true, 200, 'common', 'updated');

        } catch (\Exception $e) {
            return new Output(false, 500, 'common', 'failed');
        }
    }


    public function delete(Model $model)
    {
        try {
            $deleted = $this->repository->delete($model->id);

            if (!$deleted) {
                return new Output(false, 500, 'common', 'failed');
            }
            return new Output(true, 200, 'common', 'deleted');
        } catch (\Exception $ex) {
            return new Output(false, 500, 'common', 'failed');
        }
    }


}