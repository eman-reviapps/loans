<?php
namespace App\Services;

use App\Helpers\Output;
use App\Repositories\BusinessProfileRepository;
use DB;

class BusinessProfileService extends Service
{
    protected $repository;

    public function __construct(BusinessProfileRepository $repository)
    {
        $this->repository = $repository;
    }

    public function addBusinessProfile(array $attributes)
    {
        DB::beginTransaction();

        try {

            $created = $this->createBusinessProfile($attributes);
            if (!$created) {
                DB::rollback();
                return new Output(false, 409, 'common', 'failed');
            }

            DB::commit();

            return new Output(true, 201, 'common', 'created');
        } catch (\Exception $e) {
            DB::rollback();
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function createBusinessProfile(array $attributes)
    {
        $data = $this->prepare($attributes);

        $profile = $this->repository->create($data);
        return $profile;
    }

    public function prepare(array $attributes)
    {
        $data = [];
        if (isset($attributes['business_name'])) {
            $data['business_name'] = $attributes['business_name'];
        }
        if (isset($attributes['address'])) {
            $data['address'] = $attributes['address'];
        }
        if (isset($attributes['description'])) {
            $data['description'] = $attributes['description'];
        }
        if (isset($attributes['phone'])) {
            $data['phone'] = $attributes['phone'];
        }
        if (isset($attributes['establish_year'])) {
            $data['establish_year'] = $attributes['establish_year'];
        }
        if (isset($attributes['fisical_year_end'])) {
            $data['fisical_year_end'] = $attributes['fisical_year_end'];
        }
        if (isset($attributes['last_year_annual_revenue'])) {
            $data['last_year_annual_revenue'] = $attributes['last_year_annual_revenue'];
        }
        if (isset($attributes['pre_last_year_annual_revenue'])) {
            $data['pre_last_year_annual_revenue'] = $attributes['pre_last_year_annual_revenue'];
        }
        if (isset($attributes['pre_pre_last_year_annual_revenue'])) {
            $data['pre_pre_last_year_annual_revenue'] = $attributes['pre_pre_last_year_annual_revenue'];
        }
        if (isset($attributes['profile_creator_role'])) {
            $data['profile_creator_role'] = $attributes['profile_creator_role'];
        }
        if (isset($attributes['profile_creator_decision_maker'])) {
            $data['profile_creator_decision_maker'] = $attributes['profile_creator_decision_maker'];
        }
        return $data;
    }

}