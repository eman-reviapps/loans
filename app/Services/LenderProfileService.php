<?php
/**
 * Created by PhpStorm.
 * User: Eman
 * Date: 4/6/2017
 * Time: 1:37 PM
 */

namespace App\Services;

use App\Helpers\Output;
use App\Models\Institution;
use App\Models\LenderProfile;
use App\Models\User;
use DB;

class LenderProfileService
{
    protected $preferencesService;

    public function __construct(PreferencesService $preferencesService)
    {
        $this->preferencesService = $preferencesService;
    }

    public function create(User $user, Institution $institution, array $attributes)
    {
        $user->lenderProfile()->create([
            "user_id" => $user->id,
            "institution_id" => $institution ? $institution->id : null,
            "phone" => isset($attributes['phone']) ? $attributes['phone'] : null,
            "title" => isset($attributes['jobTitle']) ? $attributes['jobTitle'] : null,
            "line_of_business" => isset($attributes['line_of_business']) ? $attributes['line_of_business'] : null,
            "address" => isset($attributes['address']) ? $attributes['address'] : null
        ]);

        $this->preferencesService->createPreferences([], $user);

        return true;
    }

    public function update(LenderProfile $profile, $attributes)
    {
        try {
            $updated = $profile->update($attributes);

            if (!$updated) {
                return new Output(false, 500, 'common', 'failed');
            }

            return new Output(true, 200, 'common', 'updated');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }
}