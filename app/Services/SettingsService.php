<?php
namespace App\Services;

use App\Contracts\RepositoryInterface;
use App\Repositories\SettingRepository;
use Illuminate\Database\Eloquent\Model;


class SettingsService extends Service
{

    public function __construct(SettingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function prepareCreate(array $attributes)
    {
        if (!isset($attributes['slug']) && isset($attributes['title'])) {
            $attributes['slug'] = str_slug($attributes['title'], '-');
        }
        return $attributes;
    }

    public function prepareUpdate(Model $model, array $attributes)
    {
        if (!isset($attributes['slug']) && isset($attributes['title'])) {
            $attributes['slug'] = str_slug($attributes['title'], '-');
        }
        return $attributes;
    }

}