<?php
namespace App\Services;

class GeneralTypesService extends Service
{
    public function prepare(array $attributes)
    {
        if (!isset($attributes['slug']) && isset($attributes['title'])) {
            $attributes['slug'] = str_slug($attributes['title'], '-');
        }
        return $attributes;
    }

}