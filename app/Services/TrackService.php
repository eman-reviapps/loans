<?php
namespace App\Services;


use App\Models\User;
use App\Repositories\UserTrackRepository;
use Carbon\Carbon;
use Session;

class TrackService extends Service
{

    public function __construct(UserTrackRepository $repository)
    {
        $this->repository = $repository;
    }

    public function recordUserTrack(User $user)
    {
        $geo_data = (geoip()->getLocation(geoip()->getClientIP()));
        $attributes =
            [
                "user_id" => $user->id,
                "login_at" => Carbon::now(),
                "country" => $geo_data->country,
                "city" => $geo_data->city,
                "state" => $geo_data->state,
                "state_name" => $geo_data->state_name,
                "ip" => $geo_data->ip,
                "longitude" => $geo_data->lon,
                "latitude" => $geo_data->lat,
            ];

        $track = $this->repository->create($attributes);

        if ($track) {
            Session::put('track_id', $track->id);
        }
    }

    public function recordLogout($track_id)
    {
        $attributes = ["logout_at" => Carbon::now()];

        $track = $this->repository->find($track_id);

        if ($track) {
            $this->repository->update($attributes, $track->id);
        }
    }
}