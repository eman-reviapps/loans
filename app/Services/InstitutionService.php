<?php

namespace App\Services;

use App\Events\DomainStatusChanged;
use App\Models\Institution;
use App\Models\User;
use App\Repositories\InstitutionRepository;
use App\Helpers\Output;
use Illuminate\Database\Eloquent\Model;

class InstitutionService extends Service
{
    protected $repository;
    protected $locationService;

    public function __construct(InstitutionRepository $repository, LocationService $locationService)
    {
        $this->repository = $repository;

        $this->locationService = $locationService;
    }


    public function find($institution_id)
    {
        return isset($institution_id) && !empty($institution_id) ? $this->repository->find($institution_id) : null;
    }

    public function create(array $attributes)
    {
        try {
            $exists = $this->isDomainExists($attributes['domain']);
            if ($exists) {
                return new Output(false, 409, '', 'domain_already_exists');
            }
            $attributes = $this->prepare($attributes);

            $created = $this->repository->create($attributes);

            if (!$created) {
                return new Output(false, 500, 'common', 'failed');
            }

            $institution = $this->repository->findBy('domain', $attributes['domain']);

            return new Output(true, 201, 'common', 'created', $institution);
        } catch (\Exception $e) {
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function update(Model $institution, array $attributes)
    {
        try {
            $exists = $this->isDomainExists($attributes['domain'], $institution->id);
            if ($exists) {
                return new Output(false, 409, '', 'domain_already_exists');
            }

            $old_status = $institution->status;

            $attributes = $this->prepare($attributes);

            $updated = $this->repository->update($attributes, $institution->id);

            if (!$updated) {
                return new Output(false, 500, 'common', 'failed');
            }
            $institution = $this->find($institution->id);

            /*if ($institution->status != $old_status) {
                $this->notifyUsersDomainStatusChanged($institution);
            }*/

            return new Output(true, 200, 'common', 'updated');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function prepare(array $attributes)
    {
        if (isset($attributes['type'])) {
            $attributes['type_id'] = $attributes['type'];
        }
        if (isset($attributes['size'])) {
            $attributes['size_id'] = $attributes['size'];
        }
        if (isset($attributes['city']) && !empty($attributes['city'])) {
            $city = $this->locationService->findCity($attributes['city']);
            $attributes['city'] = $city ? $city->city : null;
        }

        return $attributes;
    }

    public function delete(Model $institution)
    {
        try {
            if (count($institution->lenderProfile) > 0) {
                return new Output(false, 500, '', 'institution_have_lenders');
            }

            $deleted = $institution->delete();
            if (!$deleted) {
                return new Output(false, 500, 'common', 'failed');
            }
            return new Output(true, 200, 'common', 'deleted');
        } catch (\Exception $ex) {
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function changeStatus(Institution $institution, array $attributes)
    {
        try {
            $updated = $this->repository->update($attributes, $institution->id);

            if (!$updated) {
                return new Output(false, 500, 'common', 'failed');
            }
            $institution = $this->find($institution->id);

            if (!$institution) {
                return new Output(false, 500, 'common', 'failed');
            }

            //$this->notifyUsersDomainStatusChanged($institution);

            return new Output(true, 200, 'common', 'updated');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function isDomainExists($domain, $id = null)
    {
        $parsed = $this->getDomainFromUrl($domain);
        return $this->repository->isDomainExists($parsed, $id);
    }

    public function getByDomain($domain)
    {
        return $this->repository->getByDomain($domain);
    }

    public function isBlocked(Institution $institution)
    {
        return $this->repository->isBlocked($institution->id);
    }

    public function isActive(Institution $institution)
    {
        return $this->repository->isActive($institution->id);
    }

    public function getDomainStatus($domain)
    {
        $domian = $this->repository->getByDomain($domain);
        if ($domian) {
            return $domian->status;
        }
        return false;
    }

    public function getInstitutionUsers(Institution $institution)
    {
        return User::with('lenderProfile')->get()->filter(function ($user) use ($institution) {
            return isset($user->lenderProfile) && $user->lenderProfile->institution_id == $institution->id;
        });
    }

    /**
     * @param Institution $institution
     * @param array $attributes
     */
    private function notifyUsersDomainStatusChanged(Institution $institution)
    {
        $users = $this->getInstitutionUsers($institution);
        foreach ($users as $user) {
            $user->status = $institution->status;
            $user->save();
        }

        event(new DomainStatusChanged($institution, $users));
    }

    public function getDomainFromUrl($url)
    {
        $parsed = parse_url($url);
        return isset($parsed['host']) ? $parsed['host'] : $parsed['path'];
    }

}