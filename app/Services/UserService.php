<?php

namespace App\Services;

use App\Events\UserRegistered;
use App\Events\UserStatusChanged;
use App\Helpers\CommonHelpers;
use App\Models\Bid;
use App\Models\Loan;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Models\Institution;
use App\Enums\ActivationStates;
use Illuminate\Database\Eloquent\Model;
use Sentinel;
use App\Helpers\Output;
use DB;
use App\Models\LenderProfile;
use App\Models\BorrowerProfile;


class UserService extends Service
{
    protected $institutionService;
    protected $borrowerProfileService;
    protected $lenderProfileService;

    public function __construct(UserRepository $userRepository,
                                InstitutionService $institutionService,
                                BorrowerProfileService $borrowerProfileService,
                                LenderProfileService $lenderProfileService
    )
    {
        $this->repository = $userRepository;

        $this->institutionService = $institutionService;
        $this->borrowerProfileService = $borrowerProfileService;
        $this->lenderProfileService = $lenderProfileService;
    }

    function filterRelations($data, $attributes)
    {
        if (isset($attributes['role']) && $attributes['role'] != null) {
            $role = $attributes['role'];
            $data = $data->filter(function ($data) use ($role) {
                return in_array($data->role->slug, CommonHelpers::commaValuesToArray($role));
            });


            if (isset($attributes['status']) && $attributes['status'] != null) {
                if ($attributes['role'] == RoleService::ROLE_LENDER) {
                    $data = $data->filter(function ($data) {
                        return $data->lender_bids_count > 0;
                    });
                } elseif ($attributes['role'] == RoleService::ROLE_BORROWER) {
                    $data = $data->filter(function ($data) {
                        return $data->borrower_loans_count > 0;
                    });
                }
            }
        }
        return $data;

    }

    public function store($attributes)
    {
        $is_valid = $this->isValidRegister($attributes);
        if (!$is_valid->status)
            return $is_valid;

        $institution = $is_valid->object;

        DB::beginTransaction();
        try {

            $created = $this->registerUser($attributes, $institution, null);
            if (!$created[0]) {
                DB::rollback();
                return new Output(false, 500, 'common', 'failed');
            }

            $user = $created[1];
            $activation = $created[2];

            DB::commit();

            event(new UserRegistered($user, $activation, $institution));

            return new Output(true, 201, 'common', 'created', $user);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function registerUser(array $attributes, $institution)
    {
        $user_result = $this->repository->create($attributes);
        if (!$user_result[0]) {
            return array(false);
        }

        $user = $user_result[1]['user'];
        $activation = $user_result[1]['activation'];

        $role_name = $attributes['role'];

        $registered = true;
        if ($role_name == RoleService::ROLE_LENDER) {
            $registered = $this->lenderProfileService->create($user, $institution, $attributes);
        } else if ($role_name == RoleService::ROLE_BORROWER) {
            $registered = $this->borrowerProfileService->create($user, $attributes);
        }
        if (!$registered)
            return array(false);

        return array(true, $user, $activation);
    }

    private function isValidRegister(array $attributes)
    {
        $user = Sentinel::findByCredentials(['login' => $attributes['email']]);
        if ($user) {
            return new Output(false, 409, '', 'email_already_exists');
        }

        $institution = null;
        if (isset($attributes['institution']) & !empty($attributes['institution'])) {
            $institution = $this->institutionService->find($attributes['institution']);
            if (!$institution) {
                return new Output(false, 500, 'common', 'failed'); //elmfrood institution not found
            }
        }
        return new Output(true, 200, '', '', $institution);
    }

    public function update(Model $model, array $attributes)
    {
        try {
            $old_status = $model->status;

            $updated = $this->repository->update($attributes, $model->id);

            if (!$updated) {
                return new Output(false, 500, 'common', 'failed');
            }

            $user = $this->repository->find($model->id);

            if ($user->status != $old_status) {
                $this->notifyUserStatusChanged($user);
            }

            return new Output(true, 200, 'common', 'updated');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function isUserExists($email, $id = null)
    {
        return $this->repository->isUserExists($email, $id);
    }

    public function checkUserState(User $user)
    {
        $status = $user->status;

        if ($status == ActivationStates::BLOCKED) {
            return array(false, 'user_is_blocked');
        }
        if ($status != ActivationStates::ACTIVE && $status != ActivationStates::SUSPENDED) {
            return array(false, 'user_in_activation_process');
        }
        return array(true);
    }

    public function checkUserDomaindState(User $user)
    {
        if ($user->lenderProfile->institution) {
            $domain_name = $user->lenderProfile->institution->domain;

            $status = $this->institutionService->getDomainStatus($domain_name);
            if (!$status) {
                return array(false, 'domain_not_found');
            }
            if ($status == ActivationStates::BLOCKED) {
                return array(false, 'domain_is_blocked');
            }
            if ($status != ActivationStates::ACTIVE) {
                return array(false, 'domain_in_activation_process');
            }
        }
        return array(true);
    }

    public function getInitialUserStatus(Institution $institution = null)
    {
        if ($institution == null) {
            return ActivationStates::ACTIVE;
        }

        if ($this->institutionService->isBlocked($institution)) {
            $status = ActivationStates::BLOCKED;
        } else {
            $status = ActivationStates::DEFAULT_VALUE;
        }
        return $status;

    }

    public function changePassword(User $user, array $attributes)
    {
        try {
            //check user old password is correct
            $credentials = array(
                'email' => $user->email,
                'password' => $attributes['current_password'],
            );

            if (!Sentinel::validateCredentials($user, $credentials)) {
                return new Output(false, 401, '', 'invalid_old_password');
            }

            $update = Sentinel::update($user, array('password' => $attributes['new_password']));
            if (!$update) {
                return new Output(false, 500, 'common', 'failed');
            }
            return new Output(true, 200, 'common', 'updated');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    private function notifyUserStatusChanged(User $user)
    {
        event(new UserStatusChanged($user));
    }

    public function delete(Model $user){
        try {
            $deleted = $user->delete();
            if (!$deleted) {
                return new Output(false, 500, 'common', 'failed');
            } else {
                return new Output(true, 200, 'common', 'deleted');
            }
        }
        catch(\Exception $ex){
            return new Output(false, 500, 'common', 'failed');
        }
    }


    public function numberOfLenders(){
        $count = Sentinel::findRoleBySlug(RoleService::ROLE_LENDER)->users()->where('deleted_at',null)->count();

//        $count = LenderProfile::where()->get()->count();
        return $count;
    }

    public function numberOfBorrowers(){
        $count = Sentinel::findRoleBySlug(RoleService::ROLE_BORROWER)->users()->where('deleted_at',null)->count();
//        $count = BorrowerProfile::distinct('user_id')->count('user_id');
        return $count;
    }

    public function numberOfActiveLenders(){
        $count = Bid::select('lender_id')->distinct()->count('lender_id');
        return $count;
    }

    public function numberOfActiveBorrowers(){
        $count = Loan::select('borrower_id')->distinct()->count('borrower_id');
        return $count;
    }

    public function getNumberOfFreeLiftimeLenders()
    {
        return $this->get([
            "role" => RoleService::ROLE_LENDER,
            "free_lifetime" => 1,

        ], true);

    }

}