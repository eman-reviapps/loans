<?php
/**
 * Created by PhpStorm.
 * User: Eman
 * Date: 4/6/2017
 * Time: 1:37 PM
 */

namespace App\Services;

use App\Enums\BorrowerProfileTypes;
use App\Helpers\Output;
use App\Models\Attachment;
use App\Models\BorrowerProfile;
use App\Models\BusinessProfile;
use App\Models\Property;
use App\Models\RealEstateProfile;
use App\Models\User;
use App\Models\Loan;
use Sentinel;
use DB;

class BorrowerProfileService
{
    protected $preferencesService;

    public function __construct(PreferencesService $preferencesService)
    {
        $this->preferencesService = $preferencesService;
    }

    public function select(BorrowerProfile $borrowerProfile)
    {
        $this->saveActiveProfile($borrowerProfile);

        $this->saveProfileToSession($borrowerProfile);

        return new Output(true, 201, 'common', 'succeeded');
    }

    public function saveActiveProfile(BorrowerProfile $borrowerProfile)
    {
        BorrowerProfile::where('user_id',$borrowerProfile->user_id)->update(['is_active' => 0]);

        $borrowerProfile->is_active = true;
        $borrowerProfile->save();
    }

    public function setActiveProfile()
    {
        $profile = BorrowerProfile::
        where('user_id', Sentinel::getUser()->id)
            ->enabled()
            ->active()
            ->first();

        if (!$profile) {
            $profile = BorrowerProfile::
            where('user_id', Sentinel::getUser()->id)
                ->enabled()
                ->latest()
                ->first();
        }
        if (!$profile) {
            session(['borrower_profile_id' => null]);
            false;
        }

        $this->saveProfileToSession($profile);

        return true;
    }

    private function saveProfileToSession($profile)
    {
        session(['borrower_profile_id' => $profile->id]);

        $profile = BorrowerProfile::
        where('id', $profile->id)
            ->where('user_id', Sentinel::getUser()->id)
            ->enabled()
            ->first();

        session(['borrower_profile' => $profile->toJson()]);
    }

    public function add(User $user, $attributes)
    {
        DB::beginTransaction();
        try {
            $this->create($user, $attributes);

            DB::commit();

            return new Output(true, 201, 'common', 'created', $user);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }


    public function create(User $user, $attributes)
    {
        if (isset($attributes['profile_type'])) {

            if ($attributes['profile_type'] == BorrowerProfileTypes::REAL_ESTATE) {
                $profile = $this->createRealEstateProfile($attributes);
            } elseif ($attributes['profile_type'] == BorrowerProfileTypes::BUSINESS) {
                $profile = $this->createBusinessProfile($attributes);
            }

            $profile_no = md5(uniqid(rand(), true));
//            $profile_no = uniqid(rand(), true);
//            $profile_no = Carbon::now()->year;

            $borrowerProfile = BorrowerProfile::create(
                [
                    "user_id" => $user->id,
                    "profile_id" => $profile->id,
                    "profile_type" => $attributes['profile_type'],
                    "profile_no" => $profile_no,
                ]
            );

            $this->preferencesService->createPreferences([], $user, $borrowerProfile);
        }
        return true;
    }

    private function createBusinessProfile(array $attributes)
    {
        $businessProfile = new BusinessProfile;

        $businessProfile->profile_creator_role = isset($attributes['profile_creator_role']) ? $attributes['profile_creator_role'] : null;
        $businessProfile->profile_creator_decision_maker = isset($attributes['profile_creator_decision_maker']) ? $attributes['profile_creator_decision_maker'] : null;

        $businessProfile->business_name = $attributes['business_name'];
        $businessProfile->description = $attributes['description'];

        $businessProfile->state_code = $attributes['state'];
        $businessProfile->city = $attributes['city'];
        $businessProfile->zip_code = $attributes['zip_code'];
        $businessProfile->address = $attributes['address'];
        $businessProfile->apt_suite_no = $attributes['apt_suite_no'];

        $businessProfile->phone = $attributes['phone'];
        $businessProfile->establish_year = $attributes['establish_year'];
        $businessProfile->fisical_year_end = $attributes['fisical_year_end'];

        $businessProfile->annual_revenue_year_1 = $attributes['annual_revenue_year_1'];
        $businessProfile->annual_revenue_year_2 = $attributes['annual_revenue_year_2'];
        $businessProfile->annual_revenue_year_3 = $attributes['annual_revenue_year_3'];

        $businessProfile->net_income_before_tax_year_1 = $attributes['net_income_before_tax_year_1'];
        $businessProfile->net_income_before_tax_year_2 = $attributes['net_income_before_tax_year_2'];
        $businessProfile->net_income_before_tax_year_3 = $attributes['net_income_before_tax_year_3'];

        $businessProfile->income_tax_expense_year_1 = $attributes['income_tax_expense_year_1'];
        $businessProfile->income_tax_expense_year_2 = $attributes['income_tax_expense_year_2'];
        $businessProfile->income_tax_expense_year_3 = $attributes['income_tax_expense_year_3'];

        $businessProfile->interest_expense_year_1 = $attributes['interest_expense_year_1'];
        $businessProfile->interest_expense_year_2 = $attributes['interest_expense_year_2'];
        $businessProfile->interest_expense_year_3 = $attributes['interest_expense_year_3'];

        $businessProfile->depreciation_amortization_year_1 = $attributes['depreciation_amortization_year_1'];
        $businessProfile->depreciation_amortization_year_2 = $attributes['depreciation_amortization_year_2'];
        $businessProfile->depreciation_amortization_year_3 = $attributes['depreciation_amortization_year_3'];

        $businessProfile->company_EBITDA_year_1 = $attributes['company_EBITDA_year_1'];
        $businessProfile->company_EBITDA_year_2 = $attributes['company_EBITDA_year_2'];
        $businessProfile->company_EBITDA_year_3 = $attributes['company_EBITDA_year_3'];


        $businessProfile->company_EBITDA = $attributes['company_EBITDA'];

        $businessProfile->save();

        return $businessProfile;
    }

    private function createRealEstateProfile(array $attributes)
    {
        $profile = RealEstateProfile::create(
            [
                "state_code" => $attributes['state'],
                "city" => $attributes['city'],
                "zip_code" => $attributes['zip_code'],
                "address" => $attributes['address'],
                "apt_suite_no" => $attributes['apt_suite_no'],
                "phone" => $attributes['phone'],
                "properties_no" => $attributes['properties_owned_no'],
                "company_name" => isset($attributes['company_name']) ? $attributes['company_name'] : null,
            ]
        );
        if (!$profile) {
            return false;
        }

        if (isset($attributes['properties']) && count($attributes['properties']) > 0) {
            $properties = $attributes['properties'];

            foreach ($properties as $property) {
                Property::create(
                    [
                        "profile_id" => $profile->id,
                        "property_type_id" => $property['property_type'],
                        "address" => $property['address'],
                        "estimated_market_value" => $property['estimated_market_value'],
                    ]
                );
            }
        }
        return $profile;
    }

    public function update(BorrowerProfile $profile, array $attributes)
    {
        DB::beginTransaction();
        try {
            if ($profile->profile_type == BorrowerProfileTypes::REAL_ESTATE) {
                $this->updateRealEstateProfile($profile->profile, $attributes);
            } else {
                $this->updateBusinessProfile($profile->profile, $attributes);
            }
            DB::commit();

            return new Output(true, 201, 'common', 'created');
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return new Output(false, 500, 'common', 'failed');
        }
    }

    private function updateRealEstateProfile(RealEstateProfile $realEstateProfile, array $attributes)
    {
        $realEstateProfile->company_name = isset($attributes['company_name']) ? $attributes['company_name'] : null;

        $realEstateProfile->state_code = $attributes['state'];
        $realEstateProfile->city = $attributes['city'];
        $realEstateProfile->zip_code = $attributes['zip_code'];
        $realEstateProfile->address = $attributes['address'];
        $realEstateProfile->apt_suite_no = $attributes['apt_suite_no'];

        $realEstateProfile->phone = $attributes['phone'];
        $realEstateProfile->properties_no = $attributes['properties_owned_no'];

        $realEstateProfile->save();

        $this->processDeletedProperties($realEstateProfile, isset($attributes['properties']) ? $attributes['properties'] : []);

        if (isset($attributes['properties'])) {
            foreach ($attributes['properties'] as $property_attributes) {
                if (isset($property_attributes['property_id']) && $property_attributes['property_id'] > 0) {
                    $property = Property::find($property_attributes['property_id']);
                    $property->realestate_type_id = $property_attributes['realestate_type'];
                    $property->address = $property_attributes['address'];
                    $property->estimated_market_value = $property_attributes['estimated_market_value'];
                    $property->loan_outstanding = $property_attributes['loan_outstanding'];
                    $property->loan_maturity_date = $property_attributes['loan_maturity_date'];
                    $property->save();
                } else {
                    Property::create(
                        [
                            "profile_id" => $realEstateProfile->id,
                            "realestate_type_id" => $property_attributes['realestate_type'],
                            "address" => $property_attributes['address'],
                            "estimated_market_value" => $property_attributes['estimated_market_value'],
                            "loan_outstanding" => $property_attributes['loan_outstanding'],
                            "loan_maturity_date" => $property_attributes['loan_maturity_date'],
                        ]
                    );
                }
            }
        }
        if (isset($attributes['files']) && count($attributes['files']) > 0) {

            foreach ($attributes['files'] as $file) {

                $attachment = new Attachment();

                $attachment->url = $file;

                $realEstateProfile->borrower_profile->attachments()->save($attachment);
            }
        }
    }

    private function processDeletedProperties(RealEstateProfile $realEstateProfile, array $properties)
    {
        $new_properties_ids = [];
        if (isset($properties)) {
            foreach ($properties as $property_attributes) {
                if (isset($property_attributes['property_id']) && $property_attributes['property_id'] > 0) {
                    $new_properties_ids[] = intval($property_attributes['property_id']);
                }
            }
        }

        $old_properties_ids = $realEstateProfile->properties->pluck('id')->toArray();
        $deleted_properties_ids = array_diff($old_properties_ids, $new_properties_ids);

        foreach ($deleted_properties_ids as $property_id) {
            $property = Property::find($property_id);
            $property->delete();
        }
    }

    private function updateBusinessProfile(BusinessProfile $businessProfile, array $attributes)
    {
//        dd($attributes);
        $businessProfile->profile_creator_role = $attributes['profile_creator_role'];
//        $businessProfile->profile_creator_decision_maker = $attributes['profile_creator_decision_maker'];

        $businessProfile->business_name = $attributes['business_name'];
        $businessProfile->description = $attributes['description'];

        $businessProfile->state_code = $attributes['state'];
        $businessProfile->city = $attributes['city'];
        $businessProfile->zip_code = $attributes['zip_code'];
        $businessProfile->address = $attributes['address'];
        $businessProfile->apt_suite_no = $attributes['apt_suite_no'];

        $businessProfile->phone = $attributes['phone'];
        $businessProfile->establish_year = $attributes['establish_year'];
        $businessProfile->fisical_year_end = $attributes['fisical_year_end'];

        $businessProfile->annual_revenue_year_1 = $attributes['annual_revenue_year_1'];
        $businessProfile->annual_revenue_year_2 = $attributes['annual_revenue_year_2'];
        $businessProfile->annual_revenue_year_3 = $attributes['annual_revenue_year_3'];

        $businessProfile->net_income_before_tax_year_1 = $attributes['net_income_before_tax_year_1'];
        $businessProfile->net_income_before_tax_year_2 = $attributes['net_income_before_tax_year_2'];
        $businessProfile->net_income_before_tax_year_3 = $attributes['net_income_before_tax_year_3'];

        $businessProfile->income_tax_expense_year_1 = $attributes['income_tax_expense_year_1'];
        $businessProfile->income_tax_expense_year_2 = $attributes['income_tax_expense_year_2'];
        $businessProfile->income_tax_expense_year_3 = $attributes['income_tax_expense_year_3'];

        $businessProfile->interest_expense_year_1 = $attributes['interest_expense_year_1'];
        $businessProfile->interest_expense_year_2 = $attributes['interest_expense_year_2'];
        $businessProfile->interest_expense_year_3 = $attributes['interest_expense_year_3'];

        $businessProfile->depreciation_amortization_year_1 = $attributes['depreciation_amortization_year_1'];
        $businessProfile->depreciation_amortization_year_2 = $attributes['depreciation_amortization_year_2'];
        $businessProfile->depreciation_amortization_year_3 = $attributes['depreciation_amortization_year_3'];

        $businessProfile->company_EBITDA_year_1 = $attributes['company_EBITDA_year_1'];
        $businessProfile->company_EBITDA_year_2 = $attributes['company_EBITDA_year_2'];
        $businessProfile->company_EBITDA_year_3 = $attributes['company_EBITDA_year_3'];

        $businessProfile->company_EBITDA = $attributes['company_EBITDA'];

        $businessProfile->save();
    }

    public function delete(User $user, BorrowerProfile $profile)
    {
        try {
            return new Output(true, 200, 'common', 'deleted');
        } catch (\Exception $ex) {
            return new Output(false, 500, 'common', 'failed');
        }
    }


    public function changeStatus(BorrowerProfile $borrowerProfile, array $attributes)
    {
        try {
            $borrowerProfile->is_enabled = $attributes['is_enabled'];
            $updated = $borrowerProfile->save();

            if (!$updated) {
                return new Output(false, 500, 'common', 'failed');
            }

            return new Output(true, 200, 'common', 'updated');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function getBorrowerLoansCount(User $borrower)
    {
        $loans = Loan::where('borrower_id', $borrower->id)->get()->count();
        return $loans;
    }

    public function getBorrowerBusinessLoansCount(User $borrower)
    {
        $businessProfileId = BorrowerProfile::select('id')->where('user_id', $borrower->id)->where('profile_type', 'BUSINESS')->get();
        $loans = Loan::select('id')->where('borrower_id', $borrower->id)->whereIn('profile_id', $businessProfileId)->get()->count();

        return $loans;
    }

    public function getBorrowerRealEstateLoansCount(User $borrower)
    {
        $realEstateProfileId = BorrowerProfile::select('id')->where('user_id', $borrower->id)->where('profile_type', 'REAL_ESTATE')->get();
        $loans = Loan::select('id')->where('borrower_id', $borrower->id)->whereIn('profile_id', $realEstateProfileId)->get()->count();

        return $loans;
    }
}