<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/17/17
 * Time: 6:40 PM
 */

namespace App\Services;

use App\Enums\ActivationStates;
use App\Helpers\Output;
use App\Models\Plan;
use App\Models\User;
class SubscriptionService
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function subscribe(User $user, $attributes)
    {

        try {

            // get the plan after submitting the form
            $plan = Plan::where('slug', 'premium-plan')->first();

            if ($user->subscribed('main')) {
                $user->subscription('main')->cancel();
            }
            // subscribe the user
            $subscription = $user->newSubscription('main', $plan->braintree_plan)->create($attributes['payment_method_nonce']);

            if (isset($subscription) && isset($subscription['id'])) {

                $next_payment_due = $subscription['created_at']->addMonth();

                $user = User::find($user->id);

                $this->userService->update($user, ['status' => ActivationStates::ACTIVE, 'next_payment_due' => $next_payment_due]);

            }

            return new Output(true, 201, 'common', 'succeeded', $subscription);

        } catch (\Exception $e) {

            if ($user->subscribed('main')) {
                $user->subscription('main')->resume();
            }

            $user = User::find($user->id);

            $this->userService->update($user, ['status' => ActivationStates::SUSPENDED]);
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }
}
