<?php
namespace App\Services;

use App\Events\UserLoggedIn;
use App\Models\User;
use Sentinel;
use Activation;
use App\Helpers\Output;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;

class LoginService
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function loginUser($by_credentials, array $attributes, User $user = null)
    {
        //check user credentials
        if ($by_credentials) {
            $user = $this->validateCredentials($attributes);
            if (!$user) {
                return new Output(false, 401, '', 'invalid_login');
            }
        }

        if (!Activation::completed($user)) {
            throw new NotActivatedException;
        }
        //check if user & domain is not blocked
        if ($user->inRole(RoleService::ROLE_LENDER)) {
            $check_domain = $this->userService->checkUserDomaindState($user);
            if (!$check_domain[0]) {
                return new Output(false, 403, '', $check_domain[1]);
            }

            $check_user = $this->userService->checkUserState($user);

            if (!$check_user[0]) {
                return new Output(false, 403, '', $check_user[1]);
            }
        }

        if ($by_credentials) {
            $user = $this->validateLogin($attributes);
        }
        if (!$user) {
            return new Output(false, 401, '', 'invalid_login');
        }

        Sentinel::login($user);
        event(new UserLoggedIn($user));

        return new Output(true, 200, 200);
    }

    protected function validateLogin($attributes)
    {
        // Login credentials
        $credentials = array(
            'login' => $attributes['email'],
            'password' => $attributes['password'],
        );

        // Authenticate the user
        $user = null;
        if (isset($attributes['remember']) && $attributes['remember'] = 1) {
            $user = Sentinel::authenticateAndRemember($credentials);
        } else {
            $user = Sentinel::authenticate($credentials);
        }
        return $user;
    }

    protected function validateCredentials($attributes)
    {
        $credentials = [
            'login' => $attributes['email'],
        ];
        $validate_credentials = [
            'login' => $attributes['email'],
            'password' => $attributes['password'],
        ];

        $user = Sentinel::findByCredentials($credentials);
        if (!$user) {
            return false;
        }

        $valid = Sentinel::validateCredentials($user, $validate_credentials);

        if (!$valid) {
            return false;
        }
        return $user;
    }

}