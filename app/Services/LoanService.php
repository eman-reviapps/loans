<?php

namespace App\Services;

use App\Enums\BidStatuses;
use App\Enums\BorrowerProfileTypes;
use App\Enums\LoanStatuses;
use App\Enums\LoanTypes;
use App\Events\BidPlaced;
use App\Events\BorrowerNotInterested;
use App\Events\PostEdited;
use App\Helpers\Output;
use App\Models\Attachment;
use App\Models\Bid;
use App\Models\BorrowerProfile;
use App\Models\BusinessProfile;
use App\Models\City;
use App\Models\Conversation;
use App\Models\LeaseType;
use App\Models\LenderProfile;
use App\Models\Loan;
use App\Models\LoanArchive;
use App\Models\LoanDetail;
use App\Models\LoanFeedback;
use App\Models\Preference;
use App\Models\RealEstateProfile;
use App\Models\Reply;
use App\Models\Tenant;
use App\Models\User;
use Carbon\Carbon;
use DB;
use PhpParser\Node\Expr\Cast\Int_;
use Sentinel;

class LoanService
{
    protected $preferencesService;

    public function __construct(PreferencesService $preferencesService)
    {
        $this->preferencesService = $preferencesService;
    }

    public function getLoanTemplate($loan_type)
    {
        switch ($loan_type) {
            case LoanTypes::LINE_OF_CREDIT:
                $view = 'line_of_credit';
                break;
            case LoanTypes::RE_PURCHASE_INVESTOR_0_50:
                $view = 're_purchase_investor';
                break;
            case LoanTypes::RE_PURCHASE_OWNER_51_100:
                $view = 're_purchase_owner';
                break;
            case LoanTypes::RE_REFINANCE_INVESTOR_0_50:
                $view = 're_refinance_investor';
                break;
            case LoanTypes::RE_REFINANCE_OWNER_51_100:
                $view = 're_refinance_owner';
                break;
            case LoanTypes::RE_CASH_OUT_INVESTOR_0_50:
                $view = 're_cash_out_investor';
                break;
            case LoanTypes::RE_CASH_OUT_OWNER_51_100:
                $view = 're_cash_out_owner';
                break;
            case LoanTypes::EQUIPMENT_FINANCE:
                $view = 'equipment_finance';
                break;
            case LoanTypes::TERM_LOAN_OTHER:
                $view = 'term_loan_other';
                break;
            default:
                break;
        }
        return $view;
    }

    public function create(User $user, BorrowerProfile $borrowerProfile, $attributes)
    {
        DB::beginTransaction();
        try {
            $loan = new Loan;

            $loan->borrower_id = $user->id;
            $loan->profile_id = $borrowerProfile->id;
            $loan->loan_type = $attributes['loan_type_slug'];
            $loan->status = isset($attributes['loan_status']) ? $attributes['loan_status'] : LoanStatuses::LIVE;
            $loan->loan_request_details = isset($attributes['loan_request_details']) ? $attributes['loan_request_details'] : null;

            $loan->save();

            if (!$loan) {
                DB::rollback();
                return new Output(false, 500, 'common', 'failed');
            }

            $loan_details = $this->saveLoanDetails($loan, $attributes);
            if (!$loan_details) {
                DB::rollback();
                return new Output(false, 500, 'common', 'failed');
            }

            if (isset($attributes['properties'])) {
                $this->syncLoanTenants($loan, $attributes['properties']);
            }

            if (isset($attributes['files']) && count($attributes['files']) > 0) {
                $sync_atatchments = $this->syncLoanAttachments($loan, $attributes['files']);

                if (!$sync_atatchments) {
                    DB::rollback();
                    return new Output(false, 500, 'common', 'failed');
                }
            }

            $this->preferencesService->createPreferences($attributes, $user, $borrowerProfile, $loan);

            DB::commit();

            return new Output(true, 201, 'common', 'created', $loan_details);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }

    }

    public function update(Loan $loan, User $user, BorrowerProfile $borrowerProfile, $attributes)
    {
        DB::beginTransaction();
        try {

            $loan->loan_request_details = isset($attributes['loan_request_details']) ? $attributes['loan_request_details'] : null;
            $loan->save();

            $loan_details = $this->saveLoanDetails($loan, $attributes);
            if (!$loan_details) {
                DB::rollback();
                return new Output(false, 500, 'common', 'failed');
            }

            if (isset($attributes['properties'])) {
                $this->syncLoanTenants($loan, $attributes['properties']);
            }

            if (isset($attributes['files']) && count($attributes['files']) > 0) {
                $sync_atatchments = $this->syncLoanAttachments($loan, $attributes['files']);

                if (!$sync_atatchments) {
                    DB::rollback();
                    return new Output(false, 500, 'common', 'failed');
                }
            }

            $this->preferencesService->updatePreferences(
                array_intersect_key($attributes, array_flip(PreferenceOptions::options()))
                , $user, $borrowerProfile, $loan);

            DB::commit();

            event(new PostEdited($loan));
            return new Output(true, 201, 'common', 'created', $loan_details);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function delete(Loan $loan, array $attributes)
    {
//        return new Output(true, 200, 'common', 'updated');

        try {
            $loan->status = $attributes['status'];
            $loan->save();

            if (!$loan) {
                return new Output(false, 500, 'common', 'failed');
            }

            $feedback = new LoanFeedback;
            $feedback->loan_id = $loan->id;
            $feedback->feedback_loan_status = $attributes['status'];
            $feedback->reason = $attributes['reason'];
            $feedback->description = isset($attributes['description']) ? $attributes['description'] : '';

            $feedback->save();

            if (!$feedback) {
                return new Output(false, 500, 'common', 'failed');
            }

            return new Output(true, 200, 'common', 'updated');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function updateLoan(Loan $loan, array $attributes)
    {
        try {
            if (isset($attributes['status']))
                $loan->status = $attributes['status'];

            $loan->save();

            if (!$loan) {
                return new Output(false, 500, 'common', 'failed');
            }

            return new Output(true, 200, 'common', 'updated');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function archiveLoan(Loan $loan, User $user)
    {
        try {
            $archived = LoanArchive::create([
                "loan_id" => $loan->id,
                "user_id" => $user->id
            ]);

            if (!$archived) {
                return new Output(false, 500, 'common', 'failed');
            }

            return new Output(true, 200, 'common', 'updated');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function restoreLoan(LoanArchive $archive)
    {
        try {
            $archive->delete();

            return new Output(true, 200, 'common', 'updated');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function isThereOtherBidsPlaced(Loan $loan, $not_lender_id)
    {
        $lender = User::find($not_lender_id);

        $placed_bids = Bid::
        where('loan_id', $loan->id)
            ->whereNotIn('lender_id', [$not_lender_id])
            ->whereHas('lender.lenderProfile.institution', function ($query) use ($lender) {
                $query->where('id', $lender->lenderProfile->institution->id);
            })
            ->get();

        if (count($placed_bids) > 0) {
            $msg = "";
            foreach ($placed_bids as $placed_bid) {
                $msg .= "Lender '" . $placed_bid->lender->full_name . " - " . $placed_bid->lender->lenderProfile->title . "' has bid on this already . \n";
            }
            return new Output(true, 200, 'common', 'succeeded', $msg);
        } else {
            return new Output(false, 200, 'common', 'succeeded');
        }
    }


    private function saveLoanDetails(Loan $loan, $attributes)
    {
        if ($loan->details)
            $loan_details = LoanDetail::find($loan->details->id);
        else {
            $loan_details = new LoanDetail;
            $loan_details->loan_id = $loan->id;
        }

        $loan_details = $this->prepareLoanDetails($attributes, $loan_details);

        $loan_details->save();

        return $loan_details;
    }

    private
    function syncLoanTenants(Loan $loan, $properties)
    {
        foreach ($properties as $property) {
            $lease_type = isset($property['lease_type']) ? LeaseType::where('slug', $property['lease_type'])->first() : false;

            if (isset($property['tenant_id']) && null != $property['tenant_id']) {
                $tenant = Tenant::find($property['tenant_id']);
            } else {
                $tenant = new Tenant;
            }
            $tenant->loan_id = $loan->id;
            $tenant->name = $property['name'];
            $tenant->lease_type_id = $lease_type ? $lease_type->id : null;
            $tenant->space_occupied = $property['space_occupied'];
            $tenant->lease_rate = $property['lease_rate'];
            $tenant->lease_expiration_date = Carbon::createFromFormat('m/d/Y', $property['lease_expiration_date'])->toDateTimeString();

            $tenant->save();
        }
    }

    private function syncLoanAttachments(Loan $loan, $files)
    {
        foreach ($files as $file) {

            $attachment = new Attachment;

            $attachment->url = $file;

            $loan->attachments()->save($attachment);
        }
        return true;
    }

    private
    function prepareLoanDetails($attributes, $loan_details)
    {
        switch ($attributes['loan_type_slug']) {
            case LoanTypes::LINE_OF_CREDIT:

                $loan_details->requested_amount = $attributes['requested_amount'];
                $loan_details->purpose_type_id = $attributes['purpose_type_id'];
                $loan_details->purpose = isset($attributes['purpose']) ? $attributes['purpose'] : null;
                $loan_details->receivable_range_from = $attributes['receivable_range_from'];
                $loan_details->receivable_range_to = $attributes['receivable_range_to'];
                $loan_details->inventory_range_from = $attributes['inventory_range_from'];
                $loan_details->inventory_range_to = $attributes['inventory_range_to'];
                $loan_details->current_ratio = isset($attributes['current_ratio']) ? $attributes['current_ratio'] : null;

                break;
            case LoanTypes::RE_PURCHASE_INVESTOR_0_50:

                $loan_details = $this->setAddressAttributes($attributes, $loan_details);

                $loan_details->real_estate_type_id = $attributes['real_estate_type_id'];
                $loan_details->purchase_price = $attributes['purchase_price'];
                $loan_details->requested_amount = $attributes['requested_amount'];
                $loan_details->requested_percent = $attributes['requested_percent'];

                $loan_details->building_sq_ft = isset($attributes['building_sq_ft']) ? $attributes['building_sq_ft'] : null;
                $loan_details->buildings_no = isset($attributes['buildings_no']) ? $attributes['buildings_no'] : null;
                $loan_details->units_no = isset($attributes['units_no']) ? $attributes['units_no'] : null;

                $loan_details->owner_occupied_percent = $attributes['owner_occupied_percent'];

                $loan_details->gross_annual_revenue = $attributes['gross_annual_revenue'];
                $loan_details->estimated_annual_expenses = isset($attributes['estimated_annual_expenses']) ? $attributes['estimated_annual_expenses'] : null;
                $loan_details->estimated_net_operating_income = isset($attributes['estimated_net_operating_income']) ? $attributes['estimated_net_operating_income'] : null;

                break;
            case LoanTypes::RE_CASH_OUT_INVESTOR_0_50:

                $loan_details = $this->setAddressAttributes($attributes, $loan_details);

                $loan_details->real_estate_type_id = $attributes['real_estate_type_id'];
                $loan_details->requested_amount = $attributes['requested_amount'];
                $loan_details->estimated_property_value = $attributes['estimated_property_value'];
                $loan_details->purpose = $attributes['purpose'];

                $loan_details->building_sq_ft = isset($attributes['building_sq_ft']) ? $attributes['building_sq_ft'] : null;
                $loan_details->buildings_no = isset($attributes['buildings_no']) ? $attributes['buildings_no'] : null;
                $loan_details->units_no = isset($attributes['units_no']) ? $attributes['units_no'] : null;

                $loan_details->is_loan_outstanding = $attributes['is_loan_outstanding'];
                $loan_details->outstanding_value = $attributes['outstanding_value'];

                $loan_details->owner_occupied_percent = $attributes['owner_occupied_percent'];
                $loan_details->gross_annual_revenue = $attributes['gross_annual_revenue'];
                $loan_details->estimated_annual_expenses = $attributes['estimated_annual_expenses'];
                $loan_details->net_operating_income = $attributes['net_operating_income'];

                break;
            case LoanTypes::RE_CASH_OUT_OWNER_51_100:

                $loan_details = $this->setAddressAttributes($attributes, $loan_details);

                $loan_details->real_estate_type_id = $attributes['real_estate_type_id'];
                $loan_details->requested_amount = $attributes['requested_amount'];
                $loan_details->estimated_property_value = $attributes['estimated_property_value'];
                $loan_details->purpose = $attributes['purpose'];

                $loan_details->building_sq_ft = isset($attributes['building_sq_ft']) ? $attributes['building_sq_ft'] : null;
                $loan_details->buildings_no = isset($attributes['buildings_no']) ? $attributes['buildings_no'] : null;
                $loan_details->units_no = isset($attributes['units_no']) ? $attributes['units_no'] : null;

                $loan_details->is_loan_outstanding = $attributes['is_loan_outstanding'];
                $loan_details->outstanding_value = $attributes['outstanding_value'];

                $loan_details->annual_income_from_tenants = $attributes['annual_income_from_tenants'];

                break;
            case LoanTypes::RE_PURCHASE_OWNER_51_100:

                $loan_details = $this->setAddressAttributes($attributes, $loan_details);

                $loan_details->real_estate_type_id = $attributes['real_estate_type_id'];
                $loan_details->purchase_price = $attributes['purchase_price'];
                $loan_details->requested_amount = $attributes['requested_amount'];
                $loan_details->requested_percent = $attributes['requested_percent'];

                $loan_details->building_sq_ft = isset($attributes['building_sq_ft']) ? $attributes['building_sq_ft'] : null;
                $loan_details->buildings_no = isset($attributes['buildings_no']) ? $attributes['buildings_no'] : null;
                $loan_details->units_no = isset($attributes['units_no']) ? $attributes['units_no'] : null;

                $loan_details->owner_occupied_percent = $attributes['owner_occupied_percent'];

                $loan_details->is_moving_from_leased = $attributes['is_moving_from_leased'];
                $loan_details->current_lease_expense = $attributes['current_lease_expense'];

                $loan_details->annual_income_from_tenants = isset($attributes['annual_income_from_tenants']) ? $attributes['annual_income_from_tenants'] : null;

                break;
            case LoanTypes::RE_REFINANCE_INVESTOR_0_50:

                $loan_details = $this->setAddressAttributes($attributes, $loan_details);

                $loan_details->real_estate_type_id = $attributes['real_estate_type_id'];
                $loan_details->requested_amount = $attributes['requested_amount'];
                $loan_details->estimated_market_price = $attributes['estimated_market_price'];

                $loan_details->is_loan_outstanding = $attributes['is_loan_outstanding'];
                $loan_details->outstanding_value = $attributes['outstanding_value'];

                $loan_details->purpose = $attributes['purpose'];

                $loan_details->building_sq_ft = isset($attributes['building_sq_ft']) ? $attributes['building_sq_ft'] : null;
                $loan_details->buildings_no = isset($attributes['buildings_no']) ? $attributes['buildings_no'] : null;
                $loan_details->units_no = isset($attributes['units_no']) ? $attributes['units_no'] : null;

                $loan_details->owner_occupied_percent = $attributes['owner_occupied_percent'];
                $loan_details->gross_annual_revenue = $attributes['gross_annual_revenue'];
                $loan_details->estimated_annual_expenses = $attributes['estimated_annual_expenses'];
                $loan_details->net_operating_income = $attributes['net_operating_income'];

                break;
            case LoanTypes::RE_REFINANCE_OWNER_51_100:

                $loan_details = $this->setAddressAttributes($attributes, $loan_details);

                $loan_details->real_estate_type_id = $attributes['real_estate_type_id'];
                $loan_details->requested_amount = $attributes['requested_amount'];

                $loan_details->estimated_market_price = $attributes['estimated_market_price'];

                $loan_details->is_loan_outstanding = $attributes['is_loan_outstanding'];
                $loan_details->outstanding_value = $attributes['outstanding_value'];

                $loan_details->purpose = $attributes['purpose'];

                $loan_details->building_sq_ft = isset($attributes['building_sq_ft']) ? $attributes['building_sq_ft'] : null;
                $loan_details->buildings_no = isset($attributes['buildings_no']) ? $attributes['buildings_no'] : null;
                $loan_details->units_no = isset($attributes['units_no']) ? $attributes['units_no'] : null;

                $loan_details->owner_occupied_percent = $attributes['owner_occupied_percent'];

                $loan_details->annual_income_from_tenants = $attributes['annual_income_from_tenants'];

                break;
            case LoanTypes::EQUIPMENT_FINANCE:

                $loan_details->equipment_type = $attributes['equipment_type'];
                $loan_details->vendor_name = $attributes['vendor_name'];
                $loan_details->equipment_purpose = $attributes['equipment_purpose'];
                $loan_details->equipment_description = $attributes['equipment_description'];
                $loan_details->equipment_finance_type = $attributes['equipment_finance_type'];

                $loan_details->purchase_price = $attributes['purchase_price'];
                $loan_details->less_trade = $attributes['less_trade'];
                $loan_details->less_down_payment = $attributes['less_down_payment'];
                $loan_details->requested_amount = $attributes['requested_amount'];

                break;
            case LoanTypes::TERM_LOAN_OTHER:

                $loan_details->requested_amount = $attributes['requested_amount'];
                $loan_details->term_other_loan_type = $attributes['term_other_loan_type'];

                break;
            default:
                break;
        }

        return $loan_details;
    }

    /**
     * @param $attributes
     * @param $loan_details
     */
    private function setAddressAttributes($attributes, $loan_details)
    {
        $loan_details->state_code = $attributes['state'];
        $loan_details->city = $attributes['city'];
        $loan_details->zip_code = $attributes['zip_code'];
        $loan_details->address = $attributes['address'];
        $loan_details->apt_suite_no = $attributes['apt_suite_no'];

        return $loan_details;
    }

    public function lenderLoans(User $lender)
    {
        $query = Loan::with(['bids' => function ($query) use ($lender) {
            $query->where('lender_id', $lender->id);
        }])->whereHas('bids', function ($query) use ($lender) {
            $query->where('lender_id', $lender->id);
        })->whereDoesntHave('archived', function ($query) use ($lender) {
            $query->where('user_id', $lender->id);
        });


        return $query->where('status', '<>', 'DELETED')->latest();;
    }

    public static function getLenderLoansCount(User $lender)
    {
        $query = Loan::with(['bids' => function ($query) use ($lender) {
            $query->where('lender_id', $lender->id);
        }])->whereHas('bids', function ($query) use ($lender) {
            $query->where('lender_id', $lender->id);
        })->whereDoesntHave('archived', function ($query) use ($lender) {
            $query->where('user_id', $lender->id);
        });
        return $query->get()->count();
    }

    public static function getLenderBorrowersCount(User $lender)
    {
        $loans = Bid::select('loan_id')->where('lender_id', $lender->id)->distinct("loan_id")->get();

        $lenderBorrowers = Loan::select('borrower_id')->whereIn('id', $loans)->distinct('borrower_id')->get()->count();

        return $lenderBorrowers;
    }

    public function findLoansQuery()
    {

    }

    public function findLoans(User $lender, array $attributes = [])
    {
        $lenders = LenderProfile::select('user_id')->where('institution_id', function ($query) use ($lender) {
            $query->select('institution_id')->from('lender_profiles')->where('user_id', $lender->id);
        })->get();

        DB::enableQueryLog();
        $query = Loan::
        with(['bids' => function ($query) use ($lenders) {
            $query->where('lender_id', $lenders);
        }])
            ->with('details')
            ->published()
            ->whereDoesntHave('archived', function ($query) use ($lender) {
                $query->where('user_id', $lender->id);
            })
            ->whereDoesntHave('bids', function ($query) use ($lender) {
                $query->where('lender_id', $lender->id);
            });

        $lender_preference_loan_types = isset($attributes['FAVORITE_LOAN_TYPES']) ? $attributes['FAVORITE_LOAN_TYPES'] : [];
        $lender_preference_loan_min = isset($attributes['FAVORITE_LOAN_SIZE_MIN']) ? strtr($attributes['FAVORITE_LOAN_SIZE_MIN'], array('.' => '', ',' => '')) : "";
        $lender_preference_loan_max = isset($attributes['FAVORITE_LOAN_SIZE_MAX']) ? strtr($attributes['FAVORITE_LOAN_SIZE_MAX'], array('.' => '', ',' => '')) : "";
        $lender_preference_state = isset($attributes['FAVORITE_STATE']) ? $attributes['FAVORITE_STATE'] : "";
        $lender_preference_city = isset($attributes['FAVORITE_CITY']) ? $attributes['FAVORITE_CITY'] : "";
        $lender_preference_counties = isset($attributes['FAVORITE_LOCATION']) ? $attributes['FAVORITE_LOCATION'] : "";

        //apply borrower preferences
        $query->whereIn('id', function ($query) use ($lender) {
            $query->select('loan_id')
                ->from(with(new Preference())->getTable())
                ->whereNotNull('loan_id')
                ->where(function ($query) use ($lender) {
                    $query->where('slug', PreferenceOptions::FAVORITE_INSTITUTION_TYPES)
                        ->whereRaw("value = '' or value is null");
                    if ($lender->lenderProfile->institution->type_id) {
                        $query->orwhereRaw("value REGEXP '^\"'+'" . $lender->lenderProfile->institution->type_id . "'+'\"$'");
                    }
                });
        });

        $query->whereIn('id', function ($query) use ($lender) {
            $query->select('loan_id')
                ->from(with(new Preference())->getTable())
                ->whereNotNull('loan_id')
                ->whereRaw("slug = 'FAVORITED_LENDERS' and value = ''")
                ->orwhereRaw("slug = 'FAVORITED_LENDERS' and value is null")
                ->orwhereRaw("slug = 'FAVORITED_LENDERS' and value REGEXP '^\"'+'" . $lender->lenderProfile->institution->id . "'+'\"$'");
        });

        $query->whereNotIn('id', function ($query) use ($lender) {
            $query->select('loan_id')
                ->from(with(new Preference())->getTable())
                ->whereNotNull('loan_id')
                ->whereRaw("slug = 'NON_FAVORITED_LENDERS' and value REGEXP '^\"'+'" . $lender->lenderProfile->institution->id . "'+'\"$'");
        });

        //apply lender preferences
        if ($lender_preference_loan_types && $lender_preference_loan_types != "") {
            $query->whereIn('loan_type', $lender_preference_loan_types);
        }

        if ($lender_preference_loan_min && $lender_preference_loan_min != "") {
            $query->when($lender_preference_loan_min, function ($query) use ($lender_preference_loan_min) {
                $query->whereHas('details', function ($query) use ($lender_preference_loan_min) {
                    $query->where('requested_amount', '>=', $lender_preference_loan_min);
                });
            });
        }

        if ($lender_preference_loan_max && $lender_preference_loan_max != "" && $lender_preference_loan_max != 0) {
            $query->whereHas('details', function ($query) use ($lender_preference_loan_max) {
                $query->where('requested_amount', '<=', $lender_preference_loan_max);
            });
        }

        if (($lender_preference_state && $lender_preference_state != "") || ($lender_preference_city && $lender_preference_city != "") || ($lender_preference_counties && $lender_preference_counties != "")) {
            $query->where(function ($query) use ($lender_preference_state, $lender_preference_city, $lender_preference_counties) {
                $query->whereHas('borrower_profile', function ($query) use ($lender_preference_state, $lender_preference_city, $lender_preference_counties) {
                    $query->where(function ($query) use ($lender_preference_state, $lender_preference_city, $lender_preference_counties) {
                        $query->whereRaw("borrower_profiles.profile_type = '" . BorrowerProfileTypes::REAL_ESTATE . "'")
                            ->whereIn('profile_id', function ($query) use ($lender_preference_state, $lender_preference_city, $lender_preference_counties) {
                                $query->select('id')
                                    ->from(with(new RealEstateProfile())->getTable())
                                    ->whereRaw("borrower_profiles.profile_id = real_estate_profiles.id")
                                    ->when($lender_preference_state, function ($query) use ($lender_preference_state) {
                                        return $query->whereIn('state_code', $lender_preference_state);
                                    })->when($lender_preference_city, function ($query) use ($lender_preference_city) {
                                        return $query->whereIn('city', $lender_preference_city);
                                    })->when($lender_preference_counties, function ($query) use ($lender_preference_counties) {
                                        $query->whereIn('city', function ($query) use ($lender_preference_counties) {
                                            $query->select('city')
                                                ->from('cities')
                                                ->whereIn('county', $lender_preference_counties);
                                        })->whereIn('zip_code', function ($query) use ($lender_preference_counties) {
                                            $query->select('zip_code')
                                                ->from('cities')
                                                ->whereIn('county', $lender_preference_counties);
                                        });
                                    });

                            });
                    })
                        ->orWhere(function ($query) use ($lender_preference_state, $lender_preference_city, $lender_preference_counties) {
                            $query->whereRaw("borrower_profiles.profile_type = '" . BorrowerProfileTypes::BUSINESS . "'")
                                ->whereIn('profile_id', function ($query) use ($lender_preference_state, $lender_preference_city, $lender_preference_counties) {
                                    $query->select('id')
                                        ->from(with(new BusinessProfile())->getTable())
                                        ->whereRaw("borrower_profiles.profile_id = business_profiles.id")
                                        ->when($lender_preference_state, function ($query) use ($lender_preference_state) {
                                            return $query->whereIn('state_code', $lender_preference_state);
                                        })->when($lender_preference_city, function ($query) use ($lender_preference_city) {
                                            return $query->whereIn('city', $lender_preference_city);
                                        })->when($lender_preference_counties, function ($query) use ($lender_preference_counties) {
                                            $query->whereIn('city', function ($query) use ($lender_preference_counties) {
                                                $query->select('city')
                                                    ->from('cities')
                                                    ->whereIn('county', $lender_preference_counties);
                                            })->whereIn('zip_code', function ($query) use ($lender_preference_counties) {
                                                $query->select('zip_code')
                                                    ->from('cities')
                                                    ->whereIn('county', $lender_preference_counties);
                                            });
                                        });
                                });
                        });
                });
            })
//                ->orWhere(function ($query) use ($lender_preference_state,$lender_preference_city,$lender_preference_counties) {
//                $query->whereHas('details', function ($query) use ($lender_preference_state,$lender_preference_city,$lender_preference_counties) {
//                    $query->whereNotIn('loan_type', [LoanTypes::LINE_OF_CREDIT, LoanTypes::EQUIPMENT_FINANCE, LoanTypes::TERM_LOAN_OTHER])
//                        ->when($lender_preference_state, function ($query) use ($lender_preference_state) {
//                            return $query->whereIn('state_code', $lender_preference_state);
//                        })->when($lender_preference_city, function ($query) use ($lender_preference_city) {
//                            return $query->whereIn('city', $lender_preference_city);
//                        })->when($lender_preference_counties, function ($query) use ($lender_preference_counties) {
//                            $query->whereIn('city', function ($query) use ($lender_preference_counties) {
//                                $query->select('city')
//                                    ->from('cities')
//                                    ->whereIn('county',$lender_preference_counties);
//                            })->whereIn('zip_code', function ($query) use ($lender_preference_counties) {
//                                $query->select('zip_code')
//                                    ->from('cities')
//                                    ->whereIn('county',$lender_preference_counties);
//                            });
//                        });
//                })->whereHas('borrower_profile', function ($query) use ($lender_preference_state) {
//                    $query->whereRaw("borrower_profiles.profile_type = '" . BorrowerProfileTypes::BUSINESS . "'");
//                });
//            })
            ;
        }

//        if ($lender_preference_counties && $lender_preference_counties != "") {
//            $query->whereHas('details', function ($query) use ($lender_preference_counties) {
//                $query->whereIn('county', $lender_preference_counties);
//            });
//        }

//        $query->get();
//        dd(DB::getQueryLog());
        return $query->latest();
    }

    public
    function archived(User $user, array $attributes = [])
    {
        return LoanArchive::where('user_id', $user->id)->get();
    }

    /**
     * @param User $lender
     */
    public
    function getLenderPreferences(User $lender)
    {
        $lender_preferences = $this->preferencesService->getUserPreferencesBySlug(['key_by' => 'slug'], $lender);

        $lender_preference_loan_types = $lender_preferences[PreferenceOptions::FAVORITE_LOAN_TYPES];
        $lender_preference_loan_min = $lender_preferences[PreferenceOptions::FAVORITE_LOAN_SIZE_MIN];
        $lender_preference_loan_max = $lender_preferences[PreferenceOptions::FAVORITE_LOAN_SIZE_MAX];
        $lender_preference_state = $lender_preferences[PreferenceOptions::FAVORITE_STATE];
        $lender_preference_city = $lender_preferences[PreferenceOptions::FAVORITE_CITY];
        $lender_preference_counties = $lender_preferences[PreferenceOptions::FAVORITE_LOCATION];

        return [
            $lender_preference_loan_types,
            $lender_preference_loan_min,
            $lender_preference_loan_max,
            $lender_preference_state,
            $lender_preference_city,
            $lender_preference_counties
        ];
    }


    public
    function getLoanDetails($loan_id, User $user)
    {
        if ($user->inRole(RoleService::ROLE_LENDER)) {

            $loan = Loan::where('id', $loan_id)
                ->with(['bids' => function ($query) use ($user) {
                    $query->where('lender_id', $user->id);
                }])
                ->
                with(['archived' => function ($query) use ($user, $loan_id) {
                    $query->where('user_id', $user->id)
                        ->where('loan_id', $loan_id);

                }])->first();

            $archive = LoanArchive::where('user_id', $user->id)
                ->where('loan_id', $loan_id)->get();

            return [$loan, $archive];
        }

    }

    public
    function didBorrowerReply(Bid $bid)
    {
        $count = $bid->conversation->whereHas('replies', function ($query) use ($bid) {
            $query->where('sender_id', $bid->loan->borrower_id)
                ->where('bid_id', $bid->loan->id);
        })->count();

        return $count > 0 ? true : false;
    }

    public
    function placeBid(Loan $loan, User $lender, array $attributes)
    {
        DB::beginTransaction();

        $bids_count = count($loan->bids);

        try {
            $bid = new Bid;
            $bid->loan_id = $loan->id;
            $bid->lender_id = $lender->id;
            $bid->status = BidStatuses::NO_RESPONSE;
            $bid->rate = $attributes['rate'];
            $bid->term = $attributes['term'];
            $bid->term_unit = $attributes['term_unit'];
            $bid->amortization = $attributes['amortization'];
            $bid->amortization_unit = $attributes['amortization_unit'];
            $bid->descripton = $attributes['description'];

            $bid->save();

            if (!$bid) {
                DB::rollback();
                return new Output(false, 500, 'common', 'failed');
            }

            $conversation = new Conversation;
            $conversation->bid_id = $bid->id;
            $conversation->sender_id = $lender->id;
            $conversation->receiver_id = $loan->borrower->id;
            $conversation->is_read = false;

            $conversation->save();

            if (!$conversation) {
                DB::rollback();
                return new Output(false, 500, 'common', 'failed');
            }

            $message_body = trans('loan_messages.bid_message', [
                "user_name" => $loan->borrower->first_name,
            ]);

            $reply = new Reply;
            $reply->conversation_id = $conversation->id;
            $reply->sender_id = $lender->id;
            $reply->receiver_id = $loan->borrower->id;
            $reply->message = $message_body;
            $reply->is_read = false;

            $reply->save();

            if (!$reply) {
                DB::rollback();
                return new Output(false, 500, 'common', 'failed');
            }

            if ($bids_count == 0) {
                $loan->status = LoanStatuses::LENDER_BID;
                $loan->save();

                if (!$loan) {
                    DB::rollback();
                    return new Output(false, 500, 'common', 'failed');
                }
            }

            DB::commit();

            event(new BidPlaced($loan->borrower, $bid));

            return new Output(true, 200, 'common', 'updated');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public
    function borrowerNotInterested(Bid $bid)
    {
        $bid->status = BidStatuses::BORROWER_NOT_INTERESTED;
        $bid->save();

        if (!$bid) {
            return new Output(false, 500, 'common', 'failed');
        }

        event(new BorrowerNotInterested($bid));

        return new Output(true, 200, 'common', 'updated');
    }

    public
    function totalNumberOfLoans()
    {
        $count = Loan::where('status', '<>', 'DELETED')->count();
        return $count;
    }

    public function bidsCount()
    {
        $count = Bid::all()->count();
        return $count;
    }
}

