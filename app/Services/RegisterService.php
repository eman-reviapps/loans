<?php

namespace App\Services;

use App\Enums\BorrowerProfileTypes;
use App\Enums\PeriodUnits;
use App\Events\UserRegistered;
use App\Mail\LenderProfileRequiresApproval;
use App\Models\Setting;
use Carbon\Carbon;
use Facades\App\Helpers\EmailHelpers;
use Facades\App\Helpers\EnumHelpers;
use App\Helpers\Output;

use App\Enums\ActivationStates;

use Sentinel;
use DB;

class RegisterService
{
    protected $userService;
    protected $institutionService;
    protected $locationService;

    public function __construct(InstitutionService $institutionService, UserService $userService, LocationService $locationService)
    {
        $this->institutionService = $institutionService;
        $this->userService = $userService;
        $this->locationService = $locationService;
    }

    public function registerLender($attributes)
    {
        $is_valid = $this->isValidRegisterLender($attributes);
        if (!$is_valid->status)
            return $is_valid;

        $institution = $is_valid->object;

        DB::beginTransaction();
        try {
            $prepared = $this->prepareRegisterLender($attributes, $institution);
            if (!$prepared)
                return new Output(false, 500, 'common', 'failed');


            $attributes = $prepared[1];
            $institution = $prepared[2];

            $created = $this->userService->registerUser($attributes, $institution);
            if (!$created[0])
                return new Output(false, 500, 'common', 'failed');

            $user = $created[1];
            $activation = $created[2];

            DB::commit();

            event(new UserRegistered($user, $activation, $institution));

            return new Output(true, 201, 'common', 'created', $user);

        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }

    private function prepareRegisterLender(array $attributes, $institution)
    {
        if ($attributes['institution_exists'] == 0) {
            $response = $this->institutionService->create(
                [
                    'domain' => $attributes['domain'],
//                    'title' => $attributes['title'],
                    'state' => $attributes['state'],
                    'city' => $attributes['city'],
                    'status' => ActivationStates::ACTIVATION_REQUIRED
                ]
            );
            $institution = $response->getObject();
            if (!$institution) {
                return false;
            }
        }

        $attributes['status'] = $this->userService->getInitialUserStatus($institution); //$institution->status;//
        $attributes['role'] = RoleService::ROLE_LENDER;

        $settings = Setting::first();

        $getNumberOfFreeLiftimeLenders = $this->userService->getNumberOfFreeLiftimeLenders();
        if ($getNumberOfFreeLiftimeLenders <= $settings->free_lifetime_subscriptions_count)
            $attributes['free_lifetime'] = 1;
        else
        {
            $next_payment_due = $settings->free_trial_period_unit == PeriodUnits::DAYS ? Carbon::now()->addDays($settings->free_trial_period):  Carbon::now()->addMonths($settings->free_trial_period);

            $attributes['trial_ends_at'] = $next_payment_due;
            $attributes['next_payment_due'] = $next_payment_due;
        }

        return array(true, $attributes, $institution);
    }

    private function isValidRegisterLender(array $attributes)
    {
        $user = Sentinel::findByCredentials(['login' => $attributes['email']]);
        if ($user) {
            return new Output(false, 409, '', 'email_already_exists');
        }
        //is state enabled
        $isStateEnabled = $this->locationService->isStateEnabled($attributes['state']);
        if (!$isStateEnabled) {
            return new Output(false, 500, '', 'state_not_served_yet');
        }
        //is city enabled
        $isCityEnabled = $this->locationService->isCityEnabled($attributes['state'], $attributes['city']);
        if (!$isCityEnabled) {
            return new Output(false, 500, '', 'city_not_served_yet');
        }
        $institution = null;
        if ($attributes['institution_exists'] == 1) {
            //institution already exists
            $institution = $this->institutionService->find($attributes['institution']);
            if (!$institution) {
                return new Output(false, 500, 'common', 'failed');
            }

            if ($this->institutionService->isBlocked($institution)) {
                return new Output(false, 403, '', 'domain_blocked');
            }

        }

        return new Output(true, 200, '', '', $institution);
    }

    public function registerBorrower($attributes)
    {
        if (isset($attributes['profile_type'])) {
            if ($attributes['profile_type'] == BorrowerProfileTypes::REAL_ESTATE) {
                $is_valid = $this->isValidRegisterRealEstateBorrower($attributes);
            } elseif ($attributes['profile_type'] == BorrowerProfileTypes::BUSINESS) {
                $is_valid = $this->isValidRegisterBusinessBorrower($attributes);
            }
        }

        if (!$is_valid->status)
            return $is_valid;

        $prepared = $this->prepareRegisterBorrower($attributes);
        if (!$prepared)
            return new Output(false, 500, 'common', 'failed');

        $attributes = $prepared[1];

        DB::beginTransaction();

        try {

            $created = $this->userService->registerUser($attributes, null, null);
            if (!$created[0]) {
                DB::rollback();
                return new Output(false, 500, 'common', 'failed');
            }

            $user = $created[1];
            $activation = $created[2];

            DB::commit();

            event(new UserRegistered($user, $activation));

            return new Output(true, 201, 'common', 'created', $user);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }

    }

    private function prepareRegisterBorrower(array $attributes)
    {
        $attributes['status'] = $this->userService->getInitialUserStatus();
        $attributes['role'] = RoleService::ROLE_BORROWER;

        return array(true, $attributes);
    }

    private function isValidRegisterRealEstateBorrower(array $attributes)
    {
        $user = Sentinel::findByCredentials(['login' => $attributes['email']]);
        if ($user) {
            return new Output(false, 409, '', 'email_already_exists');
        }

        //is zip_code enabled
        $isZipCodeEnabled = $this->locationService->isZipCodeEnabled($attributes['zip_code']);
        if (!$isZipCodeEnabled) {
            return new Output(false, 500, '', 'area_not_served_yet');
        }

        return new Output(true, 200, '', '');
    }

    private function isValidRegisterBusinessBorrower(array $attributes)
    {
        $user = Sentinel::findByCredentials(['login' => $attributes['email']]);
        if ($user) {
            return new Output(false, 409, '', 'email_already_exists');
        }

        return new Output(true, 200, '', '');
    }

}