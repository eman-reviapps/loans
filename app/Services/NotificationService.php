<?php

namespace App\Services;

use App\Enums\NotificationTypes;
use App\Helpers\Output;
use App\Models\Institution;
use App\Models\User;
use App\Models\UserNotification;
use Sentinel;

class NotificationService
{
    private $referenced;
    private $notification_type;
    private $notification_body;
    private $is_read;

    protected $userService;
    protected $roleService;

    public function __construct(UserService $userService, RoleService $roleService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;
    }

    public function markAsRead(UserNotification $notification)
    {
        $notification->is_read = 1;
        $notification->save();

        if (!$notification) {
            return new Output(false, 500, 'common', 'failed');
        }

        return new Output(true, 200, 'common', 'updated');
    }

    public function markAllRead(User $user)
    {
        UserNotification::where('user_id',$user->id)->update(['is_read'=>1]);

        return new Output(true, 200, 'common', 'updated');
    }

    public function newUserRegistered(User $user)
    {
        $this->referenced = $user;

        if ($user->inRole(RoleService::ROLE_LENDER)) {
            $this->notification_type = NotificationTypes::NEW_LENDER_REGISTERED;
        } else if ($user->inRole(RoleService::ROLE_BORROWER)) {
            $this->notification_type = NotificationTypes::NEW_BORROWER_REGISTERED;
        } else if ($user->inRole(RoleService::ROLE_ADMIN)) {
            $this->notification_type = NotificationTypes::NEW_ADMIN_REGISTERED;
        }

        $this->notification_body = $this->getNotificationText($this->notification_type);
        $this->is_read = false;

        $this->sendNotificationToAdmins();
    }

    public
    function lenderRequiresApproval(User $user)
    {
        $this->referenced = $user;
        $this->notification_type = NotificationTypes::LENDER_REQUIRES_APPROVAL;
        $this->notification_body = $this->getNotificationText($this->notification_type);
        $this->is_read = false;

        $this->sendNotificationToAdmins();
    }

    public
    function domainRequiresApproval(Institution $institution)
    {
        $this->referenced = $institution;
        $this->notification_type = NotificationTypes::DOMAIN_REQUIRES_APPROVAL;
        $this->notification_body = $this->getNotificationText($this->notification_type);
        $this->is_read = false;

        $this->sendNotificationToAdmins();
    }

    public
    function sendNotification(User $user, $referenced, $notification_type)
    {
        $this->referenced = $referenced;
        $this->notification_type = $notification_type;
        $this->notification_body = $this->getNotificationText($notification_type);
        $this->is_read = false;

        $attributes = $this->constructNotification();

        $this->createNotification([$user], $attributes);
    }

    private
    function sendNotificationToAdmins()
    {
        $admins = $this->roleService->getRoleUsers(RoleService::ROLE_ADMIN);

        $attributes = $this->constructNotification();

        $this->createNotification($admins, $attributes);
    }


    /**
     * @param User $user
     * @return array
     */
    private
    function constructNotification()
    {
        $attributes = [
            "referenced_id" => $this->referenced->id,
            "notification_type" => $this->notification_type,
            "message" => $this->notification_body,
            "is_read" => $this->is_read
        ];
        return $attributes;
    }

    public
    function createNotification($users, $attributes)
    {
        foreach ($users as $user) {
            $attributes = [
                "user_id" => $user->id,
                "referenced_id" => $attributes['referenced_id'],
                "notification_type" => $attributes['notification_type'],
                "message" => $attributes['message'],
                "is_read" => $attributes['is_read']
            ];
            UserNotification::create($attributes);
        }
    }


    private
    function getNotificationText($notification_type)
    {
        $array_parameters = [];

        switch ($notification_type) {
            case NotificationTypes::DOMAIN_REQUIRES_APPROVAL:
                $array_parameters = ['domain_name' => $this->referenced->domain];
                break;
            case NotificationTypes::LENDER_REQUIRES_APPROVAL:
                $array_parameters = ['lender_email' => $this->referenced->email];
                break;
            case NotificationTypes::BID_RECEIVED:
                $array_parameters = ['loan' => $this->referenced->loan_type, 'lender' => $this->referenced->lender->full_name];
                break;
            case NotificationTypes::BORROWER_NOT_INTERESTED:
                $array_parameters = ['borrower' => $this->referenced->loan->borrower->full_name];
                break;
            default:
                break;
        }

        return trans('notifications.' . $notification_type . '.body', $array_parameters);
    }


}