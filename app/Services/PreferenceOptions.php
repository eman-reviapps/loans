<?php

namespace App\Services;

use App\Helpers\Option;
use App\Enums\NotificationOptions;
use App\Enums\DataTypes;
use App\Models\User;

class PreferenceOptions
{
    private $general_options;
    private $lender_options;
    private $borrower_options;
    private $all_options;

    const NOTIFICATION_PREFERENCE = 'NOTIFICATION_PREFERENCE';

    //FOR LENDER
    const FAVORITE_PROFILE_TYPES = 'FAVORITE_PROFILE_TYPES';
    const FAVORITE_LOAN_TYPES = 'FAVORITE_LOAN_TYPES';
    const FAVORITE_LOAN_SIZE_MIN = 'FAVORITE_LOAN_SIZE_MIN';
    const FAVORITE_LOAN_SIZE_MAX = 'FAVORITE_LOAN_SIZE_MAX';
    const FAVORITE_LOCATION = 'FAVORITE_LOCATION';
    const FAVORITE_STATE = 'FAVORITE_STATE';
    const FAVORITE_CITY = 'FAVORITE_CITY';

    //FOR BORROWER
    const FAVORITE_INSTITUTION_TYPES = 'FAVORITE_INSTITUTION_TYPES';
    const FAVORITE_INSTITUTION_SIZES = 'FAVORITE_INSTITUTION_SIZES';
    const FAVORITED_LENDERS = 'FAVORITED_LENDERS';
    const NON_FAVORITED_LENDERS = 'NON_FAVORITED_LENDERS';

    public function __construct()
    {
        $this->intitializeOptions();
    }

    public static function options()
    {
        return
            [
                self::NOTIFICATION_PREFERENCE,

                self::FAVORITE_PROFILE_TYPES,
                self::FAVORITE_LOAN_TYPES,
                self::FAVORITE_LOAN_SIZE_MIN,
                self::FAVORITE_LOAN_SIZE_MAX,
                self::FAVORITE_STATE,
                self::FAVORITE_CITY,
                self::FAVORITE_LOCATION,

                self::FAVORITE_INSTITUTION_TYPES,
                self::FAVORITE_INSTITUTION_SIZES,
                self::FAVORITED_LENDERS,
                self::NON_FAVORITED_LENDERS,
            ];
    }

    public function intitializeOptions()
    {
        $this->setGeneralOptions();

        $this->setLenderOptions();

        $this->setBorrowerOptions();

        $this->all_options = array_merge($this->general_options, $this->lender_options);
        $this->all_options = array_merge($this->all_options, $this->borrower_options);
    }

    public function getOptions(User $user)
    {
        if ($user->inRole(RoleService::ROLE_LENDER)) {
            return $this->lender_options;
        } else if ($user->inRole(RoleService::ROLE_BORROWER)) {
            return $this->borrower_options;
        } else {
            return $this->general_options;
        }
    }

    public function getProfileOptions(User $user)
    {
        if ($user->inRole(RoleService::ROLE_LENDER)) {
            return $this->getProfileLenderOptions();
        } else if ($user->inRole(RoleService::ROLE_BORROWER)) {
            return $this->getProfileBorrowerOptions();
        } else {
            return [];
        }
    }

    public function getOption($key)
    {
        $item = null;
        foreach ($this->all_options as $struct) {
            if ($key == $struct->slug) {
                $item = $struct;
                break;
            }
        }
        return $item;
    }

    private function setGeneralOptions()
    {
        $this->general_options = [
            new Option(self::NOTIFICATION_PREFERENCE, 'Notification settings', 'Notification settings', DataTypes::FREE, 'radio', NotificationOptions::EMAIL),
        ];
    }

    private function setLenderOptions()
    {
        $this->lender_options = array_merge($this->general_options,
            $this->getProfileLenderOptions()
        );
    }

    private function setBorrowerOptions()
    {
        $this->borrower_options = array_merge($this->general_options,
            $this->getProfileBorrowerOptions()
        );
    }

    private function getProfileBorrowerOptions()
    {
        return [
            new Option(self::FAVORITE_INSTITUTION_TYPES, 'Institutions types', 'Favorite Institutions types', DataTypes::FREE, 'select-multiple', ''),
//            new Option(self::FAVORITE_INSTITUTION_SIZES, 'Institutions sizes', 'Favorite Institutions sizes', DataTypes::FREE, 'select-multiple', ''),
            new Option(self::FAVORITED_LENDERS, 'Favorite lenders', 'Favorite lenders', DataTypes::FREE, 'select-multiple', ''),
            new Option(self::NON_FAVORITED_LENDERS, 'Non Favorite lenders', 'Non Favorite lenders', DataTypes::FREE, 'select-multiple', ''),
        ];
    }

    private function getProfileLenderOptions()
    {
        return [
            new Option(self::FAVORITE_PROFILE_TYPES, 'Profile types', 'Profile types', DataTypes::FREE, 'select-multiple', ''),
            new Option(self::FAVORITE_LOAN_TYPES, 'Loan types', 'Loan types', DataTypes::FREE, 'select-multiple', ''),
            new Option(self::FAVORITE_LOAN_SIZE_MIN, 'Minimun loan size', 'Minimun loan size', DataTypes::NUMBER, 'text', ''),
            new Option(self::FAVORITE_LOAN_SIZE_MAX, 'Maximum loan size', 'Maximum loan size', DataTypes::NUMBER, 'text', ''),
            new Option(self::FAVORITE_STATE, 'State', 'State', DataTypes::FREE, 'select-multiple', ''),
            new Option(self::FAVORITE_CITY, 'City', 'City', DataTypes::FREE, 'select-multiple', ''),
            new Option(self::FAVORITE_LOCATION, 'Geo Location', 'Zip-code', DataTypes::FREE, 'select-multiple', ''),
        ];
    }

}