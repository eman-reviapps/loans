<?php

namespace App\Services;

use App\Models\Permission;

class PermissionsService
{

    /**
     * @param array $attributes
     * @param $permissions
     */
    public function constructPermissionsArray(array $attributes)
    {
        $permissions = array();
        $privilages = array_filter(explode(',', $attributes["privilages"]));
        foreach ($privilages as $key) {
            $permissions[$key] = true;
        }
        $un_checked_parent = array_filter(explode(',', $attributes["un_checked_parent"]));
        foreach ($un_checked_parent as $key) {
            $permissions[$key] = 2;
        }
        return $permissions;
    }

    public function getDefaultPermissions()
    {
        $array = [];
        $permissions = Permission::all();
        foreach ($permissions as $permission)
        {
            $array[$permission->slug] = true;
        }
        return $array;
    }

    /**
     * @param $default_selected
     * @param $permissions
     * @return array
     */
    public function getAdminPermissionsTree($permissions, $default_selected)
    {
        return [
            [
                "id" => "dashboard",
                "text" => trans('permissions.dashboard'),
                "state" => [
                    "selected" => $this->permissionState($permissions, "dashboard", $default_selected)
                ]
            ],
            [
                "id" => "settings",
                "text" => trans('permissions.settings'),
                "state" => [
                    "selected" => $this->permissionState($permissions, "settings", $default_selected)
                ],
                "children" => [
                    [
                        "id" => "settings.show",
                        "text" => trans('permissions.index'),
                        "icon" => "fa fa-eye icon-state-success",
                        "state" => [
                            "selected" => $this->permissionState($permissions, "settings.show", $default_selected)
                        ]
                    ],
                    [
                        "id" => "settings.edit",
                        "text" => trans('permissions.edit'),
                        "icon" => "fa fa-edit icon-state-success",
                        "state" => [
                            "selected" => $this->permissionState($permissions, "settings.edit", $default_selected)
                        ]
                    ]
                ]
            ],
            [
                "id" => "institutions_management",
                "text" => trans('permissions.institutions_management'),
                "state" => [
                    "selected" => $this->permissionState($permissions, "institutions_management", $default_selected),
                    "opened" => true
                ],
                "children" => [
                    [
                        "id" => "institutions",
                        "text" => "Institutions",
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "institutions", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "institutions.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "institutions.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "institutions.create",
                                "text" => trans('permissions.create'),
                                "icon" => "fa fa-plus icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "institutions.create", $default_selected)
                                ]
                            ],
                            [
                                "id" => "institutions.edit",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "institutions.edit", $default_selected)
                                ]
                            ],
                            [
                                "id" => "institutions.destroy",
                                "text" => trans('permissions.destroy'),
                                "icon" => "fa fa-close icon-state-danger",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "institutions.destroy", $default_selected)
                                ]
                            ]
                        ]
                    ], [
                        "id" => "institution_types",
                        "text" => "Institution Types",
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "institution_types", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "institution_types.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "institution_types.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "institution_types.create",
                                "text" => trans('permissions.create'),
                                "icon" => "fa fa-plus icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "institution_types.create", $default_selected)
                                ]
                            ],
                            [
                                "id" => "institution_types.edit",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "institution_types.edit", $default_selected)
                                ]
                            ],
                            [
                                "id" => "institution_types.destroy",
                                "text" => trans('permissions.destroy'),
                                "icon" => "fa fa-close icon-state-danger",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "institution_types.destroy", $default_selected)
                                ]
                            ]
                        ]
                    ], [
                        "id" => "institution_sizes",
                        "text" => "Institution Sizes",
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "institution_sizes", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "institution_sizes.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "institution_sizes.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "institution_sizes.create",
                                "text" => trans('permissions.create'),
                                "icon" => "fa fa-plus icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "institution_sizes.create", $default_selected)
                                ]
                            ],
                            [
                                "id" => "institution_sizes.edit",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "institution_sizes.edit", $default_selected)
                                ]
                            ],
                            [
                                "id" => "institution_sizes.destroy",
                                "text" => trans('permissions.destroy'),
                                "icon" => "fa fa-close icon-state-danger",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "institution_sizes.destroy", $default_selected)
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            [
                "id" => "users_management",
                "text" => trans('permissions.users_management'),
                "state" => [
                    "selected" => $this->permissionState($permissions, "users_management", $default_selected),
                    "opened" => true
                ],
                "children" => [
                    [
                        "id" => "users",
                        "text" => trans('permissions.users'),
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "users", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "users.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "users.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "users.create",
                                "text" => trans('permissions.create'),
                                "icon" => "fa fa-plus icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "users.create", $default_selected)
                                ]
                            ],
                            [
                                "id" => "users.edit",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "users.edit", $default_selected)
                                ]
                            ],
                            [
                                "id" => "users.destroy",
                                "text" => trans('permissions.destroy'),
                                "icon" => "fa fa-close icon-state-danger",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "users.destroy", $default_selected)
                                ]
                            ]
                        ]
                    ], [
                        "id" => "roles",
                        "text" => trans('permissions.roles'),
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "roles", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "roles.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "roles.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "roles.create",
                                "text" => trans('permissions.create'),
                                "icon" => "fa fa-plus icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "roles.create", $default_selected)
                                ]
                            ],
                            [
                                "id" => "roles.edit",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "roles.edit", $default_selected)
                                ]
                            ],
                            [
                                "id" => "roles.destroy",
                                "text" => trans('permissions.destroy'),
                                "icon" => "fa fa-close icon-state-danger",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "roles.destroy", $default_selected)
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            [
                "id" => "loans_management",
                "text" => trans('permissions.loans_management'),
                "state" => [
                    "selected" => $this->permissionState($permissions, "loans_management", $default_selected),
                    "opened" => true
                ],
                "children" => [
                    [
                        "id" => "loans",
                        "text" => trans('permissions.loans'),
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "loans", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "loans.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "loans.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "loans.create",
                                "text" => trans('permissions.create'),
                                "icon" => "fa fa-plus icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "loans.create", $default_selected)
                                ]
                            ],
                            [
                                "id" => "loans.edit",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "loans.edit", $default_selected)
                                ]
                            ],
                            [
                                "id" => "loans.destroy",
                                "text" => trans('permissions.destroy'),
                                "icon" => "fa fa-close icon-state-danger",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "loans.destroy", $default_selected)
                                ]
                            ]
                        ]
                    ],
                    [
                        "id" => "real_estate_types",
                        "text" => trans('permissions.real_estate_types'),
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "real_estate_types", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "real_estate_types.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "real_estate_types.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "real_estate_types.create",
                                "text" => trans('permissions.create'),
                                "icon" => "fa fa-plus icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "real_estate_types.create", $default_selected)
                                ]
                            ],
                            [
                                "id" => "real_estate_types.edit",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "real_estate_types.edit", $default_selected)
                                ]
                            ],
                            [
                                "id" => "real_estate_types.destroy",
                                "text" => trans('permissions.destroy'),
                                "icon" => "fa fa-close icon-state-danger",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "real_estate_types.destroy", $default_selected)
                                ]
                            ]
                        ]
                    ],
                    [
                        "id" => "property_types",
                        "text" => trans('permissions.property_types'),
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "property_types", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "property_types.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "property_types.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "property_types.create",
                                "text" => trans('permissions.create'),
                                "icon" => "fa fa-plus icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "property_types.create", $default_selected)
                                ]
                            ],
                            [
                                "id" => "property_types.edit",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "property_types.edit", $default_selected)
                                ]
                            ],
                            [
                                "id" => "property_types.destroy",
                                "text" => trans('permissions.destroy'),
                                "icon" => "fa fa-close icon-state-danger",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "property_types.destroy", $default_selected)
                                ]
                            ]
                        ]
                    ],
                    [
                        "id" => "purpose_types",
                        "text" => trans('permissions.purpose_types'),
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "purpose_types", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "purpose_types.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "purpose_types.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "purpose_types.create",
                                "text" => trans('permissions.create'),
                                "icon" => "fa fa-plus icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "purpose_types.create", $default_selected)
                                ]
                            ],
                            [
                                "id" => "purpose_types.edit",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "purpose_types.edit", $default_selected)
                                ]
                            ],
                            [
                                "id" => "purpose_types.destroy",
                                "text" => trans('permissions.destroy'),
                                "icon" => "fa fa-close icon-state-danger",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "purpose_types.destroy", $default_selected)
                                ]
                            ]
                        ]
                    ],
                    [
                        "id" => "lease_types",
                        "text" => trans('permissions.lease_types'),
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "lease_types", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "lease_types.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "lease_types.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "lease_types.create",
                                "text" => trans('permissions.create'),
                                "icon" => "fa fa-plus icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "lease_types.create", $default_selected)
                                ]
                            ],
                            [
                                "id" => "lease_types.edit",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "lease_types.edit", $default_selected)
                                ]
                            ],
                            [
                                "id" => "lease_types.destroy",
                                "text" => trans('permissions.destroy'),
                                "icon" => "fa fa-close icon-state-danger",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "lease_types.destroy", $default_selected)
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            [
                "id" => "geo_management",
                "text" => trans('permissions.geo_management'),
                "state" => [
                    "selected" => $this->permissionState($permissions, "geo_management", $default_selected),
                    "opened" => true
                ],
                "children" => [
                    [
                        "id" => "states",
                        "text" => trans('permissions.states'),
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "states", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "states.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "states.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "states.edit",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "states.edit", $default_selected)
                                ]
                            ],
                        ]
                    ],

                    [
                        "id" => "counties",
                        "text" => trans('permissions.counties'),
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "counties", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "counties.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "counties.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "counties.change_status",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "counties.change_status", $default_selected)
                                ]
                            ],
                        ]
                    ],
                    [
                        "id" => "cities",
                        "text" => trans('permissions.cities'),
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "cities", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "cities.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "cities.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "cities.change_status",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "cities.change_status", $default_selected)
                                ]
                            ],
                        ]
                    ],
                    [
                        "id" => "zip_codes",
                        "text" => trans('permissions.zip_codes'),
                        "icon" => "fa fa-folder icon-state-default",
                        "state" => [
                            "opened" => true,
                            "selected" => $this->permissionState($permissions, "zip_codes", $default_selected)
                        ],
                        "children" => [
                            [
                                "id" => "zip_codes.index",
                                "text" => trans('permissions.index'),
                                "icon" => "fa fa-eye icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "zip_codes.index", $default_selected)
                                ]
                            ],
                            [
                                "id" => "zip_codes.change_status",
                                "text" => trans('permissions.edit'),
                                "icon" => "fa fa-edit icon-state-success",
                                "state" => [
                                    "selected" => $this->permissionState($permissions, "zip_codes.change_status", $default_selected)
                                ]
                            ],
                        ]
                    ],
                ]
            ],
        ];
    }
    /**
     * @param $permissions
     * @param $default_selected
     * @return bool
     */
    private function permissionState($permissions, $key, $default_selected)
    {
        return ((isset($permissions[$key]) && $permissions[$key] == 1) || $default_selected) ? true : false;
    }


}