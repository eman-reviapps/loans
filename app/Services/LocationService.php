<?php

namespace App\Services;

use App\Helpers\Output;
use App\Models\City;
use App\Models\State;

class LocationService
{
    public function getStateCounties(State $state)
    {
        $counties = City::where('state_code', $state->state_code)->distinct()->select('county', 'state_code')->get();

        $counties = $counties->each(function ($item, $key) {
            $count = City
                ::where('state_code', $item->state_code)
                ->where('county', $item->county)
                ->where('is_enabled', 1)
                ->count();

            $item['is_enabled'] = $count > 0 ? 1 : 0;

            $item['county_cities_count'] =
                City::where('state_code', $item->state_code)
                    ->where('county', $item->county)
                    ->distinct('city')->count('city');;
        });

        return $counties->sortBy('county');
    }

    public function getCountyCities(State $state, $county)
    {
        $cities = City
            ::where('state_code', $state->state_code)
            ->where('county', $county)
            ->distinct()->select('city', 'county', 'state_code')->get();

        $cities = $cities->each(function ($item, $key) {
            $count = City
                ::where('state_code', $item->state_code)
                ->where('county', $item->county)
                ->where('city', $item->city)
                ->where('is_enabled', 1)
                ->count();

            $item['is_enabled'] = $count > 0 ? 1 : 0;

            $item['city_zip_codes_count'] =
                City::where('state_code', $item->state_code)
                    ->where('county', $item->county)
                    ->where('city', $item->city)
                    ->distinct('zip_code')->count('zip_code');;
        });

        return $cities->sortBy('city');
    }

    public function getCityZipCodes(State $state, $county, $city_name)
    {
        $zip_codes = City
            ::where('state_code', $state->state_code)
            ->where('county', $county)
            ->where('city', $city_name)
            ->orderBy('zip_code')
            ->get();

        return $zip_codes;
    }

    public function updateState(State $state, array $attributes)
    {
        try {

            $state->update($attributes);

            City::where('state_code', $state->state_code)->update(['is_enabled' => $attributes['is_enabled']]);

            if (!$state)
                return new Output(false, 500, 'common', 'failed');

            return new Output(true, 200, 'common', 'updated');

        } catch (\Exception $e) {
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function updateCounty(State $state, $county, array $attributes)
    {
        try {
            $update = City
                ::where('state_code', $state->state_code)
                ->where('county', $county)
                ->update(['is_enabled' => $attributes['is_enabled']]);

            $counties = City::where('state_code', $state->state_code)->where('county', $county);
            $state_status = State::where('state_code' , $state->state_code)->update(['is_enabled' => $attributes['is_enabled']]);
            if($attributes['is_enabled']){
                $state_status;
            }
            else{
                if($counties->count() == 0){
                    $state_status ;
                }
            }

            if (!$update)
                return new Output(false, 500, 'common', 'failed');


            return new Output(true, 200, 'common', 'updated');

        } catch (\Exception $e) {
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function updateCity(State $state, $county, $city_name, array $attributes)
    {
        try {

            $update = City
                ::where('state_code', $state->state_code)
                ->where('county', $county)
                ->where('city', $city_name)
                ->update(['is_enabled' => $attributes['is_enabled']]);

            if (!$update)
                return new Output(false, 500, 'common', 'failed');


            return new Output(true, 200, 'common', 'updated');

        } catch (\Exception $e) {
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function updateZipCode(State $state, $county, $city_name, $zip_code, array $attributes)
    {
        try {

            $update = City
                ::where('state_code', $state->state_code)
                ->where('county', $county)
                ->where('city', $city_name)
                ->where('zip_code', $zip_code)
                ->update(['is_enabled' => $attributes['is_enabled']]);

            if (!$update)
                return new Output(false, 500, 'common', 'failed');


            return new Output(true, 200, 'common', 'updated');

        } catch (\Exception $e) {
            return new Output(false, 500, 'common', 'failed');
        }
    }

    public function isStateEnabled($state_code)
    {
        $state = State::find($state_code);
        return $state ? ($state->is_enabled ? true : false) : false;
    }

    public function findCity($zip_code)
    {
        return City::find($zip_code);
    }

    public function isCityEnabled($state_code, $city) //to be changed
    {
        $count = City::
        where('state_code', $state_code)
            ->where('city', $city)
            ->where('is_enabled', 1)
            ->count();

        return $count > 0 ? true : false;
    }

    public function isZipCodeEnabled($zip_code)
    {
        $record = City::find($zip_code);
        return $record ? ($record->is_enabled ? true : false) : false;
    }

    public function isValidZipCode($attributes)
    {
        $statecode = State::
        where('state' ,$attributes['state'])
            ->where('is_enabled', 1)
            ->first()->state_code;

        $record = City::
        where('state_code', $statecode)
            ->where('city', $attributes['city'])
            ->where('zip_code', $attributes['zip_code'])
            ->where('is_enabled', 1)
            ->first();

        return $record ? true : false;
    }

    public function enableAllStates(){
        State::where('is_enabled',0)->update(['is_enabled' => '1']);
        City::where('is_enabled',0)->update(['is_enabled' => '1']);
        return true;
    }

    public function disableAllStates(){
        State::where('is_enabled',1)->update(['is_enabled' => '0']);
        City::where('is_enabled',1)->update(['is_enabled' => '0']);
        return true;
    }

    public function enableAllCountiesForState($state_code){
        State::where('state_code' , $state_code)->update(['is_enabled' => '1']);
        City::where('state_code' , $state_code)->update(['is_enabled' => '1']);
        return true ;
    }

    public function disableAllCountiesForState($state_code){
        State::where('state_code' , $state_code)->update(['is_enabled' => '0']);
        City::where('state_code' , $state_code)->update(['is_enabled' => '0']);
        return true ;
    }

    public function  enableAllCitiesForCounty(array $attributes){
        City::where('state_code' , $attributes['state_code'])
            ->where('county', $attributes['county'])->update(['is_enabled' => '1']) ;
        return true ;
    }

    public function  disableAllCitiesForCounty(array $attributes){
        City::where('state_code' , $attributes['state_code'])
            ->where('county', $attributes['county'])->update(['is_enabled' => '0']) ;
        return true ;
    }

    public function  enableAllZipCodesForCity(array $attributes){
        City::where('state_code' , $attributes['state_code'])
            ->where('county', $attributes['county'])
            ->where('city', $attributes['city'])
            ->update(['is_enabled' => '1']);
        return true ;
    }

    public function  disableAllZipCodesForCity(array $attributes){
        City::where('state_code' , $attributes['state_code'])
            ->where('county', $attributes['county'])
            ->where('city', $attributes['city'])
            ->update(['is_enabled' => '0']) ;
        return true ;
    }
}