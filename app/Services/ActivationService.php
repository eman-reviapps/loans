<?php

namespace App\Services;

use App\Helpers\Output;
use App\Models\User;
use Sentinel;
use Activation;

//use Facades\App\Services\LoginService;

class ActivationService
{
    public function activate(User $user, $activation_code)
    {
        if (!$user) {
            return new Output(false, 401, '', 'user_not_found');
        }

        if (Activation::completed($user)) {
            return new Output(false, 400, '', 'already_activated');
        }

        if (!Activation::exists($user)) {
            return new Output(false, 404, '', 'activation_not_found');
        }

        if (!Activation::complete($user, $activation_code)) {
            // Activation failed.
            return new Output(false, 500, 'common', 'failed');
        }

//        $response = LoginService::loginUser(false, [], $user);

        return new Output(true, 200, '', 200, $user);
    }

    public function doesActivationCompleted(User $user)
    {
        return Activation::completed($user) ;
    }
}