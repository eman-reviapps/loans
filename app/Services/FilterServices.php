<?php
/**
 * Created by PhpStorm.
 * User: Eman
 * Date: 3/8/2017
 * Time: 4:41 PM
 */

namespace App\Services;


class FilterServices
{
    public function add_filter($query, $key, $value, $like = false,$start_with=false)
    {
        if ($value != null && isset($value)) {
            if (is_array($value)) {
                $value = array_filter($value, function ($k) {
                    return $k != '' && !empty($k);
                });
                if (count($value) > 0) {
                    $query = $query->whereIn($key, $value);
                }
            } else {
                if ($like)
                    $query = $query->where($key, 'like', '%' . $value . '%');
                else if ($start_with)
                    $query = $query->where($key, 'like', $value . '%');
                else
                    $query = $query->where($key, $value);
            }
        }
        return $query;
    }

    public function commaValuesToArray($values)
    {
        $ids = explode(",", $values);
        $ids = array_filter($ids, function ($k) {
            return $k != '' && !empty($k);
        });
        return $ids;
    }

    public function filter($query, array $attributes, $count, $paginate = false)
    {
        if (count($attributes) == 0) {
            return $this->queryOptions($query, $count, $paginate);
        }

        foreach ($attributes as $key => $value) {
            $query = $this->add_filter($query, $key, $value);
        }

        return $this->queryOptions($query, $count, $paginate);
    }

    public function filterEnhanced($query, array $attributes, array $tableColumns, array $select = [], array $orderBy = [], $count, $paginate = false)
    {
        if (count($attributes) == 0) {
            return $this->queryOptions($query, $count, $paginate);
        }

        $query = $this->processFilter($query, $attributes, $tableColumns);

        $query = $this->processColumnsSelect($query, $select);

        $query = $this->processDataSort($query, $orderBy);

        return $this->queryOptions($query, $count, $paginate);
    }

    public function filterRelation()
    {

    }

    /**
     * @param $query
     * @param $count
     * @param $paginate
     * @return mixed
     */
    private function queryOptions($query, $count, $paginate)
    {
        return $count ? $query->count() : ($paginate ? $query->paginate(config('loan-app.page_size')) : $query->get());
    }

    /**
     * @param $query
     * @param array $attributes
     * @param array $tableColumns
     * @return mixed
     */
    private function processFilter($query, array $attributes, array $tableColumns)
    {
        foreach ($attributes as $key => $value) {
            if (in_array($key, $tableColumns)) {
                $query = $this->add_filter($query, $key, $value);
            }
        }
        return $query;
    }

    /**
     * @param $query
     * @param array $orderBy
     * @return mixed
     */
    private function processDataSort($query, array $orderBy)
    {
        if (count($orderBy) > 0) {
            foreach ($orderBy as $key) {
                $query = $query->orderBy($key->name, $key->order_type);
            }
            return $query;

        }
        return $query;
    }

    /**
     * @param $query
     * @param array $select
     * @return mixed
     */
    private function processColumnsSelect($query, array $select)
    {
        if (count($select) > 0) {
            $query = $query->select($select);
            return $query;
        }
        return $query;
    }
}