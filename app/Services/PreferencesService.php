<?php

namespace App\Services;

use App\Helpers\Option;
use App\Models\BorrowerProfile;
use App\Models\Loan;
use App\Models\Preference;
use App\Helpers\Output;
use App\Models\User;
use DB;

class PreferencesService extends Service
{
    protected $preferenceOptions;

    public function __construct(PreferenceOptions $preferenceOptions)
    {
        $this->preferenceOptions = $preferenceOptions;
    }

    public function findRecordBySlug($slug, User $user, BorrowerProfile $profile = null, Loan $loan = null)
    {
        $query = Preference::where('slug', $slug);

        if ($user) {
            $query = $query->where('user_id', $user->id);
        }
        if ($loan) {
            $query = $query->where('loan_id', $loan->id);
        }
        if ($profile) {
            $query = $query->where('profile_id', $profile->id);
        }
        $records = $query->get();

        return count($records) > 0 ? $records[0] : null;

    }

    public function createPreferences($attributes, $user, $profile = null, $loan = null)
    {
        try {
            foreach ($this->preferenceOptions->getOptions($user) as $option) {
                $value = isset($attributes[$option->slug]) ? $attributes[$option->slug] : '';

                $record = Preference::create($this->prepareRecord($option, $value, $user, $profile, $loan));
                if (!$record) {
                    return new Output(false, 500, 'common', 'failed');
                }
            }
            return new Output(true, 200, 'common', 'created');
        } catch (\Exception $e) {
            return new Output(false, 500, 'common', 'failed');
        }

    }

    public function updatePreferences($attributes, User $user, BorrowerProfile $profile = null, Loan $loan = null)
    {
        if ($user->inRole(RoleService::ROLE_LENDER) && !isset($attributes[PreferenceOptions::FAVORITE_LOCATION])) {
            $attributes[PreferenceOptions::FAVORITE_LOCATION] = null;
        }

        DB::beginTransaction();
        try {
            foreach ($this->preferenceOptions->getProfileOptions($user) as $option) {

                $userPreference = $this->findRecordBySlug($option->slug, $user, $profile, $loan);
                $userPreference->value = isset($attributes[$option->slug]) ? $attributes[$option->slug] : null;
                $updated = $userPreference->Save();

                if (!$updated) {
                    DB::rollback();
                    return new Output(false, 500, 'common', 'failed');
                }
            }
            DB::commit();
            if($user->inRole(RoleService::ROLE_LENDER)) {
                $loanServices = new LoanService($this);
                $loanServices->getLenderPreferences($user);
            }
            return new Output(true, 200, 'common', 'updated');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            return new Output(false, 500, 'common', 'failed');
        }
    }


    public function prepareRecord(Option $option, $value, User $user, BorrowerProfile $profile = null, Loan $loan = null)
    {
        $attributes = [];

        $attributes['user_id'] = $user->id;
        $attributes['profile_id'] = $profile ? $profile->id : null;
        $attributes['loan_id'] = $loan ? $loan->id : null;

        $attributes['slug'] = $option->slug;
        $attributes['name'] = $option->name;

        $attributes['description'] = $option->description;

        $attributes['value'] = $value;
        $attributes['default_value'] = $option->defaultValue;

        $attributes['data_type'] = $option->dataType;
        $attributes['field_type'] = $option->fieldType;

        $attributes['is_enabled'] = 1;

        return $attributes;
    }

    public function getUserPreferencesBySlug(array $attributes, User $user, BorrowerProfile $profile = null, Loan $loan = null)
    {
        $query = Preference
            ::where('user_id', $user->id);

        if ($profile)
            $query = $query->where('profile_id', $profile->id);
        else
            $query = $query->where('profile_id', null);

        if ($loan)
            $query = $query->where('loan_id', $loan->id);
        else
            $query = $query->where('loan_id', null);

        $preferences = $query->get();

        if (isset($attributes['key_by']))
            $preferences = $preferences->keyBy($attributes['key_by']);

        return $preferences;
    }
}