<?php

namespace App\Services;

use App\Models\Role;
use App\Repositories\RoleRepository;
use App\Helpers\Output;
use Illuminate\Database\Eloquent\Model;
use Sentinel;

class RoleService extends Service
{
    const ROLE_ADMIN = 'administrator';
    const ROLE_BORROWER = 'borrower';
    const ROLE_LENDER = 'lender';

    protected $permissionsService;

    public function __construct(RoleRepository $roleRepository, PermissionsService $permissionsService)
    {
        $this->repository = $roleRepository;
        $this->permissionsService = $permissionsService;
    }

    public function prepareUpdate(Model $model, array $attributes)
    {
        $permissions = $this->permissionsService->constructPermissionsArray($attributes);
        $attributes['permissions'] = json_encode($permissions);

        return $attributes;
    }

    public function getRolePermissionsTree(Role $role, $default_selected = false)
    {
        if ($role->slug == RoleService::ROLE_ADMIN) {
            return $this->permissionsService->getAdminPermissionsTree($role->permissions, $default_selected);
        }
        return [

        ];
    }

//    public function getRolePermissions(Role $role, $default_selected = false)
//    {
//        $permissions = $role->permissions;
//        return $this->getPermissions($role->slug, $permissions, $default_selected);
//    }
//
//    public function getDefaultRolePermissions(Role $role, $default_selected)
//    {
//        return $this->getPermissions($role->slug, [], $default_selected);
//    }
//
//    public function getDefaultRolePermissionsBySlug($slug, $default_selected)
//    {
//        return $this->getPermissions($slug, [], $default_selected);
//    }
//
//    private function getPermissions($slug, $permissions, $default_selected)
//    {
//        if ($slug == RoleService::ROLE_ADMIN) {
//            return $this->permissionsService->getAdminPermissionsTree($permissions, $default_selected);
//        }
//        return [
//
//        ];
//    }

    public function getRoleUsers($role)
    {
        $role = Sentinel::findRoleBySlug($role);
        return $role->users;
    }


}