<?php

namespace App\Enums;


class PeriodUnits extends EnumsNew
{
    const DEFAULT_VALUE = self::WEEKS;

    const SECOND = 'SECOND';
    const SECONDS = 'SECONDS';

    const MINUTE = 'MINUTE';
    const MINUTES = 'MINUTES';

    const HOUR = 'HOUR';
    const HOURS = 'HOURS';

    const DAY = 'DAY';
    const DAYS = 'DAYS';

    const WEEK = 'WEEK';
    const WEEKS = 'WEEKS';

    const MONTH = 'MONTH';
    const MONTHS = 'MONTHS';

    const YEAR = 'YEAR';
    const YEARS = 'YEARS';

    public static function items()
    {
        return [
            self::SECOND => 'Second',
            self::SECONDS => 'Seconds',
            self::MINUTE => 'Minute',
            self::MINUTES => 'Minutes',
            self::HOUR => 'Hour',
            self::HOURS => 'Hours',
            self::DAY => 'Day',
            self::DAYS => 'Days',
            self::WEEK => 'Week',
            self::WEEKS => 'Weeks',
            self::MONTH => 'Month',
            self::MONTHS => 'Months',
            self::YEAR => 'Year',
            self::YEARS => 'Years',
        ];
    }

    public static function monthsYears()
    {
        $periodUnits = static::all();

        $allowed = [static::MONTHS, static::YEARS];

        $periodUnits = array_filter(
            $periodUnits,
            function ($key) use ($allowed) {
                return in_array($key, $allowed);
            },
            ARRAY_FILTER_USE_KEY
        );

        return $periodUnits;
    }

    public static function daysMonths()
    {
        $periodUnits = static::all();

        $allowed = [static::DAYS, static::MONTHS];

        $periodUnits = array_filter(
            $periodUnits,
            function ($key) use ($allowed) {
                return in_array($key, $allowed);
            },
            ARRAY_FILTER_USE_KEY
        );

        return $periodUnits;
    }

    public static function days()
    {
        $periodUnits = static::all();

        $allowed = [static::DAYS];

        $periodUnits = array_filter(
            $periodUnits,
            function ($key) use ($allowed) {
                return in_array($key, $allowed);
            },
            ARRAY_FILTER_USE_KEY
        );

        return $periodUnits;
    }
}