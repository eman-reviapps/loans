<?php

namespace App\Enums;

class LoanStatuses extends EnumsNew
{
    const DRAFT = 'DRAFT';
    const LIVE = 'LIVE';
    const LENDER_BID = 'LENDER_BID';
    const BORROWER_RESPONDED = 'BORROWER_RESPONDED';
    const DELETED = 'DELETED';
    const EXPIRED = 'EXPIRED';
    const CLOSED = 'CLOSED';
//    const ARCHIVED = 'ARCHIVED';

    public static function items()
    {
        return [
            self::DRAFT => "Draft",
            self::LIVE => "Live",
            self::LENDER_BID => "Lender Bid",
            self::BORROWER_RESPONDED => "Borrower Responded",
            self::DELETED => "Deleted",
            self::EXPIRED => "Expired",
            self::CLOSED => "Closed",
        ];
    }

    public static function description()
    {
        return [
            self::DRAFT => "Loan saved draft by Borrower but not posted",
            self::LIVE => "Loan posted and visible to lenders",
            self::LENDER_BID => "At least 1 lender has replied",
            self::BORROWER_RESPONDED => "Borrower replied to at least 1",
            self::DELETED => "Post Deleted",
            self::EXPIRED => "Posts expire 3 weeks after they are posted",
            self::CLOSED => "Closed",
        ];
    }
}