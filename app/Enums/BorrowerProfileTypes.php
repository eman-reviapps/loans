<?php

namespace App\Enums;

class BorrowerProfileTypes extends EnumsNew
{
    const REAL_ESTATE = 'REAL_ESTATE';
    const BUSINESS = 'BUSINESS';

    public static function items()
    {
        return [
            self::REAL_ESTATE => "Real Estate Investor",
            self::BUSINESS => "Operating Company",
        ];
    }

}