<?php

namespace App\Enums;

class EquipmentFinanceTypes extends EnumsNew
{
    const LOAN = 'LOAN';
    const LEASE = 'LEASE';
    const LEASE_PURCHASE = 'LEASE_PURCHASE';

    public static function items()
    {
        return [
            self::LOAN => "Loan",
            self::LEASE => "Lease",
            self::LEASE_PURCHASE => "Lease/Purchase",
        ];
    }

}