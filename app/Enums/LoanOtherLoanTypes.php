<?php

namespace App\Enums;

class LoanOtherLoanTypes extends EnumsNew
{
    const TERM_LOAN = 'TERM_LOAN';
    const LINE_OF_CREDIT = 'LINE_OF_CREDIT';

    public static function items()
    {
        return [
            self::TERM_LOAN => "Term Loan",
            self::LINE_OF_CREDIT => "Line of Credit",
        ];
    }

}