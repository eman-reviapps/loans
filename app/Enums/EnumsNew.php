<?php
/**
 * Created by PhpStorm.
 * User: Eman
 * Date: 3/22/2017
 * Time: 11:24 AM
 */

namespace App\Enums;

abstract class EnumsNew
{
    public static function items()
    {
        return [];
    }

    public static function all()
    {
        return collect(static::items())->all();
    }

    public static function allKeyValue()
    {
        $all = self::all();
        $array = [];
        foreach ($all as $record) {
            $array[$record->slug] = $record->title;
        }
        return $array;
    }

    public static function get($filterArray)
    {
        $filtered = self::all()->filter(function ($value, $key) use ($filterArray) {
            return in_array($key, $filterArray);
        });

        return $filtered->all();
    }


}