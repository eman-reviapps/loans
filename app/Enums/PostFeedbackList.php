<?php

namespace App\Enums;

class PostFeedbackList extends EnumsNew
{
    const FOUND_LENDER = 'FOUND_LENDER';
    const NO_LENDER_BID = 'NO_LENDER_BID';
    const BIDS_NOT_COMPETITIVE = 'BIDS_NOT_COMPETITIVE';
    const DIDNOT_LIKE_EXPERIENCE = 'DIDNOT_LIKE_EXPERIENCE';
    const NO_LONGER_REQUIRED_LOAN = 'NO_LONGER_REQUIRED_LOAN';
    const OTHER = 'OTHER';

    public static function items()
    {
        return [
            self::FOUND_LENDER => "Found a lender",
            self::NO_LENDER_BID => "No lender bid",
            self::BIDS_NOT_COMPETITIVE => "Bids not competitive",
            self::DIDNOT_LIKE_EXPERIENCE => "Did not like the experience",
            self::NO_LONGER_REQUIRED_LOAN => "No longer required loan",
            self::OTHER => "Other: please specify",
        ];
    }
}