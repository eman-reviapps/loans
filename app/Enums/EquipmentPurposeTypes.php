<?php

namespace App\Enums;

class EquipmentPurposeTypes extends EnumsNew
{
    const NEW_EQUIPMENT_AGE = 'NEW_EQUIPMENT_AGE';
    const USED_EQUIPMENT_PURCHASE = 'USED_EQUIPMENT_PURCHASE';
    const GROWTH = 'GROWTH';
    const REFINANCE = 'REFINANCE';
    const REPLACEMENT = 'REPLACEMENT';

    public static function items()
    {
        return [
            self::NEW_EQUIPMENT_AGE => "New Equipment Purchase",
            self::USED_EQUIPMENT_PURCHASE => "Used Equipment Purchase",
            self::GROWTH => "Growth",
            self::REFINANCE => "Refinance",
            self::REPLACEMENT => "Replacement",
        ];
    }

}