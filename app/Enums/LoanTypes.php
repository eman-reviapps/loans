<?php

namespace App\Enums;

class LoanTypes extends EnumsNew
{
    const LINE_OF_CREDIT = 'LINE_OF_CREDIT';
    const RE_PURCHASE_INVESTOR_0_50 = 'RE_PURCHASE_INVESTOR_0_50';
    const RE_PURCHASE_OWNER_51_100 = 'RE_PURCHASE_OWNER_51_100';
    const RE_REFINANCE_INVESTOR_0_50 = 'RE_REFINANCE_INVESTOR_0_50';
    const RE_REFINANCE_OWNER_51_100 = 'RE_REFINANCE_OWNER_51_100';
    const RE_CASH_OUT_INVESTOR_0_50 = 'RE_CASH_OUT_INVESTOR_0_50';
    const RE_CASH_OUT_OWNER_51_100 = 'RE_CASH_OUT_OWNER_51_100';
    const EQUIPMENT_FINANCE = 'EQUIPMENT_FINANCE';
    const TERM_LOAN_OTHER = 'TERM_LOAN_OTHER';

    public static function items()
    {
        return [
            self::LINE_OF_CREDIT => "Line of Credit",
            self::RE_PURCHASE_INVESTOR_0_50 => "Real Estate Purchase; Investor (0%-50% Owner Occupied)",
            self::RE_PURCHASE_OWNER_51_100 => "Real Estate Purchase; Owner Occupied (51%-100% Owner Occupied)",
            self::RE_CASH_OUT_INVESTOR_0_50 => "Real Estate Cash Out; Investor (0%-50% Owner Occupied)",
            self::RE_CASH_OUT_OWNER_51_100 => "Real Estate Cash Out; Owner Occupied (51% - 100% Owner Occupied)",
            self::RE_REFINANCE_INVESTOR_0_50 => "Real Estate Refinance; Investor (0%-50% Owner Occupied)",
            self::RE_REFINANCE_OWNER_51_100 => "Real Estate Refinance; Owner Occupied (51%-100% Owner Occupied)",
            self::EQUIPMENT_FINANCE => "Equipment Finance",
            self::TERM_LOAN_OTHER => "Other - Term Loan or Line of Credit",
        ];
    }

    public static function getRealEstateTypes()
    {
        $types = collect(self::all());

        $types = $types->reject(function ($value, $key) {
            return $key == self::EQUIPMENT_FINANCE;
        });
        return $types->all();
    }

    public static function getBusinessTypes()
    {
        return self::all();
    }
}