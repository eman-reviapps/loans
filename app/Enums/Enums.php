<?php
/**
 * Created by PhpStorm.
 * User: Eman
 * Date: 3/22/2017
 * Time: 11:24 AM
 */

namespace App\Enums;

use ReflectionClass;

class Enums
{
    private $array_items = [];
    private $class;
    public $titles;

    public function __construct()
    {
        $oClass = new ReflectionClass ($this->getClass());
        $constants = $oClass->getConstants();

        foreach ($constants as $constant => $key) {
            if ($constant != 'DEFAULT_VALUE') {
                $this->array_items[] = (object)["slug" => $key, "title" => $this->getTitle($key)];
            }
        }
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    public function all()
    {
        return collect($this->array_items)->all();
    }

    public function allKeyValue()
    {
        $all = $this->all();
        $array = [];
        foreach ($all as $record) {
            $array[$record->slug] = $record->title;
        }
        return $array;
    }

    public function get($filterArray)
    {
        $filtered = $this->all()->filter(function ($value, $key) use ($filterArray) {
            return in_array($key, $filterArray);
        });

        return $filtered->all();
    }

    /**
     * @param mixed $titles
     */
    public function setTitles()
    {

    }

    /**
     * @param $value
     * @return string
     */
    public function getTitle($key)
    {
        return $this->titles[$key];
    }
}