<?php

namespace App\Enums;

class BidStatuses extends EnumsNew
{
    const NO_RESPONSE = 'NO_RESPONSE';
    const BORROWER_RESPONDED = 'BORROWER_RESPONDED';
    const BORROWER_NOT_INTERESTED = 'BORROWER_NOT_INTERESTED';

    public static function items()
    {
        return [
            self::NO_RESPONSE => "No Response",
            self::BORROWER_RESPONDED => "Borrower Responded",
            self::BORROWER_NOT_INTERESTED => "Borrower Not Interested",
        ];
    }
    public static function description()
    {
        return [
            self::NO_RESPONSE => "Lender bids but has not yet received response from borrower",
            self::BORROWER_RESPONDED => "Borrower responded and indicates that he is interested",
            self::BORROWER_NOT_INTERESTED => "Borrower indicates they are not interested in lender bid. This will not allow further communication",
        ];
    }
}