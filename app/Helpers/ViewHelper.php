<?php

namespace App\Helpers;

use App\Enums\ActivationStates;
use App\Enums\BidStatuses;
use App\Enums\LoanStatuses;

class ViewHelper
{
    public static function getActivationStatusLabel($status)
    {
        switch ($status) {
            case ActivationStates::ACTIVE:
                return 'success';
                break;
            case ActivationStates::BLOCKED:
                return 'danger';
                break;
            case ActivationStates::ACTIVATION_REQUIRED:
                return 'warning';
                break;
            case ActivationStates::IN_PROGRESS:
                return 'info';
                break;
            default:
                return 'default';
                break;
        }
    }

    public static function getActivationDataStyle($status)
    {
        switch ($status) {
            case ActivationStates::ACTIVE:
                return 'green';
                break;
            case ActivationStates::BLOCKED:
                return 'red';
                break;
            case ActivationStates::ACTIVATION_REQUIRED:
                return 'yellow';
                break;
            case ActivationStates::IN_PROGRESS:
                return 'purple';
                break;
            default:
                return '';
                break;
        }
    }

    public static function getIsEnabledLabel($is_enabled)
    {

        switch ($is_enabled) {
            case 1:
                return 'success';
                break;
            case 0:
                return 'danger';
                break;
            default:
                return 'default';
                break;
        }
    }

    public static function getLoanStatusLabel($status)
    {
        switch ($status) {
            case LoanStatuses::DRAFT:
                return 'default';
                break;
            case LoanStatuses::LIVE:
                return 'info';
                break;
            case LoanStatuses::LENDER_BID:
                return 'primary';
                break;
            case LoanStatuses::BORROWER_RESPONDED:
                return 'success';
                break;
            case LoanStatuses::DELETED:
                return 'danger';
                break;
            case LoanStatuses::EXPIRED:
                return 'warning';
                break;
            case LoanStatuses::CLOSED:
                return 'default';
                break;
            default:
                return 'default';
                break;
        }
    }

    public static function getBidStatusLabel($status)
    {
        switch ($status) {
            case BidStatuses::NO_RESPONSE:
                return 'warning';
                break;
            case BidStatuses::BORROWER_RESPONDED:
                return 'success';
                break;
            case BidStatuses::BORROWER_NOT_INTERESTED:
                return 'danger';
                break;
            default:
                return 'default';
                break;
        }
    }

}