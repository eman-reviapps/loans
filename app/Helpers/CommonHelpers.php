<?php
/**
 * Created by PhpStorm.
 * User: Eman
 * Date: 4/10/2017
 * Time: 5:12 PM
 */

namespace App\Helpers;


class CommonHelpers
{
    public static function commaValuesToArray($values)
    {
        $ids = explode(",", $values);
        $ids = array_filter($ids, function ($k) {
            return $k != '' && !empty($k);
        });
        return $ids;
    }
}