<?php

namespace App\Helpers;


class Output
{
    public $status;
    public $key;
    public $sub_key;
    public $object;
    public $http_status;

    /**
     * Output constructor.
     * @param $status
     * @param $key
     * @param $sub_key
     * @param $object
     */
    public function __construct($status, $http_status, $key, $sub_key = '', $object = null)
    {
        $this->status = $status;
        $this->key = $key;
        $this->sub_key = $sub_key;
        $this->object = $object;
        $this->http_status = $http_status;
    }


    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getSubKey()
    {
        return $this->sub_key;
    }

    /**
     * @param mixed $sub_key
     */
    public function setSubKey($sub_key)
    {
        $this->sub_key = $sub_key;
    }

    /**
     * @return mixed
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param mixed $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return null
     */
    public function getHttpStatus()
    {
        return $this->http_status;
    }

    /**
     * @param null $http_status
     */
    public function setHttpStatus($http_status)
    {
        $this->http_status = $http_status;
    }


}