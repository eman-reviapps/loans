<?php
namespace App\Helpers;

class EmailHelpers
{
    public function getDomainFromEmail($email)
    {
        $parts = explode('@', $email);
        return $parts[1];
    }
}