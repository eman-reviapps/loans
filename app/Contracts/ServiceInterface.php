<?php
namespace App\Contracts;

use Illuminate\Database\Eloquent\Model;

interface ServiceInterface
{
    public function all();

    public function get(array $attributes, $count = false);

    public function first(array $attributes);

    public function create(array $attributes);

    public function update(Model $model, array $attributes);

    public function delete(Model $model);

    public function prepareCreate(array $attributes);

    public function prepareUpdate(Model $model, array $attributes);

    public function prepare(array $attributes);

//    public function filterRelations();

}