<?php
namespace App\Contracts;

interface SortCriteriaInterface
{
    function pushSortCriteria($criteria);

    function applySortCriteria($model);

    function applySort($model, $criteria);

}