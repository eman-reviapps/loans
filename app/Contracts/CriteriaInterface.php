<?php
namespace App\Contracts;

interface CriteriaInterface
{
    function pushCriteria($criteria);

    function getByCriteria($key);

    function applyCriteria();

    function apply($model, $criteria);

}