<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Cartalyst\Sentinel\Activations\EloquentActivation;
use App\Models\User;
use App\Models\Institution;

class UserRegistered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $activation;
    public $institution;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, EloquentActivation $activation, Institution $institution=null)
    {
        $this->user = $user;
        $this->activation = $activation;
        $this->institution = $institution;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
