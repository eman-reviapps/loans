<?php

namespace App\Events;

use App\Models\User;
use Cartalyst\Sentinel\Reminders\EloquentReminder;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserRequestedEmailReset
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $reminder;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user,EloquentReminder $reminder)
    {
        $this->user = $user;
        $this->reminder = $reminder;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
