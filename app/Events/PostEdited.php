<?php

namespace App\Events;

use App\Enums\NotificationTypes;
use App\Models\Bid;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Loan;
class PostEdited
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $loan;
    public $user;
    public $notification_type;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Loan $loan)
    {
        $this->loan = $loan;
        $temp = Bid::select('lender_id')->where('loan_id', $this->loan->id)->get();
        $this->user = User::whereIn('id', $temp)->get();
        $this->notification_type = NotificationTypes::POST_EDITED;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
