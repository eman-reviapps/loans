<?php

namespace App\Events;

use App\Models\Institution;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DomainStatusChanged
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $institution;
    public $users;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Institution $institution, $users)
    {
        $this->institution = $institution;
        $this->users = $users;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
