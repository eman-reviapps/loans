<?php

namespace App\Events;

use App\Enums\NotificationTypes;
use App\Models\Bid;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BidPlaced
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $bid;
    public $notification_type;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Bid $bid)
    {
        $this->user = $user;
        $this->bid = $bid;
        $this->notification_type = NotificationTypes::BID_RECEIVED;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
