<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanArchive extends Model
{
    protected $guarded = [];

    protected $table = 'loan_archive';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id')->withTrashed();
    }

    public function loan()
    {
        return $this->belongsTo('App\Models\Loan', 'loan_id');
    }
}
