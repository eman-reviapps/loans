<?php

namespace App\Models;

use App\Enums\BidStatuses;
use App\Services\RoleService;
use Sentinel;
use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    protected $guarded = [];

    public function conversation()
    {
        return $this->hasOne('App\Models\Conversation', 'bid_id');
    }

    public function loan()
    {
        return $this->belongsTo('App\Models\Loan', 'loan_id');
    }

    public function lender()
    {
        return $this->belongsTo('App\Models\User', 'lender_id');
    }

    public function getCanFollowUpAttribute()
    {
        if (
            $this->status == BidStatuses::BORROWER_NOT_INTERESTED
            ||
            (
                $this->status == BidStatuses::NO_RESPONSE
                &&
                Sentinel::inRole(RoleService::ROLE_LENDER)
                &&
                count($this->conversation->replies->where('sender_id', Sentinel::getUser()->id)) >= 3
                &&
                count($this->conversation->replies->where('receiver_id', Sentinel::getUser()->id)) == 0
            )
        ) {
            return false;
        } else {
            return true;
        }
    }
}
