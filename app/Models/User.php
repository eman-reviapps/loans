<?php

namespace App\Models;

use App\Enums\ActivationStates;
use Carbon\Carbon;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Cashier\Billable;

class User extends EloquentUser
{
    use SoftDeletes, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'status'
    ];

    protected $dates = ['deleted_at', 'trial_ends_at'];


    public function lenderProfile()
    {
        return $this->hasOne('App\Models\LenderProfile', 'user_id');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getUserRoleNameAttribute()
    {
        $roles_name = '';
        if ($this->roles) {
            foreach ($this->roles as $role) {
                $roles_name .= $role->name;
            }
        }
        return $roles_name;
    }

    public function getLenderBidsCountAttribute()
    {
        return count($this->lender_bids);
    }

    public function getBorrowerLoansCountAttribute()
    {
        return count($this->borrower_loans);
    }

    public function getRoleAttribute()
    {
        return $this->roles[0];
    }

    public function borrowerProfiles()
    {
        return $this->hasMany('App\Models\BorrowerProfile', 'user_id', 'id');
    }

    public function tracks()
    {
        return $this->hasMany('App\Models\UserTrack', 'user_id', 'id');
    }

    public function preferences()
    {
        return $this->hasMany('App\Models\Preference', 'user_id', 'id');
    }

    public function borrower_loans()
    {
        return $this->hasMany('App\Models\Loan', 'borrower_id', 'id');
    }

    public function archived_loans()
    {
        return $this->belongsToMany('App\Models\Loan', 'loan_archive', 'user_id', 'loan_id');
    }

    public function lender_bids(){
        return $this->hasMany('App\Models\Bid', 'lender_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', ActivationStates::ACTIVE);
    }
    public function scopeFirstReminder($query)
    {
        return $query->where('free_lifetime', 0)
            ->where('first_reminder_email_sent', 0)
            ->where('second_reminder_email_sent', 0)
            ->where('third_reminder_email_sent', 0);
    }

    public function scopeSecondReminder($query)
    {
        return $query->where('free_lifetime', 0)
            ->where('first_reminder_email_sent', 1)
            ->where('second_reminder_email_sent', 0)
            ->where('third_reminder_email_sent', 0);
    }

    public function scopeThirdReminder($query)
    {
        return $query->where('free_lifetime', 0)
            ->where('first_reminder_email_sent', 1)
            ->where('second_reminder_email_sent', 1)
            ->where('third_reminder_email_sent', 0);
    }

    public function scopeTrialEndsWithinDays($query, $days)
    {
        return $query->where('trial_ends_at', '<=', Carbon::now()->addDays($days));
    }

}
