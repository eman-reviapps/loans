<?php

namespace App\Models;

use App\Enums\BorrowerProfileTypes;
use App\Enums\LoanStatuses;
use Illuminate\Database\Eloquent\Model;
use Sentinel;

class Loan extends Model
{
    protected $guarded = [];

    /*Relations*/
    public function details()
    {
        return $this->hasOne('App\Models\LoanDetail', 'loan_id');
    }

    public function preferences()
    {
        return $this->hasMany('App\Models\Preference', 'loan_id')->where('loan_id','<>',null);
    }

    public function tenants()
    {
        return $this->hasMany('App\Models\Tenant', 'loan_id');
    }

    public function borrower()
    {
        return $this->belongsTo('App\Models\User', 'borrower_id')->withTrashed();
    }

    public function borrower_profile()
    {
        return $this->belongsTo('App\Models\BorrowerProfile', 'profile_id');
    }

    public function bids()
    {
        return $this->hasMany('App\Models\Bid', 'loan_id');
    }

    public function archived()
    {
        return $this->belongsToMany('App\Models\User', 'loan_archive', 'loan_id', 'user_id');
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachable');
    }

    /*scopes*/
    public function scopeActiveBorrowerProfile($query)
    {
        return $query->where('borrower_id', Sentinel::getUser()->id)
            ->where('profile_id', session('borrower_profile_id'));
    }

    public function scopeActive($query)
    {
        return $query->where('status', '<>', LoanStatuses::DELETED)
            ->where('status', '<>', LoanStatuses::EXPIRED)
            ->where('status', '<>', LoanStatuses::CLOSED)
            ;
    }

    public function scopePublished($query)
    {
        return $query->where('status', '<>', LoanStatuses::DELETED)
            ->where('status', '<>', LoanStatuses::EXPIRED)
            ->where('status', '<>', LoanStatuses::DRAFT)
            ->where('status', '<>', LoanStatuses::CLOSED)
            ;
    }

    public function scopeExpired($query)
    {
        return $query->where('status', LoanStatuses::EXPIRED);
    }

    public function scopeDeleted($query)
    {
        return $query->where('status', LoanStatuses::DELETED);
    }

    public function scopeOfStatus($query, $type)
    {
        return $query->where('status', $type);
    }

//    public function scopeArchived($query)
//    {
//        return $query->where('lender_archived', 0);
//    }
//
//    public function scopeNotArchived($query)
//    {
//        return $query->where('lender_archived', 0);
//    }


    /*acccessors*/
    public function getIsActiveAttribute($query)
    {
        return $this->status != LoanStatuses::EXPIRED && $this->status != LoanStatuses::DELETED& $this->status != LoanStatuses::CLOSED;
    }

    public function getCompanyNameAttribute($query)
    {
        return $this->borrower_profile->profile_type == BorrowerProfileTypes::BUSINESS ?
            $this->borrower_profile->profile->business_name
            : $this->borrower->full_name;
    }

    public function getFullAddressAttribute()
    {
        return ($this->apt_suite_no && $this->apt_suite_no != 0 ? ($this->apt_suite_no . ", ") : '')
            . ($this->address ? ($this->address . ", ") : '')
            . ($this->city ? ($this->city . ", ") : '')
            . ($this->state ? $this->state->state . " " : '')
            . ($this->zip_code ? ($this->zip_code) : '');
    }
//    public function getRequestedValueAttribute($query)
//    {
//        return $this->details->requested_amount ? $this->details->requested_amount : $this->details->requested_percent;
//    }

}
