<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $guarded = [];

    protected $primaryKey = 'state_code';

    public $incrementing = false;

    public function getRouteKeyName()
    {
        return 'state_code';
    }

    public function cities()
    {
        return $this->hasMany('App\Models\City', 'state_code', 'state_code');
    }

    public function getStateCountiesCountAttribute()
    {
        return City::where('state_code', $this->state_code)->distinct('county')->count('county');
    }
    public function getCountyCitiesCountAttribute()
    {
        return City::where('state_code', $this->state_code)
            ->where('county', $this->county)->distinct('county')->count('county');
    }
}
