<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UserTrack extends Model
{
    protected $guarded = [];

    protected $dates = ['login_at', 'logout_at'];

    public function getDateAttribute()
    {
        return $this->login_at->toDateString();
    }

    public function getLoginTimeAttribute()
    {
        return $this->login_at->format('h:i:s A');
    }

    public function getLogoutTimeAttribute()
    {
        return (isset($this->logout_at) && null != $this->logout_at) ? $this->logout_at->format('h:i:s A') : '';
    }

    public function getAvailabilityAttribute()
    {
        $login_at = Carbon::parse($this->login_at);
        $logout_at = Carbon::parse($this->logout_at);

        return isset($logout_at) && null != $logout_at ? ($logout_at->diffInHours($login_at)) . " Hours , " . ($logout_at->diffInMinutes($login_at) % 60) . " Minutes" : '';
    }

    public function getAvailabilityPercentageAttribute()
    {
        $login_at = Carbon::parse($this->login_at);
        $logout_at = Carbon::parse($this->logout_at);
        $minutes = $logout_at->diffInMinutes($login_at);

        return isset($this->is_logged_out) && $this->is_logged_out ? round(($minutes / (6 * 60)) * 100, 2) : '';
    }
}
