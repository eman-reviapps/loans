<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LenderProfile extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id')->withTrashed();
    }

    public function institution()
    {
        return $this->belongsTo('App\Models\Institution', 'institution_id', 'id');
    }

}
