<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationReplyAttachment extends Model
{
    protected $guarded = [];
}
