<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPreference extends Model
{
    protected $guarded = [];

    public function getCurrentValueAttribute()
    {
        return (isset($this->value) && !empty($this->value)) ? $this->value : $this->default_value;
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = is_array($value) ? json_encode($value, JSON_FORCE_OBJECT) : $value;
    }

    public function getValueAttribute($value)
    {
        $decoded = json_decode($value);
        return $decoded ? (array)$decoded : $value;
    }
}
