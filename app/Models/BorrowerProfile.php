<?php

namespace App\Models;

use App\Enums\BorrowerProfileTypes;
use Illuminate\Database\Eloquent\Model;
use Sentinel;

class BorrowerProfile extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id')->withTrashed();
    }

    public function profile()
    {
        return $this->morphTo();
    }

    public function preferences()
    {
        return $this->hasMany('App\Models\Preference', 'profile_id')->where('loan_id', null);
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachable');
    }

    public function loans()
    {
        return $this->hasMany('App\Models\Loan', 'profile_id');
    }

    public function getProfileTitleAttribute()
    {
        if ($this->profile_type == BorrowerProfileTypes::BUSINESS) {
            return $this->profile->business_name;
        } elseif ($this->profile_type == BorrowerProfileTypes::REAL_ESTATE) {
//            return $this->user->full_name;
//            return $this->user->full_name . ' / ' . $this->profile_no;
            return $this->user->full_name . ' / ' . $this->id;
        }
    }

    public function getStateCodeAttribute()
    {
        return $this->profile->state_code;
    }

    public function getCityAttribute()
    {
        return $this->profile->city;
    }

    public function getZipCodeAttribute()
    {
        return $this->profile->zip_code;
    }

    public function getCountyAttribute()
    {
        return $this->profile->the_city->county;
    }

    public function scopeEnabled($query)
    {
        return $query->where('is_enabled', true);
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

//    public function businessProfile()
//    {
//        return $this->hasOne('App\Models\BusinessProfile','profile_id','id');
//    }
//
//    public function realEstateProfile()
//    {
//        return $this->hasOne('App\Models\RealEstateProfile','profile_id','id');
//    }
//
//    public function scopeProfile($query)
//    {
//        return $query
//            ->when($this->type === BorrowerProfileTypes::BUSINESS,function($q){
//                return $q->with('businessProfile');
//            })
//            ->when($this->type === BorrowerProfileTypes::REAL_ESTATE,function($q){
//                return $q->with('realEstateProfile');
//            });
//    }
}
