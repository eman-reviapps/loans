<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessProfile extends Model
{
    protected $guarded = [];

    public function borrower_profile()
    {
        return $this->morphOne('App\Models\BorrowerProfile', 'profile');
    }

    public function the_city()
    {
        return $this->belongsTo('App\Models\City', 'zip_code', 'zip_code');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State', 'state_code', 'state_code');
    }

    public function getFullAddressAttribute()
    {
        return ($this->apt_suite_no && $this->apt_suite_no != 0 ? ($this->apt_suite_no . ", ") : '')
            . ($this->address ? ($this->address . ", ") : '')
            . ($this->city ? ($this->city . ", ") : '')
            . ($this->state ? $this->state->state . " " : '')
//            . " -- ".($this->the_city ? $this->the_city->county . "  -- " : '')
            . ($this->zip_code ? ($this->zip_code) : '');
    }

    public function getCompanyEBITDAAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getAnnualRevenueYear1Attribute($value)
    {
        return number_format((int)($value));
    }

    public function getAnnualRevenueYear2Attribute($value)
    {
        return number_format((int)($value));
    }

    public function getAnnualRevenueYear3Attribute($value)
    {
        return number_format((int)($value));
    }


    public function getNetIncomeBeforeTaxYear1Attribute($value)
    {
        return number_format((int)($value));
    }

    public function getNetIncomeBeforeTaxYear2Attribute($value)
    {
        return number_format((int)($value));
    }

    public function getNetIncomeBeforeTaxYear3Attribute($value)
    {
        return number_format((int)($value));
    }


    public function getInterestExpenseYear1Attribute($value)
    {
        return number_format((int)($value));
    }

    public function getInterestExpenseYear2Attribute($value)
    {
        return number_format((int)($value));
    }

    public function getInterestExpenseYear3Attribute($value)
    {
        return number_format((int)($value));
    }


    public function getDepreciationAmortizationYear1Attribute($value)
    {
        return number_format((int)($value));
    }

    public function getDepreciationAmortizationYear2Attribute($value)
    {
        return number_format((int)($value));
    }

    public function getDepreciationAmortizationYear3Attribute($value)
    {
        return number_format((int)($value));
    }


    public function getIncomeTaxExpenseYear1Attribute($value)
    {
        return number_format((int)($value));
    }

    public function getIncomeTaxExpenseYear2Attribute($value)
    {
        return number_format((int)($value));
    }

    public function getIncomeTaxExpenseYear3Attribute($value)
    {
        return number_format((int)($value));
    }


    public function getCompanyEBITDAYear1Attribute($value)
    {
        return number_format((int)($value));
    }

    public function getCompanyEBITDAYear2Attribute($value)
    {
        return number_format((int)($value));
    }

    public function getCompanyEBITDAYear3Attribute($value)
    {
        return number_format((int)($value));
    }
}
