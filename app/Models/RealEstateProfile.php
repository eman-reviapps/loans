<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RealEstateProfile extends Model
{
    protected $guarded = [];

    protected $table = 'real_estate_profiles';

    public function borrower_profile()
    {
        return $this->morphOne('App\Models\BorrowerProfile', 'profile');
    }

    public function the_city()
    {
        return $this->belongsTo('App\Models\City', 'zip_code', 'zip_code');
    }


    public function state()
    {
        return $this->belongsTo('App\Models\State', 'state_code', 'state_code');
    }

    public function properties()
    {
        return $this->hasMany('App\Models\Property', 'profile_id', 'id');
    }

    /*Accessors*/
    public function getFullAddressAttribute()
    {
        return ($this->apt_suite_no && $this->apt_suite_no != 0 ? ($this->apt_suite_no . ", ") : '')
            . ($this->address ? ($this->address . ", ") : '')
            . ($this->city ? ($this->city . ", ") : '')
            . ($this->state ? $this->state->state . " " : '')
//            . " -- ".($this->the_city ? $this->the_city->county . "  -- " : '')
            . ($this->zip_code ? ($this->zip_code) : '');
    }

}
