<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'lease_expiration_date'
    ];

    public function loan()
    {
        return $this->belongsTo('App\Models\Loan', 'loan_id');
    }


    public function lease_type()
    {
        return $this->belongsTo('App\Models\LeaseType', 'lease_type_id');
    }

    public function getLeaseExpirationDateOnlyAttribute($value)
    {
        return $this->lease_expiration_date->format('m/d/Y');
    }

    public function getLeaseRateAttribute($value)
    {
        return number_format((int)($value));
    }

}
