<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstitutionBranch extends Model
{
    protected $guarded = [];

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function institution()
    {
        return $this->belongsTo('App\Models\Institution', 'institution_id', 'id');
    }

    public function branchState()
    {
        return $this->belongsTo('App\Models\State', 'state', 'state_code');
    }

    public function branchCity()
    {
        return $this->belongsTo('App\Models\City', 'city', 'city');
    }
}
