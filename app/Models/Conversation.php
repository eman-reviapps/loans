<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $guarded = [];

    public function bid()
    {
        return $this->belongsTo('App\Models\Bid', 'bid_id');
    }

    public function sender()
    {
        return $this->belongsTo('App\Models\User', 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Models\User', 'receiver_id');
    }

    public function replies()
    {
        return $this->hasMany('App\Models\Reply', 'conversation_id');
    }
}
