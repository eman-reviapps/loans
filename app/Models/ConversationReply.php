<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationReply extends Model
{
    protected $guarded = [];
}
