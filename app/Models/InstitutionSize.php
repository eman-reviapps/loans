<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstitutionSize extends Model
{
    protected $guarded = [];

    public function scopeEnabled($query)
    {
        return $query->where('is_enabled', 1);
    }
}
