<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'loan_maturity_date'
    ];

    public function profile()
    {
        return $this->belongsTo('App\Models\RealEstateProfile', 'profile_id', 'id');
    }

    public function realestatetype()
    {
        return $this->belongsTo('App\Models\RealEstateType', 'realestate_type_id', 'id');
    }

    public function getEstimatedMarketValueAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getLoanOutstandingAttribute($value)
    {
        return number_format((int)($value));
    }

//    public function getLoanMaturityDateAttribute($value)
//    {
//        return $value->toDateString();
//    }

    public function setEstimatedMarketValueAttribute($value)
    {
        $this->attributes['estimated_market_value'] = str_replace(",","",$value);
    }

}
