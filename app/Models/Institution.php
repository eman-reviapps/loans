<?php

namespace App\Models;

use App\Enums\ActivationStates;
use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    protected $guarded = [];

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function getFullTitleAttribute()
    {
        return $this->title . ' / ' . $this->domain;
    }

    public function lenderProfiles()
    {
        return $this->hasMany('App\Models\LenderProfile', 'institution_id', 'id');
    }

    public function institutionType()
    {
        return $this->belongsTo('App\Models\InstitutionType', 'type_id', 'id');
    }

    public function institutionSize()
    {
        return $this->belongsTo('App\Models\InstitutionSize', 'size_id', 'id');
    }

    public function institutionState()
    {
        return $this->belongsTo('App\Models\State', 'state', 'state_code');
    }

    public function institutionCity()
    {
        return $this->belongsTo('App\Models\City', 'city', 'city');
    }

    public function scopeActive($query)
    {
        return $query->where('status', ActivationStates::ACTIVE);
    }

//    public function lenders()
//    {
//        return $this->hasManyThrough('App\Models\User', 'App\Models\LenderProfile', 'user_id', 'institution_id');
//    }
}
