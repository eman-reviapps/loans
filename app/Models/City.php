<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $guarded = [];

    protected $primaryKey = 'zip_code';

    public $incrementing = false;

    public function getRouteKeyName()
    {
        return 'zip_code';
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State', 'state_code', 'state_code');
    }
}
