<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanDetail extends Model
{
    protected $guarded = [];

    public function loan()
    {
        return $this->belongsTo('App\Models\Loan', 'loan_id');
    }

    public function purpose_type()
    {
        return $this->belongsTo('App\Models\PurposeType', 'purpose_type_id');
    }


    public function the_city()
    {
        return $this->belongsTo('App\Models\City', 'zip_code', 'zip_code');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State', 'state_code', 'state_code');
    }

    public function getCountyAttribute()
    {
        return $this->the_city->county;
    }


    public function real_estate_type()
    {
        return $this->belongsTo('App\Models\RealEstateType', 'real_estate_type_id');
    }

    /*Accessors*/
    public function getRequestedValueAttribute()
    {
        $value = null != $this->requested_amount ? $this->requested_amount : $this->requested_percent;
        return ($value);
    }

    public function getOutstandingValueAttribute($value)
    {
        return number_format((int)($value));
    }


    public function getRequestedAmountAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getLessTradeAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getLessDownPaymentAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getEstimatedPropertyValueAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getEstimatedMarketPriceAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getReceivableRangeFromAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getReceivableRangeToAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getInventoryRangeFromAttribute($value)
    {
        return isset($value) ? number_format((int)($value)) : "";
    }

    public function getInventoryRangeToAttribute($value)
    {
        return isset($value) ? number_format((int)($value)) : "";
    }

    public function getPurchasePriceAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getCashBeforeDownPaymentAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getGrossAnnualRevenueAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getEstimatedAnnualExpensesAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getEstimatedNetOperatingIncomeAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getNetOperatingIncomeAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getAnnualIncomeFromTenantsAttribute($value)
    {
        return number_format((int)($value));
    }


    public function getCompanyEBITDAAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getTotalLiabilitiesAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getFundedDebtAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getTangibleNetWorthAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getRentalAnnualRevenueAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getCashLiquidInvestmentsAttribute($value)
    {
        return number_format((int)($value));
    }

    public function getRentalRevenueByTenantsAttribute($value)
    {
        return number_format((int)($value));
    }


    public function getFullAddressAttribute()
    {
        return ($this->apt_suite_no && $this->apt_suite_no != 0 ? ($this->apt_suite_no . ", ") : '')
            . ($this->address ? ($this->address . ", ") : '')
            . ($this->city ? ($this->city . ", ") : '')
            . ($this->state ? $this->state->state . " " : '')
//            . " -- ".($this->the_city ? $this->the_city->county . "  -- " : '')
            . ($this->zip_code ? ($this->zip_code) : '');
    }

}
