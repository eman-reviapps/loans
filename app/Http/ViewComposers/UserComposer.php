<?php

namespace App\Http\ViewComposers;

use App\Models\Conversation;
use App\Models\Loan;
use App\Models\LoanFeedback;
use App\Models\Reply;
use App\Models\UserNotification;
use App\Services\RoleService;
use Illuminate\View\View;
use Sentinel;

/**
 * Created by PhpStorm.
 * User: Eman
 * Date: 3/7/2017
 * Time: 2:25 PM
 */
class UserComposer
{

    public function __construct()
    {
    }

    public function compose(View $view)
    {
        $logged_in_user = Sentinel::getUser();

        if ($logged_in_user) {
            $notifications = UserNotification::where('user_id', $logged_in_user->id)->orderBy('created_at', 'desc')->get();
            $unread_notifications_count = UserNotification::where('user_id', $logged_in_user->id)->where('is_read', 0)->count();

            if (Sentinel::inRole(RoleService::ROLE_ADMIN)) {
                $messages = LoanFeedback::select()->orderBy('created_at', 'desc')->get();
                foreach ($messages as $message){
                    $loan = Loan::find($message->loan_id);
                    $text = $loan->borrower->email . " has DELETED his post for the following reason: " . $message->reason;
                    $message->reason = $text;
                }
                $unread_messages_count = LoanFeedback::all()->count();
            } else {
                $messages = Conversation::whereHas('replies', function ($query) use($logged_in_user) {
                    $query->where('receiver_id', $logged_in_user->id);
                })->orderBy('created_at', 'desc')->get();

                $unread_messages_count = Conversation::whereHas('replies', function ($query) use($logged_in_user) {
                    $query->where('is_read', 0)->where('receiver_id', $logged_in_user->id);
                })->count();
            }
        }

        $view
            ->with(compact('logged_in_user', 'notifications', 'unread_notifications_count', 'messages', 'unread_messages_count'));
    }
}