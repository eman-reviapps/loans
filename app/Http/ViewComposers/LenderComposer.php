<?php
/**
 * Created by PhpStorm.
 * User: Kareem Waleed
 * Date: 11/9/2017
 * Time: 5:04 PM
 */

namespace App\Http\ViewComposers;

use App\Models\Loan;
use Illuminate\View\View;
use App\Services\LoanService;
use Sentinel;

class LenderComposer
{

    private $loanService;

    public function __construct(LoanService $loanService)
    {
        $this->loanService = $loanService;
    }

    public function compose(View $view)
    {
        $bids_placed_count = $this->loanService->lenderLoans(Sentinel::getUser())->count();
        $loans_count = $this->loanService->findLoans(Sentinel::getUser())->count();
        $archived_count = $this->loanService->archived(Sentinel::getUser())->count();

        $view
            ->with(compact('bids_placed_count', 'loans_count', 'archived_count'));
    }
}