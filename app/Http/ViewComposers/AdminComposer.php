<?php
namespace App\Http\ViewComposers;

use App\Enums\ActivationStates;
use App\Services\RoleService;
use App\Services\UserService;
use App\Services\InstitutionService;
use Illuminate\View\View;

/**
 * Created by PhpStorm.
 * User: Eman
 * Date: 3/7/2017
 * Time: 2:25 PM
 */
class AdminComposer
{
    protected $institutionService;
    protected $userService;

    public function __construct(InstitutionService $institutionService, UserService $userService)
    {
        $this->institutionService = $institutionService;
        $this->userService = $userService;
    }

    public function compose(View $view)
    {
        $domains_activation_required_count = $this->institutionService->get(["status" => ActivationStates::ACTIVATION_REQUIRED], true);
        $users_activation_required_count = $this->userService->get(["status" => ActivationStates::ACTIVATION_REQUIRED], true);
        $lenders_count = $this->userService->get(["role" => RoleService::ROLE_LENDER], true);
        $borrowers_count = $this->userService->get(["role" => RoleService::ROLE_BORROWER], true);

        $view
            ->with(compact('domains_activation_required_count', 'users_activation_required_count', 'lenders_count', 'borrowers_count'));
    }
}