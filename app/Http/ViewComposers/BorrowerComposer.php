<?php

namespace App\Http\ViewComposers;

use App\Models\Loan;
use Illuminate\View\View;

/**
 * Created by PhpStorm.
 * User: Eman
 * Date: 3/7/2017
 * Time: 2:25 PM
 */
class BorrowerComposer
{

    public function compose(View $view)
    {
        $active_loans_count = Loan::activeBorrowerProfile()->active()->count();
        $expired_loans_count = Loan::activeBorrowerProfile()->expired()->count();
        $deleted_loans_count = Loan::activeBorrowerProfile()->deleted()->count();

        $view
            ->with(compact('active_loans_count', 'expired_loans_count', 'deleted_loans_count'));
    }
}