<?php
namespace App\Http\ViewComposers;

use App\Models\InstitutionSize;
use App\Models\InstitutionType;
use App\Models\State;
use Illuminate\View\View;
use Sentinel;

/**
 * Created by PhpStorm.
 * User: Eman
 * Date: 3/7/2017
 * Time: 2:25 PM
 */
class InstitutionsSearchComposer
{

    public function __construct()
    {
    }

    public function compose(View $view)
    {
        $institution_types = InstitutionType::pluck('name', 'id')->all();
        $institution_sizes = InstitutionSize::pluck('name', 'id')->all();

        $states = State::pluck('state', 'state_code')->all();

        $view
            ->with(compact('institution_types', 'institution_sizes', 'states'));
    }
}