<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\State;
use App\Services\FilterServices;
use App\Services\InstitutionService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DropdownsControllers extends Controller
{
    protected $filterServices;
    protected $institutionService;

    public function __construct(FilterServices $filterServices, InstitutionService $institutionService)
    {
        $this->filterServices = $filterServices;
        $this->institutionService = $institutionService;
    }

    public function cities(Request $request)
    {
        $query = City::where('state_code', '<>', '');

        if(isset($request->term))
        {
            $city = $request->term['term'];
            $this->filterServices->add_filter($query, 'city', $city,false,true);
        }

        if ($request->has('state_code')) {
            $this->filterServices->add_filter($query, 'state_code', $request->state_code);
        }

        $cities = $query->select(['city', 'state_code'])->distinct('city')->orderBy('city')->get();

        return $this->getResponse($cities);
    }

    public function zipCodes(Request $request)
    {
        $query = City::where('state_code', '<>', '');

        if(isset($request->term))
        {
            $zip_code = $request->term['term'];
            $this->filterServices->add_filter($query, 'zip_code', $zip_code,false,true);
        }

        if ($request->has('state_code')) {
            $this->filterServices->add_filter($query, 'state_code', $request->state_code);
        }

        $cities = $query->select(['zip_code', 'city'])->orderBy('city')->get();

        return $this->getResponse($cities);
    }

    public function states(Request $request)
    {
        $query = State::where('state_code', '<>', '');

        if(isset($request->term))
        {
            $state = $request->term['term'];
            $this->filterServices->add_filter($query, 'state', $state,false,true);
        }

        $states = $query->select(['state_code', 'state'])->orderBy('state')->get();

        return $this->getResponse($states);
    }

    public function counties(Request $request)
    {
        $query = City::where('state_code', '<>', '');

        if(isset($request->term))
        {
            $county = $request->term['term'];
            $this->filterServices->add_filter($query, 'county', $county,false,true);
        }

        if ($request->has('states')) {
            $this->filterServices->add_filter($query, 'state_code', $request->states);
        }

        if ($request->has('cities')) {
            $this->filterServices->add_filter($query, 'city', $request->cities);
        }

        $counties = $query->select(['county'])->distinct()->orderBy('county')->get();

        return $this->getResponse($counties);
    }


    private function getResponse($data)
    {
        return (new Response($data, '200'))->header('Content-Type', 'application/json');
    }

    public function zipCodesForCity(Request $request){
        $zip_codes = City::select('zip_code')->where('city', '=', $request->city_name)
            ->where('state_code', '=', $request->state_code)->get();
        return $this->getResponse($zip_codes);
    }
}
