<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Services\RoleService;
use App\Traits\ControllersTrait;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    use ControllersTrait;
    protected $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->setMessagesKey('roles');
        $this->setSuccessRedirectTo('admin/roles');

        $this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $roles = $this->roleService->all($request->all());

        if ($request->ajax()) {
            return response(
                view('v1.cp.admin.roles.filter', compact('roles')
                )->render()
            );
        }
        return view('v1.cp.admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Role $role)
    {
        $permissions_tree = json_encode($this->roleService->getRolePermissionsTree($role,false));
        return view('v1.cp.admin.roles.permissions', compact('role','permissions_tree'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        
        $response = $this->roleService->update($role,$request->all());
        return $this->handleReponse($request, $response);

    }


}
