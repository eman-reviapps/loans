<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\LoanService;
use App\Services\UserService;

class DashboardController extends Controller
{
    private $loanService;
    private $userService;

    public function __construct(LoanService $loanService, UserService $userService)
    {
        $this->loanService = $loanService;
        $this->userService = $userService;
    }

    public function getIndex()
    {
        $lenders_count = $this->userService->numberOfLenders();
        $borrowers_count = $this->userService->numberOfBorrowers();
        $loan_count = $this->loanService->totalNumberOfLoans();
        $active_lenders_count = $this->userService->numberOfActiveLenders();
        $active_borrowers_count = $this->userService->numberOfActiveBorrowers();
        $bids_count = $this->loanService->bidsCount();
        return view('v1.cp.admin.dashboards.dashboard_1',
            compact('lenders_count', 'borrowers_count', 'loan_count',
                'active_lenders_count', 'active_borrowers_count', 'bids_count'));
    }

    public function getIndex2(){
        return view('v1.cp.admin.dashboards.dashboard_2');
    }

    public function getIndex3(){
        return view('v1.cp.admin.dashboards.dashboard_3');
    }

}


