<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\State;
use App\Services\LocationService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Traits\ControllersTrait;

class StateController extends Controller
{
    use ControllersTrait;

    protected $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->setSuccessRedirectTo('admin/states');

        $this->locationService = $locationService;
    }

    public function index(Request $request)
    {
        $states = State::all();

        if ($request->ajax()) {
            return response(view('v1.cp.admin.geo-location.states.filter', compact('states'))->render());
        }
        return view('v1.cp.admin.geo-location.states.index', compact('states'));
    }

    public function counties(Request $request, State $state)
    {
        $counties = $this->locationService->getStateCounties($state);

        if ($request->ajax()) {
            return response(view('v1.cp.admin.geo-location.counties.filter',
                compact('counties', 'state')
            )->render());
        }
        return view('v1.cp.admin.geo-location.counties.index',
            compact('counties', 'state')
        );
    }

    public function cities(Request $request, State $state, $county)
    {
        $cities = $this->locationService->getCountyCities($state, $county);

        if ($request->ajax()) {
            return response(view('v1.cp.admin.geo-location.cities.filter',
                compact('cities', 'state', 'county')
            )->render());
        }
        return view('v1.cp.admin.geo-location.cities.index',
            compact('cities', 'state', 'county')
        );
    }

    public function zip_codes(Request $request, State $state, $county, $city_name)
    {
        $zip_codes = $this->locationService->getCityZipCodes($state, $county, $city_name);

        if ($request->ajax()) {
            return response(view('v1.cp.admin.geo-location.zip_codes.filter',
                compact('zip_codes', 'state', 'county', 'city_name')
            )->render());
        }
        return view('v1.cp.admin.geo-location.zip_codes.index',
            compact('zip_codes', 'state', 'county', 'city_name')
        );
    }

    public function changeStatus(Request $request, State $state)
    {
        $response = $this->locationService->updateState($state, $request->all());
        if($response) {
            return redirect()->back();
        }
    }

    public function changeCountyStatus(Request $request, State $state, $county)
    {
        $response = $this->locationService->updateCounty($state, $county, $request->all());
        if($response) {
            return redirect()->back();
        }
    }

    public function changeCityStatus(Request $request, State $state, $county, $city_name)
    {
        $response = $this->locationService->updateCity($state, $county, $city_name, $request->all());
        if($response) {
            return redirect()->back();
        }
    }

    public function changeZipCodeStatus(Request $request, State $state, $county, $city_name, $zip_code)
    {
        $response = $this->locationService->updateZipCode($state, $county, $city_name, $zip_code, $request->all());
        if($response) {
            return redirect()->back();
        }
    }

    public function checkEnabled(Request $request)
    {
        $state = State::find($request->state_code);
        $is_enabled = ($state && $state->is_enabled) ? true : false;

        return (new Response($is_enabled ? "true" : "false", '200'))->header('Content-Type', 'application/json');
    }

    public function checkCityEnabled(Request $request)
    {
        $is_enabled = $this->locationService->isCityEnabled($request->state_code, $request->city);
        return (new Response($is_enabled ? "true" : "false", '200'))->header('Content-Type', 'application/json');
    }

    public function checkZipCodeEnabled(Request $request)
    {
        $is_enabled = $this->locationService->isZipCodeEnabled($request->zip_code);
        return (new Response($is_enabled ? "true" : "false", '200'))->header('Content-Type', 'application/json');
    }

    public function checkValidZipCode(Request $request)
    {
        $is_enabled = $this->locationService->isValidZipCode($request->all());
        return (new Response($is_enabled ? "true" : "false", '200'))->header('Content-Type', 'application/json');
    }

    public function selectAllStates(){
        $response = $this->locationService->enableAllStates();
        if($response) {
            return redirect('admin/states');
        }
    }

    public function unselectAllStates(){
        $response = $this->locationService->disableAllStates();
        if($response) {
            return redirect('admin/states');
        }
    }

    public function selectAllCounties(Request $request){
        $response = $this->locationService->enableAllCountiesForState($request->state_code);
        if($response) {
            return redirect('admin/states/' . $request->state_code . '/counties');
        }
    }

    public function unselectAllCounties(Request $request){
        $response = $this->locationService->disableAllCountiesForState($request->state_code);
        if($response) {
            return redirect('admin/states/' . $request->state_code . '/counties');
        }
    }

    public function selectAllCities(Request $request){
        $response = $this->locationService->enableAllCitiesForCounty($request->all());
        if($response) {
            return redirect('admin/states/' . $request->state_code . '/counties/' . $request->county . '/cities');
        }
    }

    public function unselectAllCities(Request $request){
        $response = $this->locationService->disableAllCitiesForCounty($request->all());
        if($response) {
            return redirect('admin/states/' . $request->state_code . '/counties/' . $request->county . '/cities');
        }
    }

    public function selectAllZipCodes(Request $request){
        $response = $this->locationService->enableAllZipCodesForCity($request->all());
        if($response) {
            return redirect('admin/states/' . $request->state_code . '/counties/' . $request->county . '/cities/' . $request->city . '/zip_codes');
        }
    }

    public function unselectAllZipCodes(Request $request){
        $response = $this->locationService->disableAllZipCodesForCity($request->all());
        if($response) {
            return redirect('admin/states/' . $request->state_code . '/counties/' . $request->county . '/cities/' . $request->city . '/zip_codes');
        }
    }

}
