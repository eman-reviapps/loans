<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SettingRequest;
use App\Services\SettingsService;
use App\Enums\PeriodUnits;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Traits\ControllersTrait;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    use ControllersTrait;
    protected $service;

    public function __construct(SettingsService $service)
    {
        $this->setMessagesKey('settings');
        $this->setSuccessRedirectTo('admin/settings');

        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $setting = $this->service->first($request->all());
        $periodUnits = PeriodUnits::all();
        $daysMonthsUnits = PeriodUnits::daysMonths();
        $daysUnits = PeriodUnits::days();

        if ($request->ajax()) {
            return response(
                view('v1.cp.admin.settings', compact('setting', 'periodUnits','daysMonthsUnits','daysUnits')
                )->render()
            );
        }
        return view('v1.cp.admin.settings', compact('setting', 'periodUnits','daysMonthsUnits','daysUnits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingRequest $request, Setting $setting)
    {

        $response = $this->service->update($setting, $request->all());
        return $this->handleReponse($request, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
