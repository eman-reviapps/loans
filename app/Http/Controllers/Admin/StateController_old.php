<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\State;
use App\Services\LocationService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Traits\ControllersTrait;

class StateController_old extends Controller
{
    use ControllersTrait;

    protected $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->setSuccessRedirectTo('admin/states');

        $this->locationService = $locationService;
    }

    public function index(Request $request)
    {
        $states = $this->locationService->allStates();

        if ($request->ajax()) {
            return response(view('v1.cp.admin.geo-location.states.filter', compact('states'))->render());
        }
        return view('v1.cp.admin.geo-location.states.index', compact('states'));
    }

    public function changeStatus(Request $request, State $state)
    {

        $response = $this->locationService->update($state, $request->all());
        return $this->handleReponse($request, $response);
    }

    public function checkEnabled(Request $request)
    {
        $is_enabled = $this->locationService->isStateEnabled($request->state_code);
        return (new Response($is_enabled ? "true" : "false", '200'))->header('Content-Type', 'application/json');
    }

    public function cities(Request $request, State $state)
    {
        $cities = $this->locationService->getCities(['state_code' => $state->state_code]);
        if ($request->ajax()) {
            return response(view('v1.cp.admin.geo-location.states.cities.filter', compact('cities', 'state'))->render());
        }
        return view('v1.cp.admin.geo-location.states.cities.index', compact('cities', 'state'));
    }

    public function changeCityStatus(Request $request, State $state, City $city)
    {
        $response = $this->locationService->updateCity($city, $request->all());
        return $this->handleReponse($request, $response);
    }

    public function checkCityEnabled(Request $request)
    {
        $is_enabled = $this->locationService->isCityEnabled($request->zip_code);
        return (new Response($is_enabled ? "true" : "false", '200'))->header('Content-Type', 'application/json');
    }
}
