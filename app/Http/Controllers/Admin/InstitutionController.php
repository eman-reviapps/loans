<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\InstitutionRequest;
use App\Models\City;
use App\Models\InstitutionSize;
use App\Models\InstitutionType;
use App\Models\State;
use Facades\App\Enums\ActivationStates;
use App\Http\Controllers\Controller;
use App\Models\Institution;
use Illuminate\Http\Request;
use App\Traits\ControllersTrait;
use App\Services\InstitutionService;
use function MongoDB\BSON\toJSON;
use Sentinel;
use Illuminate\Http\Response;
use App\Models\Bid;
use App\Models\User;

class InstitutionController extends Controller
{
    use ControllersTrait;

    protected $institutionService;

    public function __construct(InstitutionService $institutionService)
    {
        $this->setMessagesKey('institutions');
        $this->setSuccessRedirectTo('admin/institutions');

        $this->institutionService = $institutionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $attributes = array_filter($request->all(), function ($value) {
            return $value !== '' && $value != null;
        });

        $institutions = $this->institutionService->get($attributes);

        $activationStates = ActivationStates::all();

        if ($request->ajax()) {
            return response(
                view('v1.cp.admin.institutions.filter', compact('institutions', 'activationStates')
                )->render()
            );
        }
        return view('v1.cp.admin.institutions.index', compact('institutions', 'activationStates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $institution_types = InstitutionType::pluck('name', 'id')->all();
        $institution_sizes = InstitutionSize::pluck('name', 'id')->all();

        $states = State::pluck('state', 'state_code')->all();

        $activationStates = ActivationStates::all();

        return view('v1.cp.admin.institutions.create', compact('institution', 'institution_types', 'institution_sizes', 'states', 'activationStates'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(InstitutionRequest $request)
    {
        $response = $this->institutionService->create($request->all());

        return $this->handleReponse($request, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Institution $institution
     * @return \Illuminate\Http\Response
     */
    public function show(Institution $institution, Request $request)
    {
        return view('v1.cp.admin.institutions.show', compact('institution'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Institution $institution
     * @return \Illuminate\Http\Response
     */
    public function edit(Institution $institution, Request $request)
    {
        $institution_types = InstitutionType::pluck('name', 'id')->all();
        $institution_sizes = InstitutionSize::pluck('name', 'id')->all();

        $states = State::pluck('state', 'state_code')->all();
        $cities = City::where('state_code', $institution->state)->pluck('city', 'zip_code')->all();

        $activationStates = ActivationStates::all();

        return view('v1.cp.admin.institutions.edit', compact('institution', 'institution_types', 'institution_sizes', 'states', 'cities', 'activationStates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Institution $institution
     * @return \Illuminate\Http\Response
     */
    public function update(InstitutionRequest $request, Institution $institution)
    {
        $response = $this->institutionService->update($institution, $request->all());
        return $this->handleReponse($request, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Institution $institution
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Institution $institution)
    {
        $response = $this->institutionService->delete($institution);
        return $this->handleReponse($request, $response);
    }

    public function changeStatus(Request $request, Institution $institution)
    {
        $response = $this->institutionService->changeStatus($institution, $request->all());
        return $this->handleReponse($request, $response);
    }

    public function isNewDomainValid(Request $request)
    {
        $exists = $this->institutionService->isDomainExists($request->domain);
        return (new Response($exists ? "false" : "true", '200'))->header('Content-Type', 'application/json');
    }


    public function institutionLoanHandlingChart()
    {
        $institutions = Institution::select('title')->get();
        $json_result = array();
        foreach ($institutions as $institution) {
            $title = $institution['title'];
            $count = Bid::with('lender')->whereHas('lender', function ($query) use ($title) {
                $query->from('users')->whereHas('lenderProfile', function ($query) use ($title) {
                    $query->from('lender_profiles')->whereHas('institution', function ($query) use ($title) {
                        $query->from('institutions')->where('title', '=', $title);
                    });
                });
            })->count();
            array_push($json_result, [
                'institution' => $title,
                'count' => $count,
                'color' => $this->rand_color()
            ]);
        }
        return $this->getResponse($json_result);
    }

    private function rand_color()
    {
        return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    }

    private function getResponse($data)
    {
        return (new Response($data, '200'))->header('Content-Type', 'application/json');
    }
}
