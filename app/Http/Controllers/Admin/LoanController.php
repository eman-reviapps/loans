<?php

namespace App\Http\Controllers\Admin;

use App\Enums\BidStatuses;
use App\Enums\BorrowerProfileTypes;
use Illuminate\Http\Request;
use App\Traits\ControllersTrait;
use App\Http\Controllers\Controller;
use App\Models\Loan;
use App\Enums\LoanStatuses;
use App\Helpers\ViewHelper;
use App\Enums\LoanTypes;
use Validator;
use Sentinel;
use Yajra\Datatables\Datatables;
use App\Models\Bid;
use App\Services\LoanService;
use Illuminate\Http\Response;

class LoanController extends Controller
{
    use ControllersTrait;

    private $loanService;

    public function __construct(LoanService $loanService)
    {
        $this->loanService = $loanService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $active_loans = Loan
                ::with('details');
            if ($request->has('status')) {
                $active_loans = $active_loans->where('status', strtoupper($request->status));
            } else {
                $active_loans = $active_loans->active();
            }
            $active_loans = $active_loans->orderBy('created_at','desc')->get();

            return Datatables::of($active_loans)
                ->addColumn('name', function ($loan) {
                    return LoanTypes::items()[$loan->loan_type];
                })
                ->editColumn('created_at', function ($loan) {
                    return '<a href="' . url('admin/loans/' . $loan->id) . '">' . $loan->created_at . '</a>';
                })
                ->editColumn('status', function ($loan) {
                    return '<span class="badge badge-roundless badge-' . ViewHelper::getLoanStatusLabel($loan->status) . '"> 
                    ' . LoanStatuses::items()[$loan->status] . '</span>' . ' 
                    <span class="badge badge-default">'
                        . ($loan->status == LoanStatuses::LENDER_BID
                        || $loan->status == LoanStatuses::BORROWER_RESPONDED ? count($loan->bids) : '') . '</span>';
                })
                ->addColumn('requested_amount', function ($loan) {
                    return $loan->details->requested_amount;
                })
                ->addColumn('action', function ($loan) {
                    return view('v1.cp.admin.loans.datatables.actions', compact('loan')
                    )->render();
                })
                ->rawColumns(['created_at', 'status', 'action'])
                ->make(true);
        }
        $title_key = 'active_deals';
        return view('v1/cp/admin/loans/index', compact('title_key'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Loan $loan)
    {
        return view('v1.cp.admin.loans.show', compact('loan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function bids(Request $request)
    {
        if ($request->ajax()) {

            $bids = Bid::all();


            return Datatables::of($bids)
                ->addColumn('borrower_name', function ($bid) { //borrower_name
                    return
                        $bid->loan->borrower_profile->profile_type == BorrowerProfileTypes::BUSINESS ?
                            $bid->loan->borrower_profile->profile->business_name
                            : $bid->loan->borrower->full_name;
                })
                ->addColumn('loan_date', function ($bid) { //create loan date
                    return $bid->loan->created_at->toFormattedDateString();
                })
                ->editColumn('created_at', function ($bid) { //create loan date
                    return '<a href="' . url('lender/loans/bids/' . $bid->id . '/show') . '">' . $bid->created_at->toFormattedDateString() . '</a>';
                })
                ->editColumn('loan_type', function ($bid) {
                    return LoanTypes::items()[$bid->loan->loan_type];
                })
                ->addColumn('bid_status', function ($bid) {
                    return '<span class="badge badge-roundless badge-' . ViewHelper::getBidStatusLabel($bid->status) . '">' . BidStatuses::items()[$bid->status] . '</span>';
                })
                ->addColumn('location', function ($bid) {
                    return $bid->loan->borrower_profile->profile->full_address;
                })
                ->addColumn('requested_amount', function ($bid) {
                    return $bid->loan->details->requested_amount;
                })
                ->addColumn('lender_name', function ($bid) {
                    return $bid->lender->lenderProfile->title;
                })
                ->addColumn('action', function ($bid) {
                    return view('v1.cp.admin.loans.datatables.bids_actions', compact('bid')
                    )->render();
                })
                ->rawColumns(['created_at', 'bid_status', 'action'])
                ->make(true);
        }

        return view('v1/cp/admin/loans/bids');
    }

    public function showBid(Bid $bid)
    {
        list($loan, $archive) = $this->loanService->getLoanDetails($bid->loan->id, $bid->lender);

        $borrower_replied = $this->loanService->didBorrowerReply($bid);

        return view('v1.cp.admin.bids.show', compact('loan', 'archive', 'bid', 'borrower_replied'));
    }


    public function loansCount()
    {
        $business_profiles_count = Loan::whereHas('borrower_profile', function ($query) {
            $query->from('borrower_profiles')->where('profile_type', 'BUSINESS');
        })->count();

        $real_estate_profile_count = Loan::whereHas('borrower_profile', function ($query) {
            $query->from('borrower_profiles')->where('profile_type', 'REAL_ESTATE');
        })->count();

        $json_result = array([
            'count_type' => 'Business',
            'count_value' => $business_profiles_count,
            'color' => '#ff0000'
        ], [
            'count_type' => 'Real Estate',
            'count_value' => $real_estate_profile_count,
            'color' => '#ffff00'
        ]);
        return $this->getResponse($json_result);
    }

    public function loansStatusStatistics()
    {
        $draft_count = Loan::where('status', '=', LoanStatuses::DRAFT)->count();
        $live_count = Loan::where('status', '=', LoanStatuses::LIVE)->count();
        $lender_bid_count = Loan::where('status', '=', LoanStatuses::LENDER_BID)->count();
        $borrower_responded_count = Loan::where('status', '=', LoanStatuses::BORROWER_RESPONDED)->count();
        $deleted_count = Loan::where('status', '=', LoanStatuses::DELETED)->count();
        $expired_count = Loan::where('status', '=', LoanStatuses::EXPIRED)->count();
        $closed_count = Loan::where('status', '=', LoanStatuses::CLOSED)->count();

        $json_result = array([
            'status' => 'Draft',
            'count' => $draft_count,
            'color' => '#0000ff'
        ], [
            'status' => 'Live',
            'count' => $live_count,
            'color' => '#ff0000'
        ], [
            'status' => 'Lender Bid',
            'count' => $lender_bid_count,
            'color' => '#ffff00'
        ], [
            'status' => 'Borrower Responded',
            'count' => $borrower_responded_count,
            'color' => '#795636'
        ], [
            'status' => 'Deleted',
            'count' => $deleted_count,
            'color' => '#3cb878'
        ], [
            'status' => 'Expired',
            'count' => $expired_count,
            'color' => '#41511D'
        ], [
            'status' => 'Closed',
            'count' => $closed_count,
            'color' => '#1d7373'
        ]);

        return $this->getResponse($json_result);
    }

    private function getResponse($data)
    {
        return (new Response($data, '200'))->header('Content-Type', 'application/json');
    }
}
