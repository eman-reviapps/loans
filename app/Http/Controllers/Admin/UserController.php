<?php

namespace App\Http\Controllers\Admin;

use App\Enums\BorrowerProfileTypes;
use App\Http\Controllers\Controller;

use App\Http\Requests\UserRequest;
use App\Models\Institution;
use App\Models\State;
use App\Models\User;
use App\Models\UserTrack;
use App\Services\BorrowerProfileService;
use App\Services\PreferencesService;
use App\Services\InstitutionService;
use App\Services\RoleService;
use App\Services\TrackService;
use App\Services\UserService;
use Facades\App\Enums\ActivationStates;
use Illuminate\Http\Request;
use App\Traits\ControllersTrait;
use Illuminate\Http\Response;
use App\Models\LenderProfile;
use App\Models\BorrowerProfile;

use Sentinel;
use Facades\App\Enums\NotificationOptions;

class UserController extends Controller
{
    use ControllersTrait;

    protected $userService;
    protected $trackService;
    protected $preferencesService;
    protected $institutionService;
    protected $roleService;
    protected $borrowerProfileService ;

    public function __construct(UserService $userService, TrackService $trackService, PreferencesService $preferencesService, InstitutionService $institutionService, RoleService $roleService,
                                BorrowerProfileService $borrowerProfileService)
    {
        $this->setMessagesKey('users');
        $this->setSuccessRedirectTo('admin/users');

        $this->userService = $userService;
        $this->trackService = $trackService;
        $this->preferencesService = $preferencesService;
        $this->institutionService = $institutionService;
        $this->roleService = $roleService;
        $this->borrowerProfileService = $borrowerProfileService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = $this->userService->get($request->all(),false,['created_at'=>'DESC']);
        $activationStates = ActivationStates::all();

        if ($request->ajax()) {
            return response(
                view('v1.cp.admin.users.filter', compact('users', 'activationStates')
                )->render()
            );
        }
        return view('v1.cp.admin.users.index', compact('users', 'activationStates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $activationStates = ActivationStates::all();
        $roles = $this->roleService->all();
        $institutions = $this->institutionService->all();

        return view('v1.cp.admin.users.create', compact('activationStates', 'roles', 'institutions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $attributes = $request->all();

        if (isset($request->user_picture)) {
            $picture_path = $this->uploadUserPicture($request);
            $attributes['photo'] = $picture_path;
        }
        $response = $this->userService->store($attributes);
        return $this->handleReponse($request, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Request $request)
    {
        return $this->viewProfile($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, Request $request)
    {
        $request->session()->flash('active_tab', null != session('active_tab') ? session('active_tab') : 'tab_account_personnel_info');

        return $this->viewProfile($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $response = $this->userService->update($user, $request->all());
        return $this->handleReponse($request, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $response = $this->userService->delete($user);
        return $this->handleReponse($request, $response);
    }

    public function changeStatus(Request $request, User $user)
    {
        $response = $this->userService->update($user, $request->all());
        return $this->handleReponse($request, $response);
    }

    public function isNewEmailValid(Request $request)
    {
        $exists = $this->userService->isUserExists($request->email);
        return (new Response($exists ? "false" : "true", '200'))->header('Content-Type', 'application/json');
    }

    public function showTrack(Request $request, User $user, UserTrack $user_track)
    {
        return view('v1.cp.admin.user.login_history.show', compact('user', 'user_track'));
    }

    public function numberOfLendersPerInstitution(){
        $json_result = array();
        $institutions = Institution::select('title')->get();
        foreach ($institutions as $institution){
            $count = LenderProfile::whereHas('institution', function($query) use($institution){
               $query->from('institutions')->where('title', '=', $institution->title);
            })->count();
            array_push($json_result, ['institution' => $institution->title, 'count' => $count, 'color' => $this->rand_color()]);
        }

        return $this->getResponse($json_result);
    }

    public function lendersPerState(){
        $json_result = array();
        $states = State::select('state_code')->get();
        foreach ($states as $state){
            $count = LenderProfile::select('id')->whereHas('institution', function($query) use($state){
                $query->from('institutions')->where('state', '=', $state->state_code);
            })->count();
            if($count == 0)
                continue;
            array_push($json_result, [
                'state' => $state->state_code,
                'count' => $count,
                'color' => $this->rand_color()
            ]);
        }
        return $this->getResponse($json_result);
    }

    private function rand_color()
    {
        return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function viewProfile(User $user)
    {
        $activationStates = ActivationStates::all();
        $notificationOptions = NotificationOptions::allKeyValue();
        $borrowerProfileTypes = BorrowerProfileTypes::items();
        $loansCount= $this->borrowerProfileService->getBorrowerLoansCount($user);
        $realEstateLoansCount =  $this->borrowerProfileService->getBorrowerRealEstateLoansCount($user);
        $businessLoansCount =  $this->borrowerProfileService->getBorrowerBusinessLoansCount($user);

        $invoices = $user->inRole(RoleService::ROLE_LENDER) ? ( $user->subscribed('main') ? $user->invoices() : []) : [];

        return view('v1.cp.admin.users.show', compact('user', 'activationStates', 'notificationOptions','borrowerProfileTypes',
            'loansCount' , 'businessLoansCount', 'realEstateLoansCount','invoices'));
    }

    /**
     * @param UserRequest $request
     * @return false|string
     */
    private function uploadUserPicture(Request $request)
    {
        $picture_path = $request->file('user_picture')->store('uploads/users/avatars');
        $picture_path = 'storage/' . $picture_path;
        return $picture_path;
    }

    private function getResponse($data)
    {
        return (new Response($data, '200'))->header('Content-Type', 'application/json');
    }
}
