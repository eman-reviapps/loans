<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GeneralTypeRequest;
use App\Models\PurposeType;
use App\Repositories\PurposeTypeRepository;
use App\Services\GeneralTypesService;
use Illuminate\Http\Request;
use App\Traits\ControllersTrait;

class PurposeTypeController extends Controller
{
    use ControllersTrait;
    protected $service;

    public function __construct(PurposeTypeRepository $repository)
    {
        $this->setMessagesKey('purpose_types');
        $this->setSuccessRedirectTo('admin/purpose_types');

        $this->service = new GeneralTypesService($repository);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $root_url = substr($request->getRequestUri(), 1);
        $root_url = strtok($root_url, '?');
        $messagesKey = $this->getMessagesKey();

        $types = $this->service->all();

        if ($request->ajax()) {
            return response(view('v1.cp.admin.types_general.filter', compact('types', 'root_url', 'messagesKey'))->render());
        }
        return view('v1.cp.admin.types_general.index', compact('types', 'root_url', 'messagesKey'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(GeneralTypeRequest $request)
    {
        $response = $this->service->create($request->all());
        return $this->handleReponse($request, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PurposeType $purpose_type
     * @return \Illuminate\Http\Response
     */
    public function show(PurposeType $purpose_type, Request $request)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PurposeType $purpose_type
     * @return \Illuminate\Http\Response
     */
    public function edit(PurposeType $purpose_type, Request $request)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\PurposeType $purpose_type
     * @return \Illuminate\Http\Response
     */
    public function update(GeneralTypeRequest $request, PurposeType $purpose_type)
    {
        $response = $this->service->update($purpose_type, $request->all());
        return $this->handleReponse($request, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PurposeType $purpose_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, PurposeType $purpose_type)
    {
        $response = $this->service->delete($purpose_type);
        return $this->handleReponse($request, $response);
    }

    public function changeStatus(Request $request, PurposeType $purpose_type)
    {
        $response = $this->service->update($purpose_type, $request->all());
        return $this->handleReponse($request, $response);
    }
}
