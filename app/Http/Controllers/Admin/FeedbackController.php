<?php

namespace App\Http\Controllers\Admin;

use App\Enums\PostFeedbackList;
use App\Models\LoanFeedback;
use Illuminate\Http\Request;
use App\Traits\ControllersTrait;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class FeedbackController extends Controller
{
    use ControllersTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function loansFeedbackStatistics(){
        $found_lender_count = LoanFeedback::select('reason')->where('reason', '=', PostFeedbackList::FOUND_LENDER)->count('reason');
        $no_lender_bid_count = LoanFeedback::select('reason')->where('reason', '=', PostFeedbackList::NO_LENDER_BID)->count('reason');
        $bids_not_competitive_count = LoanFeedback::select('reason')->where('reason', '=', PostFeedbackList::BIDS_NOT_COMPETITIVE)->count('reason');
        $didnot_like_experience_count = LoanFeedback::select('reason')->where('reason', '=', PostFeedbackList::DIDNOT_LIKE_EXPERIENCE)->count('reason');
        $no_longer_required_loan_count = LoanFeedback::select('reason')->where('reason', '=', PostFeedbackList::NO_LONGER_REQUIRED_LOAN)->count('reason');
        $other_count = LoanFeedback::select('reason')->where('reason', '=', PostFeedbackList::OTHER)->count('reason');

        $json_result = array([
            'reason' => 'Found Lender',
            'count' => $found_lender_count,
            'color' => '#0000ff'
        ], [
            'reason' => 'No Lender Bid',
            'count' => $no_lender_bid_count,
            'color' => '#ff0000'
        ], [
            'reason' => 'Bids Not Competitive',
            'count' => $bids_not_competitive_count,
            'color' => '#1d7373'
        ], [
            'reason' => 'Did not Like the Experience',
            'count' => $didnot_like_experience_count,
            'color' => '#795636'
        ], [
            'reason' => 'No Longer Required Loan',
            'count' => $no_longer_required_loan_count,
            'color' => '#3cb878'
        ], [
            'reason' => 'Other',
            'count' => $other_count,
            'color' => '#41511D'
        ]);

        return $this->getResponse($json_result);
    }

    private function getResponse($data)
    {
        return (new Response($data, '200'))->header('Content-Type', 'application/json');
    }
}
