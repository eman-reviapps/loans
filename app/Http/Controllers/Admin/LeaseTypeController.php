<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GeneralTypeRequest;
use App\Models\LeaseType;
use App\Repositories\LeaseTypeRepository;
use App\Services\GeneralTypesService;
use Illuminate\Http\Request;
use App\Traits\ControllersTrait;

class LeaseTypeController extends Controller
{
    use ControllersTrait;
    protected $service;

    public function __construct(LeaseTypeRepository $repository)
    {
        $this->setMessagesKey('lease_types');
        $this->setSuccessRedirectTo('admin/lease_types');

        $this->service = new GeneralTypesService($repository);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $root_url = substr($request->getRequestUri(), 1);
        $root_url = strtok($root_url, '?');
        $messagesKey = $this->getMessagesKey();

        $types = $this->service->all();

        if ($request->ajax()) {
            return response(view('v1.cp.admin.types_general.filter', compact('types', 'root_url', 'messagesKey'))->render());
        }
        return view('v1.cp.admin.types_general.index', compact('types', 'root_url', 'messagesKey'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(GeneralTypeRequest $request)
    {

        $response = $this->service->create($request->all());
        return $this->handleReponse($request, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LeaseType $lease_type
     * @return \Illuminate\Http\Response
     */
    public function show(LeaseType $lease_type,Request $request)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LeaseType $lease_type
     * @return \Illuminate\Http\Response
     */
    public function edit(LeaseType $lease_type,Request $request)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\LeaseType $lease_type
     * @return \Illuminate\Http\Response
     */
    public function update(GeneralTypeRequest $request, LeaseType $lease_type)
    {
        $response = $this->service->update($lease_type, $request->all());
        return $this->handleReponse($request, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LeaseType $lease_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, LeaseType $lease_type)
    {
        $response = $this->service->delete($lease_type);
        return $this->handleReponse($request, $response);
    }

    public function changeStatus(Request $request, LeaseType $lease_type)
    {
        
        $response = $this->service->update($lease_type, $request->all());
        return $this->handleReponse($request, $response);
    }
}
