<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\RoleService;
use Sentinel;

class HomeController extends Controller
{
    public function getIndex()
    {
        if (Sentinel::inRole(RoleService::ROLE_LENDER)) {
            return redirect('lender/find-loans');
        }
        if (Sentinel::inRole(RoleService::ROLE_BORROWER)) {
            return redirect('borrower/loans');
        }
        if (Sentinel::inRole(RoleService::ROLE_ADMIN)) {
            return redirect('admin/dashboard');
        }
    }


}