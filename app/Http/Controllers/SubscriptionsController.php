<?php

namespace App\Http\Controllers;

use App\Services\SubscriptionService;
use Illuminate\Http\Request;

use App\Traits\ControllersTrait;


class SubscriptionsController extends Controller
{
    use ControllersTrait;

    protected $subscriptionService;

    public function __construct(SubscriptionService $subscriptionService)
    {
        $this->setSuccessRedirectTo('account');

        $this->subscriptionService = $subscriptionService;
    }

    public function index(Request $request)
    {
        return view('v1.cp.agent.lender.subscribe');
    }

    public function store(Request $request)
    {
        $response = $this->subscriptionService->subscribe($request->user(), $request->all());
        return $this->handleReponse($request, $response);
    }

    public function invoice(Request $request,$invoice_id)
    {
        return $request->user()->downloadInvoice($invoice_id, [
            'vendor'  => config('name'),
            'product' => 'Monthly subscription',
        ]);
    }


}
