<?php

namespace App\Http\Controllers\Sentinel;

use App\Events\UserLoggedOut;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Services\LoginService;
use App\Traits\ControllersTrait;
use Session;

class LoginController extends Controller
{
    use ControllersTrait;

    protected $loginService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LoginService $loginService)
    {
        $this->setMessagesKey('login');
        $this->setSuccessRedirectTo('/');

        $this->loginService = $loginService;
    }

    public function getLogin()
    {
        return View('v1.public.sign-in');
    }

    public function postLogin(LoginRequest $request)
    {
        $response = $this->loginService->loginUser(true, $request->all(), null);

        return $this->handleReponse($request, $response);
    }

    public function getLogout(Request $request)
    {
        $track_id = Session::get('track_id');
        $user = Sentinel::getUser();

        Sentinel::logout();

        event(new UserLoggedOut($user, $track_id));

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }


}
