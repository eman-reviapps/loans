<?php

namespace App\Http\Controllers\Sentinel;

use App\Http\Controllers\Controller;
use App\Services\RoleService;
use App\Traits\ControllersTrait;

use App\Services\ActivationService;
use App\Services\LoginService;

use Illuminate\Http\Request;
use Sentinel;
use App\Models\User;
use App\Enums\ActivationStates;

class ActivationController extends Controller
{
    use ControllersTrait;

    protected $activationService;
    protected $loginService;

    public function __construct(ActivationService $activationService, LoginService $loginService)
    {
        $this->setMessagesKey('activation');
        $this->setSuccessRedirectTo('/');

        $this->activationService = $activationService;
        $this->loginService = $loginService;
    }

    public function getActivate(Request $request, User $user, $activation_code)
    {
        $response = $this->activationService->activate($user, $activation_code);
        return $this->handleReponse($request, $response);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function handleFail()
    {
        $title = trans("messages." . $this->getMessagesKey() . '.activation_failed');
        $description = trans("messages." . $this->response->key . '.' . $this->response->sub_key);

        $this->unFlashMessage();

        return view('v1.public.custom', compact('title', 'description'));
    }

    /**
     * @param $response
     * @return \Illuminate\Http\Response
     */
    public function handleSuccess()
    {
        $user = $this->response->object;

        if ($user->inROle(RoleService::ROLE_LENDER && $user->status != ActivationStates::ACTIVE)) {

            $title = trans("messages." . $this->getMessagesKey() . '.activation_done');
            $description = trans("messages." . 'sign_up' . '.' . 'account_in_review');

            $this->unFlashMessage();

            return response()->view('v1.public.custom', compact('title', 'description'));
        }
//        $this->loginService->loginUser(false, [], $user);
        return redirect($this->getSuccessRedirectTo());
    }
}