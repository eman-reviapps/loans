<?php

namespace App\Http\Controllers\Sentinel;

use App\Enums\ActivationStates;
use App\Enums\BorrowerProfileTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\BorrowerRegisterRequest;
use App\Http\Requests\LenderRegisterRequest;
use App\Models\City;
use App\Models\PropertyType;
use App\Models\State;
use App\Services\ActivationService;
use App\Services\BorrowerProfilesService;
use App\Services\InstitutionService;
use App\Services\RoleService;
use App\Services\RegisterService;
use App\Traits\ControllersTrait;

class RegisterController extends Controller
{
    use ControllersTrait;

    protected $registerService;

    protected $activationService;

    public function __construct(RegisterService $registerService,ActivationService $activationService)
    {
        $this->setMessagesKey('sign_up_lender');
        $this->registerService = $registerService;
        $this->activationService = $activationService;
    }

    public function getRegister()
    {
        return View('v1.public.sign-up.index');
    }

    public function getRegisterBorrower($profile_type = null)
    {
        $old = session()->getOldInput();

        $states = State::where('is_enabled' , 1)->get();
        $cities = null;
        $zip_codes = null ;
        if ($old) {
            if (isset($old['state']))
                $cities = City::where('state_code', $old['state'])->select(['city', 'state_code'])->distinct('city')->orderBy('city')->get();

            if(isset($old['city']))
                $zip_codes = City::select('zip_code')->where('city', '=', $old['city'])
                    ->where('state_code', '=',$old['state'])->get();
        }

        if (!$profile_type) {
            return View('v1.public.sign-up.borrower-choose-profile');
        }
        if ($profile_type == 'real-estate') {
            $profile_type = BorrowerProfileTypes::REAL_ESTATE;

            $property_types = PropertyType::pluck('name', 'id')->all();
            return View('v1.public.sign-up.borrower-real-estate', compact('property_types', 'profile_type', 'states', 'cities', 'zip_codes'));

        } else if ($profile_type == 'business') {

            $profile_type = BorrowerProfileTypes::BUSINESS;
            return View('v1.public.sign-up.borrower-business', compact('profile_type', 'states', 'cities', 'zip_codes'));
        }

    }

    public function postRegisterBorrower(BorrowerRegisterRequest $request)
    {
        $response = $this->registerService->registerBorrower($request->all());
        return $this->handleReponse($request, $response);
    }

    public function getRegisterLender(InstitutionService $institutionService)
    {
        $states = State::pluck('state', 'state_code')->all();
        $institutions = $institutionService->all();

        return View('v1.public.sign-up.lender', compact('states', 'institutions'));
    }

    public function postRegisterLender(LenderRegisterRequest $request)
    {
        $response = $this->registerService->registerLender($request->all());
        return $this->handleReponse($request, $response);
    }

    /**
     * @param $response
     * @return \Illuminate\Http\Response
     */
    public function handleSuccess()
    {
        $user = $this->response->object;

        $this->unFlashMessage();

        if ($user->inROle(RoleService::ROLE_LENDER) && $this->activationService->doesActivationCompleted($user)) {
            return response()->view('v1.public.sign-up.verify_lender', ["user" => $user]);
        }

        return response()->view('v1.public.sign-up.activate_account', ["user" => $user]);
    }
}
