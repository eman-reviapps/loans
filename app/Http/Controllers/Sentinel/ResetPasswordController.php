<?php

namespace App\Http\Controllers\Sentinel;

use App\Http\Requests\ResetPasswordRequest;
use App\Http\Controllers\Controller;
use App\Services\PasswordService;
use App\Traits\ControllersTrait;
use App\Models\User;
use Sentinel;

class ResetPasswordController extends Controller
{
    use ControllersTrait;
    
    protected $passwordService;

    public function __construct(PasswordService $passwordService)
    {
        $this->setMessagesKey('reset_password');
        $this->setFailRedirectTo('reset/error');

        $this->passwordService = $passwordService;
    }

    public function getResetPassword(User $user, $reset_code)
    {
        $check = $this->passwordService->isValidResetCode($user, $reset_code);
        if (!$check->status) {
            return redirect($this->getFailRedirectTo());
        }
        return View('v1.public.reset.reset-password', compact('user', 'reset_code'));
    }

    public function postResetPassword(ResetPasswordRequest $request)
    {
        $response = $this->passwordService->resetPassword($request->all());
        return $this->handleReponse($request, $response);
    }

    public function getResetError()
    {
        return view('v1.public.reset.reset_error');
    }

    public function handleFail()
    {
        $this->unFlashMessage();
        return redirect($this->getFailRedirectTo());
    }
}
