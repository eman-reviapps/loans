<?php

namespace App\Http\Controllers\Sentinel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ForgotPasswordRequest;
use App\Traits\ControllersTrait;
use App\Services\PasswordService;

class ForgotPasswordController extends Controller
{
    use ControllersTrait;

    protected $passwordService;

    public function __construct(PasswordService $passwordService)
    {
        $this->setMessagesKey('forgot_password');
        $this->setSuccessRedirectTo('forgot/confrm');

        $this->passwordService = $passwordService;
    }

    public function getForgetPassword()
    {
        return View('v1.public.reset.forgot-password');
    }

    public function postForgetPassword(ForgotPasswordRequest $request)
    {
        $response = $this->passwordService->forgotPassword($request->all());
        return $this->handleReponse($request, $response);
    }

    public function handleSuccess()
    {
        $this->unFlashMessage();
        return redirect($this->getSuccessRedirectTo());
    }

    public function getForgetConfirm()
    {
        return view('v1.public.reset.forgot-confirm');
    }
}
