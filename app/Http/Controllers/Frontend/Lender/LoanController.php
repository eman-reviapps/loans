<?php

namespace App\Http\Controllers\Frontend\Lender;

use App\Traits\ControllersTrait;
use App\Http\Controllers\Controller;

class LoanController extends Controller
{
    use ControllersTrait;

    public function findLoans()
    {
        return view('v1.cp.frontend.lender.loans.index');
    }
}
