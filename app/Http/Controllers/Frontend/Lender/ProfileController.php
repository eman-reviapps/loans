<?php

namespace App\Http\Controllers\Frontend\Lender;

use App\Models\LenderProfile;
use App\Models\User;
use App\Traits\ControllersTrait;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sentinel;
use Storage;
use App\Services\UserService;

class ProfileController extends Controller
{
    use ControllersTrait;

    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->setMessagesKey('lender_profiles');
        $this->userService = $userService;
    }

    public function update(Request $request, User $lender, LenderProfile $profile)
    {
        $response = $this->userService->updateLenderProfile($profile, $request->except(['_method', '_token', 'save_continue_tab']));
        return $this->handleReponse($request, $response);
    }

}
