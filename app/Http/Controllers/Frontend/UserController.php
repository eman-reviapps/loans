<?php

namespace App\Http\Controllers\Frontend;

use Facades\App\Enums\BorrowerProfileTypes;
use App\Models\User;
use App\Traits\ControllersTrait;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sentinel;
use Storage;
use App\Services\UserService;
use App\Services\PreferencesService;
use Facades\App\Enums\ActivationStates;
use Facades\App\Enums\NotificationOptions;

class UserController extends Controller
{
    use ControllersTrait;

    protected $userService;
    protected $preferencesService;

    public function __construct(UserService $userService, PreferencesService $preferencesService)
    {
        $this->setMessagesKey('users');

        $this->userService = $userService;
        $this->preferencesService = $preferencesService;
    }

    public function show(Request $request)
    {
        session()->forget('active_tab');
        return $this->viewProfile();
    }

    public function edit(Request $request)
    {
        $request->session()->flash('active_tab', null != session('active_tab') ? session('active_tab') : 'tab_account_personnel_info');
        return $this->viewProfile();
    }

    function update(UserRequest $request, User $user)
    {
        $response = $this->userService->update($user, $request->all());
        return $this->handleReponse($request, $response);
    }

    public function changePassword(ChangePasswordRequest $request, User $user)
    {
        $response = $this->userService->changePassword($user, $request->all());
        return $this->handleReponse($request, $response);
    }


    public function changePicture(Request $request, User $user)
    {

        $picture_path = $this->uploadUserPicture($request);

        $response = $this->userService->update($user, ['photo' => $picture_path]);

        return $this->handleReponse($request, $response);
    }


    public function updatePreferences(Request $request, User $user)
    {
        $response = $this->preferencesService->updateUserPreferences($user, $request->all());
        return $this->handleReponse($request, $response);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function viewProfile()
    {
        $user = Sentinel::getUser();

        $activationStates = ActivationStates::all();
        $notificationOptions = NotificationOptions::allKeyValue();
        $borrowerProfileTypes = BorrowerProfileTypes::allKeyValue();

        return view('v1.cp.frontend.profile', compact('user', 'activationStates', 'notificationOptions', 'borrowerProfileTypes'));
    }

    private function uploadUserPicture(Request $request)
    {
        $picture_path = $request->file('user_picture')->store('users/avatars');
        return $picture_path;
    }


}
