<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Models\Institution;
use App\Models\InstitutionSize;
use App\Models\InstitutionType;
use App\Models\Preference;
use App\Services\RoleService;
use Illuminate\Http\Request;
use Sentinel;

class PreferenceController extends Controller
{
    public function index(Request $request)
    {
        $query = Preference
            ::where('user_id', '>', 0);

        if ($request->has('user_id') && !empty($request->user_id))
            $query = $query->where('user_id', $request->user_id);

        if ($request->has('profile_id') && !empty($request->profile_id))
            $query = $query->where('profile_id', $request->profile_id);
        else
            $query = $query->where('profile_id', null);

        if ($request->has('loan_id') && !empty($request->loan_id))
            $query = $query->where('loan_id', $request->loan_id);
        else
            $query = $query->where('loan_id', null);

        $preferences = $query->get();

        if ($request->has('key_by'))
            $preferences = $preferences->keyBy('slug');


        $institutionTypes = InstitutionType::pluck('name', 'id')->all();
        $institutionSizes = InstitutionSize::pluck('name', 'id')->all();


        $lender_role = Sentinel::findRoleBySlug(RoleService::ROLE_LENDER);
//        $lenders = $lender_role->users->pluck('full_name', 'id')->all();
        $lenders = Institution::pluck('domain', 'id')->all();

        return view('v1.cp.components.preferences.preferences',
            compact('preferences', 'institutionTypes', 'institutionSizes', 'lenders')
        );

    }
}
