<?php

namespace App\Http\Controllers\Agent\Borrower;

use App\Models\BorrowerProfile;
use App\Models\City;
use App\Models\Institution;
use App\Models\InstitutionSize;
use App\Models\InstitutionType;
use App\Models\PropertyType;
use App\Models\RealEstateType;
use App\Models\State;
use App\Services\BorrowerProfileService;
use App\Services\PreferencesService;
use App\Services\RoleService;
use App\Traits\ControllersTrait;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use Sentinel;
use Illuminate\Http\Request;
use Facades\App\Enums\ActivationStates;
use Facades\App\Enums\NotificationOptions;
use App\Enums\BorrowerProfileTypes;
use Illuminate\Support\Facades\Storage;


class ProfileController extends Controller
{
    use ControllersTrait;

    protected $borrowerProfileService;
    protected $preferencesService;

    public function __construct(BorrowerProfileService $borrowerProfileService, PreferencesService $preferencesService)
    {
        $this->setSuccessRedirectTo('borrower/profiles');
        $this->setMessagesKey('lender_profiles');
        $this->borrowerProfileService = $borrowerProfileService;
        $this->preferencesService = $preferencesService;
    }

    public function index(Request $request)
    {
        $user = Sentinel::getUser();
        $profiles = BorrowerProfile::where('user_id', $user->id)->latest()->get();

        return view('v1.cp.agent.borrower.profiles.index',
            compact('user', 'profiles')
        );
    }

    public function create(Request $request)
    {
        $user = Sentinel::getUser();
        $profile_type = $request->profile_type == 'real_estate' ? BorrowerProfileTypes::REAL_ESTATE : BorrowerProfileTypes::BUSINESS;
        $property_types = PropertyType::pluck('name', 'id')->all();
        $realestate_types = RealEstateType::pluck('name', 'id')->all();

        $states = State::where('is_enabled' , 1)->get();
        $cities = null;
        if ($request->profile_type)
            return view('v1.cp.agent.borrower.profiles.create',
                compact('user', 'profile_type', 'property_types', 'states', 'cities','realestate_types')
            );

        return view('v1.cp.agent.borrower.profiles.create_type', compact('user'));

    }

    public function store(Request $request)
    {
        $response = $this->borrowerProfileService->add(
            Sentinel::getUser(),
            $request->except(['_token', 'save_continue'])
        );

        return $this->handleReponse($request, $response);
    }

    public function show(Request $request, BorrowerProfile $profile)
    {
        $request->session()->forget('active_tab');
        return $this->showProfile($profile);
    }

    public function edit(Request $request, BorrowerProfile $profile)
    {
//        dd(Storage::delete('/uploads/borrower_profiles/DataScienceSlides.pdf'));

        $request->session()->flash('active_tab', null != session('active_tab') ? session('active_tab') : 'tab_account_profile_info');

        return $this->showProfile($profile);
    }

    public function update(Request $request, BorrowerProfile $profile)
    {
//        dd($request->toArray());
        $response = $this->borrowerProfileService->update(
            $profile,
            $request->except(['_method', '_token', 'save_continue_tab'])
        );

        return $this->handleReponse($request, $response);
    }

    public function destroy(Request $request, BorrowerProfile $profile)
    {
        $response = $this->borrowerProfileService->delete(Sentinel::getUser(), $profile);
        return $this->handleReponse($request, $response);
    }

    private function showProfile(BorrowerProfile $profile)
    {
        $user = Sentinel::getUser();

        $lender_role = Sentinel::findRoleBySlug(RoleService::ROLE_LENDER);
//        $lenders = $lender_role->users->pluck('full_name', 'id')->all();
        $lenders = Institution::pluck('title', 'id')->all();

        $activationStates = ActivationStates::all();
        $notificationOptions = NotificationOptions::allKeyValue();
        $borrowerProfileTypes = BorrowerProfileTypes::items();
        $realestate_types = RealEstateType::pluck('name', 'id')->all();

        $institutionTypes = InstitutionType::pluck('name', 'id')->all();
        $institutionSizes = InstitutionSize::pluck('name', 'id')->all();

        $old = session()->getOldInput();

        $states = State::all();
        $cities = null;
        $zip_codes = null;

        if ($old) {
            if (isset($old['state']))
                $cities = City::where('state_code', $old['state'])->select(['city', 'state_code'])->distinct('city')->orderBy('city')->get();
        }
        if ($profile) {
            $cities = City::where('state_code', $profile->profile->state_code)->select(['city', 'state_code'])->distinct('city')->orderBy('city')->get();
            $zip_codes = City
                ::where('state_code', $profile->profile->state_code)
                ->where('city', $profile->profile->city)
                ->orderBy('zip_code')
                ->get();
        }

        $loansCount= $this->borrowerProfileService->getBorrowerLoansCount($user);
        $realEstateLoansCount =  $this->borrowerProfileService->getBorrowerRealEstateLoansCount($user);
        $businessLoansCount =  $this->borrowerProfileService->getBorrowerBusinessLoansCount($user);

        return view('v1.cp.agent.borrower.profiles.show',
            compact('user', 'profile', 'activationStates', 'notificationOptions',
                'borrowerProfileTypes', 'realestate_types', 'lenders', 'states', 'cities','zip_codes',
                'institutionTypes', 'institutionSizes','loansCount','realEstateLoansCount', 'businessLoansCount'));
    }

    public function changeStatus(Request $request, BorrowerProfile $profile)
    {
        $response = $this->borrowerProfileService->changeStatus($profile, $request->all());
        return $this->handleReponse($request, $response);
    }

    public function select(Request $request, BorrowerProfile $profile)
    {
        $response = $this->borrowerProfileService->select($profile);

        return $this->handleReponse($request, $response);
    }

    public function updatePreferences(Request $request, BorrowerProfile $profile)
    {
//        dd($request->toArray());

        $response = $this->preferencesService->updatePreferences(
            $request->except(['_method', '_token', 'save_continue_tab']),
            Sentinel::getUser(),
            $profile
        );
        return $this->handleReponse($request, $response);
    }


}
