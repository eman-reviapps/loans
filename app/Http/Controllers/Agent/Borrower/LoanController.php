<?php

namespace App\Http\Controllers\Agent\Borrower;

use App\Enums\BorrowerProfileTypes;
use App\Enums\EquipmentFinanceTypes;
use App\Enums\EquipmentPurposeTypes;
use App\Enums\LoanOtherLoanTypes;
use App\Enums\LoanStatuses;
use App\Helpers\ViewHelper;
use App\Http\Requests\LoanRequest;
use App\Models\Bid;
use App\Models\BorrowerProfile;
use App\Models\City;
use App\Models\LeaseType;
use App\Models\PurposeType;
use App\Models\RealEstateType;
use App\Models\State;
use App\Services\LoanService;
use App\Enums\LoanTypes;

use App\Models\Loan;
use App\Traits\ControllersTrait;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Sentinel;
use Yajra\Datatables\Datatables;

class LoanController extends Controller
{
    use ControllersTrait;

    protected $loanService;

    public function __construct(LoanService $loanService)
    {
        $this->setSuccessRedirectTo('borrower/loans');
        $this->setMessagesKey('loans');

        $this->loanService = $loanService;
    }

    private function addCommas($number)
    {

        $number_text = (string)$number;
        $number_text = strrev($number_text);
        $percision = substr($number_text, 0, 2);
        $number_text = substr($number_text, 3);

        $arr = str_split($number_text, "3"); // break string in 3 character sets
        $number_new_text = implode(",", $arr);  // implode array with comma

        $number_new_text = strrev($number_new_text);
        $number_new_text .= '.' . strrev($percision);

        return $number_new_text;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {

            $active_loans = Loan
                ::with('details')
                ->activeBorrowerProfile();
            if ($request->has('status')) {
                $active_loans = $active_loans->where('status', strtoupper($request->status));
            } else {
                $active_loans = $active_loans->active();
            }
            $active_loans = $active_loans->latest();

            return Datatables::of($active_loans)
                ->addColumn('name', function ($loan) {
                    return LoanTypes::items()[$loan->loan_type];
                })
                ->editColumn('created_at', function ($loan) {
                    return '<a href="' . url('borrower/loans/' . $loan->id) . '">' . $loan->created_at . '</a>';
                })
//                ->editColumn('status', function ($loan) {
//                    return '<span class="badge badge-roundless badge-' . ViewHelper::getLoanStatusLabel($loan->status) . '"> ' . LoanStatuses::items()[$loan->status] . '</span>' . ' <span class="badge badge-default">' . ($loan->status == LoanStatuses::LENDER_BID || $loan->status == LoanStatuses::BORROWER_RESPONDED ? count($loan->bids) : '') . '</span>';
//                })
                ->editColumn('status', function ($loan) {
                    return '<div>
                                <span class="badge badge-roundless badge-' . ViewHelper::getLoanStatusLabel($loan->status) . ' popovers" data-trigger="hover" rel="popover" class="popovers" data-placement="top" data-original-title="'.LoanStatuses::items()[$loan->status].'" data-content="'.LoanStatuses::description()[$loan->status].'">
                                    '.LoanStatuses::items()[$loan->status].'
                                </span>
                                <span class="badge badge-default">' . ($loan->status == LoanStatuses::LENDER_BID || $loan->status == LoanStatuses::BORROWER_RESPONDED ? count($loan->bids) : '') . '</span>
                        
                                <script>
                                    $(document).ready(function() {
                                        $(".popovers").popover({
                                            html: true,
                                            animation: false,
                                        });
                                    });
                                </script>
                            </div>';
                })
                ->addColumn('requested_amount', function ($loan) {
                    setlocale(LC_MONETARY, 'en_US');

                    return "$ " . ($loan->details->requested_amount);
                })
                ->addColumn('action', function ($loan) {
                    return view('v1.cp.agent.borrower.loans.datatables.actions', compact('loan')
                    )->render();
                })
                ->rawColumns(['created_at', 'status', 'action'])
                ->make(true);
        }
        $title_key = $request->has('status') ? $request->status : 'active_deals';

        return view('v1.cp.agent.borrower.loans.index', compact('title_key'));
    }

    public function create(Request $request)
    {
        list($borrower_profile, $profile, $loanTypes, $loan_type, $cities, $zip_codes, $purposes, $lease_types, $states, $realestate_types, $term_other_loan_type
            , $equipment_purposes, $equipment_finance_types)
            = $this->prepareShow();

        return view('v1.cp.agent.borrower.loans.create',
            compact(
                'loanTypes', 'loan_type', 'purposes', 'profile', 'borrower_profile',
                'states', 'cities', 'zip_codes', 'realestate_types', 'lease_types', 'term_other_loan_type', 'equipment_purposes', 'equipment_finance_types'
            ));
    }

    public function store(LoanRequest $request)
    {

        $response = $this->loanService->create(
            Sentinel::getUser(),
            BorrowerProfile::find(session('borrower_profile_id')),
            $request->except(['_token', 'save_continue'])
        );
        return $this->handleReponse($request, $response);
    }

    public function getTemplate(Request $request)
    {
        $view = $this->loanService->getLoanTemplate($request->loan_type);
        $purposes = PurposeType::all();
        $lease_types = LeaseType::all();
        $states = State::all();
        $realestate_types = RealEstateType::all();

        return response(
            view('v1.cp.agent.borrower.loan_templates.' . $view,
                compact('purposes', 'states', 'realestate_types', 'lease_types'))->render()
        );
    }

    public function show(Request $request, Loan $loan)
    {
        return view('v1.cp.agent.borrower.loans.show', compact('loan'));
    }

    public function edit(Request $request, Loan $loan)
    {
        list($borrower_profile, $profile, $loanTypes, $loan_type, $cities, $zip_codes, $purposes, $lease_types, $states, $realestate_types, $term_other_loan_type
            , $equipment_purposes, $equipment_finance_types)
            = $this->prepareShow($loan);

        return view('v1.cp.agent.borrower.loans.edit',
            compact(
                'loanTypes', 'loan_type', 'purposes', 'profile', 'loan',
                'states', 'cities', 'zip_codes', 'realestate_types', 'lease_types', 'term_other_loan_type', 'equipment_purposes', 'equipment_finance_types'
            ));
    }

    public function update(Request $request, Loan $loan)
    {
        $response = $this->loanService->update(
            $loan,
            Sentinel::getUser(),
            BorrowerProfile::find(session('borrower_profile_id')),
            $request->except(['_token', 'save_continue'])
        );
        return $this->handleReponse($request, $response);
    }

    public function changeStatus(Request $request, Loan $loan)
    {
        $response = $this->loanService->updateLoan($loan, $request->all());
        return $this->handleReponse($request, $response);
    }

    public function destroy(Request $request, Loan $loan)
    {
        $response = $this->loanService->delete($loan, $request->all());
        return $this->handleReponse($request, $response);
    }

    /**
     * @return array
     */
    private function prepareShow(Loan $loan = null)
    {
        $profile = json_decode(session('borrower_profile'));

        $borrower_profile = BorrowerProfile::
        where('id', $profile->id)
            ->where('user_id', Sentinel::getUser()->id)
            ->first();

        if ($profile->profile_type == BorrowerProfileTypes::BUSINESS) {
            $loanTypes = LoanTypes::getBusinessTypes();
        } else if ($profile->profile_type == BorrowerProfileTypes::REAL_ESTATE) {
            $loanTypes = LoanTypes::getRealEstateTypes();
        }
        $loan_type = LoanTypes::LINE_OF_CREDIT;

//        $states = State::where('is_enabled' , 1)->get();
        $states = State::all();
        $cities = null;
        $zip_codes = null;
        $old = session()->getOldInput();
        if ($old) {
            if (isset($old['state']))
                $cities = City::where('state_code', $old['state'])->select(['city', 'state_code'])->distinct('city')->orderBy('city')->get();
            if (isset($old['city']))
                $zip_codes = City::select('zip_code')->where('city', '=', $old['city'])
                    ->where('state_code', '=', $old['state'])->get();
            $loan_type = $old['loan_type_slug'];
        }
        if ($loan) {
            $cities = City::where('state_code', $loan->details->state_code)->select(['city', 'state_code'])->distinct('city')->orderBy('city')->get();
            $zip_codes = City::select('zip_code')->where('city', '=', $loan->details->city)
                ->where('state_code', '=', $loan->details->state_code)->get();

            $loan_type = $loan->loan_type;
        }

        $purposes = PurposeType::all();
        $lease_types = LeaseType::all();
        $term_other_loan_type = LoanOtherLoanTypes::all();
        $equipment_purposes = EquipmentPurposeTypes::all();
        $equipment_finance_types = EquipmentFinanceTypes::all();

        $realestate_types = RealEstateType::all();

        return array($borrower_profile, $profile, $loanTypes, $loan_type, $cities, $zip_codes, $purposes, $lease_types, $states, $realestate_types, $term_other_loan_type, $equipment_purposes, $equipment_finance_types);
    }

    public function bidShow(Request $request, Bid $bid)
    {
        list($loan, $archive) = $this->loanService->getLoanDetails($bid->loan->id, $bid->lender);

        $borrower_replied = $this->loanService->didBorrowerReply($bid);

        return view('v1.cp.agent.borrower.bids.show', compact('loan', 'archive', 'bid', 'borrower_replied'));
    }

    public function borrowerNotInterested(Request $request, Bid $bid)
    {
        $response = $this->loanService->borrowerNotInterested($bid);
        return $this->handleReponse($request, $response);
    }

    public function uploadFile(Request $request)
    {
        $file_path = $request->file('file')->storeAs('uploads/loans', str_replace(' ', '_', $request->file->getClientOriginalName()));
        $file_path = 'storage/' . $file_path;

        return response($file_path, 200);
    }

}
