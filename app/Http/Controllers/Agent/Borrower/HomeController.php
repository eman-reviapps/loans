<?php

namespace App\Http\Controllers\Agent\Borrower;


class HomeController
{
    public function index()
    {
        return redirect()->route('borrower.get-loans');
    }
}