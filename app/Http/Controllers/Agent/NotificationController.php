<?php

namespace App\Http\Controllers\Agent;


use App\Http\Controllers\Controller;
use App\Models\UserNotification;
use App\Services\NotificationService;
use Illuminate\Http\Request;
use App\Traits\ControllersTrait;
use Sentinel;

class NotificationController extends Controller
{
    use ControllersTrait;
    protected $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function index(Request $request)
    {
        $user = Sentinel::getUser();
        $notifications = UserNotification::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();
        $unread_notifications_count = UserNotification::where('user_id', $user->id)->where('is_read', 0)->count();


        return view('v1.cp.agent.notifications.index',compact('notifications'));
    }

    public function markAsRead(Request $request, UserNotification $notification)
    {
        $response = $this->notificationService->markAsRead($notification);
        return $this->handleReponse($request, $response);
    }

    public function markAllRead(Request $request)
    {
        $response = $this->notificationService->markAllRead(Sentinel::getUser());
        return $this->handleReponse($request, $response);
    }
}
