<?php

namespace App\Http\Controllers\Agent\Lender;

use App\Models\LenderProfile;
use App\Models\User;
use App\Services\LenderProfileService;
use App\Traits\ControllersTrait;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sentinel;
use Storage;

class ProfileController extends Controller
{
    use ControllersTrait;

    protected $lenderProfileService;

    public function __construct(LenderProfileService $lenderProfileService)
    {
        $this->setMessagesKey('lender_profiles');
        $this->lenderProfileService = $lenderProfileService;
    }

    public function update(Request $request, LenderProfile $profile)
    {
        $response = $this->lenderProfileService->update($profile, $request->except(['_method', '_token', 'save_continue_tab']));
        return $this->handleReponse($request, $response);
    }

}
