<?php

namespace App\Http\Controllers\Agent\Lender;

use App\Enums\BidStatuses;
use App\Enums\BorrowerProfileTypes;
use App\Enums\LoanTypes;
use App\Enums\PeriodUnits;
use App\Helpers\ViewHelper;
use App\Http\Requests\BidRequest;
use App\Mail\LenderRequiresApproval;
use App\Models\Bid;
use App\Models\Loan;
use App\Models\LoanArchive;
use App\Models\User;
use App\Services\LoanService;
use App\Services\PreferencesService;
use App\Traits\ControllersTrait;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Sentinel;
use Yajra\Datatables\Datatables;
use DB;

class LoanController extends Controller
{
    use ControllersTrait;

    protected $loanService;

    public function __construct(LoanService $loanService)
    {
        $this->loanService = $loanService;
    }

    public function findLoans(Request $request)
    {
//        $admins = User::join('role_users', 'id','=', 'user_id')->where('role_id', '=', 1)->get();
//        Mail::to($admins)->send(new LenderRequiresApproval(Sentinel::getUser()));

        if ($request->ajax()) {
            $loans = $this->loanService->findLoans(Sentinel::getUser(), $request->all());

            $this->saveSessionLenderPrefer($request);

            return Datatables::of($loans)
                ->addColumn('name', function ($loan) {
                    return
                        $loan->borrower_profile->profile_type == BorrowerProfileTypes::BUSINESS ?
                            $loan->borrower_profile->profile->business_name
                            : $loan->borrower->full_name;
                })
                ->editColumn('created_at', function ($loan) {
                    return '<a href="' . url('lender/loans/' . $loan->id . '/details') . '">' . $loan->created_at->toFormattedDateString() . '</a>';
                })
                ->editColumn('loan_type', function ($loan) {
                    return LoanTypes::items()[$loan->loan_type];
                })
                ->editColumn('profile_type', function ($loan) {
                    return BorrowerProfileTypes::items()[$loan->borrower_profile->profile_type];
                })
                ->addColumn('location', function ($loan) {
                    return $loan->borrower_profile->profile->full_address;
//                    return
//                        $loan->borrower_profile->profile_type == BorrowerProfileTypes::REAL_ESTATE
//                        ||
//                        ( $loan->borrower_profile->profile_type == BorrowerProfileTypes::BUSINESS && in_array($loan->loan_type, [LoanTypes::LINE_OF_CREDIT, LoanTypes::EQUIPMENT_FINANCE, LoanTypes::TERM_LOAN_OTHER]) )
//                        ? $loan->borrower_profile->profile->full_address : $loan->details->full_address;
                })
                ->addColumn('requested_amount', function ($loan) {
                    return " $ ".$loan->details->requested_amount;
                })
                ->addColumn('action', function ($loan) {
                    return view('v1.cp.agent.lender.loans.datatables.find_loans_actions', compact('loan')
                    )->render();
                })
                ->rawColumns(['created_at', 'action'])
                ->make(true);
        }

        if (!session()->has('lender.preferences.FAVORITE_LOAN_TYPES')) {

            list(
                $lender_preference_loan_types,
                $lender_preference_loan_min,
                $lender_preference_loan_max,
                $lender_preference_state,
                $lender_preference_city,
                $lender_preference_counties
                ) =
                $this->loanService->getLenderPreferences(Sentinel::getUser());

            session(['lender.preferences.FAVORITE_LOAN_TYPES' => $lender_preference_loan_types]);
            session(['lender.preferences.FAVORITE_LOAN_SIZE_MIN' => $lender_preference_loan_min]);
            session(['lender.preferences.FAVORITE_LOAN_SIZE_MAX' => $lender_preference_loan_max]);
            session(['lender.preferences.FAVORITE_STATE' => $lender_preference_state]);
            session(['lender.preferences.FAVORITE_CITY' => $lender_preference_city]);
            session(['lender.preferences.FAVORITE_LOCATION' => $lender_preference_counties]);

        } else {
            list(
                $lender_preference_loan_types,
                $lender_preference_loan_min,
                $lender_preference_loan_max,
                $lender_preference_state,
                $lender_preference_city,
                $lender_preference_counties
                )
                =
                [
                    session('lender.preferences.FAVORITE_LOAN_TYPES'),
                    session('lender.preferences.FAVORITE_LOAN_SIZE_MIN'),
                    session('lender.preferences.FAVORITE_LOAN_SIZE_MAX'),
                    session('lender.preferences.FAVORITE_STATE'),
                    session('lender.preferences.FAVORITE_CITY'),
                    session('lender.preferences.FAVORITE_LOCATION')
                ];
        }

        $ebitdaDetails = ["annual_revenue", "net_income_before_tax", "interest_expense", "depreciation_amortization", "income_tax_expense"];

        return view('v1.cp.agent.lender.loans.find-loans',
            compact(
                'lender_preference_loan_types',
                'lender_preference_loan_min',
                'lender_preference_loan_max',
                'lender_preference_state',
                'lender_preference_city',
                'lender_preference_counties',
                'ebitdaDetails'
            )
        );
    }

    public function archive(Request $request, Loan $loan, User $user)
    {
        $response = $this->loanService->archiveLoan($loan, $user);
        return $this->handleReponse($request, $response);
    }

    public function restore(Request $request, LoanArchive $archive)
    {
        $response = $this->loanService->restoreLoan($archive);
        return $this->handleReponse($request, $response);
    }

    public function loanDetails(Request $request, Loan $loan)
    {
        list($loan, $archive) = $this->loanService->getLoanDetails($loan->id, Sentinel::getUser());

        $annual_revenue = "annual_revenue";
        $ebitdaDetails = ["net_income_before_tax", "interest_expense", "depreciation_amortization", "income_tax_expense"];

        return view('v1.cp.agent.lender.loans.loan-details', compact('loan', 'archive', 'ebitdaDetails','annual_revenue'));
    }

    public function myWork(Request $request)
    {

        if ($request->ajax()) {

            $loans = $this->loanService->lenderLoans(Sentinel::getUser(), $request->all());

            return Datatables::of($loans)
                ->addColumn('name', function ($loan) {
                    return
                        $loan->borrower_profile->profile_type == BorrowerProfileTypes::BUSINESS ?
                            $loan->borrower_profile->profile->business_name
                            : $loan->borrower->full_name;
                })
                ->editColumn('created_at', function ($loan) {
                    return '<a href="' . url('lender/loans/bids/' . $loan->bids[0]->id . '/show') . '">' . $loan->created_at->toFormattedDateString() . '</a>';
                })
                ->editColumn('loan_type', function ($loan) {
                    return LoanTypes::items()[$loan->loan_type];
                })
//                ->addColumn('bid_status', function ($loan) {
//                    return '<span class="badge badge-roundless badge-' . ViewHelper::getBidStatusLabel($loan->bids[0]->status) . '">' . BidStatuses::items()[$loan->bids[0]->status] . '</span>';
//                })
                ->addColumn('bid_status', function ($loan) {
                    return '<div>
                                <span class="badge badge-roundless badge-' . ViewHelper::getBidStatusLabel($loan->bids[0]->status) . ' popovers" data-trigger="hover" rel="popover" class="popovers" data-placement="top" data-original-title="'.BidStatuses::items()[$loan->bids[0]->status].'" data-content="'.BidStatuses::description()[$loan->bids[0]->status].'">
                                    '.BidStatuses::items()[$loan->bids[0]->status].'
                                </span>
                                <script>
                                    $(document).ready(function() {
                                        $(".popovers").popover({
                                            html: true,
                                            animation: false,
                                        });
                                    });
                                </script>
                            </div>';
                })
                ->addColumn('location', function ($loan) {
                    return $loan->borrower_profile->profile->full_address;
                })
                ->addColumn('requested_amount', function ($loan) {
                    return $loan->details->requested_amount;
                })
                ->addColumn('action', function ($loan) {
                    return view('v1.cp.agent.lender.loans.datatables.my_work_actions', compact('loan')
                    )->render();
                })
                ->rawColumns(['created_at', 'bid_status', 'action'])
                ->make(true);
        }

        return view('v1.cp.agent.lender.loans.my-work');
    }

    public function archived(Request $request)
    {
        if ($request->ajax()) {

            $loans = $this->loanService->archived(Sentinel::getUser(), $request->all());

            return Datatables::of($loans)
                ->addColumn('name', function ($record) {
                    return
                        $record->loan->borrower_profile->profile_type == BorrowerProfileTypes::BUSINESS ?
                            $record->loan->borrower_profile->profile->business_name
                            : $record->loan->borrower->full_name;
                })
                ->editColumn('created_at', function ($record) {
                    return '<a href="' . url('lender/loans/' . $record->loan_id . '/details') . '">' . $record->loan->created_at->toFormattedDateString() . '</a>';
                })
                ->editColumn('loan_type', function ($record) {
                    return LoanTypes::items()[$record->loan->loan_type];
                })
                ->editColumn('profile_type', function ($record) {
                    return BorrowerProfileTypes::items()[$record->loan->borrower_profile->profile_type];
                })
                ->addColumn('location', function ($record) {
                    return $record->loan->borrower_profile->profile->full_address;
                })
                ->addColumn('requested_amount', function ($record) {
                    return $record->loan->details->requested_amount;
                })
                ->addColumn('action', function ($record) {
                    return view('v1.cp.agent.lender.loans.datatables.archived_loans_actions', compact('record')
                    )->render();
                })
                ->rawColumns(['created_at', 'action'])
                ->make(true);
        }

        return view('v1.cp.agent.lender.loans.archived');
    }

    public function showPlaceBid(Request $request, Loan $loan)
    {
        $periodUnits = PeriodUnits::monthsYears();

        return view('v1.cp.agent.lender.loans.place-bid', compact('loan', 'periodUnits'));
    }

    public function placeBid(BidRequest $request, Loan $loan, User $lender)
    {
        $response = $this->loanService->placeBid($loan, $lender, $request->all());
        return $this->handleReponse($request, $response);
    }

    public function bidShow(Request $request, Bid $bid)
    {
        list($loan, $archive) = $this->loanService->getLoanDetails($bid->loan->id, $bid->lender);

        $borrower_replied = $this->loanService->didBorrowerReply($bid);

        $annual_revenue = "annual_revenue";

        $ebitdaDetails = ["net_income_before_tax", "interest_expense", "depreciation_amortization", "income_tax_expense"];

        return view('v1.cp.agent.lender.bids.show', compact('loan', 'archive', 'bid', 'borrower_replied', 'ebitdaDetails','annual_revenue'));
    }

    public function saveToProfile(Request $request,PreferencesService $preferencesService)
    {
        $this->saveSessionLenderPrefer($request);

        $response = $preferencesService->updatePreferences($request, Sentinel::getUser());

        return $this->handleReponse($request, $response);
    }

    public function isThereOtherBidsPlaced(Request $request, Loan $loan, $not_lender_id)
    {
        $response = $this->loanService->isThereOtherBidsPlaced($loan, $not_lender_id);
        return $this->handleReponse($request, $response);
    }

}
