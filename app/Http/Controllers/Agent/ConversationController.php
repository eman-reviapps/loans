<?php

namespace App\Http\Controllers\Agent;

use App\Models\Bid;
use App\Models\Conversation;
use App\Models\Reply;
use App\Services\ConversationService;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ControllersTrait;

class ConversationController extends Controller
{
    use ControllersTrait;

    protected $conversationService;

    public function __construct(ConversationService $conversationService)
    {
        $this->conversationService = $conversationService;
    }

    public function addReply(Request $request, Conversation $conversation)
    {
        $response = $this->conversationService->addReply($conversation, $request->all());
        return $this->handleReponse($request, $response);
    }


    public function renderReply(Request $request, Reply $reply)
    {
        $bid = $reply->conversation->bid;
        return response(
            view('v1.cp.components.conversations.reply', compact('reply', 'bid')
            )->render()
        );
    }

    public function canFollowUp(Request $request, Bid $bid)
    {
        $response = $this->conversationService->canFollowUp(Sentinel::getUser(), $bid);

        return $this->handleReponse($request, $response);
    }

    public function canFollowMessageRender(Request $request, Bid $bid)
    {
        return response(
            view('v1.cp.components.conversations.can_followup_message', compact('bid')
            )->render()
        );
    }

    public function markAsRead(Request $request, Reply $reply)
    {
        $response = $this->conversationService->markAsRead($reply);
        return $this->handleReponse($request, $response);
    }

    public function markAllRead(Request $request)
    {
        $response = $this->conversationService->markAllRead(Sentinel::getUser());
        return $this->handleReponse($request, $response);
    }

    public function getLenderFollowUpNumber(Request $request, Bid $bid)
    {

        $response = $this->conversationService->getLenderFollowUpNumber(Sentinel::getUser(), $bid);

        return $this->handleReponse($request, $response);
    }
}
