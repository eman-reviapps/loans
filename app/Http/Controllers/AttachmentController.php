<?php

namespace App\Http\Controllers;

use App\Models\Attachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AttachmentController extends Controller
{
    public function uploadFiles(Request $request)
    {
        $files = [];
        foreach ($request->files as $file) {
            $file_name = $file->storeAs($request->directory, str_replace(' ', '_', $request->file->getClientOriginalName()));
            $file_path = 'storage/' . $file_name;

            $file_object = new \stdClass();
            $file_object->name = str_replace('photos/', '',$file_object->getClientOriginalName());
            $file_object->size = round(Storage::size($file_object) / 1024, 2);

            $files[] = $file_object;
        }

        return response()->json(array('files' => $files), 200);
    }
    public function uploadFile(Request $request)
    {
        $file_name = $request->file('file')->storeAs($request->directory, str_replace(' ', '_', $request->file->getClientOriginalName()));
        $file_path = 'storage/' . $file_name;

        return response($file_path, 200);
    }

    public function destroy(Request $request, Attachment $attachment)
    {
        Storage::delete(str_replace('storage/','',$attachment->url));

        $attachment->delete();

        return response()->json(
            [
                "message" => 'succeesdded',
                "level" => 'success',
                "object" => ''
            ], 200);

    }
}
