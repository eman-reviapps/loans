<?php

namespace App\Http\Requests;

use App\Enums\LoanTypes as LoanTypess;
use Illuminate\Foundation\Http\FormRequest;

class LoanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $loan_type = $this->input('loan_type_slug');

        switch ($loan_type) {
            case LoanTypess::LINE_OF_CREDIT:
                $rules = $this->getRules([
                    'requested_amount',
                    'purpose_type_id',
                    'receivable_range_from',
                    'receivable_range_to',
                    'inventory_range_from',
                    'inventory_range_to',
                ]);
                break;
            case LoanTypess::RE_PURCHASE_INVESTOR_0_50:
                $rules = $this->getRules([
                    'state',
                    'city',
                    'zip_code',
                    'address',
                    'apt_suite_no',
                    'real_estate_type_id',
                    'purchase_price',
                    'requested_amount',
                    'requested_percent',
                    'building_sq_ft',
                    'buildings_no',
                    'units_no',
                    'owner_occupied_percent',
                    'gross_annual_revenue',
                    'estimated_annual_expenses',
                    'estimated_net_operating_income',
                    'properties'
                ]);
                break;
            case LoanTypess::RE_PURCHASE_OWNER_51_100:
                $rules = $this->getRules([
                    'state',
                    'city',
                    'zip_code',
                    'address',
                    'apt_suite_no',
                    'real_estate_type_id',
                    'purchase_price',
                    'requested_amount',
                    'requested_percent',
                    'building_sq_ft',
                    'buildings_no',
                    'units_no',
                    'owner_occupied_percent',
                    'annual_income_from_tenants',
                    'current_lease_expense',
                    'properties'
                ]);
                break;
            case LoanTypess::RE_CASH_OUT_INVESTOR_0_50:
                $rules = $this->getRules([
                    'state',
                    'city',
                    'zip_code',
                    'address',
                    'apt_suite_no',
                    'real_estate_type_id',
                    'estimated_property_value',
                    'requested_amount',
                    'purpose',
                    'building_sq_ft',
                    'buildings_no',
                    'units_no',
                    'outstanding_value',
                    'owner_occupied_percent',
                    'gross_annual_revenue',
                    'estimated_annual_expenses',
                    'net_operating_income',
                    'properties'
                ]);
                break;
            case LoanTypess::RE_CASH_OUT_OWNER_51_100:
                $rules = $this->getRules([
                    'state',
                    'city',
                    'zip_code',
                    'address',
                    'apt_suite_no',
                    'real_estate_type_id',
                    'estimated_property_value',
                    'requested_amount',
                    'purpose',
                    'building_sq_ft',
                    'buildings_no',
                    'units_no',
                    'outstanding_value',
                    'annual_income_from_tenants',
                ]);
                break;
            case LoanTypess::RE_REFINANCE_INVESTOR_0_50:
                $rules = $this->getRules([
                    'state',
                    'city',
                    'zip_code',
                    'address',
                    'apt_suite_no',
                    'real_estate_type_id',
                    'requested_amount',
                    'estimated_market_price',
                    'outstanding_value',
                    'purpose',
                    'building_sq_ft',
                    'buildings_no',
                    'units_no',
                    'owner_occupied_percent',
                    'gross_annual_revenue',
                    'estimated_annual_expenses',
                    'net_operating_income',
                    'properties'
                ]);
                break;
            case LoanTypess::RE_REFINANCE_OWNER_51_100:
                $rules = $this->getRules([
                    'state',
                    'city',
                    'zip_code',
                    'address',
                    'apt_suite_no',
                    'real_estate_type_id',
                    'requested_amount',
                    'estimated_market_price',
                    'outstanding_value',
                    'purpose',
                    'building_sq_ft',
                    'buildings_no',
                    'units_no',
                    'owner_occupied_percent',
                    'annual_income_from_tenants',
                    'properties'
                ]);
                break;
            case LoanTypess::EQUIPMENT_FINANCE:
                $rules = $this->getRules([
                    'vendor_name',
                    'equipment_purpose',
                    'equipment_description',
                    'purchase_price',
                    'less_trade',
                    'less_down_payment',
                    'requested_amount',
                    'equipment_finance_type',
                ]);
                break;
            case LoanTypess::TERM_LOAN_OTHER:
                $rules = $this->getRules([
                    'requested_amount',
                ]);
                break;
            default:
                $rules = [];
                break;
        }
        return $rules;
    }

    private function getRules($attributes, $required = [])
    {
        $rules = [];

        foreach ($attributes as $attribute) {
            switch ($attribute) {
                case 'requested_amount':
//                    $rules[$attribute] = 'required_if:requested_percent,==,null|nullable|numeric';
                    $rules[$attribute] = 'required|numeric|nullable';
                    break;
                case 'requested_percent':
                    $rules[$attribute] = 'required|numeric|nullable';
                    break;
                case 'purpose_type_id':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'purchase_price':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'purpose':
                    $rules[$attribute] = 'required';
                    break;
                case 'receivable_range_from':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'receivable_range_to':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'inventory_range_from':
                    $rules[$attribute] = 'numeric|nullable';
                    break;
                case 'inventory_range_to':
                    $rules[$attribute] = 'numeric|nullable';
                    break;
                case 'current_ratio':
                    $rules[$attribute] = 'numeric';
                    break;
                case 'state':
                    $rules[$attribute] = 'required';
                    break;
                case 'city':
                    $rules[$attribute] = 'required';
                    break;
                case 'zip_code':
                    $rules[$attribute] = 'required|regex:/\b\d{5}\b/';
                    break;
                case 'address':
                    $rules[$attribute] = 'required';
                    break;
                case 'real_estate_type_id':
                    $rules[$attribute] = 'required';
                    break;
                case 'estimated_property_value':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'building_sq_ft':
                    $rules[$attribute] = 'required_if:buildings_no,==,null|nullable|numeric';
                    break;
                case 'buildings_no':
                    $rules[$attribute] = 'required_if:building_sq_ft,==,null|nullable|integer';
                    break;
                case 'units_no':
                    $rules[$attribute] = 'required_if:building_sq_ft,==,null|nullable|integer';
                    break;
                case 'owner_occupied_percent':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'owner_not_occupied_percent':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'gross_annual_revenue':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'estimated_annual_expenses':
                    $rules[$attribute] = 'numeric|nullable|';
                    break;
                case 'estimated_net_operating_income':
                    $rules[$attribute] = 'numeric|nullable|';
                    break;
                case 'net_operating_income':
                    $rules[$attribute] = 'numeric|nullable|';
                    break;
                case 'company_EBITDA':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'total_liabilities':
                    $rules[$attribute] = (isset($required['total_liabilities']) ? 'required|' : '') . 'nullable|numeric';
                    break;
                case 'funded_debt':
                    $rules[$attribute] = (isset($required['funded_debt']) ? 'required|' : '') . 'nullable|numeric';
                    break;
                case 'tangible_net_worth':
                    $rules[$attribute] = (isset($required['tangible_net_worths']) ? 'required|' : '') . 'nullable|numeric';
                    break;
                case 'cash_liquid_investments':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'rental_annual_revenue':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'rental_revenue_by_tenants':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'equipment_type':
                    $rules[$attribute] = 'required';
                    break;
                case 'vendor_name':
                    $rules[$attribute] = 'required';
                    break;
                case 'equipment_purpose':
                    $rules[$attribute] = 'required';
                    break;
                case 'equipment_description':
                    $rules[$attribute] = 'required';
                    break;
                case 'equipment_finance_type':
                    $rules[$attribute] = 'required';
                    break;
                case 'less_trade':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'less_down_payment':
                    $rules[$attribute] = 'required|numeric';
                    break;
                case 'properties':
                    $rules[$attribute] = '';

//                    $rules[$attribute . "." . 'tenant_id'] = 'nullable|integer';
//                    $rules[$attribute . "." . 'space_occupied'] = 'required|numeric';
//                    $rules[$attribute . "." . 'lease_rate'] = 'required|numeric';
//                    $rules[$attribute . "." . 'lease_expiration_date'] = 'required|date';
//                    $rules[$attribute . "." . 'lease_type'] = 'required|numeric|min:1';
                    break;
                default :
                    break;
            }
        }

        return $rules;
    }
}
