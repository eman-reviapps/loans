<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'loan_expire_period' => 'required|numeric',
            'loan_expire_period_unit' => 'required',
            'lender_max_loan_bids' => 'required|numeric'
        ];
        return $rules;
    }
}
