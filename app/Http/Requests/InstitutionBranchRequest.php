<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstitutionBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //active_url  for domain , must have a valid A or AAAA record according to the dns_get_record
        $rules = [
            'title' => 'required|unique:institution_branches,title',
        ];

        // handle unique on update
        $id = null;
        if ($this->route('branch')) {
            $id = $this->route('branch')->id;
        }
        if (!$id) {
            return $rules;
        }

        $rules['title'] .= ',' . $id . ',id';

        return $rules;
    }
}
