<?php

namespace App\Http\Requests;

use App\Services\RoleService;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'email' => 'required|email|unique:users,email',
        ];
        if (Sentinel::inRole(RoleService::ROLE_ADMIN)) {
            $rules['status'] = 'required';
            $rules['role'] = 'required';
        }
        // handle unique on update
        $id = null;
        if ($this->route('user')) {
            $id = $this->route('user')->id;
        }
        if (!$id) {
            return $rules;
        }

        $rules['email'] .= ',' . $id . ',id';

        return $rules;
    }
}
