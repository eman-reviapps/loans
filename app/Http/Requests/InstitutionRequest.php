<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstitutionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //active_url  for domain , must have a valid A or AAAA record according to the dns_get_record
        $rules = [
//            'title' => 'min:3',
            'domain' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/|unique:institutions,domain',
//            'zip_code' => 'regex:/\b\d{5}\b/',
//            'phone' => 'regex:/(01)[0-9]{9}',
        ];

        // handle unique on update
        $id = null;
        if ($this->route('institution')) {
            $id = $this->route('institution')->id;
        }
        if (!$id) {
            return $rules;
        }

        $rules['domain'] .= ',' . $id . ',id';

        return $rules;
    }
}
