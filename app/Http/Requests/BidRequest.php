<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BidRequest  extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'rate' => 'required|numeric',
            'term' => 'required|integer',
            'term_unit' => 'required',
            'amortization' => 'required|integer',
            'amortization_unit' => 'required',
            'description' => 'required|min:6',
        ];
    }
    //required:email|unique:users,email

}
