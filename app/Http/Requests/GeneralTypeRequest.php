<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GeneralTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "name" => "required|min:3|max:50",
            "slug" => "required"
        ];
        $url = $this->route()->uri();

        $url = substr($url, (strpos($url, '/') + 1));
        $url = strtok($url, '/');

        $parameter = rtrim($url, 's');

        $rules['slug'] .= '|unique:' . $url . ',slug';

        // handle unique on update
        $id = null;
        if ($this->route($parameter)) {
            $id = $this->route($parameter)->id;
        }
        if (!$id) {
            return $rules;
        }

        $rules['slug'] .= ',' . $id . ',id';
        return $rules;
    }
}
