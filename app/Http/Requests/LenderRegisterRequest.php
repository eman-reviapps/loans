<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LenderRegisterRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'state' => 'required',
            'city' => 'required',
            'institution' => 'required_if:institution_exists,==,1',
//            'title' => 'required_if:institution_exists,==,0|min:3',
            'domain' => 'required_if:institution_exists,==,0|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/|unique:institutions,domain',
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'email' => 'required|min:3|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ];

        $institution_exists = $this->input('institution_exists');

        if ($institution_exists == 1) {
            $rules['institution'] = "required";

//            unset($rules['title']);
            unset($rules['domain']);

        } else {
            unset($rules['institution']);

//            $rules['title'] = "required|min:3";
            $rules['domain'] = "required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/|unique:institutions,domain";
        }

        return $rules;
    }
}
