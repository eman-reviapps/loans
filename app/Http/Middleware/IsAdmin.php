<?php

namespace App\Http\Middleware;

use App\Services\RoleService;
use Closure;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class IsAdmin {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public
    function handle($request, Closure $next)
    {
        if (!Sentinel::inRole(RoleService::ROLE_ADMIN)) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Forbidden', 403);
            } else {
                abort(403);
            }
        }

        return $next($request);
    }

}
