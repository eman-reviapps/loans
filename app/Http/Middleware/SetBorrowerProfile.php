<?php

namespace App\Http\Middleware;

use App\Services\BorrowerProfileService;
use Sentinel;
use Closure;
use App\Models\BorrowerProfile;

class SetBorrowerProfile
{
    protected $borrowerProfileService;

    public function __construct(BorrowerProfileService $borrowerProfileService)
    {
        $this->borrowerProfileService = $borrowerProfileService;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public
    function handle($request, Closure $next)
    {

        $profile_id = session('borrower_profile_id');
        $profile = BorrowerProfile::
        where('id', $profile_id)
            ->where('user_id', Sentinel::getUser()->id)
            ->enabled()
            ->first();

        if (!isset($profile_id) || !$profile) {

            $done = $this->borrowerProfileService->setActiveProfile();

            if (!$done) {
                return redirect('borrower/profiles');
            }

        }

        return $next($request);
    }

}
