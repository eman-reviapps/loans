<?php

namespace App\Http\Middleware;

use Closure;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

/**
 * Description of SentinelAuthMiddleware
 *
 * @author Eman
 */
class Authenticate {

    public function handle($request, Closure $next) {

        $user = Sentinel::check();

        if (Sentinel::guest() || !$user) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }
        return $next($request);
    }

}
