<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/18/17
 * Time: 1:29 PM
 */

namespace App\Http\Middleware;

use App\Enums\ActivationStates;
use Closure;
use Sentinel;

class IsUserSuspended
{
    public
    function handle($request, Closure $next)
    {
        $user = Sentinel::getUser();

        if ($user->status == ActivationStates::SUSPENDED) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Forbidden', 403);
            } else {
//                abort(403);
                return redirect('account');
            }
        }

        return $next($request);
    }
}