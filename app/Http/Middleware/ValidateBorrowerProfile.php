<?php

namespace App\Http\Middleware;

use Sentinel;
use Closure;
use App\Models\BorrowerProfile;

class ValidateBorrowerProfile
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public
    function handle($request, Closure $next)
    {
        //validate profile_id in url, if exists and if belongs to currently logged user
//        $profile_id = $request->segment(3);
//        $profile = BorrowerProfile::
//        where('id', $profile_id)
//            ->where('user_id', Sentinel::getUser()->id)
//            ->first();
//
//        if (!$profile_id || !$profile) {
//            $profile_id = BorrowerProfile::MostRecentProfile()->id;
//            session(['borrower_profile_id' => $profile_id]);
//            return redirect('borrowers');
//        }

        return $next($request);
    }

}
