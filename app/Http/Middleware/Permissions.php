<?php

namespace App\Http\Middleware;

use App\Services\RoleService;
use Closure;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class Permissions
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $action = $request->route()->getName();
//        dd($request->route()->getName());
        if ($action == 'home') {
            return $next($request);
        }
//        if (!Sentinel::hasAccess($action)) {
//            if ($request->ajax() || $request->wantsJson()) {
//                return response('forbidden.', 403);
//            } else {
//                abort(403);
//            }
//        }

        return $next($request);
    }

}
