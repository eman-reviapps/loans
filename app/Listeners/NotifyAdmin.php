<?php

namespace App\Listeners;

use App\Enums\ActivationStates;
use App\Events\UserRegistered;
use App\Services\RoleService;

use App\Services\NotificationService;
use Illuminate\Foundation\Bus\DispatchesJobs;

class NotifyAdmin
{

    use DispatchesJobs;

    protected $notificationService;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $this->notificationService->newUserRegistered($event->user);

        if ($event->user->inRole(RoleService::ROLE_LENDER)) {

            if ($event->institution) {
                if ($event->institution->status == ActivationStates::ACTIVATION_REQUIRED) {
                    $this->notificationService->domainRequiresApproval($event->institution);
                }
            }
            if ($event->user->status == ActivationStates::ACTIVATION_REQUIRED) {
                $this->notificationService->lenderRequiresApproval($event->user);
            }
        }
    }
}
