<?php

namespace App\Listeners;

use App\Events\UserRequestedEmailReset;
use App\Mail\PasswordResetEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPasswordResetEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRequestedEmailReset  $event
     * @return void
     */
    public function handle(UserRequestedEmailReset $event)
    {
        //queue instead of send
        \Mail::to($event->user)->queue(new PasswordResetEmail($event->user,$event->reminder));
    }
}
