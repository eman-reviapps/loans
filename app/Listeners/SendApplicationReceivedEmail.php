<?php
namespace App\Listeners;

use App\Enums\Roles;
use App\Events\UserRegistered;
use App\Services\RoleService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\ApplicationReceived;

class SendApplicationReceivedEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        if ($event->user->inRole(RoleService::ROLE_LENDER)) {
            \Mail::to($event->user)->queue(new ApplicationReceived($event->user));
        }

    }
}