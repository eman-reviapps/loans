<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\Mail\LenderRequiresApproval;
use App\Models\User;
use App\Services\RoleService;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Bus\DispatchesJobs;

class SendUserRequiresApprovalMail
{
    use DispatchesJobs;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        if ($event->user->inRole(RoleService::ROLE_LENDER)) {
            $admins = User::join('role_users', 'id', '=', 'user_id')->where('role_id', '=', 1)->get();
            Mail::to($admins)->queue(new LenderRequiresApproval($event->user));
        }
    }
}
