<?php

namespace App\Listeners;

use App\Events\PostEdited;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\NotificationService;

class NotifyLenderPostEdited
{

    protected $notificationService;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  PostEdited  $event
     * @return void
     */
    public function handle(PostEdited $event)
    {
        foreach ($event->user as $user) {
            $this->notificationService->sendNotification($user, $event->loan, $event->notification_type);
        }
    }
}
