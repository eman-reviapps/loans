<?php

namespace App\Listeners;

use App\Events\UserLoggedIn;
use App\Services\TrackService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordUserTrack
{
    protected $trackService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(TrackService $trackService)
    {
        $this->trackService = $trackService;
    }

    /**
     * Handle the event.
     *
     * @param  UserLoggedIn $event
     * @return void
     */
    public function handle(UserLoggedIn $event)
    {
        $this->trackService->recordUserTrack($event->user);
    }
}
