<?php

namespace App\Listeners;

use App\Events\BidPlaced;
use App\Services\NotificationService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyBorrower
{
    protected $notificationService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  BidPlaced $event
     * @return void
     */
    public function handle(BidPlaced $event)
    {
        $this->notificationService->sendNotification($event->user, $event->bid, $event->notification_type);
    }
}
