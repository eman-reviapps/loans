<?php

namespace App\Listeners;

use App\Events\BorrowerNotInterested;
use App\Services\NotificationService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyLender
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  BorrowerNotInterested  $event
     * @return void
     */
    public function handle(BorrowerNotInterested $event)
    {
        $this->notificationService->sendNotification($event->user, $event->bid, $event->notification_type);
    }
}
