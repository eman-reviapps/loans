<?php

namespace App\Listeners;

use App\Events\DomainStatusChanged;
use App\Mail\UserStatusChangedEmail;
use App\Services\RoleService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sentinel;
class SendStatusChangedEmailToUsers
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DomainStatusChanged $event
     * @return void
     */
    public function handle(DomainStatusChanged $event)
    {
        foreach ($event->users as $user) {
            if ($user->inRole(RoleService::ROLE_LENDER)) {
                \Mail::to($user)->queue(new UserStatusChangedEmail($user, $event->institution));
            }
        }

    }
}
