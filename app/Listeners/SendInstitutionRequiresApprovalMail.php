<?php

namespace App\Listeners;

use App\Enums\ActivationStates;
use App\Events\UserRegistered;
use App\Mail\InstitutionRequiresApproval;
use App\Models\User;
use App\Services\RoleService;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Bus\DispatchesJobs;

class SendInstitutionRequiresApprovalMail
{
    use DispatchesJobs;


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        if ($event->user->inRole(RoleService::ROLE_LENDER && $event->institution && $event->institution->status == ActivationStates::ACTIVATION_REQUIRED)) {
            $admins = User::join('role_users', 'id', '=', 'user_id')->where('role_id', '=', 1)->get();
            Mail::to($admins)->queue(new InstitutionRequiresApproval($event->institution));
        }
    }
}
