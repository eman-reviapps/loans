<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBusinessProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_profiles', function (Blueprint $table) {
            $table->string('apt_suite_no', 150)->nullable()->change();
            $table->decimal('annual_revenue_year_1', 17, 2)->change();
            $table->decimal('annual_revenue_year_2', 17, 2)->change();
            $table->decimal('annual_revenue_year_3', 17, 2)->change();
            $table->decimal('net_income_before_tax_year_1', 17, 2)->change();
            $table->decimal('net_income_before_tax_year_2', 17, 2)->change();
            $table->decimal('net_income_before_tax_year_3', 17, 2)->change();
            $table->decimal('interest_expense_year_1', 17, 2)->change();
            $table->decimal('interest_expense_year_2', 17, 2)->change();
            $table->decimal('interest_expense_year_3', 17, 2)->change();
            $table->decimal('depreciation_amortization_year_1', 17, 2)->change();
            $table->decimal('depreciation_amortization_year_2', 17, 2)->change();
            $table->decimal('depreciation_amortization_year_3', 17, 2)->change();
            $table->decimal('company_EBITDA', 52, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_profiles', function (Blueprint $table) {
            $table->integer('apt_suite_no')->change();
            $table->decimal('annual_revenue_year_1', 8, 2)->change();
            $table->decimal('annual_revenue_year_2', 8, 2)->change();
            $table->decimal('annual_revenue_year_3', 8, 2)->change();
            $table->decimal('net_income_before_tax_year_1', 8, 2)->change();
            $table->decimal('net_income_before_tax_year_2', 8, 2)->change();
            $table->decimal('net_income_before_tax_year_3', 8, 2)->change();
            $table->decimal('interest_expense_year_1', 8, 2)->change();
            $table->decimal('interest_expense_year_2', 8, 2)->change();
            $table->decimal('interest_expense_year_3', 8, 2)->change();
            $table->decimal('depreciation_amortization_year_1', 8, 2)->change();
            $table->decimal('depreciation_amortization_year_2', 8, 2)->change();
            $table->decimal('depreciation_amortization_year_3', 8, 2)->change();
            $table->decimal('company_EBITDA', 8, 2)->change();
        });
    }
}
