<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPreferencesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned(); //admin or lender
            $table->integer('profile_id')->unsigned()->nullable(); //to borrower_profiles table
            $table->integer('loan_id')->unsigned()->nullable();

            $table->string('slug');
            $table->string('name');

            $table->text('value')->nullable();
            $table->text('default_value')->nullable();

            $table->text('description');

            $table->enum('data_type', ['Free', 'Integer', 'Number', 'Boolean']);
            $table->string('field_type'); //select textarea input yes/no

            $table->boolean('is_enabled')->default(true);

            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('profile_id')
                ->references('id')
                ->on('borrower_profiles')
                ->onDelete('cascade');

            $table->foreign('loan_id')
                ->references('id')
                ->on('loans')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferences');
    }
}
