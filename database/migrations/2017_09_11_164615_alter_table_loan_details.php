<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableLoanDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loan_details', function (Blueprint $table) {
            $table->decimal('receivable_range_from', 17, 2)->change();
            $table->decimal('receivable_range_to', 17, 2)->change();
            $table->decimal('inventory_range_from', 17, 2)->change();
            $table->decimal('inventory_range_to', 17, 2)->change();
            $table->decimal('current_ratio', 17, 2)->change();
            $table->decimal('cash_before_down_payment', 17, 2)->change();
            $table->decimal('owner_occupied_percent', 17, 2)->change();
            $table->decimal('owner_not_occupied_percent', 17, 2)->change();
            $table->decimal('gross_annual_revenue', 17, 2)->change();
            $table->decimal('estimated_annual_expenses', 17, 2)->change();
            $table->decimal('cash_liquid_investments', 17, 2)->change();
            $table->decimal('outstanding_value', 17, 2)->change();
            $table->decimal('current_lease_expense', 17, 2)->change();
            $table->decimal('company_EBITDA', 17, 2)->change();
            $table->decimal('total_liabilities', 17, 2)->change();
            $table->decimal('funded_debt', 17, 2)->change();
            $table->decimal('tangible_net_worth', 17, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_details', function (Blueprint $table) {
            $table->decimal('receivable_range_from', 8, 2)->change();
            $table->decimal('receivable_range_to', 8, 2)->change();
            $table->decimal('inventory_range_from', 8, 2)->change();
            $table->decimal('inventory_range_to', 8, 2)->change();
            $table->decimal('current_ratio', 8, 2)->change();
            $table->decimal('cash_before_down_payment', 8, 2)->change();
            $table->decimal('owner_occupied_percent', 8, 2)->change();
            $table->decimal('owner_not_occupied_percent', 8, 2)->change();
            $table->decimal('gross_annual_revenue', 8, 2)->change();
            $table->decimal('estimated_annual_expenses', 8, 2)->change();
            $table->decimal('cash_liquid_investments', 8, 2)->change();
            $table->decimal('outstanding_value', 8, 2)->change();
            $table->decimal('current_lease_expense', 8, 2)->change();
            $table->decimal('company_EBITDA', 8, 2)->change();
            $table->decimal('total_liabilities', 8, 2)->change();
            $table->decimal('funded_debt', 8, 2)->change();
            $table->decimal('tangible_net_worth', 8, 2)->change();
        });
    }
}
