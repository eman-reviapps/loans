<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBorrowerProfilesAddProfileNo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('borrower_profiles', function (Blueprint $table) {
            $table->string('profile_no', 32)->unique()->nullable();
            $table->boolean('is_enabled')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('borrower_profiles', function (Blueprint $table) {
            $table->dropColumn('profile_no');
            $table->dropColumn('is_enabled');
        });
    }
}
