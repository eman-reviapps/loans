<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToTableBusinessProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_profiles', function (Blueprint $table) {
            $table->decimal('income_tax_expense_year_1');
            $table->decimal('income_tax_expense_year_2');
            $table->decimal('income_tax_expense_year_3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_profiles', function (Blueprint $table) {
            $table->dropColumn('income_tax_expense_year_1');
            $table->dropColumn('income_tax_expense_year_2');
            $table->dropColumn('income_tax_expense_year_3');
        });
    }
}
