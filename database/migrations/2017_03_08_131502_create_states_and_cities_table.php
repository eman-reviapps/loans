<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesAndCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {

            $table->string('state_code',2);
            $table->string('state',50);

            $table->boolean('is_enabled')->default(false);

            $table->timestamps();

            $table->primary('state_code');
        });
        Schema::create('cities', function (Blueprint $table) {

            $table->string('zip_code',5);
            $table->string('city',100);
            $table->string('state_code',2);
            $table->double('latitude');
            $table->double('longitude');
            $table->string('county',100);

            $table->primary('zip_code');

            $table->boolean('is_enabled')->default(false);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
        Schema::dropIfExists('cities');
    }
}
