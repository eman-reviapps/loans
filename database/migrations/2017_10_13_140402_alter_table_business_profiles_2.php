<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBusinessProfiles2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_profiles', function (Blueprint $table) {
            $table->decimal('income_tax_expense_year_1', 17, 2)->change();
            $table->decimal('income_tax_expense_year_2', 17, 2)->change();
            $table->decimal('income_tax_expense_year_3', 17, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_profiles', function (Blueprint $table) {
            $table->decimal('income_tax_expense_year_1', 8, 2)->change();
            $table->decimal('income_tax_expense_year_2', 8, 2)->change();
            $table->decimal('income_tax_expense_year_3', 8, 2)->change();
        });
    }
}
