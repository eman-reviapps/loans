<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInstitutionBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institution_branches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('institution_id')->unsigned();

            $table->string('title');
            $table->string('address')->nullable();
            $table->string('state',150)->nullable();
            $table->string('city',150)->nullable();
            $table->string('zip_code',5)->nullable();
            $table->string('phone',20)->nullable();
            $table->boolean('active',100)->default(1);

            $table->timestamps();

            $table->foreign('institution_id')
                ->references('id')
                ->on('institutions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institution_branches');
    }
}
