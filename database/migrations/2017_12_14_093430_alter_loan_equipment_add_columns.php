<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLoanEquipmentAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loan_details', function (Blueprint $table) {

            $table->string('vendor_name')->nullable();
            $table->string('equipment_purpose')->nullable();
            $table->string('equipment_description')->nullable();

            $table->decimal('less_trade', 17, 2)->nullable();
            $table->decimal('less_down_payment', 17, 2)->nullable();

            $table->string('equipment_finance_type')->nullable();

            $table->dropColumn('equipment_name');
            $table->dropColumn('equipment_state');
            $table->dropColumn('equipment_age');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_details', function (Blueprint $table) {

            $table->dropColumn('vendor_name');
            $table->dropColumn('equipment_purpose');
            $table->dropColumn('equipment_description');

            $table->dropColumn('less_trade');
            $table->dropColumn('less_down_payment');

            $table->dropColumn('equipment_finance_type');

            $table->string('equipment_name')->nullable();
            $table->string('equipment_state')->nullable();
            $table->string('equipment_age')->nullable();

        });
    }
}
