<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLoanDetailsPurchaseInvestorAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loan_details', function (Blueprint $table) {
            $table->integer('buildings_no')->nullable();
            $table->integer('units_no')->nullable();
            $table->decimal('estimated_net_operating_income', 17, 2)->nullable();

            $table->dropColumn('cash_before_down_payment');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_details', function (Blueprint $table) {
            $table->dropColumn('buildings_no');
            $table->dropColumn('units_no');
            $table->dropColumn('estimated_net_operating_income');

            $table->decimal('cash_before_down_payment', 17, 2)->nullable();
        });
    }
}
