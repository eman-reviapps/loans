<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableLenderProfilesAddLineOfBusiness extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lender_profiles', function (Blueprint $table) {
            $table->string('line_of_business')->after('linked_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lender_profiles', function (Blueprint $table) {
            $table->dropColumn('line_of_business');
        });
    }
}
