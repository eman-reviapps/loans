<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsersAddSubscriptionReminder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->boolean('first_reminder_email_sent')->default(0);
            $table->boolean('second_reminder_email_sent')->default(0);
            $table->boolean('third_reminder_email_sent')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('first_reminder_email_sent');
            $table->dropColumn('second_reminder_email_sent');
            $table->dropColumn('third_reminder_email_sent');
        });
    }
}
