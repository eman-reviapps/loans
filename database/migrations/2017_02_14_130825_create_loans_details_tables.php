<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansDetailsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('loan_id')->unsigned();

            $table->integer('real_estate_type_id')->unsigned()->nullable();
            $table->integer('purpose_type_id')->unsigned()->nullable();

            $table->string('purpose',150)->nullable();

            $table->decimal('requested_amount')->nullable();
            $table->decimal('requested_percent')->nullable();

            $table->integer('purchase_price')->nullable();

            $table->decimal('receivable_range_from')->nullable();
            $table->decimal('receivable_range_to')->nullable();

            $table->string('inventory_type')->nullable();
            $table->decimal('inventory_range_from')->nullable();
            $table->decimal('inventory_range_to')->nullable();
            $table->decimal('current_ratio')->nullable();

            $table->decimal('cash_before_down_payment')->nullable();

            $table->string('state_code',150)->nullable();
            $table->string('city',150)->nullable();
            $table->string('zip_code',5)->nullable();
            $table->string('address')->nullable();
            $table->integer('apt_suite_no')->nullable();

            $table->string('building_sq_ft')->nullable();
            $table->string('tips')->nullable();

            $table->decimal('owner_occupied_percent')->nullable();
            $table->integer('owner_occupied_no')->nullable();
            $table->decimal('owner_not_occupied_percent')->nullable();
            $table->integer('owner_not_occupied_no')->nullable();

            $table->decimal('gross_annual_revenue')->nullable();
            $table->integer('rental_annual_revenue')->nullable();
            $table->integer('company_revenue')->nullable();
            $table->integer('rental_revenue_by_tenants')->nullable();

            $table->decimal('estimated_annual_expenses')->nullable();
            $table->decimal('cash_liquid_investments')->nullable();

            $table->boolean('is_loan_outstanding')->nullable();
            $table->decimal('outstanding_value')->nullable();

            $table->boolean('is_moving_from_leased')->nullable();
            $table->decimal('current_lease_expense')->nullable();

            $table->decimal('company_EBITDA')->nullable();
            $table->decimal('total_liabilities')->nullable();
            $table->decimal('funded_debt')->nullable();
            $table->decimal('tangible_net_worth')->nullable();

            $table->string('equipment_type')->nullable();
            $table->string('equipment_name')->nullable();
            $table->string('equipment_state')->nullable();
            $table->string('equipment_age')->nullable();

            $table->timestamps();

            $table->foreign('loan_id')
                ->references('id')
                ->on('loans')
                ->onDelete('cascade');

            $table->foreign('real_estate_type_id')
                ->references('id')
                ->on('real_estate_types')
                ->onDelete('cascade');

            $table->foreign('purpose_type_id')
                ->references('id')
                ->on('purpose_types')
                ->onDelete('cascade');
        });

        Schema::create('tenants', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('loan_id')->unsigned();

            $table->string('name')->nullable();

            $table->integer('lease_type_id')->unsigned();

            $table->decimal('space_occupied');
            $table->decimal('lease_rate');
            $table->timestamp('lease_expiration_date');

            $table->timestamps();

            $table->foreign('loan_id')
                ->references('id')
                ->on('loans')
                ->onDelete('cascade');

            $table->foreign('lease_type_id')
                ->references('id')
                ->on('lease_types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants');
        Schema::dropIfExists('loan_details');
    }
}
