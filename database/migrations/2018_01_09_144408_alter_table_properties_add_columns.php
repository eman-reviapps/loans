<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePropertiesAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropForeign('properties_property_type_id_foreign');
            $table->dropColumn('property_type_id');

            $table->integer('realestate_type_id')->nullable()->unsigned();
            $table->foreign('realestate_type_id')
                ->references('id')
                ->on('real_estate_types')
                ->onDelete('cascade');

            $table->decimal('estimated_market_value', 17, 2)->nullable()->change();
            $table->decimal('loan_outstanding', 17, 2)->nullable();
            $table->timestamp('loan_maturity_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {

            $table->integer('property_type_id')->nullable()->unsigned();

            $table->foreign('property_type_id')
                ->references('id')
                ->on('property_types')
                ->onDelete('cascade');

            $table->dropForeign('properties_realestate_type_id_foreign');
            $table->dropColumn('realestate_type_id');

            $table->dropColumn('loan_outstanding');
            $table->dropColumn('loan_maturity_date');

        });
    }
}
