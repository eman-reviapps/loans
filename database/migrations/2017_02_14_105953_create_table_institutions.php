<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInstitutions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institution_types', function (Blueprint $table) {
            $table->increments('id');

            $table->string('slug')->unique();
            $table->string('name');
            $table->boolean('is_enabled')->default(true);

            $table->timestamps();
        });

        Schema::create('institution_sizes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('slug')->unique();
            $table->string('name');
            $table->boolean('is_enabled')->default(true);

            $table->timestamps();
        });

        Schema::create('institutions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->nullable();
            $table->string('domain');

            $table->string('address')->nullable();
            $table->string('state',150)->nullable();
            $table->string('city',150)->nullable();
            $table->string('zip_code',5)->nullable();
            $table->string('phone',20)->nullable();
            $table->integer('type_id')->unsigned()->nullable();
            $table->integer('size_id')->unsigned()->nullable();
            $table->string('status',100)->nullable();
            
            $table->timestamps();

            $table->foreign('type_id')
                ->references('id')
                ->on('institution_types')
                ->onDelete('cascade');

            $table->foreign('size_id')
                ->references('id')
                ->on('institution_sizes')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institutions');
        Schema::dropIfExists('institution_types');
        Schema::dropIfExists('institution_sizes');
    }
}
