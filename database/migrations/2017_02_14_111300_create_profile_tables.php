<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lender_profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->integer('institution_id')->unsigned()->nullable();
            $table->string('address')->nullable();
            $table->string('title')->nullable();
            $table->string('phone', 20)->nullable();
            $table->text('bio')->nullable();
            $table->string('linked_link')->nullable();

            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('institution_id')
                ->references('id')
                ->on('institutions')
                ->onDelete('cascade');
        });

        Schema::create('borrower_profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();

            $table->integer('profile_id')->unsigned();
            $table->string('profile_type');

            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('business_profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->string('profile_creator_role')->nullable();
            $table->boolean('profile_creator_decision_maker')->nullable();

            $table->string('business_name');
            $table->text('description');
            $table->string('phone', 20);

            $table->string('state_code', 150);
            $table->string('city', 150);
            $table->string('zip_code', 5);
            $table->string('address');
            $table->integer('apt_suite_no');

            $table->string('establish_year', 4);
            $table->string('fisical_year_end', 5);

            $table->decimal('annual_revenue_year_1');
            $table->decimal('annual_revenue_year_2');
            $table->decimal('annual_revenue_year_3');

            $table->decimal('net_income_before_tax_year_1');
            $table->decimal('net_income_before_tax_year_2');
            $table->decimal('net_income_before_tax_year_3');

            $table->decimal('interest_expense_year_1');
            $table->decimal('interest_expense_year_2');
            $table->decimal('interest_expense_year_3');

            $table->decimal('depreciation_amortization_year_1');
            $table->decimal('depreciation_amortization_year_2');
            $table->decimal('depreciation_amortization_year_3');

            $table->decimal('company_EBITDA');

            $table->timestamps();
        });

        Schema::create('real_estate_profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->string('state_code', 150);
            $table->string('city', 150);
            $table->string('zip_code', 5);
            $table->string('address');
            $table->integer('apt_suite_no');

            $table->string('phone', 20)->nullable();
            $table->integer('properties_no');

            $table->timestamps();
        });

        Schema::create('property_types', function (Blueprint $table) {
            $table->increments('id');

            $table->string('slug')->unique();
            $table->string('name');
            $table->boolean('is_enabled')->default(true);

            $table->timestamps();
        });

        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('profile_id')->unsigned();
            $table->integer('property_type_id')->unsigned();

            $table->string('address')->nullable();
            $table->decimal('estimated_market_value');
            $table->boolean('is_enabled')->default(true);

            $table->timestamps();

            $table->foreign('profile_id')
                ->references('id')
                ->on('real_estate_profiles')
                ->onDelete('cascade');

            $table->foreign('property_type_id')
                ->references('id')
                ->on('property_types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
        Schema::dropIfExists('property_types');
        Schema::dropIfExists('real_estate_profiles');
        Schema::dropIfExists('business_profiles');
        Schema::dropIfExists('borrower_profiles');
        Schema::dropIfExists('lender_profiles');
    }
}
