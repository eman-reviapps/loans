<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBusinessProfilesAddEbtidaDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_profiles', function (Blueprint $table) {
            $table->decimal('company_EBITDA_year_1', 52, 2)->default(0);
            $table->decimal('company_EBITDA_year_2', 52, 2)->default(0);
            $table->decimal('company_EBITDA_year_3', 52, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_profiles', function (Blueprint $table) {
            $table->dropColumn('company_EBITDA_year_1');
            $table->dropColumn('company_EBITDA_year_2');
            $table->dropColumn('company_EBITDA_year_3');
        });
    }
}
