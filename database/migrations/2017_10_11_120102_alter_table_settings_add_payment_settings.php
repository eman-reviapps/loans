<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSettingsAddPaymentSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {

            $table->integer('free_trial_period');
            $table->string('free_trial_period_unit', 100);

            $table->integer('free_lifetime_subscriptions_count');

            $table->integer('subscribe_reminder_email_first');
            $table->string('subscribe_reminder_email_first_unit', 100);

            $table->integer('subscribe_reminder_email_second');
            $table->string('subscribe_reminder_email_second_unit', 100);

            $table->integer('subscribe_reminder_email_third');
            $table->string('subscribe_reminder_email_third_unit', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {

            $table->dropColumn('free_trial_period');
            $table->dropColumn('free_trial_period_unit');

            $table->dropColumn('free_lifetime_subscriptions_count');

            $table->dropColumn('subscribe_reminder_email_first');
            $table->dropColumn('subscribe_reminder_email_first_unit');

            $table->dropColumn('subscribe_reminder_email_second');
            $table->dropColumn('subscribe_reminder_email_second_unit');

            $table->dropColumn('subscribe_reminder_email_third');
            $table->dropColumn('subscribe_reminder_email_third_unit');
        });
    }
}
