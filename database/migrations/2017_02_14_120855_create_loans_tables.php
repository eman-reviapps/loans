<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('borrower_id')->unsigned();
            $table->integer('profile_id')->unsigned();

            $table->integer('original_loan_id')->unsigned()->nullable();

            $table->string('loan_type');

            $table->string('status', 100);

            $table->timestamps();

            $table->foreign('borrower_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('profile_id')
                ->references('id')
                ->on('borrower_profiles')
                ->onDelete('cascade');
        });

        Schema::table('loans', function (Blueprint $table) {
            $table->foreign('original_loan_id')
                ->references('id')
                ->on('loans')
                ->onDelete('cascade');
        });

        Schema::create('bids', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('loan_id')->unsigned();
            $table->integer('lender_id')->unsigned();

            $table->string('status', 100);
            $table->decimal('rate');
            $table->integer('term');
            $table->string('term_unit', 100);
            $table->integer('amortization');
            $table->string('amortization_unit', 100);
            $table->boolean('requires_recourse')->nullable();
            $table->text('descripton');

            $table->timestamps();

            $table->foreign('loan_id')
                ->references('id')
                ->on('loans')
                ->onDelete('cascade');

            $table->foreign('lender_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('loan_archive', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('loan_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->timestamps();

            $table->foreign('loan_id')
                ->references('id')
                ->on('loans')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bids');
        Schema::dropIfExists('loan_archive');
        Schema::dropIfExists('loans');
    }
}
