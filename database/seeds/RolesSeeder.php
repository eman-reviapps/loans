<?php

use Illuminate\Database\Seeder;
use App\Services\RoleService;
use App\Services\PermissionsService;
use App\Models\Role;

class RolesSeeder extends Seeder
{
    protected $roleService;
    protected $permissionsService;

    public function __construct(RoleService $roleService, PermissionsService $permissionsService)
    {
        $this->roleService = $roleService;
        $this->permissionsService = $permissionsService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = Role::create([
            'name' => 'Administrator',
            'slug' => 'administrator',
        ]);
        $administrator->permissions = $this->permissionsService->getDefaultPermissions();
        $administrator->save();

        $borrower = Role::create([
            'name' => 'Borrower',
            'slug' => 'borrower'
        ]);
//        $borrower->permissions = '';
        $borrower->save();

        $lender = Role::create([
            'name' => 'Lender',
            'slug' => 'lender'
        ]);
//        $lender->permissions = '';
        $lender->save();

    }
}
