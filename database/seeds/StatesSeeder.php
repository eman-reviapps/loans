<?php

use Illuminate\Database\Seeder;
use App\Models\State;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        State::create(["state" => 'Alaska', 'state_code' => 'AK']);
        State::create(["state" => 'Alabama', 'state_code' => 'AL']);
        State::create(["state" => 'Arkansas', 'state_code' => 'AR']);
        State::create(["state" => 'Arizona', 'state_code' => 'AZ']);
        State::create(["state" => 'California', 'state_code' => 'CA']);
        State::create(["state" => 'Colorado', 'state_code' => 'CO']);
        State::create(["state" => 'Connecticut', 'state_code' => 'CT']);
        State::create(["state" => 'District of Columbia', 'state_code' => 'DC']);
        State::create(["state" => 'Delaware', 'state_code' => 'DE']);
        State::create(["state" => 'Florida', 'state_code' => 'FL']);
        State::create(["state" => 'Georgia', 'state_code' => 'GA']);
        State::create(["state" => 'Hawaii', 'state_code' => 'HI']);
        State::create(["state" => 'Iowa', 'state_code' => 'IA']);
        State::create(["state" => 'Idaho', 'state_code' => 'ID']);
        State::create(["state" => 'Illinois', 'state_code' => 'IL']);
        State::create(["state" => 'Indiana', 'state_code' => 'IN']);
        State::create(["state" => 'Kansas', 'state_code' => 'KS']);
        State::create(["state" => 'Kentucky', 'state_code' => 'KY']);
        State::create(["state" => 'Louisiana', 'state_code' => 'LA']);
        State::create(["state" => 'Massachusetts', 'state_code' => 'MA']);
        State::create(["state" => 'Maryland', 'state_code' => 'MD']);
        State::create(["state" => 'Maine', 'state_code' => 'ME']);
        State::create(["state" => 'Michigan', 'state_code' => 'MI']);
        State::create(["state" => 'Minnesota', 'state_code' => 'MN']);
        State::create(["state" => 'Missouri', 'state_code' => 'MO']);
        State::create(["state" => 'Mississippi', 'state_code' => 'MS']);
        State::create(["state" => 'Montana', 'state_code' => 'MT']);
        State::create(["state" => 'North Carolina', 'state_code' => 'NC']);
        State::create(["state" => 'North Dakota', 'state_code' => 'ND']);
        State::create(["state" => 'Nebraska', 'state_code' => 'NE']);
        State::create(["state" => 'New Hampshire', 'state_code' => 'NH']);
        State::create(["state" => 'New Jersey', 'state_code' => 'NJ']);
        State::create(["state" => 'New Mexico', 'state_code' => 'NM']);
        State::create(["state" => 'Nevada', 'state_code' => 'NV']);
        State::create(["state" => 'New York', 'state_code' => 'NY']);
        State::create(["state" => 'Ohio', 'state_code' => 'OH']);
        State::create(["state" => 'Oklahoma', 'state_code' => 'OK']);
        State::create(["state" => 'Oregon', 'state_code' => 'OR']);
        State::create(["state" => 'Pennsylvania', 'state_code' => 'PA']);
        State::create(["state" => 'Rhode Island', 'state_code' => 'RI']);
        State::create(["state" => 'South Carolina', 'state_code' => 'SC']);
        State::create(["state" => 'South Dakota', 'state_code' => 'SD']);
        State::create(["state" => 'Tennessee', 'state_code' => 'TN']);
        State::create(["state" => 'Texas', 'state_code' => 'TX']);
        State::create(["state" => 'Utah', 'state_code' => 'UT']);
        State::create(["state" => 'Virginia', 'state_code' => 'VA']);
        State::create(["state" => 'Vermont', 'state_code' => 'VT']);
        State::create(["state" => 'Washington', 'state_code' => 'WA']);
        State::create(["state" => 'Wisconsin', 'state_code' => 'WI']);
        State::create(["state" => 'West Virginia', 'state_code' => 'WV']);
        State::create(["state" => 'Wyoming', 'state_code' => 'WY']);
    }
}
