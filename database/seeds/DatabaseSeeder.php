<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(InstitutionSizesSeeder::class);
        $this->call(InstitutionTypesSeeder::class);

        $this->call(PropertyTypesSeeder::class);
        $this->call(PurposeTypesSeeder::class);
        $this->call(RealEstateTypesSeeder::class);

        $this->call(LeaseTypesSeeder::class);

        $this->call(PermissionsSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(UsersSeeder::class);

        $this->call(SettingSeeder::class);

        $this->call(StatesSeeder::class);
        $this->call(CitiesSeeder::class);


    }
}
