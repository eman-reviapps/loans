<?php

use Illuminate\Database\Seeder;
use \App\Models\InstitutionSize;

class InstitutionSizesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        InstitutionSize::create([
            "slug"=>"small",
            "name"=>"Small",
        ]);

        InstitutionSize::create([
            "slug"=>"medium",
            "name"=>"Medium",
        ]);

        InstitutionSize::create([
            "slug"=>"large",
            "name"=>"Large",
        ]);

    }
}
