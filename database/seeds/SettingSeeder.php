<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new Setting;

        $setting->loan_expire_period = config('loan-app.loan_expire_period');
        $setting->loan_expire_period_unit =config('loan-app.loan_expire_period_unit');
        $setting->lender_max_loan_bids = config('loan-app.lender_max_loan_bids');

        $setting->free_trial_period = config('loan-app.free_trial_period');
        $setting->free_trial_period_unit = config('loan-app.free_trial_period_unit');

        $setting->free_lifetime_subscriptions_count = config('loan-app.free_lifetime_subscriptions_count');

        $setting->subscribe_reminder_email_first = config('loan-app.subscribe_reminder_email_first');
        $setting->subscribe_reminder_email_first_unit = config('loan-app.subscribe_reminder_email_first_unit');

        $setting->subscribe_reminder_email_second = config('loan-app.subscribe_reminder_email_second');
        $setting->subscribe_reminder_email_second_unit = config('loan-app.subscribe_reminder_email_second_unit');

        $setting->subscribe_reminder_email_third = config('loan-app.subscribe_reminder_email_third');
        $setting->subscribe_reminder_email_first_unit = config('loan-app.subscribe_reminder_email_third_unit');

        $setting->save();
    }
}
