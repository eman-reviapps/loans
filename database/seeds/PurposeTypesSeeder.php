<?php

use Illuminate\Database\Seeder;
use App\Models\PurposeType;

class PurposeTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PurposeType::create([
            "slug"=>"working-capital-financing",
            "name"=>"Working Capital Financing",
        ]);

        PurposeType::create([
            "slug"=>"inventory-financing",
            "name"=>"Inventory Financing",
        ]);

        PurposeType::create([
            "slug"=>"business-expansion",
            "name"=>"Business Expansion",
        ]);

        PurposeType::create([
            "slug"=>"other",
            "name"=>"Other",
        ]);
    }
}
