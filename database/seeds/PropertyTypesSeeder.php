<?php

use Illuminate\Database\Seeder;
use App\Models\PropertyType;

class PropertyTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PropertyType::create([
            "slug"=>"apartment",
            "name"=>"Apartment",
        ]);

        PropertyType::create([
            "slug"=>"studio",
            "name"=>"Studio",
        ]);

        PropertyType::create([
            "slug"=>"condo",
            "name"=>"Condo",
        ]);
        PropertyType::create([
            "slug"=>"land",
            "name"=>"Land",
        ]);

        PropertyType::create([
            "slug"=>"house",
            "name"=>"House",
        ]);

        PropertyType::create([
            "slug"=>"other",
            "name"=>"Other",
        ]);
    }
}
