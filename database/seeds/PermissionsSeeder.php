<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $routeCollection = \Route::getRoutes();
        foreach ($routeCollection as $route) {
            if (strpos($route->getPrefix(), 'admin') !== false && in_array('admin', $route->gatherMiddleware())) {
                Permission::firstOrCreate([
                    'slug' => $route->getName(),
                    'name' => $route->getName(),
                    'description' => $route->getName()
                ]);
            }
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
