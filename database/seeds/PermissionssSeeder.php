<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use DB;

class PermissionssSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $routeCollection = \Route::getRoutes();

//        foreach ($routeCollection as $route) {
//            if ($route->getPrefix() == '/backend' && in_array('backend', $route->gatherMiddleware())) {
//                $permission = Permission::where('slug', $route->getName())->first();
//                if (!$permission && !str_contains($route->getName())) {
//                    Permission::create([
//                        'slug' => $route->getName(),
//                        'name' => $route->getName(),
//                        'description' => $route->getName()
//                    ]);
//                }
//            }
//        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
