<?php

use Illuminate\Database\Seeder;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Services\PreferencesService;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(PreferencesService $preferencesService)
    {
        $credentials = [
            'first_name' => 'Website',
            'last_name' => 'Admin',
            'email' => 'admin@loans.dev',
            'password' => '123456',
            'status' => \App\Enums\ActivationStates::ACTIVE
        ];

        $user = Sentinel::registerAndActivate($credentials);

        $admin_role = Sentinel::findRoleBySlug('administrator');
        $admin_role->users()->attach($user);

        $preferencesService->createPreferences([], $user);
    }
}
