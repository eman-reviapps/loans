<?php

use Illuminate\Database\Seeder;
use App\Models\RealEstateType;

class RealEstateTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RealEstateType::create([
            "slug"=>"office-building",
            "name"=>"Office Building",
        ]);

        RealEstateType::create([
            "slug"=>"research-and-development",
            "name"=>"Research And Development",
        ]);

        RealEstateType::create([
            "slug"=>"warehouse",
            "name"=>"Warehouse",
        ]);

        RealEstateType::create([
            "slug"=>"restaurant-stores",
            "name"=>"Restaurant, Stores",
        ]);

        RealEstateType::create([
            "slug"=>"commercial-industrial",
            "name"=>"Commercial Industrial",
        ]);

        RealEstateType::create([
            "slug"=>"auto-repair-and-service",
            "name"=>"Auto Repair And Service",
        ]);

        RealEstateType::create([
            "slug"=>"shopping-centre",
            "name"=>"Shopping Centre",
        ]);
    }
}
