<?php

use Illuminate\Database\Seeder;
use \App\Models\InstitutionType;

class InstitutionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InstitutionType::create([
            "slug"=>"bank",
            "name"=>"Bank",
        ]);

        InstitutionType::create([
            "slug"=>"credit-union",
            "name"=>"Credit Union",
        ]);

        InstitutionType::create([
            "slug"=>"insurance-company",
            "name"=>"Insurance Company",
        ]);

        InstitutionType::create([
            "slug"=>"equipment-lender",
            "name"=>"Equipment Lender",
        ]);
    }
}
