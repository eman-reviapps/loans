<?php

use Illuminate\Database\Seeder;
use App\Models\LeaseType;

class LeaseTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LeaseType::create([
            "slug"=>"gross",
            "name"=>"Gross",
        ]);
    }
}
