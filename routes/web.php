<?php

Route::group(['namespace' => 'Sentinel'], function () {
    Route::middleware('guest')->group(function () {
        Route::get('login', 'LoginController@getLogin');
        Route::get('signup', 'RegisterController@getRegister');

        Route::get('signup/borrower/{profile_type?}', 'RegisterController@getRegisterBorrower');
        Route::get('signup/lender', 'RegisterController@getRegisterLender');

        Route::post('signup/borrower', 'RegisterController@postRegisterBorrower');
        Route::post('signup/lender', 'RegisterController@postRegisterLender');

        Route::get('activate/{user}/{activation_code}', 'ActivationController@getActivate');

        Route::get('forgot/password', 'ForgotPasswordController@getForgetPassword');
        Route::post('forgot/password', 'ForgotPasswordController@postForgetPassword');
        Route::get('forgot/confrm', 'ForgotPasswordController@getForgetConfirm');

        Route::get('reset/password/{user}/{reset_code}', 'ResetPasswordController@getResetPassword');
        Route::post('reset/password', 'ResetPasswordController@postResetPassword');
        Route::get('reset/error', 'ResetPasswordController@getResetError');

        Route::get('get-loans', 'RegisterController@getRegisterBorrower');
        Route::get('aspire-borrowers', 'RegisterController@getRegisterBorrower');
    });

    Route::post('login', 'LoginController@postLogin');
    Route::get('logout', 'LoginController@getLogout');
});

Route::group(['namespace' => 'Admin'], function () {
    Route::get('institutions/check_domain_taken', 'InstitutionController@isNewDomainValid');
    Route::get('users/check_email_exists', 'UserController@isNewEmailValid');

    Route::get('states/check_enabled', 'StateController@checkEnabled');
    Route::get('states/check_city_enabled', 'StateController@checkCityEnabled');

    Route::get('states/check_zipcode_enabled', 'StateController@checkZipCodeEnabled');
    Route::get('states/check_valid_zip_code', 'StateController@checkValidZipCode');
});
Route::group(['prefix' => 'dropdowns'], function () {
    Route::get('zip_codes', 'DropdownsControllers@zipCodes'); //dy mfrod tb2a zip_codes
    Route::get('zip_codes_for_city', 'DropdownsControllers@zipCodesForCity');

    Route::get('cities', 'DropdownsControllers@cities'); //dy mfrood tb2a cities

    Route::get('states', 'DropdownsControllers@states');

    Route::get('counties', 'DropdownsControllers@counties');
});

Route::middleware(['auth', 'web', 'permissions'])->group(function () {

    Route::get('/', 'HomeController@getIndex')->name('home');

    Route::middleware('subscribed')->group(function () {
        Route::get('braintree/token', 'BraintreeTokenController@token');
        Route::resource('subscribe', 'SubscriptionsController');
    });

    Route::get('user/invoice/{invoice}', 'SubscriptionsController@invoice');

    Route::post('upload_files', 'AttachmentController@uploadFiles')->name('upload_files');
    Route::post('upload_file', 'AttachmentController@uploadFile')->name('upload_file');
    Route::resource('attachment', 'AttachmentController');

    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'admin'], function () {

        Route::get('/', 'DashboardController@getIndex')->name('admin.home');

        Route::get('dashboard', 'DashboardController@getIndex')->name('dashboard');
        Route::get('dashboard_2', 'DashboardController@getIndex2')->name('dashboard2');
        Route::get('dashboard_3', 'DashboardController@getIndex3')->name('dashboard3');

        Route::group(['prefix' => 'institutions'], function () {

            Route::put('{institution}/change_status', 'InstitutionController@changeStatus')->name('institutions.change_status');
            Route::get('institution_loan_handling_chart', 'InstitutionController@institutionLoanHandlingChart'); //Returns JSON
        });

        Route::resource('institutions', 'InstitutionController');

        Route::put('institution_types/{institution_type}/change_status', 'InstitutionTypeController@changeStatus')->name('institution_types.change_status');
        Route::resource('institution_types', 'InstitutionTypeController');

        Route::put('institution_sizes/{institution_size}/change_status', 'InstitutionSizeController@changeStatus')->name('institution_sizes.change_status');
        Route::resource('institution_sizes', 'InstitutionSizeController');

        Route::group(['prefix' => 'users'], function () {
            Route::put('{user}/change_status', 'UserController@changeStatus')->name('users.change_status');
            Route::get('{user}/tracks/{user_track}', 'UserController@showTrack')->name('users.track'); //to be checked
        });
        Route::resource('users', 'UserController');

        Route::get('lenders_per_state', 'UserController@lendersPerState');

        Route::get('number_of_lenders_per_institution', 'UserController@numberOfLendersPerInstitution');

        Route::get('loans_feedback_statistics', 'FeedbackController@loansFeedbackStatistics');

        Route::resource('roles', 'RoleController');

        Route::put('real_estate_types/{real_estate_type}/change_status', 'RealEstateTypeController@changeStatus')->name('real_estate_types.change_status');
        Route::resource('real_estate_types', 'RealEstateTypeController');

        Route::put('property_types/{property_type}/change_status', 'PropertyTypeController@changeStatus')->name('property_types.change_status');
        Route::resource('property_types', 'PropertyTypeController');

        Route::put('purpose_types/{purpose_type}/change_status', 'PurposeTypeController@changeStatus')->name('purpose_types.change_status');
        Route::resource('purpose_types', 'PurposeTypeController');

        Route::put('lease_types/{lease_type}/change_status', 'LeaseTypeController@changeStatus')->name('lease_types.change_status');
        Route::resource('lease_types', 'LeaseTypeController');

        Route::resource('settings', 'SettingController');

        Route::group(['prefix' => 'states/{state}'], function () {

            Route::put('change_status', 'StateController@changeStatus')->name('states.change_status'); //done
            Route::get('counties', 'StateController@counties')->name('counties.index');

            Route::group(['prefix' => 'counties'], function () {
                Route::post('select_all' , 'StateController@selectAllCounties');
                Route::post('unselect_all' , 'StateController@unselectAllCounties');

                Route::put('{county}/change_status', 'StateController@changeCountyStatus')->name('counties.change_status');
                Route::get('{county}/cities', 'StateController@cities')->name('cities.index');

                Route::post('{county}/cities/select_all' , 'StateController@selectAllCities') ;
                Route::post('{county}/cities/unselect_all' , 'StateController@unselectAllCities') ;

                Route::group(['prefix' => '{county}/cities/{city}'], function () {
                    Route::post('zip_codes/select_all' , 'StateController@selectAllZipCodes') ;
                    Route::post('zip_codes/unselect_all' , 'StateController@unselectAllZipCodes') ;

                    Route::put('change_status', 'StateController@changeCityStatus')->name('cities.change_status');
                    Route::get('zip_codes', 'StateController@zip_codes')->name('zip_codes.index');

                    Route::group(['prefix' => 'zip_codes/{zip_code}'], function () {
                        Route::put('change_status', 'StateController@changeZipCodeStatus')->name('zip_codes.change_status');
                    });
                });
            });

        });
        Route::post('states/select_all', 'StateController@selectAllStates');
        Route::post('states/unselect_all', 'StateController@unselectAllStates');

        Route::resource('states', 'StateController');

        Route::get('loans/bids/{bid}/show', 'LoanController@showBid');
        Route::get('loans/bids', 'LoanController@bids');

        Route::resource('loans', 'LoanController');

        Route::get('loans_count', 'LoanController@loansCount'); // Returns JSON
        Route::get('loans_status_statistics', 'LoanController@loansStatusStatistics'); //Returns JSON
        Route::get('total_number_of_loans', 'LoanController@totalNumberOfLoans'); // Returns JSON


    });

    Route::group(['namespace' => 'Agent'], function () {

        Route::get('profile', 'UserController@show')->name('profile');
        Route::get('account', 'UserController@edit')->name('account');

        Route::get('preferences', 'PreferenceController@index')->name('preferences.index');

        Route::group(['prefix' => 'users'], function () {
            Route::put('{user}/account_settings', 'UserController@update')->name('user.account_settings');
            Route::put('{user}/change_password', 'UserController@changePassword')->name('user.change_password');
            Route::put('{user}/change_picture', 'UserController@changePicture')->name('user.change_picture');
            Route::put('{user}/notification_settings', 'UserController@updatePreferences')->name('user.notification_settings');
            Route::put('{user}/preferences', 'UserController@updatePreferences')->name('user.preferences');
        });

        Route::put('notifications/{notification}/mark_read', 'NotificationController@markAsRead')->name('notification.mark_read');
        Route::put('notifications/mark_all_read', 'NotificationController@markAllRead')->name('notifications.mark_all_read');

        Route::resource('notifications', 'NotificationController');

        Route::post('conversations/{conversation}/replies', 'ConversationController@addReply')->name('conversation.add_reply');
        Route::put('conversations/mark_all_read', 'ConversationController@markAllRead')->name('conversations.mark_all_read');

        Route::get('replies/{reply}/render', 'ConversationController@renderReply')->name('conversation.render_reply');
        Route::put('replies/{reply}/mark_read', 'ConversationController@markAsRead')->name('reply.mark_read');

        Route::get('bids/{bid}/can_follow_up', 'ConversationController@canFollowUp');
        Route::get('bids/{bid}/follow_up_number', 'ConversationController@getLenderFollowUpNumber');
        Route::get('bids/{bid}/render_followup_message', 'ConversationController@canFollowMessageRender');
    });

    Route::group(['namespace' => 'Agent\Lender', 'prefix' => 'lender'], function () {

        Route::resource('profiles', 'ProfileController');

        Route::middleware('user_not_suspended')->group(function () {
            Route::get('find-loans', 'LoanController@findLoans')->name('lender.find-loans');
            Route::get('archived-loans', 'LoanController@archived')->name('lender.archived-loans');

            Route::get('loans/{loan}/place_bid', 'LoanController@showPlaceBid')->name('lender.loans.show_place_bid');
            Route::post('loans/{loan}/lender/{lender}/place_bid', 'LoanController@placeBid')->name('lender.loans.place_bid');

            Route::get('save_to_profile', 'LoanController@saveToProfile');
        });

        Route::get('my-work', 'LoanController@myWork')->name('lender.my-work');
        Route::get('loans/{loan}/details', 'LoanController@loanDetails')->name('lender.loan-details');
        Route::put('loans/{loan}/user/{user}/archive', 'LoanController@archive')->name('lender.loans.archive');
        Route::put('loans/archived/{archive}/restore', 'LoanController@restore')->name('lender.loans.restore');
        Route::get('loans/{loan}/is_there_other_bids_placed/{not_lender_id}', 'LoanController@isThereOtherBidsPlaced')->name('lender.is_there_other_bids_placed');

        Route::get('loans/bids/{bid}/show', 'LoanController@bidShow')->name('lender.bid-show');
    });

    Route::group(['namespace' => 'Agent\Borrower', 'prefix' => 'borrower'], function () {

        Route::put('profiles/{profile}/change_status', 'ProfileController@changeStatus')->name('profiles.change_status');
        Route::get('profiles/{profile}/select', 'ProfileController@select')->name('profiles.select');
        Route::put('profiles/{profile}/preferences', 'ProfileController@updatePreferences')->name('profiles.preferences');

        Route::resource('profiles', 'ProfileController');

        Route::group(['middleware' => ['borrower.profile']], function () {

            Route::get('loans/get-template', 'LoanController@getTemplate')->name('loans.get-template');

            Route::put('loans/{loan}/change_status', 'LoanController@changeStatus')->name('loans.change_status');
            Route::post('loans/upload_file', 'LoanController@uploadFile')->name('loans.upload_file');

            Route::resource('loans', 'LoanController');
            Route::get('loans/bids/{bid}/show', 'LoanController@bidShow')->name('borrower.bid-show');
            Route::get('loans/bids/{bid}/not_interested', 'LoanController@borrowerNotInterested')->name('borrower.bid.not_interested');

        });
    });
});